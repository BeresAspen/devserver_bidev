USE [IntacctDM]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW dbo.vw_IntacctVendorsForInsuranceScreen
AS
/* -----------------------------------------------------------------------------------------------------------
   Table Name	   :	dbo.vw_IntacctVendorsForInsuranceScreen
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	09/29/2017

   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date       Author        Description
   --------   -----------   ------------

*/ -----------------------------------------------------------------------------------------------------------

SELECT	CAST([IntacctVendorID] AS VARCHAR(64)) AS IntacctVendorID
	,	[VendorName]
	,	[VendorType]
	,	[VendorAddress1]
	,	[VendorAddress2]
	,	[VendorCity]
	,	[VendorState]
	,	[AFRVendorName]
	,	[TIN]
	,	[LeretaID]
	,	[DynamicsVendorID]
	,	[MARSVendorID]
	,	[DefaultPaymentType]
	,	[VendorStatus]
	,	[UpdatedBy]
	,	[UpdatedDate]
	,	[InsertedBy]
	,	[InsertedDate]
FROM dbo.Vendors
WHERE VendorType = 'INSURANCE'
	
GO


