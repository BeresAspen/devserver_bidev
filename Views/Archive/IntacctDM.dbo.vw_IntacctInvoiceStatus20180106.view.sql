USE [IntacctDM]
GO

/****** Object:  View [dbo].[vw_IntacctInvoiceStatus]    Script Date: 1/10/2018 3:38:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[vw_IntacctInvoiceStatus]
AS
/* -----------------------------------------------------------------------------------------------------------
   Table Name	   :	dbo.IntacctInvoiceStatus
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	09/18/2017

   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date       Author        Description
   --------   -----------   ------------

*/ -----------------------------------------------------------------------------------------------------------

SELECT	t.VendorPaymentsQueueID
	,	t.PaymentDate
	,	t.TaxYear
	,	t.PaymentCycle
	,	t.EconomicLossDate
	,	t.Amount
	,	t.IntacctVendorID
	,	t.VendorName
	,	t.DynamicsVendorID
	,	t.MARSVendorID
	,	t.MARSAccount
	,	t.InvoiceStatus
	,	t.RequestNbr
FROM (
		SELECT	i.VendorPaymentsQueueID
			,	i.PaymentDate
			,	i.TaxYear
			,	i.TaxCycle AS PaymentCycle
			,	i.EconomicLossDate
			,	i.Amount
			,	v.IntacctVendorID
			,	v.VendorName
			,	v.DynamicsVendorID
			,	v.MARSVendorID
			,	i.MARSAccount
			,	i.InvoiceStatus
			,	i.IntacctInvoiceNbr AS RequestNbr
			,	ROW_NUMBER() OVER(PARTITION BY i.IntacctInvoiceNbr,i.VendorPaymentsQueueID  ORDER BY i.InsertedDate DESC) AS RowNum
		FROM IntacctDM.dbo.Invoices i
		JOIN IntacctDM.dbo.Vendors v
			ON	v.IntacctVendorID = i.IntacctVendorID
	) t
WHERE t.RowNum = 1

	


GO


