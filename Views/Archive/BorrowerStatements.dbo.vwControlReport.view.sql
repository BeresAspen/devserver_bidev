USE [BorrowerStatements]
GO

/****** Object:  View [dbo].[vwControlReport]    Script Date: 7/14/2017 4:07:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Always grab the latest SvcVersion from the StmtSchedule
create view [dbo].[vwControlReport] as
select 
    ss.Account, 
    ss.StmtDate, 
    ss.LastStmtDate, 
    ss.Exclusions, 
    ss.Mail, 
    cr.FileSize, 
    cr.FileSHA1, 
    case 
        when ss.Exclusions is null and
            ss.Mail = 1 and
            cr.FileSize is null then 1
        else 0
    end as Missing,
    ss.SvcVersion
from BorrowerStatements..StmtSchedule ss
join (select distinct Account, StmtDate, MAX(SvcVersion) as SvcVersion from BorrowerStatements..StmtSchedule ss group by Account, StmtDate) groupSvc
    on ss.Account = groupSvc.Account and ss.StmtDate = groupSvc.StmtDate and ss.SvcVersion = groupSvc.SvcVersion
left join BorrowerStatements..StmtFileInfo cr on ss.Account = cr.Account and ss.StmtDate = cr.StmtDate

GO


