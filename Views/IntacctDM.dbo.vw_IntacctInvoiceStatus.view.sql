USE IntacctDM;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('dbo.vw_IntacctInvoiceStatus') IS NOT NULL
	DROP VIEW dbo.vw_IntacctInvoiceStatus;
GO

CREATE VIEW dbo.vw_IntacctInvoiceStatus
AS
/* -----------------------------------------------------------------------------------------------------------
   Table Name	   :	dbo.IntacctInvoiceStatus
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	09/18/2017

   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date			Author			Description
   --------		-----------		------------
   01/06/2018	Benjacob Beres	Removed the subselect. The Invoice Table no longer is effective dated.
*/ -----------------------------------------------------------------------------------------------------------

SELECT	i.VendorPaymentsQueueID
	,	i.PaymentDate
	,	i.TaxYear
	,	i.TaxCycle AS PaymentCycle
	,	i.EconomicLossDate
	,	i.Amount
	,	v.IntacctVendorID
	,	v.VendorName
	,	v.DynamicsVendorID
	,	i.MARSAccount
	,	i.InvoiceStatus
	,	i.IntacctInvoiceNbr AS RequestNbr
FROM IntacctDM.dbo.Invoices i
JOIN IntacctDM.dbo.Vendors v
	ON	v.IntacctVendorID = i.IntacctVendorID

	