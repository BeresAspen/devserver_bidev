USE IntacctDM;
GO

IF OBJECT_ID('dbo.SyncReportingPeriodListing') IS NOT NULL
	DROP VIEW dbo.SyncReportingPeriodListing;
GO

CREATE VIEW dbo.SyncReportingPeriodListing
AS

SELECT	NAME
	,	CAST(END_DATE AS DATE) AS END_DATE
FROM IntacctDM.dbo.ReportingPeriod
WHERE	CAST(END_DATE AS DATE) < GETDATE()
	--AND CAST(END_DATE AS DATE) > '12/02/2017'
	AND NAME LIKE '%Month%'
	AND NAME NOT LIKE '%Month Ended July 2018%'
--ORDER BY CAST(END_DATE AS DATE)