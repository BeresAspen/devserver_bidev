USE [BorrowerStatements];
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/* ********************************************************************************************************* *
   Object Name		:	vwControlReport
   Description		:	Provide reportable data from the Borrower Statement schedule
						Always grab the latest SvcVersion from the StmtSchedule
   Author			:	Nathan Reed
   Create Date		:	Unknown (04/2017)

   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date			Author				Issue #			Description
   --------		-----------			---------		------------
   07/14/2017	Benjacob Beres		MARS-2266		Changed the sub select to only show the most current svcDate 
														for each day.
*  ********************************************************************************************************* */


CREATE VIEW [dbo].[vwControlReport] AS
SELECT		ss.Account
		,	ss.StmtDate
		,	ss.LastStmtDate
		,	ss.Exclusions
		,	ss.Mail
		,	cr.FileSize
		,	cr.FileSHA1
		,	CASE
				WHEN	ss.Exclusions IS NULL
					AND ss.Mail = 1
					AND cr.FileSize IS NULL
					THEN 1
				ELSE 0
			END AS Missing
		,	ss.SvcVersion
FROM BorrowerStatements..StmtSchedule ss
	JOIN (	SELECT	ss.StmtDate
				,	MAX(ss.SvcVersion) AS SvcVersion
			FROM BorrowerStatements..StmtSchedule ss
			GROUP BY ss.StmtDate
		 ) groupSvc
		ON	ss.SvcVersion = groupSvc.SvcVersion
		AND ss.StmtDate = groupSvc.StmtDate
	LEFT JOIN BorrowerStatements..StmtFileInfo cr
		ON	ss.Account = cr.Account
		AND ss.StmtDate = cr.StmtDate

GO