
CREATE VIEW [dbo].[vw_HomeTrustSubjectComparibles]
AS

SELECT	HomeTrustOrderID
	,	'Listing' AS ComparibleType
	,	1 AS ComparibleIndex
	,	CL1Address AS Address
	,	CL1City AS City
	,	CL1State AS State
	,	CL1SqFt AS SqFt
	,	CL1Bath AS Bath
	,	CL1Bedroom AS Bedroom
	,	CASE WHEN CL1Basement IN ('NO', 'NA', 'N/A')
				THEN 0
			 ELSE 1
		END AS Basement
	,	CASE WHEN CL1Basement IN ('NO', 'NA', 'N/A')
				THEN NULL
			 ELSE CL1Basement
		END AS BasementType
	,	CASE WHEN CL1Garage IN ('NO', 'NA', 'N/A')
				THEN 0
			 ELSE 1
		END AS Garage
	,	CASE WHEN CL1Garage IN ('NO', 'NA', 'N/A')
				THEN 0
			 ELSE	CASE WHEN RIGHT(CL1Garage,1) = 'A'
							THEN dbo.fn_RemoveNonNumericCharacters(CL1Garage)
						 ELSE 0
					END
		END AS GarageAttached
	,	CASE WHEN CL1Garage IN ('NO', 'NA', 'N/A')
				THEN 0
			 ELSE	CASE WHEN RIGHT(CL1Garage,1) = 'D'
							THEN dbo.fn_RemoveNonNumericCharacters(CL1Garage)
						 ELSE 0
					END
		END AS GarageDetached
	,	CASE WHEN CL1Garage IN ('%CARPORT%')
				THEN CASE WHEN ISNULL(dbo.fn_RemoveNonNumericCharacters(CL1Garage),'') = ''
							THEN 1
						 ELSE dbo.fn_RemoveNonNumericCharacters(CL1Garage)
					END
		END AS Carport
	,	CAST(ROUND(CL1LotSize,4) AS VARCHAR(10)) AS LotSize
	,	CL1Units AS Units
	,	CL1Condition AS Condition
	,	CL1Dom AS Dom
	,	CL1ListDate AS ListDate
	,	CL1PriceList AS PriceList
	,	CL1PriceListOrig AS PriceListOrig
	,	NULL AS PriceSale
	,	CL1Proximity AS Proximity
	,	NULL AS SaleDate
	,	CL1YearBuilt AS YearBuilt
FROM	dbo.HomeTrustOrders

UNION ALL

SELECT	HomeTrustOrderID
	,	'Listing' AS ComparibleType
	,	2 AS ComparibleIndex
	,	CL2Address
	,	CL2City
	,	CL2State
	,	CL2SqFt
	,	CL2Bath
	,	CL2Bedroom
	,	CASE WHEN CL2Basement IN ('NO', 'NA', 'N/A')
				THEN 0
			 ELSE 1
		END AS Basement
	,	CASE WHEN CL2Basement IN ('NO', 'NA', 'N/A')
				THEN NULL
			 ELSE CL2Basement
		END AS BasementType
	,	CASE WHEN CL2Garage IN ('NO', 'NA', 'N/A')
				THEN 0
			 ELSE 1
		END AS Garage
	,	CASE WHEN CL2Garage IN ('NO', 'NA', 'N/A')
				THEN 0
			 ELSE	CASE WHEN RIGHT(CL2Garage,1) = 'A'
							THEN dbo.fn_RemoveNonNumericCharacters(CL2Garage)
						 ELSE 0
					END
		END AS GarageAttached
	,	CASE WHEN CL2Garage IN ('NO', 'NA', 'N/A')
				THEN 0
			 ELSE	CASE WHEN RIGHT(CL2Garage,1) = 'D'
							THEN dbo.fn_RemoveNonNumericCharacters(CL2Garage)
						 ELSE 0
					END
		END AS GarageDetached
	,	CASE WHEN CL2Garage IN ('%CARPORT%')
				THEN CASE WHEN ISNULL(dbo.fn_RemoveNonNumericCharacters(CL2Garage),'') = ''
							THEN 1
						 ELSE dbo.fn_RemoveNonNumericCharacters(CL2Garage)
					END
		END AS Carport
	,	CAST(ROUND(CL2LotSize,4) AS VARCHAR(10)) AS CL2LotSize
	,	CL2Units
	,	CL2Condition
	,	CL2Dom
	,	CL2ListDate
	,	CL2PriceList
	,	CL2PriceListOrig
	,	NULL AS PriceSale
	,	CL2Proximity
	,	NULL AS SaleDate
	,	CL2YearBuilt
FROM	dbo.HomeTrustOrders

UNION ALL

SELECT	HomeTrustOrderID
	,	'Listing' AS ComparibleType
	,	3 AS ComparibleIndex
	,	CL3Address
	,	CL3City
	,	CL3State
	,	CL3SqFt
	,	CL3Bath
	,	CL3Bedroom
	,	CASE WHEN CL3Basement IN ('NO', 'NA', 'N/A')
				THEN 0
			 ELSE 1
		END AS Basement
	,	CASE WHEN CL3Basement IN ('NO', 'NA', 'N/A')
				THEN NULL
			 ELSE CL3Basement
		END AS BasementType
	,	CASE WHEN CL3Garage IN ('NO', 'NA', 'N/A')
				THEN 0
			 ELSE 1
		END AS Garage
	,	CASE WHEN CL3Garage IN ('NO', 'NA', 'N/A')
				THEN 0
			 ELSE	CASE WHEN RIGHT(CL3Garage,1) = 'A'
							THEN dbo.fn_RemoveNonNumericCharacters(CL3Garage)
						 ELSE 0
					END
		END AS GarageAttached
	,	CASE WHEN CL3Garage IN ('NO', 'NA', 'N/A')
				THEN 0
			 ELSE	CASE WHEN RIGHT(CL3Garage,1) = 'D'
							THEN dbo.fn_RemoveNonNumericCharacters(CL3Garage)
						 ELSE 0
					END
		END AS GarageDetached
	,	CASE WHEN CL3Garage IN ('%CARPORT%')
				THEN CASE WHEN ISNULL(dbo.fn_RemoveNonNumericCharacters(CL3Garage),'') = ''
							THEN 1
						 ELSE dbo.fn_RemoveNonNumericCharacters(CL3Garage)
					END
		END AS Carport
	,	CAST(ROUND(CL3LotSize,4) AS VARCHAR(10)) AS CL3LotSize
	,	CL3Units
	,	CL3Condition
	,	CL3Dom
	,	CL3ListDate
	,	CL3PriceList
	,	CL3PriceListOrig
	,	NULL AS PriceSale
	,	CL3Proximity
	,	NULL AS SaleDate
	,	CL3YearBuilt
FROM	dbo.HomeTrustOrders

UNION ALL

SELECT	HomeTrustOrderID
	,	'Sales' AS ComparibleType
	,	1 AS ComparibleIndex
	,	CS1Address
	,	CS1City
	,	CS1State
	,	CS1SqFt
	,	CS1Bath
	,	CS1Bedroom
	,	CASE WHEN CS1Basement IN ('NO', 'NA', 'N/A')
				THEN 0
			 ELSE 1
		END AS CS1Basement
	,	CASE WHEN CS1Basement IN ('NO', 'NA', 'N/A')
				THEN NULL
			 ELSE CS1Basement
		END AS CS1BasementType
	,	CASE WHEN CS1Garage IN ('NO', 'NA', 'N/A')
				THEN 0
			 ELSE 1
		END AS CS1Garage
	,	CASE WHEN CS1Garage IN ('NO', 'NA', 'N/A')
				THEN 0
			 ELSE	CASE WHEN RIGHT(CS1Garage,1) = 'A'
							THEN dbo.fn_RemoveNonNumericCharacters(CS1Garage)
						 ELSE 0
					END
		END AS CS1GarageAttached
	,	CASE WHEN CS1Garage IN ('NO', 'NA', 'N/A')
				THEN 0
			 ELSE	CASE WHEN RIGHT(CS1Garage,1) = 'D'
							THEN dbo.fn_RemoveNonNumericCharacters(CS1Garage)
						 ELSE 0
					END
		END AS CS1GarageDetached
	,	CASE WHEN CS1Garage IN ('%CARPORT%')
				THEN CASE WHEN ISNULL(dbo.fn_RemoveNonNumericCharacters(CS1Garage),'') = ''
							THEN 1
						 ELSE dbo.fn_RemoveNonNumericCharacters(CS1Garage)
					END
		END AS CS1Carport
	,	CAST(ROUND(CS1LotSize,4) AS VARCHAR(10)) AS CS1LotSize
	,	CS1Units
	,	CS1Condition
	,	CS1Dom
	,	NULL AS ListDate
	,	CS1PriceList
	,	NULL AS CS1PriceListOrig
	,	CS1PriceSale
	,	CS1Proximity
	,	CS1SaleDate
	,	CS1YearBuilt
FROM	dbo.HomeTrustOrders

UNION ALL

SELECT	HomeTrustOrderID
	,	'Sales' AS ComparibleType
	,	2 AS ComparibleIndex
	,	CS2Address
	,	CS2City
	,	CS2State
	,	CS2SqFt
	,	CS2Bath
	,	CS2Bedroom
	,	CASE WHEN CS2Basement IN ('NO', 'NA', 'N/A')
				THEN 0
			 ELSE 1
		END AS CS2Basement
	,	CASE WHEN CS2Basement IN ('NO', 'NA', 'N/A')
				THEN NULL
			 ELSE CS2Basement
		END AS CS2BasementType
	,	CASE WHEN CS2Garage IN ('NO', 'NA', 'N/A')
				THEN 0
			 ELSE 1
		END AS CS2Garage
	,	CASE WHEN CS2Garage IN ('NO', 'NA', 'N/A')
				THEN 0
			 ELSE	CASE WHEN RIGHT(CS2Garage,1) = 'A'
							THEN dbo.fn_RemoveNonNumericCharacters(CS2Garage)
						 ELSE 0
					END
		END AS CS2GarageAttached
	,	CASE WHEN CS2Garage IN ('NO', 'NA', 'N/A')
				THEN 0
			 ELSE	CASE WHEN RIGHT(CS2Garage,1) = 'D'
							THEN dbo.fn_RemoveNonNumericCharacters(CS2Garage)
						 ELSE 0
					END
		END AS CS2GarageDetached
	,	CASE WHEN CS2Garage IN ('%CARPORT%')
				THEN CASE WHEN ISNULL(dbo.fn_RemoveNonNumericCharacters(CS2Garage),'') = ''
							THEN 1
						 ELSE dbo.fn_RemoveNonNumericCharacters(CS2Garage)
					END
		END AS CS2Carport
	,	CAST(ROUND(CS2LotSize,4) AS VARCHAR(10)) AS CS2LotSize
	,	CS2Units
	,	CS2Condition
	,	CS2Dom
	,	NULL AS ListDate
	,	CS2PriceList
	,	NULL AS CS2PriceListOrig
	,	CS2PriceSale
	,	CS2Proximity
	,	CS2SaleDate
	,	CS2YearBuilt
FROM	dbo.HomeTrustOrders

UNION ALL

SELECT	HomeTrustOrderID
	,	'Sales' AS ComparibleType
	,	3 AS ComparibleIndex
	,	CS3Address
	,	CS3City
	,	CS3State
	,	CS3SqFt
	,	CS3Bath
	,	CS3Bedroom
	,	CASE WHEN CS3Basement IN ('NO', 'NA', 'N/A')
				THEN 0
			 ELSE 1
		END AS CS3Basement
	,	CASE WHEN CS3Basement IN ('NO', 'NA', 'N/A')
				THEN NULL
			 ELSE CS3Basement
		END AS CS3BasementType
	,	CASE WHEN CS3Garage IN ('NO', 'NA', 'N/A')
				THEN 0
			 ELSE 1
		END AS CS3Garage
	,	CASE WHEN CS3Garage IN ('NO', 'NA', 'N/A')
				THEN 0
			 ELSE	CASE WHEN RIGHT(CS3Garage,1) = 'A'
							THEN dbo.fn_RemoveNonNumericCharacters(CS3Garage)
						 ELSE 0
					END
		END AS CS3GarageAttached
	,	CASE WHEN CS3Garage IN ('NO', 'NA', 'N/A')
				THEN 0
			 ELSE	CASE WHEN RIGHT(CS3Garage,1) = 'D'
							THEN dbo.fn_RemoveNonNumericCharacters(CS3Garage)
						 ELSE 0
					END
		END AS CS3GarageDetached
	,	CASE WHEN CS3Garage IN ('%CARPORT%')
				THEN CASE WHEN ISNULL(dbo.fn_RemoveNonNumericCharacters(CS3Garage),'') = ''
							THEN 1
						 ELSE dbo.fn_RemoveNonNumericCharacters(CS3Garage)
					END
		END AS CS3Carport
	,	CAST(ROUND(CS3LotSize,4) AS VARCHAR(10)) AS CS3LotSize
	,	CS3Units
	,	CS3Condition
	,	CS3Dom
	,	NULL AS ListDate
	,	CS3PriceList
	,	NULL AS CS3PriceListOrig
	,	CS3PriceSale
	,	CS3Proximity
	,	CS3SaleDate
	,	CS3YearBuilt
FROM	dbo.HomeTrustOrders