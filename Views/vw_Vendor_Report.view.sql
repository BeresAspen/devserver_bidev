USE [MARS]
GO

/****** Object:  View [dbo].[vw_Vendor_Report]    Script Date: 4/20/2017 2:50:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE  VIEW [dbo].[vw_Vendor_Report]  
AS  

SELECT     VL.VendorName, ISNULL(VL.GlobalVendorID, '') AS 'Global Vendor Id', ISNULL(VT.VendorType, '') AS 'Vendor Type'  
		,   
		ISNULL(VRA.PContactFirstName,'') as 'PContactFirstName', ISNULL(VRA.PContactMiddleName,'') as 'PContactMiddleName', ISNULL(VRA.PContactLastName,'') as 'PContactLastName',   
		ISNULL(VRA.PContactFullName,'')  as 'PContactFullName', ISNULL(VRA.PhoneNo,'') as 'PPhoneNo', ISNULL(VRA.FaxNo,'')  as 'PFaxNo',   
		ISNULL(VRA.Email,'') as 'PEmail', ISNULL(VRA.Address1,'') as 'PAddress1', ISNULL(VRA.Address2,'') as 'PAddress2',  
		ISNULL(VRA.Address3,'') as 'PAddress3', ISNULL(VRA.City,'') as 'PCity', ISNULL(VRA.[State],'') as 'PState', ISNULL(VRA.Zip,'') as 'PZip'  
		,                        
		ISNULL(VRA1.MContactFirstName,'') as 'MContactFirstName', ISNULL(VRA1.MContactMiddleName,'') as 'MContactMiddleName', ISNULL(VRA1.MContactLastName,'') as 'MContactLastName',   
		ISNULL(VRA1.MContactFullName,'')  as 'MContactFullName', ISNULL(VRA1.PhoneNo,'') as 'MPhoneNo', ISNULL(VRA1.FaxNo,'')  as 'MFaxNo',   
		ISNULL(VRA1.Email,'') as 'MEmail', ISNULL(VRA1.Address1,'') as 'MAddress1', ISNULL(VRA1.Address2,'') as 'MAddress2',  
		ISNULL(VRA1.Address3,'') as 'MAddress3', ISNULL(VRA1.City,'') as 'MCity', ISNULL(VRA1.[State],'') as 'MState', ISNULL(VRA1.Zip,'') as 'MZip'  
		,               
		VR.ReferenceID,           
		coalesce( convert( varchar(10), R.[Last AFR Created Date], 120 ), '') as 'Last AFR Created Date'  ,
		vl.TIN,
		vl.TinName
                        
FROM VendorsList AS VL   
	INNER JOIN VendorTypes AS VT 
		ON VL.VendorTypeId = VT.VendorTypeId  
	INNER JOIN (	Select VendorID, ReferenceID 
					from VendorReference 
					where SourceDatabase = 'FCALPAPP'
				) VR 
		ON VL.VendorId = VR.VendorID  
	LEFT JOIN ( Select ISNULL(ContactFirstName,'') as 'PContactFirstName', ISNULL(ContactMiddleName,'') as 'PContactMiddleName', ISNULL(ContactLastName,'') as 'PContactLastName', ISNULL(ContactFullName,'') as 'PContactFullName',   
					ISNULL(PhoneNo,'') as 'PhoneNo', ISNULL(FaxNo,'') as 'FaxNo', ISNULL(EmailAddresss,'') as 'Email', ISNULL(Address1,'') as 'Address1', ISNULL(Address2,'') as 'Address2', ISNULL(Address3,'') as 'Address3',   
					ISNULL(City,'') as 'City', ISNULL([State],'') as 'State', ISNULL(Zip,'') as 'Zip', VendorID    
				from VendorRemittanceAddresses 
				where Physical = '1' and IsActive = '1'  
			  ) AS VRA 
		ON VL.VendorId = VRA.VendorID   
	LEFT JOIN (	Select ISNULL(ContactFirstName,'') as 'MContactFirstName', ISNULL(ContactMiddleName,'') as 'MContactMiddleName', ISNULL(ContactLastName,'') as 'MContactLastName', ISNULL(ContactFullName,'') as 'MContactFullName',   
					ISNULL(PhoneNo,'') as 'PhoneNo', ISNULL(FaxNo,'') as 'FaxNo', ISNULL(EmailAddresss,'') as 'Email', ISNULL(Address1,'') as 'Address1', ISNULL(Address2,'') as 'Address2', ISNULL(Address3,'') as 'Address3',   
					ISNULL(City,'') as 'City', ISNULL([State],'') as 'State', ISNULL(Zip,'') as 'Zip', VendorID    
				from VendorRemittanceAddresses where Mailing = '1' and IsActive = '1'  
				) AS VRA1 
		ON VL.VendorId = VRA1.VendorID   
	--LEFT JOIN (Select VendorID, MAX(R.RequestID) as 'RequestID' from vw_Requests R INNER JOIN vw_RequestDetailInvoice RDI on R.RequestID = RDI.RequestID Group by VendorID) R on VL.VendorId = R.VendorID  
	--LEFT JOIN vw_Requests R1 on R.RequestID = R1.RequestID   
	LEFT JOIN (	Select VendorID, Convert(Date, MAX([Created Date])) as 'Last AFR Created Date' 
				from Requests R  
					INNER join vw_RequestDetailInvoice RDI 
						on R.RequestID = RDI.RequestID  
					INNER JOIN (	Select RecordID, MIN(ChangeDate) as 'Created Date' 
									from vw_ColumnChangeHistory_MARS 
									where TableName = 'Requests' 
									Group by RecordID) M 
						on R.RequestID = M.RecordID  
				GROUP By VendorID  
			  ) R 
		on VL.VendorId = R.VendorID  
WHERE     (VL.IsActive = '1')   
  
  
  
  
  
GO


