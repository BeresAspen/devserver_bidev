USE [IntacctDM]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.di_SyncDMSetEarliestPeriod') IS NOT NULL
	DROP PROCEDURE dbo.di_SyncDMSetEarliestPeriod;
GO


CREATE PROCEDURE [dbo].[di_SyncDMSetEarliestPeriod]
AS
BEGIN

/********************************************************************************************************** *
   Procedure Name  :	di_SyncDMSetEarliestPeriod
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	09/30/2018

   *********************************************************************************************************
   **         Change History                                                                              **
   *********************************************************************************************************

   Date			Author				Description
   --------		-----------			------------
   
************************************************************************************************************ */


DECLARE	@FirstEntryDate DATE
	,	@PeriodName VARCHAR(100)
	,	@User     varchar(128) = NULL
	,	@Application varchar(128) = 'di_SyncDMSetEarliestPeriod'
	,	@Version     varchar(10) = '1.00.00'
	
IF @User IS NULL 
  SET @User = SUSER_NAME ();
   
	SET @FirstEntryDate = (	SELECT MIN(CAST(ENTRY_DATE AS DATE))
							FROM Staging.dbo.IntacctStgGLEntry
							)

	SELECT @PeriodName = NAME
	FROM IntacctDM.dbo.ReportingPeriod
	WHERE @FirstEntryDate BETWEEN CAST(START_DATE AS DATE) AND CAST(END_DATE AS DATE)
		AND NAME LIKE '%Month Ended%'


BEGIN TRY
	BEGIN TRANSACTION

		UPDATE sp 
		SET		EarliestPeriod = ISNULL(@PeriodName,'NO DATA')
			,	LastRunDate = GETDATE()
		FROM IntacctDM.dbo.SyncParameters sp

	COMMIT

END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK
		
      DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
      SELECT @ErrMsg = ERROR_MESSAGE(),
             @ErrSeverity = ERROR_SEVERITY()

      RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH


END