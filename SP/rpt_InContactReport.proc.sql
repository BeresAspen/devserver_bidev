USE [MARS_DW]
GO

/****** Object:  StoredProcedure [dbo].[rpt_InContactReport]    Script Date: 4/6/2017 7:58:45 AM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


/************************************************************************

	Object : rpt_InContactReport.proc.sql
	Author: Benjacob Beres
	Create date: 01/10/2017
	Description: SSRS report for the PreDialer

*************************************************************************/

CREATE PROCEDURE [dbo].[rpt_InContactReport]
AS
BEGIN

/*****************************************************************
Gather all Accounts and their Phone Numbers per every archive date

*****************************************************************/
SELECT distinct cast(ArchiveDate as date) ArchiveDate 
	,	account
	,	mars.dbo.fn_RemoveNonNumericCharacters(phone) AS phone
into #BorrowerPhoneNumbers
FROM 
   (SELECT account, l.ArchiveDate, l.Home, l.Work, l.Mobil, c.PhoneHome, c.PhoneWork, c.PhoneCell
	FROM mars_dw.dbo.vw_Loans_Archive l  
		LEFT JOIN dbo.vw_CoBorrowers_Archive c
			ON	c.[LoanRecID] = l.RecID
			AND c.ArchiveDate = l.ArchiveDate) p
UNPIVOT
   (phone for phones IN 
      (home, work, mobil, PhoneHome, PhoneWork, PhoneCell)
)AS unpvt
where phone <> ''


/*****************************************************************
Pull list of accounts and their Category for Manual Outbound and Inbound

*****************************************************************/

SELECT DISTINCT 
		l.Account
	,	CAST(lc.MyDateTime AS DATE) AS MyDateTime
	,	lc.Category
INTO	#CategoryIdentifying
FROM	MARS.dbo.LoansContacts lc (NOLOCK)
LEFT JOIN MARS.dbo.vw_Loans l
	ON	l.RecID = lc.LoanRecID
WHERE lc.MyDateTime >= '10/10/2016'
	and lc.Category IN (	'NO - Outbound Attempt/no Contact with Borrower'
						,	'TA - Outbound Attempt/no Contact with Authorized NO3'
						,	'AA - Outbound Attempt/no Contact with Attorney'
						,	'TO - Authorized 3rd Party on Outbound Call'
						,	'AO - Attorney Contact on Outbound Call'
						,	'BO - Borrower Contact on Outbound Call'
						,	'AI - Attorney Contact on Inbound Call'
						,	'BI - Borrower Contact on Inbound Call'
						,	'TC - Authorized 3rd Party on Inbound'
						,	'NA - Non-Authorized 3rd Party on Inbound Call') 


SELECT	c.Account
	,	c.MyDateTime
	,	SUM(CASE WHEN c.Category IN (	'AI - Attorney Contact on Inbound Call'
									,	'BI - Borrower Contact on Inbound Call'
									,	'TC - Authorized 3rd Party on Inbound'
									,	'NA - Non-Authorized 3rd Party on Inbound Call') 
					THEN 1 
				 ELSE 0
			END) AS InboundNotOnDialer
	,	SUM(CASE WHEN c.Category IN (	'NO - Outbound Attempt/no Contact with Borrower'
									,	'TA - Outbound Attempt/no Contact with Authorized NO3'
									,	'AA - Outbound Attempt/no Contact with Attorney') 
					THEN 1 
				 ELSE 0
			END) AS OutboundAttemptNotOnDialer
	,	SUM(CASE WHEN c.Category IN (	'TO - Authorized 3rd Party on Outbound Call'
									,	'AO - Attorney Contact on Outbound Call'
									,	'BO - Borrower Contact on Outbound Call') 
					THEN 1 
				 ELSE 0
			END) AS OutboundContactNotOnDialer
INTO #ManualOutbound
FROM	#CategoryIdentifying c
GROUP BY c.Account
	,	c.MyDateTime


/*****************************************************************
All accounts recorded in the In Contact file for the dialer analysis

*****************************************************************/

select	b.ArchiveDate
	,	b.account
	,   ddf.skill_name
	,	MAX(ddf.Total_Time) AS Total_Time
into #FullList
from	mars_dw.dbo.InContactDialerFile ddf
	left join	#BorrowerPhoneNumbers b
		on	b.ArchiveDate = ddf.[start_date]
		and b.phone = ddf.ani_dialnum
where b.Account IS NOT NULL
GROUP BY b.ArchiveDate
	,	b.account
	,   ddf.skill_name
order by b.Account DESC



/*****************************************************************
Count the dialer records per report needs

*****************************************************************/

select CAST(f.ArchiveDate AS DATE) AS CallDate
	,	SUM(CASE 
				WHEN ISNULL(f.skill_name,'') IN ('IB Collections Spanish', 'IB Collections')
					THEN 1
				ELSE 0
			END) AS InboundContact
	,	SUM(CASE 
				WHEN ISNULL(f.skill_name,'') IN ('OB Manual', 'Outbound Lists 2')
						AND f.Total_Time <> 0
					THEN 1
				ELSE 0
			END) AS OutboundContact
	,	SUM(CASE 
				WHEN ISNULL(f.skill_name,'') IN ('OB Manual', 'Outbound Lists 2')
						AND f.Total_Time = 0
					THEN 1
				ELSE 0
			END) AS OutboundAttempt
	,	CAST(0 AS INT) AS OutboundAttemptNotOnDialer
	,	CAST(0 AS INT) AS OutboundContactNotOnDialer
INTO #ReportResults
FROM #FullList f
WHERE	ISNULL(f.skill_name,'') <> 'Inbound Default (No Agents)'
GROUP BY CAST(f.ArchiveDate AS DATE)
ORDER BY CAST(f.ArchiveDate AS DATE)


/*****************************************************************
Update the reports with the Manual attempts and contacts not
	on the dialer.

*****************************************************************/

;WITH cteManualCounts
AS
(
	SELECT	m.MyDateTime
		,	SUM(m.InboundNotOnDialer) AS InboundNotOnDialer
		,	SUM(OutboundAttemptNotOnDialer) AS OutboundAttemptNotOnDialer
		,	SUM(OutboundContactNotOnDialer) AS OutboundContactNotOnDialer
	FROM	#ManualOutbound m
		LEFT JOIN #FullList f
			ON	m.Account = f.Account
			AND CAST(m.MyDateTime AS DATE) = CAST(f.ArchiveDate AS DATE)
	WHERE	f.Account IS NULL
	GROUP BY m.MyDateTime
)
UPDATE r
	SET InboundContact = r.InboundContact + cmc.InboundNotOnDialer
	,	OutboundAttemptNotOnDialer = cmc.OutboundAttemptNotOnDialer
	,	OutboundContactNotOnDialer = cmc.OutboundContactNotOnDialer
	FROM #ReportResults r
	JOIN cteManualCounts cmc 
		ON	cmc.MyDateTime = r.CallDate



SELECT	r.CallDate
	,	r.InboundContact
	,	r.OutboundContact
	,	r.OutboundAttempt
	,	r.OutboundAttemptNotOnDialer
	,	r.OutboundContactNotOnDialer
FROM	#ReportResults r
WHERE r.CallDate = '01/12/2017'  --bberes - 20130113

END
GO


