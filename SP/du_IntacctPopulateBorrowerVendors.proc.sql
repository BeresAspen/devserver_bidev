USE MARS_DW;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.du_IntacctPopulateBorrowerVendors') IS NOT NULL
	DROP PROCEDURE dbo.du_IntacctPopulateBorrowerVendors;

GO

/* ********************************************************************************************************* *
   Procedure Name  :	du_IntacctPopulateBorrowerVendors
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	02/15/2017

   *********************************************************************************************************
   **         Change History                                                                              **
   *********************************************************************************************************

   Date			Author				Description
   --------		-----------			------------
   
*  ********************************************************************************************************* */

CREATE PROCEDURE dbo.du_IntacctPopulateBorrowerVendors
AS
BEGIN

	TRUNCATE TABLE dbo.IntacctVendor


	INSERT INTO dbo.IntacctVendor
			([NAME], TAX_ID, CURRENCY, CONTACT_NAME, FIRST_NAME, MI, LAST_NAME, PRINT_AS, PHONE1, PHONE2, CELLPHONE, EMAIL1, ADDRESS1, CITY, [STATE], ZIP, MERGE_PAYMENT_REQUESTS ,DYNAMIC_VENDOR_ID, VENDOR_PAYMENT_FOR_CHECKLIST, VENDOR_TYPE)
	SELECT	l.FullName
		,	MARS.dbo.fn_RemoveNonNumericCharacters(l.TIN) AS TAX_ID
		,	'USD' AS CURRENCY
		,	l.FullName AS CONTACT_NAME
		,	l.[First Name]
		,	l.MI
		,	l.[Last Name]
		,	l.FullName AS PRINT_AS
		,	MARS.dbo.fn_RemoveNonNumericCharacters(l.Home) AS Phone1
		,	MARS.dbo.fn_RemoveNonNumericCharacters(l.Work) AS Phone2
		,	MARS.dbo.fn_RemoveNonNumericCharacters(l.Mobil) AS CellPhone
		,	CASE WHEN l.EmailAddress like '%@%#%'
					THEN	CASE WHEN l.EmailAddress like '%[[]%#%' THEN SUBSTRING(l.EmailAddress,CHARINDEX('[',l.EmailAddress)+1,CHARINDEX(']',l.EmailAddress)-1-CHARINDEX('[',l.EmailAddress))
								 WHEN l.EmailAddress like '%;%#%' THEN SUBSTRING(l.EmailAddress,1,CHARINDEX(';',l.EmailAddress)-1)
								 WHEN l.EmailAddress like '%#%' THEN SUBSTRING(l.EmailAddress,1,CHARINDEX('#',l.EmailAddress)-1)
							END
				 ELSE l.EmailAddress
			END AS Email1
		,	l.Street AS Address1
		,	l.City
		,	l.[State]
		,	l.[Zip Code] AS Zip
		,	CASE WHEN vl.MultiCheck = 1 THEN 'NO' ELSE 'YES' END AS MERGE_PAYMENT_REQUESTS
		,	vl.VendorId AS DYNAMIC_VENDOR_ID
		,	pt.PaymentType as VENDOR_PAYMENT_FOR_CHECKLIST
		,	'Borrower' AS VENDOR_TYPE
	FROM MARS.dbo.vw_Loans l
		INNER JOIN MARS.dbo.VendorsList AS vl  
			ON	vl.TIN = l.TIN
			AND vl.VendorTypeId = 10
		INNER JOIN MARS.dbo.VendorPaymentsInfo AS vpi
			ON	vpi.VendorId = vl.VendorId
		INNER JOIN MARS.dbo.PaymentTypeDetail AS ptd 
			ON vpi.VendorPaymentInfoId = ptd.PaymentTypeDetailID
		INNER JOIN MARS.dbo.PaymentTypes AS pt 
			ON ptd.PaymentTypeID = pt.PaymentTypeId
	WHERE	(	l.isActive = 1
			OR	EXISTS	(	SELECT 1
							FROM MARS.dbo.vw_PaymentHistory ph
							WHERE	ph.Account = l.Account
								AND ph.DateRec > DATEADD(MM,-12,GETDATE())
						)
			)
END
