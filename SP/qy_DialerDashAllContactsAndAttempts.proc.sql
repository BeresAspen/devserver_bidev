USE MARS_DW;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.qy_DialerDashAllContactsAndAttempts') IS NOT NULL
	DROP PROCEDURE dbo.qy_DialerDashAllContactsAndAttempts;
GO

/* ********************************************************************************************************* *
   Procedure Name  :	qy_DialerDashAllContactsAndAttempts
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	02/07/2017

   *********************************************************************************************************
   **         Change History                                                                              **
   *********************************************************************************************************

   Date			Author				Description
   --------		-----------			------------
   02/14/2017	Benjacob Beres		Removed the InboundNotOnDialer grouping and uneccesary fields from results.
									Also removed the carryover which was causing duplicate counts
   03/24/2017	Benjacob Beres		Changed the process date from 12/08/2016 to 90 days previous.
*  ********************************************************************************************************* */

CREATE PROCEDURE dbo.qy_DialerDashAllContactsAndAttempts
AS
BEGIN

	DECLARE @ProcessFromDate DATE

	SET @ProcessFromDate = GETDATE()-90

	/****************************************************************
	Create Ranking Table
	*****************************************************************/
	CREATE TABLE #Ranking
		(	CodeRank	INT
		,	CodeDescr	VARCHAR(50)
		)

	INSERT INTO #Ranking
	VALUES	(	1,'OutboundContact')
		,	(	2,'InboundContact')
		,	(	3,'OutboundContactNotOnDialer')
		,	(	4,'OutboundAttempt')
		,	(	5,'OutboundAttemptNotOnDialer' )
		,	(	6,'Untouched Loans' )

	/*****************************************************************
	Count of all possible calls per day since the dailerfile table creation

	*****************************************************************/

	SELECT DISTINCT
			d.Account
		,	CAST(d.RunDate AS DATE) AS RunDate
		,	CAST('Untouched Loans' AS VARCHAR(30)) AS CallType
	INTO	#FullList
	FROM	MARS.dbo.dialerfile d
	WHERE	CAST(d.RunDate AS DATE) >= @ProcessFromDate
	ORDER BY CAST(d.RunDate AS DATE) 

	/*****************************************************************
	Pull list of accounts and their Category for Manual Outbound and Inbound

	*****************************************************************/

	SELECT	l.Account
		,	CAST(lc.MyDateTime AS DATE) AS MyDateTime
		,	CASE WHEN lc.Category IN (	'AI - Attorney Contact on Inbound Call'
										,	'BI - Borrower Contact on Inbound Call'
										,	'TC - Authorized 3rd Party on Inbound'
										,	'NA - Non-Authorized 3rd Party on Inbound Call') 
						THEN 'InboundContact'
				 WHEN lc.Category IN (	'NO - Outbound Attempt/no Contact with Borrower'
										,	'TA - Outbound Attempt/no Contact with Authorized NO3'
										,	'AA - Outbound Attempt/no Contact with Attorney') 
						THEN 'OutboundAttemptNotOnDialer' 
				 WHEN lc.Category IN (	'TO - Authorized 3rd Party on Outbound Call'
										,	'AO - Attorney Contact on Outbound Call'
										,	'BO - Borrower Contact on Outbound Call') 
						THEN 'OutboundContactNotOnDialer'
			END AS CallType
	INTO	#Manual
	FROM	MARS.dbo.LoansContacts lc (NOLOCK)
	LEFT JOIN MARS.dbo.vw_Loans l
		ON	l.RecID = lc.LoanRecID
	WHERE lc.MyDateTime >= @ProcessFromDate
		AND lc.Category IN (	'NO - Outbound Attempt/no Contact with Borrower'
							,	'TA - Outbound Attempt/no Contact with Authorized NO3'
							,	'AA - Outbound Attempt/no Contact with Attorney'
							,	'TO - Authorized 3rd Party on Outbound Call'
							,	'AO - Attorney Contact on Outbound Call'
							,	'BO - Borrower Contact on Outbound Call'
							,	'AI - Attorney Contact on Inbound Call'
							,	'BI - Borrower Contact on Inbound Call'
							,	'TC - Authorized 3rd Party on Inbound'
							,	'NA - Non-Authorized 3rd Party on Inbound Call') 


	/*****************************************************************
	Gather all Accounts and their Phone Numbers per every archive date
		and associate those with the InContact table records to get account
	*****************************************************************/
	SELECT distinct cast(ArchiveDate as date) ArchiveDate 
		,	account
		,	mars.dbo.fn_RemoveNonNumericCharacters(phone) AS phone
	INTO #BorrowerPhoneNumbers
	FROM 
	   (SELECT account, l.ArchiveDate, l.Home, l.Work, l.Mobil, c.PhoneHome, c.PhoneWork, c.PhoneCell
		FROM mars_dw.dbo.vw_Loans_Archive l  
			LEFT JOIN dbo.vw_CoBorrowers_Archive c
				ON	c.[LoanRecID] = l.RecID
				AND c.ArchiveDate = l.ArchiveDate
		WHERE CAST(l.ArchiveDate AS DATE) >= @ProcessFromDate) p
	UNPIVOT
	   (phone for phones IN 
		  (home, work, mobil, PhoneHome, PhoneWork, PhoneCell)
	)AS unpvt
	where phone <> ''


	--------------------------------------------

	SELECT	b.account
		,	b.ArchiveDate
		,	CASE WHEN ISNULL(ddf.skill_name,'') IN ('IB Collections Spanish', 'IB Collections')
					THEN 'InboundContact'
				 WHEN ISNULL(ddf.skill_name,'') IN ('OB Manual', 'Outbound Lists 2')
							AND ddf.Total_Time <> 0
					THEN 'OutboundContact'
				 WHEN ISNULL(ddf.skill_name,'') IN ('OB Manual', 'Outbound Lists 2')
							AND ddf.Total_Time = 0
					THEN 'OutboundAttempt'
			END AS CallType
	INTO #ThroughDialer
	FROM	mars_dw.dbo.InContactDialerFile ddf
		LEFT JOIN	#BorrowerPhoneNumbers b
			ON	b.ArchiveDate = ddf.[start_date]
			AND b.phone = ddf.ani_dialnum
	WHERE	ISNULL(ddf.skill_name,'') IN ('IB Collections Spanish', 'IB Collections', 'OB Manual', 'Outbound Lists 2')
		AND	b.Account IS NOT NULL
		AND CAST(ddf.[start_date] AS DATE) >= @ProcessFromDate
		

	/*****************************************************************
	  Union the Manual and through dialer records together
	*****************************************************************/

	SELECT	d.Account
		,	d.ArchiveDate
		,	d.CallType
	INTO #Results
	FROM #ThroughDialer d

	UNION ALL

	SELECT	m.Account
		,	m.MyDateTime AS ArchiveDate
		,	m.CallType
	FROM #Manual m
	WHERE NOT EXISTS (	SELECT 'Here' 
						FROM	#ThroughDialer d
						WHERE	d.Account = m.Account
							AND	d.ArchiveDate = m.MyDateTime)

	/*****************************************************************
	 Pull ACH Counts for the time frame
	*****************************************************************/

	SELECT	CAST(a.InsertedDate AS DATE) AS InsertedDate
		,	COUNT(DISTINCT a.LoanNumber) AS ACHCount
	INTO #ACHData
	FROM MARS.[dbo].[vw_ACHBillingsEnteredByLoanByRep] a
	WHERE	CAST(a.InsertedDate AS DATE) >= @ProcessFromDate
		AND	[Reoccuring Schedule/One Time] = 'One Time'
	GROUP BY CAST(a.InsertedDate AS DATE)

	/*****************************************************************
	 Group fields by dateQA?
	*****************************************************************/

	SELECT	r.ArchiveDate
		,	ISNULL(a.ACHCount,0) AS ACHCount
		,	SUM(CASE WHEN r.CallType = 'InboundContact' THEN 1 ELSE 0 END) AS InboundContact
		,	SUM(CASE WHEN r.CallType = 'OutboundContact' THEN 1 ELSE 0 END) AS OutboundContact
		,	SUM(CASE WHEN r.CallType = 'OutboundAttempt' THEN 1 ELSE 0 END) AS OutboundAttempt
		,	SUM(CASE WHEN r.CallType = 'OutboundAttemptNotOnDialer' THEN 1 ELSE 0 END) AS OutboundAttemptNotOnDialer
		,	SUM(CASE WHEN r.CallType = 'OutboundContactNotOnDialer' THEN 1 ELSE 0 END) AS OutboundContactNotOnDialer
		,	SUM(CASE WHEN r.CallType = 'Untouched Loans' THEN 1 ELSE 0 END) AS UnTouchedLoans
	INTO #TotalCounts
	FROM	#Results r
		LEFT JOIN #ACHData a
			ON	a.InsertedDate = r.ArchiveDate
	GROUP BY r.ArchiveDate, ISNULL(a.ACHCount,0)
	ORDER BY r.ArchiveDate
	

	/*****************************************************************
	 Return results
	*****************************************************************/

	SELECT	t.ArchiveDate AS CallDate
		,	t.ACHCount
		,	t.InboundContact
		,	t.OutboundContact
		,	t.OutboundAttempt
		,	t.OutboundAttemptNotOnDialer
		,	t.OutboundContactNotOnDialer
	FROM #TotalCounts t
		INNER JOIN Support.[dbo].[DimDate] dd
			ON	dd.[Date] = t.ArchiveDate
	WHERE	t.ArchiveDate >= @ProcessFromDate
		AND	((dd.IsWeekday = 1 AND dd.IsHoliday = 0)
			OR (t.InboundContact + t.OutboundContact + t.OutboundContactNotOnDialer) >= 20)



END