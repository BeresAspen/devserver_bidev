USE [MARS_DW]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* ********************************************************************************************************* *
   Procedure Name	:	qy_UPBReport
   Business Analyis	:	
   Project/Process	:   
   Description		:	
   Author			:	Benjacob Beres
   Create Date		:	11/01/2017
   Ticket #			:	JIRA : MARS-2748
   Notes			:	Created from existing Adhoc report being run for many months. SP created to 
							automate the report for Accounting.

   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date			Author				Description
   --------		-----------			------------
   
*  ********************************************************************************************************* */
CREATE PROCEDURE dbo.qy_UPBReport

AS
BEGIN

	SELECT	ISNULL(vl.Account, '') AS [Loan #]
		,	ISNULL(CONVERT(VARCHAR(10),vl.PaidOffDate,101),'') AS [Inactive Date]
		,	ISNULL(vl.LoanStatus, '') AS [Loan Status]
		,	vl.isActive
		,	ISNULL(vl.ConvertedToPropertyID, '') AS [REO ID]
		,	CASE
				WHEN ISNULL(vl.isactive, '') = 1
					THEN ISNULL(vl.PrinBal, 0)
				WHEN ISNULL(vl.isactive, '') = 0 AND vl.LoanStatus = 'Trailing Claims'
					THEN ISNULL(vl.PrinBal, 0)
				WHEN ISNULL(vl.isactive, '') = 0
					THEN 0
			END AS UPB --pulls only active UPB's
		,	CASE
				WHEN ISNULL(vl.isactive, '') = 1
					THEN ISNULL(vd.Amount, 0)
				WHEN ISNULL(vl.isactive, '') = 0 AND vl.LoanStatus = 'Trailing Claims'
					THEN ISNULL(vd.Amount, 0)
				WHEN ISNULL(vl.isactive, '') = 0
					THEN 0
			END AS DUPB --pulls only active DUPB's
		,	ISNULL(vl.NoteRate, '') AS [Interest Rate]
		,	CASE
				WHEN vl.PaidToDate IS NULL
					THEN ''
				ELSE CONVERT(VARCHAR(50), vl.PaidToDate, 101)
			END AS [Paid To Date]
		,	CASE
				WHEN vl.NextDueDate IS NULL
					THEN ''
				ELSE CONVERT(VARCHAR(50), vl.NextDueDate, 101)
			END AS [Next Due Date]
		,	CASE
				WHEN vb.DatePerformed IS NULL
					THEN ''
				ELSE CONVERT(VARCHAR(50), vb.DatePerformed, 101)
			END AS [BPO Date]
		,	ISNULL(vb.ListingAsIsValue, '') AS [BPO Value]
		,	CASE WHEN r.Status = 'Contract' THEN r.ContractPrice
				 ELSE 0.00
			END AS ContractPrice
		,	CASE WHEN r.Status = 'For Sale' THEN r.OfferedPrice
				 ELSE 0.00
			END	AS OfferedPrice
		,	ISNULL(vl.PropertyState, '') AS State --pulling property state as "State" field references borrower mailing address
		,	CASE
				WHEN vl.MaturityDate IS NULL
					THEN ''
				ELSE CONVERT(VARCHAR(50), vl.MaturityDate, 101)
			END AS [Maturity Date]
		,	ISNULL(vl.PmtPI, 0) AS 'P&I'
		,	CASE
				WHEN vl.ServiceXferDate IS NULL
					THEN ''
				ELSE CONVERT(VARCHAR(50), vl.ServiceXferDate, 101)
			END AS [Service Transfer Date]
		,	ISNULL(vl.NoteOwner, '') AS [Note Owner]
	FROM MARS.dbo.vw_Loans vl
		LEFT OUTER JOIN MARS.dbo.vw_BPOCurrent  vb
			ON vl.Account = vb.LoanID
		LEFT OUTER JOIN MARS.dbo.vw_DUPBbyLoan vd
			ON vl.Account = vd.Account
		LEFT JOIN MARS.dbo.vw_REO r 
			ON	r.FormerGFLoanNumber = vl.Account
	ORDER BY 1

END