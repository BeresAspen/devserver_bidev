USE [MARS_DW]
GO
/****** Object:  StoredProcedure [dbo].[rpt_DialerFileReport]    Script Date: 4/6/2017 7:51:28 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

/************************************************************************

	Object : DialerFileReport.proc.sql
	Author: Benjacob Beres
	Create date: 01/10/2017
	Description: SSRS report for the Daily Dialer

	ChangeLog:
	ChangeDate		Changed By			Change#		Reason
	01/18/2017		Benjacob Beres					Misty requested additional fields.
*************************************************************************/

ALTER PROCEDURE [dbo].[rpt_DialerFileReport]
AS
BEGIN

/*****************************************************************
Count of all possible calls per day since the dailerfile table creation

*****************************************************************/

SELECT	CAST(d.RunDate AS DATE) AS RunDate
	,	COUNT(d.Account) AS AccountCnt
INTO	#TotalCounts
FROM	MARS.dbo.dialerfile d
GROUP BY CAST(d.RunDate AS DATE) 


/*****************************************************************
Gather all Accounts and their Phone Numbers per every archive date

*****************************************************************/
SELECT distinct cast(ArchiveDate as date) ArchiveDate 
	,	account
	,	mars.dbo.fn_RemoveNonNumericCharacters(phone) AS phone
into #BorrowerPhoneNumbers
FROM 
   (SELECT account, l.ArchiveDate, l.Home, l.Work, l.Mobil, c.PhoneHome, c.PhoneWork, c.PhoneCell
	FROM mars_dw.dbo.vw_Loans_Archive l  
		LEFT JOIN dbo.vw_CoBorrowers_Archive c
			ON	c.[LoanRecID] = l.RecID
			AND c.ArchiveDate = l.ArchiveDate) p
UNPIVOT
   (phone for phones IN 
      (home, work, mobil, PhoneHome, PhoneWork, PhoneCell)
)AS unpvt
where phone <> ''

	
/*****************************************************************
Associate the records in the In Contact file with the correct account

*****************************************************************/


select	b.ArchiveDate
	,	b.account
	,   ddf.skill_name
	,	ddf.Total_Time
into #FullList
from	mars_dw.dbo.InContactDialerFile ddf
	left join	#BorrowerPhoneNumbers b
		on	b.ArchiveDate = ddf.[start_date]
		and b.phone = ddf.ani_dialnum
order by b.Account DESC


/*****************************************************************
Pull list of accounts and their Category for Manual Outbound and Inbound

*****************************************************************/

SELECT DISTINCT 
		l.Account
	,	CAST(lc.MyDateTime AS DATE) AS MyDateTime
	,	lc.Category
INTO	#CategoryIdentifying
FROM	MARS.dbo.LoansContacts lc (NOLOCK)
LEFT JOIN MARS.dbo.vw_Loans l
	ON	l.RecID = lc.LoanRecID
WHERE lc.MyDateTime >= '12/08/2016'
	AND lc.Category IN (	'NO - Outbound Attempt/no Contact with Borrower'
						,	'TA - Outbound Attempt/no Contact with Authorized NO3'
						,	'AA - Outbound Attempt/no Contact with Attorney'
						,	'TO - Authorized 3rd Party on Outbound Call'
						,	'AO - Attorney Contact on Outbound Call'
						,	'BO - Borrower Contact on Outbound Call'
						,	'AI - Attorney Contact on Inbound Call'
						,	'BI - Borrower Contact on Inbound Call'
						,	'TC - Authorized 3rd Party on Inbound'
						,	'NA - Non-Authorized 3rd Party on Inbound Call') 


SELECT	c.Account
	,	c.MyDateTime
	,	SUM(CASE WHEN c.Category IN (	'AI - Attorney Contact on Inbound Call'
									,	'BI - Borrower Contact on Inbound Call'
									,	'TC - Authorized 3rd Party on Inbound'
									,	'NA - Non-Authorized 3rd Party on Inbound Call') 
					THEN 1 
				 ELSE 0
			END) AS InboundNotOnDialer
	,	SUM(CASE WHEN c.Category IN (	'NO - Outbound Attempt/no Contact with Borrower'
									,	'TA - Outbound Attempt/no Contact with Authorized NO3'
									,	'AA - Outbound Attempt/no Contact with Attorney') 
					THEN 1 
				 ELSE 0
			END) AS OutboundAttemptNotOnDialer
	,	SUM(CASE WHEN c.Category IN (	'TO - Authorized 3rd Party on Outbound Call'
									,	'AO - Attorney Contact on Outbound Call'
									,	'BO - Borrower Contact on Outbound Call') 
					THEN 1 
				 ELSE 0
			END) AS OutboundContactNotOnDialer
INTO #ManualOutbound
FROM	#CategoryIdentifying c
GROUP BY c.Account
	,	c.MyDateTime


--------------------------------------

DECLARE @ErrorCatCheck INT
SET @ErrorCatCheck = (	select COUNT(m.Account)
						FROM #ManualOutbound m
						GROUP BY m.Account, m.MyDateTime
						HAVING COUNT(m.Account) > 1
						)
IF @ErrorCatCheck > 0 
	BEGIN
		RAISERROR('Duplicate Category for an Account per day. Will cause incorrect counts',16,1)
	END


/*****************************************************************
Count the dialer records per report needs

*****************************************************************/

SELECT CAST(d.RunDate AS DATE) AS CallDate
	,	SUM(CASE 
				WHEN ISNULL(f.skill_name,'') IN ('IB Collections Spanish', 'IB Collections')
					THEN 1
				ELSE 0
			END) AS InboundContact
	,	SUM(CASE 
				WHEN ISNULL(f.skill_name,'') IN ('OB Manual', 'Outbound Lists 2')
						AND f.Total_Time <> 0
					THEN 1
				ELSE 0
			END) AS OutboundContact
	,	SUM(CASE 
				WHEN ISNULL(f.skill_name,'') IN ('OB Manual', 'Outbound Lists 2')
						AND f.Total_Time = 0
					THEN 1
				ELSE 0
			END) AS OutboundAttempt
	,	CAST(0 AS INT) AS OutboundAttemptNotOnDialer
	,	CAST(0 AS INT) AS OutboundContactNotOnDialer
	,	CAST(0 AS INT) AS UnTouchedLoans
	,	CAST(0 AS INT) AS TotalCount
INTO #ReportResults
FROM MARS.dbo.dialerfile d
	LEFT JOIN #FullList f
		ON	f.Account = d.Account
		AND	f.ArchiveDate = CAST(d.RunDate AS DATE)
WHERE	ISNULL(f.skill_name,'') <> 'Inbound Default (No Agents)'
GROUP BY CAST(d.RunDate AS DATE)
ORDER BY CAST(d.RunDate AS DATE)


/*****************************************************************
Update the reports with the Manual attempts and contacts not
	on the dialer.

*****************************************************************/

;WITH cteManualCounts
AS
(
	SELECT	m.MyDateTime
		,	SUM(m.InboundNotOnDialer) AS InboundNotOnDialer
		,	SUM(OutboundAttemptNotOnDialer) AS OutboundAttemptNotOnDialer
		,	SUM(OutboundContactNotOnDialer) AS OutboundContactNotOnDialer
	FROM	#ManualOutbound m
		LEFT JOIN MARS.dbo.dialerfile d
			ON	m.Account = d.Account
			AND CAST(m.MyDateTime AS DATE) = CAST(d.RunDate AS DATE)
	WHERE	d.Account IS NULL
	GROUP BY m.MyDateTime
)
UPDATE r
	SET InboundContact = r.InboundContact + cmc.InboundNotOnDialer
	,	OutboundAttemptNotOnDialer = cmc.OutboundAttemptNotOnDialer
	,	OutboundContactNotOnDialer = cmc.OutboundContactNotOnDialer
	FROM #ReportResults r
	JOIN cteManualCounts cmc 
		ON	cmc.MyDateTime = r.CallDate

		
/*****************************************************************
Calculate Untouched Loans
*****************************************************************/

UPDATE r
	SET UnTouchedLoans = t.AccountCnt - (r.InboundContact + r.OutboundContact + r.OutboundAttempt + r.OutboundAttemptNotOnDialer + r.OutboundContactNotOnDialer)
	,	TotalCount = t.AccountCnt
	FROM #ReportResults r
	JOIN #TotalCounts t
		ON	t.RunDate = r.CallDate

		
/*****************************************************************
Report 1 Results
*****************************************************************/

SELECT	DATENAME(DW,r.CallDate) AS DOW
	,	r.CallDate
	,	r.TotalCount AS TotalDialable
	,	r.InboundContact
	,	r.OutboundContact
	,	r.OutboundAttempt
	,	r.OutboundAttemptNotOnDialer
	,	r.OutboundContactNotOnDialer
	,	r.UnTouchedLoans
	,	CAST((CAST((r.InboundContact + r.OutboundContact + r.OutboundAttempt + r.OutboundAttemptNotOnDialer + r.OutboundContactNotOnDialer) AS FLOAT)/CAST(r.TotalCount AS FLOAT)) AS DECIMAL(3,2)) AS Penetration
	,	CAST((CAST((r.InboundContact + r.OutboundContact) AS FLOAT)/CAST(r.TotalCount AS FLOAT)) AS DECIMAL(3,2)) AS ContactPenetration
	,	CAST((CAST((r.OutboundContact + r.OutboundContactNotOnDialer) AS FLOAT)/CAST(r.OutboundAttempt + r.OutboundAttemptNotOnDialer AS FLOAT)) AS DECIMAL(3,2)) AS RPC
FROM #ReportResults r
	INNER JOIN Support.[dbo].[DimDate] dd
		ON	dd.[Date] = r.CallDate
WHERE	((dd.IsWeekday = 1 AND dd.IsHoliday = 0)
		OR (r.InboundContact + r.OutboundContact + r.OutboundAttempt + r.OutboundAttemptNotOnDialer + r.OutboundContactNotOnDialer) >= 20)


END