USE [MARS_DW]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.qy_LTR050EI45DayNoContactWithLM') IS NOT NULL
	DROP PROCEDURE dbo.qy_LTR050EI45DayNoContactWithLM;
GO

CREATE PROCEDURE dbo.qy_LTR050EI45DayNoContactWithLM
AS

/* ********************************************************************************************************* *
   Procedure Name  :	dbo.qy_LTR050EI45DayNoContactWithLM
   Business Analyis:	
   Project/Process :   
   Description     :	
   Issue ID			:	Jira - MARS-2082
   Author          :	Benjacob Beres
   Create Date     :	08/21/2017

   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************
   
   Date			Author			Story				Description
   --------		-----------		-----------			------------
   09/22/2017	Benjacob Beres	SD14748				Switching the BKDischarge elimination to a inclusion
*  ********************************************************************************************************* */

BEGIN

IF OBJECT_ID('tempdb..#Temp') IS NOT NULL
	DROP TABLE #Temp;

SELECT	l.Account
	,	l.RecID
	,	ls.LoanID
	,	l.LoanStatus
	,	l.[State]
	,	CASE WHEN LEFT(l.Account,1) IN ('5', '6')
				THEN 'Y'
			 ELSE 'N'
		END AS EliminatedByCommercial
	,	CASE WHEN bk.IsActive = 1
				THEN 'Y'
			 ELSE 'N'
		END AS EliminatedByBKActive	
	,	CASE WHEN l.LoanStatus IN (	'BK11','BK11,FC','BK12','BK13','BK13, DL','BK13,FC','BK7','BK7, DL','BK7,FC','FC,DIL','Inactive - REO/FC',
									'PRELIM','Service Xfer','Trailing Claims','Paid in Full','Paidoff', 'DIL','FBA','PP') --'FC', removed per new instructions
				THEN 'Y'
			 WHEN l.LoanStatus LIKE '%Inactive%'
				THEN 'Y'
			 WHEN l.LoanStatus LIKE 'BK13%'
				THEN 'Y'
			 ELSE 'N'
		END AS EliminatedByLoanStatus
	,	CASE WHEN BKDischarge IS NOT NULL 
				THEN 'N'
			 ELSE 'N'
		END AS EliminatedByBKDischarge
	,	vgl.ReasonForRestrictCommunication
	,	CASE WHEN vgl.ReasonForRestrictCommunication LIKE '%Cease and Desist%'
				THEN 'Y'
			 ELSE 'N'
		END AS EliminatedByCandD
	,	DATEDIFF(D,l.NextDueDate,GETDATE()) AS dayspastdue
	,	CASE WHEN DATEDIFF(D,l.NextDueDate,GETDATE()) > 45
				THEN 'N'
			 ELSE 'Y'
		END AS EliminatedByDaysPastDue
INTO #Temp
FROM	mars.dbo.vw_loans l WITH(NOLOCK)
	JOIN	MARS.dbo.Loans ls
		ON	ls.LoanRecID = l.RecID
	LEFT JOIN (	SELECT	b.LoanRecID
					,	b.IsActive
					,	ROW_NUMBER() OVER(PARTITION BY b.LoanRecID ORDER BY b.PetitionDate DESC) AS RowNum
				FROM	MARS.dbo.vw_BKs b
			  ) bk
		ON	bk.LoanRecID = l.RecID
		AND	bk.RowNum = 1
	LEFT JOIN MARS.dbo.vw_GetLoans vgl
		ON	vgl.LoanRecID = l.RecID
	LEFT JOIN
			(	SELECT	cf.Account
					,	cf.FieldValue AS BKDischarge 
				FROM	MARS.dbo.vw_CustomFieldsForLoans cf
				WHERE	cf.FieldName ='BK - Debt Discharged'
			) discharge
		ON	discharge.Account = l.Account
WHERE	l.isActive = 1


IF OBJECT_ID('tempdb..#Temp2') IS NOT NULL
	DROP TABLE #Temp2;


SELECT	t.*
	,	CASE WHEN f.Account IS NOT NULL 
				THEN 'Y'
			 ELSE 'N'
		END AS EliminatedByForeclosure
	,	CASE WHEN fc.Account IS NOT NULL 
				THEN 'Y'
			 ELSE 'N'
		END AS EliminatedByContestedFC
	,	CASE WHEN flf.Account IS NOT NULL 
				THEN 'Y'
			 ELSE 'N'
		END AS EliminatedByMDCTFirstLegalFiles
INTO #Temp2
FROM #Temp t
	LEFT JOIN (	SELECT	DISTINCT fc1.Account
				FROM	Foreclosures.dbo.Foreclosures fc1
				WHERE	fc1.IsContested = 1
			  ) fc
		ON	fc.Account = t.Account
	LEFT JOIN (	SELECT DISTINCT f.Account
				FROM Foreclosures.dbo.Records r
					JOIN Foreclosures.dbo.Foreclosures f
						ON	f.ForeclosureId = r.FcId
				WHERE	r.StageName IN ('Sale Scheduled for Date', 'Judgment Entered Date')
					AND r.CompletedDate IS NOT NULL
			  ) f
		ON	f.Account = t.Account
	LEFT JOIN (	SELECT DISTINCT f.Account
				FROM Foreclosures.dbo.Records r
					JOIN Foreclosures.dbo.Foreclosures f
						ON	f.ForeclosureId = r.FcId
				WHERE r.StageName = 'First Legal Filed'
			  ) flf
		ON	flf.Account = t.Account
		AND t.[State] IN ('MD','CT')




------------------------------------------------------------------------------------

IF OBJECT_ID('tempdb..#Temp3') IS NOT NULL
	DROP TABLE #Temp3;

SELECT	t.*
	,	CASE WHEN cfpb.LoanId IS NOT NULL 
				THEN 'Y'
			 ELSE 'N'
		END AS EliminatedByCurrentInLM
	,	CASE WHEN l.LoanRecID IS NOT NULL 
				THEN 'Y'
			 ELSE 'N'
		END AS EliminatedByLTR050Sentin6Months
	,	CASE WHEN lm.LoanId IS NOT NULL 
				THEN 'Y'
			 ELSE 'N'
		END AS EliminatedByLMInLastYear
INTO #Temp3	
FROM	#Temp2 t
	LEFT JOIN MARS.dbo.CFPBShortedUserEntry cfpb --currently in Loss Mit
		ON	cfpb.LoanId = t.LoanID
		AND cfpb.LMFilter = 'LM'
		AND cfpb.ProcessCompleted = 0
	LEFT JOIN
			(	SELECT	DISTINCT vlc.LoanRecID
				FROM	mars.dbo.vw_LoansContacts vlc WITH(NOLOCK) 
				WHERE	vlc.Category IN ('LT - Letter Sent') 
					AND vlc.Contact = 'LTR050'
					AND	DATEDIFF(D,vlc.MyDateTime,GETDATE()) < 180 
			) l
		ON	t.RecID = l.LoanRecID
	LEFT JOIN  
			(	SELECT DISTINCT lmh.LoanId
				FROM	MARS.dbo.CFPBShortedUserEntry lmh
				WHERE	lmh.LMFilter = 'LM'
					AND lmh.ProcessCompleted = 1
					AND lmh.ProcessCompletedDate >= GETDATE()-365
			) lm
		ON	lm.LoanId = t.LoanID	


SELECT	t.Account
	,	t.dayspastdue
	,	t.LoanStatus
FROM #Temp3 t
WHERE	t.EliminatedByCommercial = 'N'
	AND	t.EliminatedByBKActive = 'N'
	AND	t.EliminatedByLoanStatus = 'N'
	AND	t.EliminatedByBKDischarge = 'N'
	AND	t.EliminatedByCandD = 'N'
	AND	t.EliminatedByDaysPastDue = 'N'
	AND t.EliminatedByForeclosure = 'N'
	AND t.EliminatedByContestedFC = 'N'
	AND t.EliminatedByMDCTFirstLegalFiles = 'N'
	AND t.EliminatedByCurrentInLM = 'N'
	AND t.EliminatedByLTR050Sentin6Months = 'N'
	AND t.EliminatedByLMInLastYear = 'N'

END