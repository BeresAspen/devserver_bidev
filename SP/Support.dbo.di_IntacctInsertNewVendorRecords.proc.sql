USE [Support]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[di_IntacctInsertNewVendorRecords]
AS
BEGIN

/* ********************************************************************************************************* *
   Procedure Name  :	di_IntacctInsertNewVendorRecords
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	09/20/2017

   *********************************************************************************************************
   **         Change History                                                                              **
   *********************************************************************************************************

   Date			Author				Description
   --------		-----------			------------
   
*  ********************************************************************************************************* */

--In case Dynamics Vendor ID was not included in the initial load
--UPDATE siv
--SET DynamicsVendorID = vr.DynamicsVendorID
----select *
--FROM Support.dbo.StagingIntacctVendors siv
--JOIN MARS.dbo.VendorsList vl
--	ON	vl.VendorDescription = REVERSE(left(REVERSE(siv.VendorName),CHARINDEX('-',REVERSE(siv.VendorName))-1))
--JOIN (	SELECT DISTINCT 
--				VendorID
--			,	ReferenceID AS DynamicsVendorID
--		FROM MARS.dbo.VendorReference
--	 ) vr
--	ON	vr.VendorID = vl.VendorId
--WHERE siv.DynamicsVendorID IS NULL
--	AND CHARINDEX('-',REVERSE(siv.VendorName)) <> 0


--Non-Borrowers - Insert Records into VendorList from Intacct to get the MARSVendorID created
ALTER TABLE MARS.dbo.VendorsList Disable trigger tr_Vendor_VendorsList

INSERT INTO MARS.dbo.VendorsList
				(VendorName, VendorDescription, TIN, Is1099, MultiCheck, VendorTypeId, IsActive, IsAuthorized, GlobalVendorID)
SELECT	CASE WHEN ISNUMERIC(SUBSTRING(siv.VendorName,charindex('-',siv.VendorName)+1,11)) = 1
				THEN SUBSTRING(siv.VendorName,1,charindex('-',siv.VendorName)-1)
			 ELSE siv.VendorName
		END AS VendorName
	,	CASE WHEN ISNUMERIC(SUBSTRING(siv.VendorName,charindex('-',siv.VendorName)+1,11)) = 1
				THEN SUBSTRING(siv.VendorName,charindex('-',siv.VendorName)+1,11)
			 ELSE ''
		END AS VendorDiscription
	,	siv.TIN
	,	CASE WHEN siv.TIN IS NULL THEN 0 ELSE 1 END AS Is1099
	,	0 AS MultiCheck
	,	vt.VendorTypeId AS VendorType			------there are extra in Intacct NEED TO ADD TO VendorType table
	,	CASE WHEN siv.VendorStatus = 'active' THEN 1 ELSE 0 END AS IsActive
	,	CASE WHEN siv.VendorStatus = 'active' THEN 1 ELSE 0 END AS IsAuthorized		----check this
	,	siv.IntacctVendorID AS GlobalVendorID
	--select *
FROM Support.dbo.StagingIntacctVendors siv
LEFT JOIN MARS.dbo.VendorTypes vt
	ON	vt.VendorType = CASE WHEN siv.VendorType = 'INVESTOR' THEN 'INVESTOR-RESTRICTED' ELSE siv.VendorType END
LEFT JOIN IntacctDM.dbo.Vendors v
	ON	v.IntacctVendorID = siv.IntacctVendorID
WHERE	v.IntacctVendorID IS NULL
	AND	siv.VendorType <> 'BORROWER'
	--AND siv.DynamicsVendorID IS NULL


--Non-Borrower - Insert Address From Intacct after MARS VendorID is created

INSERT INTO MARS.dbo.VendorRemittanceAddresses
			(ContactFirstName, Address1, Address2, City, State, Zip, Physical, Mailing, IsActive, VendorID)
SELECT  siv.VendorName AS ContactFirstName
	,	siv.VendorAddress1
	,	siv.VendorAddress2
	,	siv.VendorCity
	,	siv.VendorState
	,	siv.VendorZip
	,	0 AS Physical
	,	1 AS Mailing
	,	1 AS IsActive	
	,	vl.VendorId
FROM Support.dbo.StagingIntacctVendors siv
JOIN MARS.dbo.VendorsList vl
	ON	vl.GlobalVendorID = siv.IntacctVendorID
LEFT JOIN IntacctDM.dbo.Vendors v
	ON	v.IntacctVendorID = siv.IntacctVendorID
WHERE v.IntacctVendorID IS NULL
	AND	siv.VendorType <> 'BORROWER'
	--AND siv.DynamicsVendorID IS NULL


--Update Intacct Vendors table with changes
UPDATE v 
SET		VendorName = siv.VendorName
	,	VendorAddress1 = siv.VendorAddress1
	,	VendorAddress2 = siv.VendorAddress2
	,	VendorCity = siv.VendorCity
	,	VendorState = siv.VendorState
	,	VendorZip = siv.VendorZip
	,	VendorStatus = siv.VendorStatus
FROM IntacctDM.dbo.Vendors v
JOIN Support.dbo.StagingIntacctVendors siv
	ON	v.IntacctVendorID = siv.IntacctVendorID
WHERE	(	v.VendorName <> siv.VendorName
		OR	v.VendorAddress1 <> siv.VendorAddress1
		OR	v.VendorAddress2 <> siv.VendorAddress2
		OR	v.VendorCity <> siv.VendorCity
		OR	v.VendorState <> siv.VendorState
		OR	v.VendorZip <> siv.VendorZip
		OR	v.VendorStatus <> siv.VendorStatus
		)
	AND	siv.VendorType <> 'BORROWER'


--Change the status of the MARS vendor when the Intacct Vendor Status Changes
UPDATE vl 
SET IsActive = CASE WHEN siv.VendorStatus = 'active' THEN 1 ELSE 0 END
FROM MARS.dbo.VendorsList vl
JOIN IntacctDM.dbo.Vendors v
	ON	v.MARSVendorID = vl.VendorId
JOIN Support.dbo.StagingIntacctVendors siv
	ON	v.IntacctVendorID = siv.IntacctVendorID
WHERE	v.VendorStatus <> siv.VendorStatus
	AND	siv.VendorType <> 'BORROWER'


--Non-Borrower - Insert records into IntacctDM Vendors table when it is a new vendor.
INSERT INTO IntacctDM.dbo.Vendors
				(IntacctVendorID, VendorName, VendorType, VendorAddress1, VendorAddress2, VendorCity, VendorState, VendorZip, VendorCountry, TIN
				, DynamicsVendorID, MARSVendorID, DefaultPaymentType, VendorStatus, InsertedBy, InsertedDate)
SELECT	siv.IntacctVendorID
	,	siv.VendorName
	,	siv.VendorType
	,	siv.VendorAddress1
	,	siv.VendorAddress2
	,	siv.VendorCity
	,	siv.VendorState
	,	siv.VendorZip
	,	siv.VendorCountry
	,	siv.TIN
	,	siv.DynamicsVendorID
	,	vl.VendorID AS MARSVendorID
	,	siv.DefaultPaymentType
	,	siv.VendorStatus
	,	'Intacct system' AS InsertedBy
	,	GETDATE() AS InsertedDate
FROM Support.dbo.StagingIntacctVendors siv
JOIN MARS.dbo.VendorsList vl
	ON	(	vl.GlobalVendorID = siv.IntacctVendorID
		OR	vl.VendorDescription =	CASE WHEN CHARINDEX('-',REVERSE(siv.VendorName)) > 0 
											THEN REVERSE(left(REVERSE(siv.VendorName),CHARINDEX('-',REVERSE(siv.VendorName))-1))
									END
		)
LEFT JOIN IntacctDM.dbo.Vendors v
	ON	v.IntacctVendorID = siv.IntacctVendorID
WHERE	v.IntacctVendorID IS NULL
	AND	siv.VendorType <> 'BORROWER'

--Borrower - Insert records into IntacctDM Vendors table when it is a new vendor.
INSERT INTO IntacctDM.dbo.Vendors
				(IntacctVendorID, VendorName, VendorType, VendorAddress1, VendorAddress2, VendorCity, VendorState, VendorZip, VendorCountry, TIN
				, DynamicsVendorID, MARSVendorID, DefaultPaymentType, VendorStatus, InsertedBy, InsertedDate)
SELECT	siv.IntacctVendorID
	,	siv.VendorName
	,	siv.VendorType
	,	siv.VendorAddress1
	,	siv.VendorAddress2
	,	siv.VendorCity
	,	siv.VendorState
	,	siv.VendorZip
	,	siv.VendorCountry
	,	siv.TIN
	,	siv.DynamicsVendorID
	,	vl.VendorID AS MARSVendorID
	,	siv.DefaultPaymentType
	,	siv.VendorStatus
	,	'Intacct system' AS InsertedBy
	,	GETDATE() AS InsertedDate
FROM Support.dbo.StagingIntacctVendors siv
JOIN MARS.dbo.VendorsList vl
	ON	REPLACE(vl.TIN, '-', '') = REPLACE(siv.TIN, '-', '')
LEFT JOIN IntacctDM.dbo.Vendors v
	ON	v.IntacctVendorID = siv.IntacctVendorID
WHERE	v.IntacctVendorID IS NULL
	AND	siv.VendorType = 'BORROWER'

END