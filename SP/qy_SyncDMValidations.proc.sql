USE [IntacctDM]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('qy_SyncDMValidations') IS NOT NULL
	DROP PROCEDURE dbo.qy_SyncDMValidations;
GO


CREATE PROCEDURE dbo.qy_SyncDMValidations
AS
BEGIN

/********************************************************************************************************** *
   Procedure Name  :	qy_SyncDMValidations
   Business Analyis:	
   Project/Process :   
   Description     :	Calculate the sum of the balances for previous days and if they are not 0, mark 
							the ValidationPass field in the SyncParameters table as FALSE
   Author          :	Benjacob Beres
   Create Date     :	08/16/2018

   *********************************************************************************************************
   **         Change History                                                                              **
   *********************************************************************************************************

   Date			Author				Description
   --------		-----------			------------
   
************************************************************************************************************ */

	DECLARE @EndDate DATE

	SELECT @EndDate = CAST(rp.END_DATE AS DATE)
	FROM IntacctDM.dbo.SyncParameters sp
	JOIN IntacctDM.dbo.ReportingPeriod rp
		ON	rp.NAME = sp.EarliestPeriod


	SELECT DISTINCT	NAME
	INTO #tmpPERIODS
	FROM	IntacctDM.dbo.ReportingPeriod
	WHERE	CAST(END_DATE AS DATE) < GETDATE()-60
		AND CAST(END_DATE AS DATE) >= @EndDate
		AND NAME LIKE '%Month%'
		--AND NAME NOT IN ('Month Ended July 2018')
	ORDER BY NAME

	SELECT PERIOD, SUM(ENDBAL) AS TotalBal
	INTO #PeriodBalance
	FROM IntacctDM.dbo.GLAccountBalance ab
	JOIN #tmpPERIODS tp
		ON	tp.NAME = ab.PERIOD
	GROUP BY PERIOD

	IF (SELECT SUM(TotalBal) FROM #PeriodBalance) <> 0 
	BEGIN 
		UPDATE sp 
		SET ValidationPass = 1
		FROM dbo.SyncParameters
	END;

END
GO
