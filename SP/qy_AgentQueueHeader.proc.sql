USE [MARS]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.qy_AgentQueueHeader') IS NOT NULL
	DROP PROCEDURE dbo.qy_AgentQueueHeader;
GO

CREATE PROCEDURE dbo.qy_AgentQueueHeader
			(	@ResultSet INT
			,	@AssignedAgent VARCHAR(128)
			)
AS

/* ********************************************************************************************************* *
   Procedure Name  :	qy_AgentQueueHeader
   Run Example	   :	exec qy_AgentQueueHeader 3, 'Carlos.Galeano'
   Business Analyis:	
   Project/Process :   
   Description     :	All the header counts call this stored proc. This is the hard coded sql in the rdl combined.
   Author          :	Benjacob Beres
   Create Date     :	02/28/2017

   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date			Author				Description
   --------		-----------			------------
*  ********************************************************************************************************* */


/************************************************************************
--Total ACH Today 

--For Dataset OneTimeACHToday
*************************************************************************/
IF @ResultSet = 1 
	BEGIN

		SELECT	SUM([Billing Amount]) AS BilledToday
		FROM	dbo.vw_ACHBillingsEnteredByLoanByRep WITH(NOLOCK)
		WHERE	CAST(inserteddate AS DATE) = CAST(GETDATE() AS DATE) 
			AND [Reoccuring Schedule/One Time] = 'One Time' 
			AND InsertedBy = @AssignedAgent

	END



/************************************************************************
--Total ACH MTD

--For Dataset OneTimeACHMTD
*************************************************************************/
IF @ResultSet = 2
	BEGIN

		SELECT	SUM([Billing Amount]) AS BilledMTD
		FROM	dbo.vw_ACHBillingsEnteredByLoanByRep WITH(NOLOCK)
		WHERE	CAST(inserteddate AS DATE) BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) 
												AND  DATEADD(DAY, -(DAY(DATEADD(MONTH, 1, GETDATE()))),DATEADD(MONTH, 1, GETDATE()))
			AND [Reoccuring Schedule/One Time] = 'One Time' 
			AND InsertedBy = @AssignedAgent

	END


/************************************************************************
--Total Outbound Contacts Today SUM(TotalOutBoundContactsToday)
--Total Attempts Today SUM(TotalAttemptsToday)
--Total Outbound Calls Today SUM(TotalOutboundCallsToday)
--Total InBound Calls Today SUM(TotalInBoundContactsToday)

--For DatasetS TotalCallsToday
*************************************************************************/
IF @ResultSet = 3
	BEGIN

		SELECT	SUM(CASE WHEN Category IN (	'AA - Outbound Attempt/no Contact with Attorney',
											'NO - Outbound Attempt/no Contact with Borrower',
											'TA - Outbound Attempt/no Contact with Authorized NO3',
											'AO - Attorney Contact on Outbound Call'
										   ) 
								THEN 1 
						 ELSE 0 
					END
					) AS TotalAttemptsToday
			,	SUM(CASE WHEN Category IN (	'TO - Authorized 3rd Party on Outbound Call',
											'BO - Borrower Contact on Outbound Call'
										  ) 
							THEN 1 
						 ELSE 0 
					END
					) AS TotalOutBoundContactsToday
			,	SUM(CASE WHEN Category IN (	'AI - Attorney Contact on Inbound Call',
											'BI - Borrower Contact on Inbound Call',
											'TC - Authorized 3rd Party on Inbound',
											'NA - Non-Authorized 3rd Party on Inbound Call'
										  ) 
							THEN 1 
						 ELSE 0 
					END
					) AS TotalInBoundContactsToday
			,	SUM(CASE WHEN Category IN (	'AA - Outbound Attempt/no Contact with Attorney',
											'NO - Outbound Attempt/no Contact with Borrower',
											'TA - Outbound Attempt/no Contact with Authorized NO3',
											'AO - Attorney Contact on Outbound Call',
											'TO - Authorized 3rd Party on Outbound Call',
											'BO - Borrower Contact on Outbound Call'
										  ) 
							THEN 1 
						 ELSE 0 
					END
					) AS TotalOutboundCallsToday
		FROM	MARS.dbo.vw_LoansContacts 
		WHERE	CAST(mydatetime AS DATE) = CAST(GETDATE() AS DATE) 
			AND rep = @AssignedAgent

	END


/************************************************************************
--Total OutboundContacts MTD SUM(TotalOutBoundContactsMTD)
--Total Attempts MTD SUM(TotalAttemptsMTD)
--Total Outbound Calls MTD SUM(TotalOutboundCallsMTD)
--Total Inbound Calls MTD SUM(TotalInBoundContactsMTD)

--For Dataset TotalCallsMTD
*************************************************************************/
IF @ResultSet = 4
	BEGIN

		SELECT 
				SUM(	CASE WHEN Category IN (	'AA - Outbound Attempt/no Contact with Attorney',
												'NO - Outbound Attempt/no Contact with Borrower',
												'TA - Outbound Attempt/no Contact with Authorized NO3',
												'AO - Attorney Contact on Outbound Call'
											  ) THEN 1 
							 ELSE 0 
						END
					) AS TotalAttemptsMTD,
				SUM(	CASE WHEN Category IN (	'TO - Authorized 3rd Party on Outbound Call',
												'BO - Borrower Contact on Outbound Call'
											  ) THEN 1 
							 ELSE 0 
						END
					) AS TotalOutBoundContactsMTD,
				SUM(	CASE WHEN Category IN (	'AI - Attorney Contact on Inbound Call',
												'BI - Borrower Contact on Inbound Call',
												'TC - Authorized 3rd Party on Inbound',
												'NA - Non-Authorized 3rd Party on Inbound Call'
											  ) THEN 1 
							 ELSE 0 
						END
					) AS TotalInBoundContactsMTD,
				SUM(	CASE WHEN Category IN ( 'AA - Outbound Attempt/no Contact with Attorney',
												'NO - Outbound Attempt/no Contact with Borrower',
												'TA - Outbound Attempt/no Contact with Authorized NO3',
												'AO - Attorney Contact on Outbound Call',
												'TO - Authorized 3rd Party on Outbound Call',
												'BO - Borrower Contact on Outbound Call'
											   ) THEN 1 
							 ELSE 0 
						END
					) AS TotalOutboundCallsMTD
		FROM	MARS.dbo.vw_LoansContacts 
		WHERE	CAST(MyDateTime AS DATE) BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) 
											AND  DATEADD(DAY, -(DAY(DATEADD(MONTH, 1, GETDATE()))),	DATEADD(MONTH, 1, GETDATE())) 
			AND rep = @AssignedAgent

	END
