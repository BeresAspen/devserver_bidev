USE [IntacctDM]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('di_SyncDMTablesFromIntacctNonStandard') IS NOT NULL
	DROP PROCEDURE di_SyncDMTablesFromIntacctNonStandard;
GO



CREATE PROCEDURE dbo.di_SyncDMTablesFromIntacctNonStandard (@IntacctObject VARCHAR(50))
AS
BEGIN

/********************************************************************************************************** *
   Procedure Name  :	di_SyncDMTablesFromIntacctNonStandard
   Business Analyis:	
   Project/Process :   
   Description     :	Built off of di_SyncDMTablesFromIntacct. There are Intacct Objects that require more 
						specific key fields than the standard RECORDNO.
   Author          :	Benjacob Beres
   Create Date     :	06/12/2018

   *********************************************************************************************************
   **         Change History                                                                              **
   *********************************************************************************************************

   Date			Author				Description
   --------		-----------			------------
   
************************************************************************************************************ */

	--When Records exists already, check if the modified date is different. If so, move Primary Record to History and insert Staging into Primary
	--Archive records that exist in DM but not in Intacct
	--List of Existing Records that have been modified
	--Move existing records to History table
	--Delete the records from the Primary table
	--Insert records that do not exist in Primary (this includes new records and the replacements for the records that were just deleted)

DECLARE	@MinDate DATETIME
	,	@MaxDate DATETIME
	,	@ValidObject BIT
	,	@GLAcctGrpHierarchyTrunc BIT
	,	@User     varchar(128) = NULL
	,	@Application varchar(128) = 'di_SyncDMTablesFromIntacctNonStandard'
	,	@Version     varchar(10) = '1.00.00'
	
SET @ValidObject = 0
SET @GLAcctGrpHierarchyTrunc = 0

IF @User IS NULL 
  SET @User = SUSER_NAME ();


BEGIN TRY
	BEGIN TRANSACTION

		IF @IntacctObject = 'GLACCOUNTBALANCE'
		BEGIN  

			IF OBJECT_ID('tempdb..#tempGLAccountBalance') IS NOT NULL      
			DROP TABLE #tempGLAccountBalance;     
			IF OBJECT_ID('tempdb..#DeletedInIntacctGLAccountBalance') IS NOT NULL      
			DROP TABLE #DeletedInIntacctGLAccountBalance;       


			SELECT	@MinDate = MIN(CAST(sab.WHENMODIFIED AS DATETIME))
				,	@MaxDate = MAX(CAST(sab.WHENMODIFIED AS DATETIME))
			FROM Staging.dbo.IntacctStgGLAccountBalance sab

       
			SELECT	iab.BOOKID
				,	iab.PERIOD
				,	iab.ACCOUNTREC
				,	iab.DEPARTMENTDIMKEY
				,	iab.LOCATIONDIMKEY
				,	iab.CUSTOMERDIMKEY
				,	iab.VENDORDIMKEY
				,	iab.CLASSDIMKEY
				,	iab.ITEMDIMKEY
				,	iab.GLDIMACTIVITY
				,	iab.GLDIMSTATUS
				,	iab.WHENCREATED
			INTO #DeletedInIntacctGLAccountBalance     
			FROM IntacctDM.dbo.GLAccountBalance iab      
			JOIN (	SELECT DISTINCT BOOKID
						,	PERIOD
					FROM Staging.dbo.IntacctStgGLAccountBalance
				  ) sabs
				ON	sabs.BOOKID = iab.BOOKID
				AND	sabs.PERIOD = iab.PERIOD
			LEFT JOIN Staging.dbo.IntacctStgGLAccountBalance sab      
				ON	iab.BOOKID = sab.BOOKID
				AND iab.PERIOD = sab.PERIOD
				AND iab.WHENCREATED = sab.WHENCREATED
				AND iab.ACCOUNTREC = sab.ACCOUNTREC
				AND iab.DEPARTMENTDIMKEY = sab.DEPARTMENTDIMKEY
				AND iab.LOCATIONDIMKEY = sab.LOCATIONDIMKEY
				AND iab.CUSTOMERDIMKEY = sab.CUSTOMERDIMKEY
				AND iab.VENDORDIMKEY = sab.VENDORDIMKEY
				AND iab.CLASSDIMKEY = sab.CLASSDIMKEY
				AND ISNULL(iab.ITEMDIMKEY,0) = ISNULL(sab.ITEMDIMKEY,0)
				AND iab.GLDIMACTIVITY = sab.GLDIMACTIVITY
				AND iab.GLDIMSTATUS = sab.GLDIMSTATUS
			WHERE	sab.PERIOD IS NULL      
				AND CAST(iab.WHENMODIFIED AS DATETIME) BETWEEN @MinDate AND @MaxDate


			INSERT INTO IntacctDM.dbo.ObsoleteGLAccountBalance      
			SELECT	iab.*      
				,	GETDATE() AS DMObsoletedDate     
			FROM IntacctDM.dbo.GLAccountBalance iab      
			JOIN #DeletedInIntacctGLAccountBalance dii      
				ON	iab.BOOKID = dii.BOOKID
				AND iab.PERIOD = dii.PERIOD
				AND iab.WHENCREATED = dii.WHENCREATED
				AND iab.ACCOUNTREC = dii.ACCOUNTREC
				AND iab.DEPARTMENTDIMKEY = dii.DEPARTMENTDIMKEY
				AND iab.LOCATIONDIMKEY = dii.LOCATIONDIMKEY
				AND iab.CUSTOMERDIMKEY = dii.CUSTOMERDIMKEY
				AND iab.VENDORDIMKEY = dii.VENDORDIMKEY
				AND iab.CLASSDIMKEY = dii.CLASSDIMKEY
				AND ISNULL(iab.ITEMDIMKEY,0) = ISNULL(dii.ITEMDIMKEY,0)
				AND iab.GLDIMACTIVITY = dii.GLDIMACTIVITY
				AND iab.GLDIMSTATUS = dii.GLDIMSTATUS


			DELETE      
			FROM IntacctDM.dbo.GLAccountBalance      
			WHERE EXISTS (	SELECT 2           
							FROM #DeletedInIntacctGLAccountBalance dii    
							WHERE	GLAccountBalance.BOOKID = dii.BOOKID
								AND GLAccountBalance.PERIOD = dii.PERIOD
								AND GLAccountBalance.WHENCREATED = dii.WHENCREATED
								AND GLAccountBalance.ACCOUNTREC = dii.ACCOUNTREC
								AND GLAccountBalance.DEPARTMENTDIMKEY = dii.DEPARTMENTDIMKEY
								AND GLAccountBalance.LOCATIONDIMKEY = dii.LOCATIONDIMKEY
								AND GLAccountBalance.CUSTOMERDIMKEY = dii.CUSTOMERDIMKEY
								AND GLAccountBalance.VENDORDIMKEY = dii.VENDORDIMKEY
								AND GLAccountBalance.CLASSDIMKEY = dii.CLASSDIMKEY
								AND ISNULL(GLAccountBalance.ITEMDIMKEY,0) = ISNULL(dii.ITEMDIMKEY,0)
								AND GLAccountBalance.GLDIMACTIVITY = dii.GLDIMACTIVITY
								AND GLAccountBalance.GLDIMSTATUS = dii.GLDIMSTATUS)       


			SELECT	iab.*     
			INTO #tempGLAccountBalance     
			FROM Staging.dbo.IntacctStgGLAccountBalance sab     
			JOIN IntacctDM.dbo.GLAccountBalance iab   
				ON	iab.BOOKID = sab.BOOKID
				AND iab.PERIOD = sab.PERIOD
				AND iab.WHENCREATED = sab.WHENCREATED
				AND iab.ACCOUNTREC = sab.ACCOUNTREC
				AND iab.DEPARTMENTDIMKEY = sab.DEPARTMENTDIMKEY
				AND iab.LOCATIONDIMKEY = sab.LOCATIONDIMKEY
				AND iab.CUSTOMERDIMKEY = sab.CUSTOMERDIMKEY
				AND iab.VENDORDIMKEY = sab.VENDORDIMKEY
				AND iab.CLASSDIMKEY = sab.CLASSDIMKEY
				AND ISNULL(iab.ITEMDIMKEY,0) = ISNULL(sab.ITEMDIMKEY,0)
				AND iab.GLDIMACTIVITY = sab.GLDIMACTIVITY
				AND iab.GLDIMSTATUS = sab.GLDIMSTATUS
			WHERE	CAST(iab.WHENMODIFIED AS DATETIME) < CAST(sab.WHENMODIFIED AS DATETIME)         


			INSERT INTO IntacctDM.dbo.hstGLAccountBalance     
			SELECT	*      
				,	GETDATE() AS InsertedDate      
				,	SYSTEM_USER AS InsertedBy      
			FROM #tempGLAccountBalance      


			DELETE      
			FROM IntacctDM.dbo.GLAccountBalance     
			WHERE EXISTS (	SELECT 1         
							FROM #tempGLAccountBalance t          
							WHERE	GLAccountBalance.BOOKID = t.BOOKID
								AND GLAccountBalance.PERIOD = t.PERIOD
								AND GLAccountBalance.WHENCREATED = t.WHENCREATED
								AND GLAccountBalance.ACCOUNTREC = t.ACCOUNTREC
								AND GLAccountBalance.DEPARTMENTDIMKEY = t.DEPARTMENTDIMKEY
								AND GLAccountBalance.LOCATIONDIMKEY = t.LOCATIONDIMKEY
								AND GLAccountBalance.CUSTOMERDIMKEY = t.CUSTOMERDIMKEY
								AND GLAccountBalance.VENDORDIMKEY = t.VENDORDIMKEY
								AND GLAccountBalance.CLASSDIMKEY = t.CLASSDIMKEY
								AND ISNULL(GLAccountBalance.ITEMDIMKEY,0) = ISNULL(t.ITEMDIMKEY,0)
								AND GLAccountBalance.GLDIMACTIVITY = t.GLDIMACTIVITY
								AND GLAccountBalance.GLDIMSTATUS = t.GLDIMSTATUS)                        


			DELETE      
			FROM Staging.dbo.IntacctStgGLAccountBalance     
			WHERE EXISTS (	SELECT 1         
							FROM IntacctDM.dbo.GLAccountBalance t          
							WHERE	IntacctStgGLAccountBalance.BOOKID = t.BOOKID
								AND IntacctStgGLAccountBalance.PERIOD = t.PERIOD
								AND IntacctStgGLAccountBalance.WHENCREATED = t.WHENCREATED
								AND IntacctStgGLAccountBalance.ACCOUNTREC = t.ACCOUNTREC
								AND IntacctStgGLAccountBalance.DEPARTMENTDIMKEY = t.DEPARTMENTDIMKEY
								AND IntacctStgGLAccountBalance.LOCATIONDIMKEY = t.LOCATIONDIMKEY
								AND IntacctStgGLAccountBalance.CUSTOMERDIMKEY = t.CUSTOMERDIMKEY
								AND IntacctStgGLAccountBalance.VENDORDIMKEY = t.VENDORDIMKEY
								AND IntacctStgGLAccountBalance.CLASSDIMKEY = t.CLASSDIMKEY
								AND ISNULL(IntacctStgGLAccountBalance.ITEMDIMKEY,0) = ISNULL(t.ITEMDIMKEY,0)
								AND IntacctStgGLAccountBalance.GLDIMACTIVITY = t.GLDIMACTIVITY
								AND IntacctStgGLAccountBalance.GLDIMSTATUS = t.GLDIMSTATUS) 


			INSERT INTO IntacctDM.dbo.GLAccountBalance     
			SELECT	sab.*     
			FROM Staging.dbo.IntacctStgGLAccountBalance sab     
			LEFT JOIN IntacctDM.dbo.GLAccountBalance iab
				ON	iab.BOOKID = sab.BOOKID
				AND iab.PERIOD = sab.PERIOD
				AND iab.WHENCREATED = sab.WHENCREATED
				AND iab.ACCOUNTREC = sab.ACCOUNTREC
				AND iab.DEPARTMENTDIMKEY = sab.DEPARTMENTDIMKEY
				AND iab.LOCATIONDIMKEY = sab.LOCATIONDIMKEY
				AND iab.CUSTOMERDIMKEY = sab.CUSTOMERDIMKEY
				AND iab.VENDORDIMKEY = sab.VENDORDIMKEY
				AND iab.CLASSDIMKEY = sab.CLASSDIMKEY
				AND ISNULL(iab.ITEMDIMKEY,0) = ISNULL(sab.ITEMDIMKEY,0)
				AND iab.GLDIMACTIVITY = sab.GLDIMACTIVITY
				AND iab.GLDIMSTATUS = sab.GLDIMSTATUS
			WHERE	iab.ACCOUNTREC IS NULL

			SET @ValidObject = 1	
			
			COMMIT

		END

		ELSE 
		IF @IntacctObject = 'GLACCTGRPHIERARCHY'
		BEGIN  

			TRUNCATE TABLE IntacctDM.dbo.hstGLAcctGrpHierarchy

			INSERT INTO IntacctDM.dbo.hstGLAcctGrpHierarchy
			SELECT *
			FROM IntacctDM.dbo.GLAcctGrpHierarchy

			
			TRUNCATE TABLE IntacctDM.dbo.GLAcctGrpHierarchy
			SET @GLAcctGrpHierarchyTrunc = 1
			
			INSERT INTO IntacctDM.dbo.GLAcctGrpHierarchy
			SELECT *
			FROM Staging.dbo.IntacctStgGLAcctGrpHierarchy

			
			SET @ValidObject = 1	
			
			COMMIT

		END

		IF @ValidObject = 0 
		BEGIN

			DECLARE @ErrorMsg VARCHAR(200)
			SET @ErrorMsg = @IntacctObject + ' is not a valid object for this stored procedure.'

			RAISERROR(@ErrorMsg, 16, 1)

		END

END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK

		IF @IntacctObject = 'GLACCTGRPHIERARCHY' AND @GLAcctGrpHierarchyTrunc = 1
			BEGIN 
				INSERT INTO IntacctDM.dbo.GLAcctGrpHierarchy
				SELECT [RECORDNO],[GLACCTGRPKEY],[GLACCTGRPNAME],[GLACCTGRPTITLE],[GLACCTGRPNORMALBALANCE],[GLACCTGRPMEMBERTYPE],[GLACCTGRPHOWCREATED],[GLACCTGRPLOCATIONKEY],[ACCOUNTKEY],[ACCOUNTNO],[ACCOUNTTITLE],[ACCOUNTNORMALBALANCE],[ACCOUNTTYPE],[ACCOUNTLOCATIONKEY],[DMImportDate]
				FROM IntacctDM.dbo.hstGLAcctGrpHierarchy
			END
			
		
      DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
      SELECT @ErrMsg = ERROR_MESSAGE(),
             @ErrSeverity = ERROR_SEVERITY()

      RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH

END