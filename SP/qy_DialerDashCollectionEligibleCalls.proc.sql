USE MARS_DW;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.qy_DialerDashCollectionEligibleCalls') IS NOT NULL
	DROP PROCEDURE dbo.qy_DialerDashCollectionEligibleCalls;
GO

/* ********************************************************************************************************* *
   Procedure Name  :	qy_DialerDashCollectionEligibleCalls
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	02/07/2017

   *********************************************************************************************************
   **         Change History                                                                              **
   *********************************************************************************************************

   Date       Author        Description
   --------   -----------   ------------
   
*  ********************************************************************************************************* */
CREATE PROCEDURE dbo.qy_DialerDashCollectionEligibleCalls 

AS
BEGIN

	DECLARE @ProcessFromDate DATE = NULL
	SET @ProcessFromDate = ISNULL(@ProcessFromDate, CAST(GETDATE()-36 AS DATE))

	CREATE TABLE #ResultswithCarryover
		(	Account	VARCHAR(30)
		,	ArchiveDate DATE
		,	CallType VARCHAR(30)
		,	Carryover INT
		)

	INSERT INTO #ResultswithCarryover
	EXEC qy_DialerDashDistinctAccountContact 36


	SELECT	r.ArchiveDate
		,	COUNT(r.Account) AS Total
		,	SUM(r.Carryover) AS CarryoverCount
		,	SUM(CASE WHEN r.CallType = 'InboundContact' THEN 1 ELSE 0 END) AS InboundContact
		,	SUM(CASE WHEN r.CallType = 'OutboundContact' THEN 1 ELSE 0 END) AS OutboundContact
		,	SUM(CASE WHEN r.CallType = 'OutboundAttempt' THEN 1 ELSE 0 END) AS OutboundAttempt
		,	SUM(CASE WHEN r.CallType = 'OutboundAttemptNotOnDialer' THEN 1 ELSE 0 END) AS OutboundAttemptNotOnDialer
		,	SUM(CASE WHEN r.CallType = 'OutboundContactNotOnDialer' THEN 1 ELSE 0 END) AS OutboundContactNotOnDialer
		,	SUM(CASE WHEN r.Carryover = 1 AND r.CallType IN ('Untouched Loans', 'OutboundAttempt', 'OutboundAttemptNotOnDialer') THEN 1 ELSE 0 END) AS UntouchedCarryoverLoans
		,	SUM(CASE WHEN r.CallType = 'Untouched Loans' THEN 1 ELSE 0 END) AS UntouchedLoans
	INTO #TotalCounts
	FROM	#ResultswithCarryover r
	GROUP BY r.ArchiveDate
	ORDER BY r.ArchiveDate


		SELECT	t.ArchiveDate AS CallDate
			,	t.Total AS TotalDialable
			,	t.CarryoverCount
			,	t.InboundContact
			,	t.OutboundContact
			,	t.OutboundContactNotOnDialer
			,	t.UntouchedCarryoverLoans AS UntouchedCarryoverLoans
			,	t.Total - (t.InboundContact + t.OutboundContact + t.OutboundContactNotOnDialer) - t.UntouchedCarryoverLoans AS UntouchedLoans
			,	CAST((CAST((t.InboundContact + t.OutboundContact + t.OutboundContactNotOnDialer) AS FLOAT)/CAST(t.Total AS FLOAT)) AS DECIMAL(3,2)) AS ContactPenetration
		FROM #TotalCounts t
			INNER JOIN Support.[dbo].[DimDate] dd
				ON	dd.[Date] = t.ArchiveDate
		WHERE	t.ArchiveDate >= DATEADD(DD,1,@ProcessFromDate)
			AND	((dd.IsWeekday = 1 AND dd.IsHoliday = 0)
				OR (t.InboundContact + t.OutboundContact + t.OutboundContactNotOnDialer) >= 20)



END