USE [Support]
GO

IF OBJECT_ID('di_IntacctIntegrationInsurmark') IS NOT NULL
	DROP PROCEDURE dbo.di_IntacctIntegrationInsurmark;
GO

CREATE PROCEDURE dbo.di_IntacctIntegrationInsurmark
AS
/* ********************************************************************************************************* *
   Procedure Name  :	di_IntacctIntegrationInsurmark
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Nancy Tanaka
   Create Date     :	
   
   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date			Author				Description
   --------		-----------			------------
   08/23/2017	Benjacob Beres		Modified the procedure to fit the changes of the past few months.

*  ********************************************************************************************************* */
BEGIN 

	IF OBJECT_ID('tempdb..#Insurmark') IS NOT NULL
		DROP TABLE #Insurmark;

	SELECT	'Purchasing Transaction'  AS [TRANSACTIONTYPE]  --Carol: 1 Purchasing Transactio per Vendor 7/27/2017; testing purpose, remove all except for the first line--
		,	CONVERT(VARCHAR(10),GETDATE(),101) AS [DATE]  --The submitting date--
		,	''AS [GLPOSTINGDATE]
		,	'' AS [DOCUMENTNO]
		,	'10001735' AS [VENDOR_ID] --need VendorID from Intacct Benjacob will follow up server, db, table, logic--
		,	''AS [TERMNAME]      
		,	convert(varchar(10),DATEADD(D,10,GETDATE()),101) AS [DATEDUE]
		,	ISM.InvoiceNumber AS [REFERENCENO]
		,	''AS [MESSAGE]
		,	''AS [SHIPPINGMETHOD]
		,	''AS [PAYTO]
		,	''AS [RETURNTO]
		,	'USD' AS [CURRENCY]
		,	'USD' AS [BASECURR]
		,	''AS [EXCHRATEDATE]
		,	''AS [EXCHRATETYPE]
		,	''AS [EXCHRATE]
		,	'Submitted' AS [STATE]  --Carol 7/28/2017: Submitted for testing purpose
		,	ROW_NUMBER() OVER (PARTITION BY ISM.InvoiceNumber ORDER BY ISM.InvoiceNumber) AS [LINE] --reset back to 1 whenever there is a new Insurmark Invoice Number--
		,	CASE 
				/*1. where asset is a loan and it is active then code to item 532004*/
				WHEN LOA.Account IS NOT NULL AND LOA.isActive = 1 THEN CAST('532004' AS VARCHAR(20))

				/*2. where asset is an REO and it is active
					a. check to see if REO note owner contains US Bank Trust then code to 532005
					b. check to see if REO note owner does not contain US Bank Trust then code to 532006*/
				WHEN REO.PropertyID IS NOT NULL AND REO.Active = 1 THEN CASE 
																			WHEN REO.OWNER LIKE  '%U.S. Bank%Series%' THEN CAST('532005' AS VARCHAR(20)) 
																			WHEN REO.OWNER NOT LIKE  '%U.S. Bank%Series%' THEN CAST('532006' AS VARCHAR(20))
																	END 
																
				/*3. where asset is a loan and
								a. it is not active 
								b. it did not convert to an REO then code to 532010 (just added a few minutes ago)*/							
				WHEN LOA.Account IS NOT NULL AND LOA.isActive = 0 AND 			--Inactive loan; Carol/Roy will follow up; new category--
						REO.PropertyID IS NULL   THEN CAST('532010' AS VARCHAR(20))

				/*4. where asset is a loan and
								a. it is not active
								b. it did convert to an REO then
								c. check to see if REO note owner contains US Bank Trust then code to 532005
								d. check to see if REO note owner does not contain US Bank Trust then code to 532006*/			
				WHEN LOA.Account IS NOT NULL AND LOA.isActive = 0 AND 
						REO.PropertyID IS NOT NULL THEN CASE	WHEN REO.OWNER LIKE  '%U.S. Bank%Series%' THEN CAST('532005' AS VARCHAR(20))			--Active REIT REO--
															WHEN REO.OWNER NOT LIKE  '%U.S. Bank%Series%' THEN CAST('532006' AS VARCHAR(20))			--Active REIT REO--
														END
				/*5. where asset is an REO and 
								a. it is not active
								b. check to see if REO note owner contains US Bank Trust then code to 532005
								c. check to see if REO note owner does not contain US Bank Trust then code to 532006*/				 
				WHEN REO.PropertyID IS NOT NULL AND REO.Active = 0 THEN CASE 
																				WHEN REO.OWNER LIKE  '%U.S. Bank%Series%' THEN CAST('532005' AS VARCHAR(20))			--Active NON-REIT REO--
																				WHEN REO.OWNER NOT LIKE  '%U.S. Bank%Series%' THEN CAST('532006' AS VARCHAR(20))		--Active NON-REIT REO--
																		END
				/*6. where asset is an REO and 
								a. it is not active
								b. and it has an active loan then
								c. where asset is a loan and it is active then code to item 532004*/				 
				WHEN REO.PropertyID IS NOT NULL AND REO.Active = 0 AND 
						LOA.Account IS NOT NULL AND LOA.isActive = 1	THEN  CAST('532004' AS VARCHAR(20))


				--WHEN REO.PropertyID IS NOT NULL AND REO.Active = 0 AND REO.OWNER NOT LIKE  '%U.S. Bank%Series%' THEN CAST('532006' AS VARCHAR(20)) 
				--WHEN REO.PropertyID IS NOT NULL AND LOA.isActive = 1 AND REO.OWNER LIKE  '%U.S. Bank%Series%' THEN CAST('532005' AS VARCHAR(20))  --securitized--
				--WHEN REO.PropertyID IS NOT NULL AND LOA.isActive = 1 AND REO.OWNER NOT LIKE  '%U.S. Bank%Series%' THEN CAST('532006' AS VARCHAR(20)) 

				--WHEN REO.PropertyID IS NOT NULL AND REO.Active = 1 AND REO.OWNER LIKE  '%U.S. Bank%Series%' THEN CAST('532005' AS VARCHAR(20))  --securitized--
				--WHEN REO.PropertyID IS NOT NULL AND REO.Active = 1 AND REO.OWNER NOT LIKE  '%U.S. Bank%Series%' THEN CAST('532006' AS VARCHAR(20)) 
				ELSE 'Further Review' END AS [ITEMID]  --confirmationed & done from Roy--
			
		,	CASE 
				/*1. where asset is a loan and it is active then code to item 532004*/
				WHEN LOA.Account IS NOT NULL AND LOA.isActive = 1 THEN CAST('Insurance - Loan' AS VARCHAR(100))

				/*2. where asset is an REO and it is active
					a. check to see if REO note owner contains US Bank Trust then code to 532005
					b. check to see if REO note owner does not contain US Bank Trust then code to 532006*/
				WHEN REO.PropertyID IS NOT NULL AND REO.Active = 1 THEN CASE 
																			WHEN REO.OWNER LIKE  '%U.S. Bank%Series%' THEN CAST('Insurance - REO - REIT' AS VARCHAR(100)) 
																			WHEN REO.OWNER NOT LIKE  '%U.S. Bank%Series%' THEN CAST('Insurance - REO - Non-REIT' AS VARCHAR(100))
																	END 
																
				/*3. where asset is a loan and
								a. it is not active 
								b. it did not convert to an REO then code to 532010 (just added a few minutes ago)*/							
				WHEN LOA.Account IS NOT NULL AND LOA.isActive = 0 AND 															--Inactive loan; Carol/Roy will follow up; new category--
						REO.PropertyID IS NULL   THEN CAST('Insurance - Inactive Loan No Active REO' AS VARCHAR(100))

				/*4. where asset is a loan and
								a. it is not active
								b. it did convert to an REO then
								c. check to see if REO note owner contains US Bank Trust then code to 532005
								d. check to see if REO note owner does not contain US Bank Trust then code to 532006*/			
				WHEN LOA.Account IS NOT NULL AND LOA.isActive = 0 AND 
						REO.PropertyID IS NOT NULL THEN CASE	WHEN REO.OWNER LIKE  '%U.S. Bank%Series%' THEN CAST('Insurance - REO - REIT' AS VARCHAR(100))			--Active REIT REO--
															WHEN REO.OWNER NOT LIKE  '%U.S. Bank%Series%' THEN CAST('Insurance - REO - Non-REIT' AS VARCHAR(100))			--Active REIT REO--
														END
				/*5. where asset is an REO and 
								a. it is not active
								b. check to see if REO note owner contains US Bank Trust then code to 532005
								c. check to see if REO note owner does not contain US Bank Trust then code to 532006*/				 
				WHEN REO.PropertyID IS NOT NULL AND REO.Active = 0 THEN CASE 
																				WHEN REO.OWNER LIKE  '%U.S. Bank%Series%' THEN CAST('Insurance - REO - REIT' AS VARCHAR(100))			--Active NON-REIT REO--
																				WHEN REO.OWNER NOT LIKE  '%U.S. Bank%Series%' THEN CAST('Insurance - REO - Non-REIT' AS VARCHAR(100))		--Active NON-REIT REO--
																		END
				/*6. where asset is an REO and 
								a. it is not active
								b. and it has an active loan then
								c. where asset is a loan and it is active then code to item 532004*/				 
				WHEN REO.PropertyID IS NOT NULL AND REO.Active = 0 AND 
						LOA.Account IS NOT NULL AND LOA.isActive = 1	THEN  CAST('Insurance - Loan' AS VARCHAR(100))


				--WHEN REO.PropertyID IS NOT NULL AND REO.Active = 0 AND REO.OWNER NOT LIKE  '%U.S. Bank%Series%' THEN CAST('532006' AS VARCHAR(20)) 
				--WHEN REO.PropertyID IS NOT NULL AND LOA.isActive = 1 AND REO.OWNER LIKE  '%U.S. Bank%Series%' THEN CAST('532005' AS VARCHAR(20))  --securitized--
				--WHEN REO.PropertyID IS NOT NULL AND LOA.isActive = 1 AND REO.OWNER NOT LIKE  '%U.S. Bank%Series%' THEN CAST('532006' AS VARCHAR(20)) 

				--WHEN REO.PropertyID IS NOT NULL AND REO.Active = 1 AND REO.OWNER LIKE  '%U.S. Bank%Series%' THEN CAST('532005' AS VARCHAR(20))  --securitized--
				--WHEN REO.PropertyID IS NOT NULL AND REO.Active = 1 AND REO.OWNER NOT LIKE  '%U.S. Bank%Series%' THEN CAST('532006' AS VARCHAR(20)) 
				ELSE 'Further Review' END AS [ITEMDESC]  --confirmationed & done from Roy--
		,	''AS [WAREHOUSEID]
		,	1 AS [QUANTITY]
		,	'Each' AS [UNIT]
		,	ISM.[Net Due] AS [PRICE]
		,	''AS [TAXABLE]
		,	CAST(''  AS VARCHAR(40)) AS [DEPARTMENTID]   --Carol 7/28/2017; the first 4 characters of ITEMID--
		,	'150' AS [LOCATIONID]  
		,	CASE WHEN CAST(LOA.Account AS VARCHAR(20)) IS NULL THEN CAST( ISNULL(REO.PropertyID,'') AS VARCHAR(20)) ELSE CAST(LOA.Account AS VARCHAR(20)) END AS  [PODOCUMENTENTRY_CLASSID]	  	  
		,	''AS [PODOCUMENTENTRY_PROJECTID]
		,	CASE	WHEN LOA.Account IS NOT NULL THEN CAST( LoanNoteOwners.[CUSTOMER ID] AS VARCHAR(5))			
					WHEN REO.PropertyID IS NOT NULL THEN CAST(REONoteOwners.[CUSTOMER ID] AS VARCHAR(5)) 			
					ELSE '' 
			END AS [PODOCUMENTENTRY_CUSTOMERID]      --to be completed after mars note owner is fixed; AJXM Trust 1--
		,	''AS [PODOCUMENTENTRY_EMPLOYEEID]
		,	''AS [PODOCUMENTENTRY_VENDORID]
		,	''AS [SUB_LINE_NO]
		,	''AS [SUBDESC]
		,	''AS [ABSVAL]
		,	''AS [PERCENTVAL]
		,	''AS [SUBLOCATIONID]
		,	''AS [SUBDEPARTMENTID]
		,	''AS [PODOCUMENTSUBTOTALS_CLASSID]
		,	''AS [PODOCUMENTSUBTOTALS_PROJECTID]
		,	''AS [PODOCUMENTSUBTOTALS_CUSTOMERID]
		,	''AS [PODOCUMENTSUBTOTALS_EMPLOYEEID]
		,	''AS [PODOCUMENTSUBTOTALS_VENDORID]
		,	ISM.[Lender Id] AS [VENDORDOCNO]
		,	CAST(ISM.[Property Number] AS VARCHAR(35)) AS [MEMO] 
		,	''AS [DISCOUNT_MEMO]
		,	''AS [FORM1099]
		,	''AS [BILLABLE]
		,	''AS [RSEMINAR]
		,	''AS [PODOCUMENTENTRY_GLDIMRELEASE]
		,	''AS [PODOCUMENTENTRY_GLDIMATTENDEE]
		,	''AS [OVERRIDETAXAMOUNT]
		,	'' AS TRACK_QUANTITY
		,	'' AS SERIALNO
		,	'' AS AISLEID
		,	'' AS ROWID
		,	'' AS BINID
		,	'' AS LOTNO
		,	'' AS EXPIRATION
		,	CASE 
				--WHEN LOA.Account IS NOT NULL AND LOA.isActive = 1 THEN CAST('11' AS VARCHAR(20))			--Insurance Loan--
				WHEN LOA.Account IS NOT NULL THEN CAST('11' AS VARCHAR(2))			--Loan--
				--WHEN REO.PropertyID IS NOT NULL AND LOA.isActive = 1 AND REO.status like '%rent%' THEN CAST('22' AS VARCHAR(20))  --rental--
				WHEN REO.PropertyID IS NOT NULL AND REO.status NOT LIKE '%rent%' THEN CAST('21' AS VARCHAR(2))  --REO - HFS --
				--WHEN REO.PropertyID IS NOT NULL AND LOA.isActive = 1 AND REO.status NOT like '%rent%' THEN CAST('21' AS VARCHAR(20))  
				WHEN REO.PropertyID IS NOT NULL AND REO.status LIKE '%rent%' THEN CAST('22' AS VARCHAR(2))  --REO - LT(Long Term) Rental--
				ELSE '' 
			END AS [PODOCUMENTENTRY_GLDIMSTATUS]   --NON rental-- 
		,	'PMTO'AS [PODOCUMENTENTRY_GLDIMACTIVITY]	  
		,	''AS [PODOCUMENTSUBTOTALS_ITEMID]
		,	'' AS [ATTACHMENT_URL_PURCH]  --Benjacob/Roy to give file/folder structure--
		,	'' AS [TAX_CYCLE]
		,	'' AS [TAX_YEAR]
		,	'BoA Check' AS [PAYMENT_TYPE_HEADER]
		,	CONVERT(VARCHAR(10),ISM.[Month End Date], 101) AS [INVOICE_DATE]
		,	'High' AS [INVOICE_PRIORITY]
		,	'9525 BA 13705' AS [BANK_ACCOUNT_ENTRY_LEVEL]

	INTO #Insurmark
	FROM support.dbo.insurmark AS ISM
		LEFT JOIN mars.dbo.vw_loans AS LOA ON ISM.[Loan No] = LOA.Account
		LEFT JOIN MARS.dbo.vw_REO as REO ON REO.PropertyID = ISM.[Loan No]
		LEFT JOIN mars.dbo.vw_Pools as P on LOA.PoolID = P.PoolID
		LEFT JOIN mars.dbo.vw_Pools as PREO on REO.PoolID = PREO.PoolID
		LEFT JOIN mars.dbo.vw_PropertiesByLoannumber as LoanState on LOA.Account = LoanState.Account
		LEFT JOIN mars.dbo.vw_PropertiesByLoannumber as REOState on REO.FormerGFLoanNumber = REOState.Account
		LEFT JOIN [Support].[dbo].[MarsNoteOwners] as LoanNoteOwners on  LOA.NoteOwner = LoanNoteOwners.[MARS NOTE OWNER]
		LEFT JOIN [Support].[dbo].[MarsNoteOwners] as REONoteOwners on  REO.[Owner] = REONoteOwners.[MARS NOTE OWNER]
	WHERE ISM.[Net Due] > 0.0;


	UPDATE #Insurmark
	SET [TRANSACTIONTYPE] = ''
	WHERE LINE > 1;

	UPDATE #Insurmark
	SET [DEPARTMENTID]  = LEFT(ITEMID,4);


	TRUNCATE TABLE dbo.IntacctPurchasingTransactions;

	INSERT INTO dbo.IntacctPurchasingTransactions
		SELECT *
		FROM #Insurmark

END