USE [Support]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.di_IntacctDLTProcessToExport') IS NOT NULL
	DROP PROCEDURE dbo.di_IntacctDLTProcessToExport;
GO

CREATE PROCEDURE dbo.di_IntacctDLTProcessToExport
AS
BEGIN

/* ********************************************************************************************************* *
   Procedure Name  :	di_IntacctDLTProcessToExport
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	10/10/2017

   *********************************************************************************************************
   **         Change History                                                                              **
   *********************************************************************************************************

   Date			Author			JIRA#		Description
   --------		-----------		--------	------------
   01/16/2018	Benjacob Beres				Modified the @lookback variable to pull the correct weekend/holiday days.
   06/22/2018	Benjacob Beres	MARS-3865	Made the MARS_DW.dbo.AcctConversionNoteOwnerMapping JOIN a distict sub select 
												to account for there being multiple MARS Note Owners with the same Intacct 
												CompanyID.
*  ********************************************************************************************************* */

declare @today date
declare @lookback int
declare @YestLookback int

set @today = CAST(GETDATE() AS DATE)

IF OBJECT_ID('tempdb..#Temp') IS NOT NULL 
	DROP TABLE #Temp;
IF OBJECT_ID('tempdb..#DLTRaw') IS NOT NULL 
	DROP TABLE #DLTRaw;
IF OBJECT_ID('tempdb..#DLTListing') IS NOT NULL 
	DROP TABLE #DLTListing;
IF OBJECT_ID('tempdb..#Header') IS NOT NULL 
	DROP TABLE #Header;
IF OBJECT_ID('tempdb..#DLTJETemp') IS NOT NULL 
	DROP TABLE #DLTJETemp;
IF OBJECT_ID('tempdb..#CashMoves') IS NOT NULL 
	DROP TABLE #CashMoves;
IF OBJECT_ID('tempdb..#CashMovesFinal') IS NOT NULL 
	DROP TABLE #CashMovesFinal;
IF OBJECT_ID('tempdb..#PrevDate') IS NOT NULL 
	DROP TABLE #PrevDate;

-------------------------------------------------------------------------------------------
;WITH cteDates
AS
(
	SELECT	dd.[Date]
		,	CAST(MAX(d1.[Date]) AS DATE) AS LastBusDay
		,	CAST(MIN(d2.[Date]) AS DATE) AS NextBusDay
	--INTO #PrevDate
	FROM	Support.dbo.DimDate dd
		JOIN Support.dbo.DimDate d1 
			ON	d1.DateKey < dd.DateKey
			AND d1.IsWeekday = 1 
			AND d1.IsHoliday = 0
			AND d1.[Date] BETWEEN DATEADD(DD,-90,@today) AND DATEADD(DD,90,@today)
		JOIN Support.dbo.DimDate d2
			ON	d2.DateKey > dd.DateKey
			AND d2.IsWeekday = 1 
			AND d2.IsHoliday = 0 
			AND d2.[Date] BETWEEN DATEADD(DD,-90,@today) AND DATEADD(DD,90,@today)
	WHERE	dd.[Date] BETWEEN DATEADD(DD,-45,@today) AND DATEADD(DD,45,@today)
		--AND dd.IsWeekday = 1 
		--AND dd.IsHoliday = 0
	GROUP BY dd.[Date]
)
, cteDates2
AS
(
	SELECT	c.[Date]
		,	c.LastBusDay
		,	c.NextBusDay
		,	c1.NextBusDay AS Next2ndBusDay
	FROM cteDates c
	JOIN cteDates c1
		ON	CAST(c1.[Date] AS DATE) = c.NextBusDay
)
, cteDates3
AS
(
	SELECT	c.[Date]
		,	c1.LastBusDay AS Last2ndBusDay
		,	c.LastBusDay
		,	c.NextBusDay
		,	c.Next2ndBusDay
	FROM cteDates2 c
	JOIN cteDates2 c1
		ON	CAST(c1.[Date] AS DATE) = c.LastBusDay
)
	SELECT	c.[Date]
		,	c.Last2ndBusDay
		,	c.LastBusDay
		,	c.NextBusDay
		,	c.Next2ndBusDay
		,	c1.NextBusDay AS Next3rdBusDay
	INTO #PrevDate
	FROM cteDates3 c
	JOIN cteDates3 c1
		ON	CAST(c1.[Date] AS DATE) = c.Next2ndBusDay
	ORDER BY c.[Date];
	
-------------------------------------------------------------------------------------------
--Define the @lookback variable to determine how many days of transactions to pull.
--On the day after a weekend and/or holiday, the lookback becomes -1 and the @today becomes the 
--	day after the last business day. Then, the second day after a weekend/holiday, @lookback is 
--	set to pull all the days since the last business day.
-------------------------------------------------------------------------------------------

SELECT @lookback = DATEDIFF(DD,[Date],LastBusDay)
FROM #PrevDate
WHERE [Date] = @today

SELECT @YestLookback = DATEDIFF(DD,[Date],LastBusDay)
FROM #PrevDate
WHERE [Date] = DATEADD(DD,-1,@today)

IF @lookback < -1
	BEGIN
		set @today = DATEADD(DD,@lookback+1,@today)
		set @lookback = -1
	END
ELSE
	BEGIN
		SET @lookback = @YestLookback
	END;

--SELECT @lookback;

--SET @lookback = -28;
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
--This CTE does the watefall for the PAYOFF PIF Loans so we can distribute the balances
;with ctepayoffdata (Account, 
					RecID,
					Reference,
					TotalCash,
					SUSLNCHGSPayoffDateActivity,
					UPBCashPortion,
					UPBNonCashPortion,
					DUPBCashPortion,
					DUPBNonCashPortion,
					IntCashPortion,
					IntNonCashPortion,
					LateCashPortion,
					LateNonCashPortion,
					ChargePrinCashPortion,
					ChargePrinNonCashPortion,
					ChargeIntCashPortion,
					ChargeIntNonCashPortion,
					ImpoundCashPortion,
					ImpoundNonCashPortion,
					LenderFeeCashPortion,
					LenderFeeNonCashPortion,
					SusCashPortion,
					SusNonCashPortion,
					RefundPortion)

as (select LHwaterfall.Account, 
		   LHwaterfall.RecID,
		   LHwaterfall.Reference,
	       LHwaterfall.TotalCash,
	       LHwaterfall.SUSLNCHGSPayoffDateActivity,
		   case 
				when LHwaterfall.UPBPaid >= 0 then LHActual.ToPrincipal
				else LHwaterfall.TotalCash
		   end as UPBCashPortion,
		   case 
				when LHwaterfall.UPBPaid >= 0 then 0
				else LHwaterfall.UPBPaid * -1
		   end as UPBNonCashPortion,
---------------------------------------------------		
		   case 
				when LHwaterfall.DUPBPaid >= 0 then LHWaterfall.DUPBBal
				when LHwaterfall.DUPBPaid < 0 and LHwaterfall.UPBPaid > 0 then LHwaterfall.UPBPaid
				else 0
		   end as DUPBCashPortion,
		   case 
				when LHwaterfall.DUPBPaid >= 0 then 0
				when LHwaterfall.DUPBPaid < 0 and LHwaterfall.UPBPaid > 0 then LHwaterfall.DUPBPaid * -1
				else (LHwaterfall.DUPBPaid - LHwaterfall.UPBPaid) * -1
		   end as DUPBNonCashPortion,
---------------------------------------------------		
		   case 
				when LHwaterfall.IntPaid >= 0 then LHActual.ToInterest
				when LHwaterfall.IntPaid < 0 and LHwaterfall.DUPBPaid > 0 then LHwaterfall.DUPBPaid
				else 0
		   end as IntCashPortion,
		   case 
				when LHwaterfall.IntPaid >= 0 then 0
				when LHwaterfall.IntPaid < 0 and LHwaterfall.DUPBPaid > 0 then LHwaterfall.IntPaid * -1
				else (LHwaterfall.IntPaid - LHwaterfall.DUPBPaid) * -1
		   end as IntNonCashPortion,
---------------------------------------------------		
		   case 
				when LHwaterfall.LatePaid >= 0 then LHActual.ToLateCharge
				when LHwaterfall.LatePaid < 0 and LHwaterfall.IntPaid > 0 then LHwaterfall.IntPaid 
				else 0
		   end as LateCashPortion,
		   case 
				when LHwaterfall.LatePaid >= 0 then 0
				when LHwaterfall.LatePaid < 0 and LHwaterfall.IntPaid > 0 then LHwaterfall.LatePaid * -1
				else (LHwaterfall.LatePaid - LHwaterfall.IntPaid) * -1
		   end as LateNonCashPortion,
---------------------------------------------------		
		   case 
				when LHwaterfall.ChargePrinPaid >= 0 then LHActual.ToChargesPrin
				when LHwaterfall.ChargePrinPaid < 0 and LHwaterfall.LatePaid > 0 then LHwaterfall.LatePaid
				else 0
		   end as ChargePrinCashPortion,
		   case 
				when LHwaterfall.ChargePrinPaid >= 0 then 0
				when LHwaterfall.ChargePrinPaid < 0 and LHwaterfall.LatePaid > 0 then LHwaterfall.ChargePrinPaid * -1
				else (LHwaterfall.ChargePrinPaid - LHwaterfall.LatePaid) * -1
		   end as ChargePrinNonCashPortion,
---------------------------------------------------	
		   case 
				when LHwaterfall.ChargeIntPaid >= 0 then LHActual.ToChargesInt
				when LHwaterfall.ChargeIntPaid < 0 and LHwaterfall.ChargePrinPaid > 0 then LHwaterfall.ChargePrinPaid
				else 0
		   end as ChargeIntCashPortion,
		   case 
				when LHwaterfall.ChargeIntPaid >= 0 then 0
				when LHwaterfall.ChargeIntPaid < 0 and LHwaterfall.ChargePrinPaid > 0 then LHwaterfall.ChargeIntPaid * -1
				else (LHwaterfall.ChargeIntPaid - LHwaterfall.ChargePrinPaid) * -1
		   end as ChargeIntNonCashPortion,
---------------------------------------------------		
		   case 
				when LHwaterfall.ImpoundPaid >= 0 then LHActual.ToImpound
				when LHwaterfall.ImpoundPaid < 0 and LHwaterfall.ChargeIntPaid > 0 then LHwaterfall.ChargeIntPaid
				else 0
		   end as ImpoundCashPortion,
		   case 
				when LHwaterfall.ImpoundPaid >= 0 then 0
				when LHwaterfall.ImpoundPaid < 0 and LHwaterfall.ChargeIntPaid > 0 then LHwaterfall.ChargeIntPaid * -1
				else (LHwaterfall.ImpoundPaid - LHwaterfall.ChargeIntPaid) * -1
		   end as ImpoundNonCashPortion,
---------------------------------------------------		
		   case 
				when LHwaterfall.LenderPaid >= 0 then LHActual.ToLenderFee
				when LHwaterfall.LenderPaid < 0 and LHwaterfall.ImpoundPaid > 0 then LHwaterfall.ImpoundPaid
				else 0
		   end as LenderFeeCashPortion,
		   case 
				when LHwaterfall.LenderPaid >= 0 then 0
				when LHwaterfall.LenderPaid < 0 and LHwaterfall.ImpoundPaid > 0 then LHwaterfall.ImpoundPaid * -1
				else (LHwaterfall.LenderPaid - LHwaterfall.ImpoundPaid) * -1
		   end as LenderFeeNonCashPortion,
---------------------------------------------------		
		   case 
				when LHwaterfall.SusPaid >= 0 then  SuspenseDebit
				when LHwaterfall.SusPaid < 0 and LHwaterfall.LenderPaid > 0 then LHwaterfall.LenderPaid
				else 0
		   end as SusCashPortion,
		   case 
				when LHwaterfall.SusPaid >= 0 then 0
				when LHwaterfall.SusPaid < 0 and LHwaterfall.LenderPaid > 0 then LHwaterfall.LenderPaid * -1
				else (LHwaterfall.SusPaid - LHwaterfall.LenderPaid) * -1
		   end as SusNonCashPortion,
---------------------------------------------------				
		   case 
				when LHwaterfall.SusPaid >= 0 then LHWaterfall.SusPaid
				else 0
		   end as RefundPortion
	
	from (select vw_loans.Account,
				 vw_LoanHistory.RecID,
				 vw_LoanHistory.Reference,
				 vw_LoanHistory.SourceTyp,
				 total as TotalNewCash,
				 ISNULL(DUPBBal,0) as DUPBBal,
				 Total + ImpoundCredit + UICredit + ISNULL(SuspenseCredit,0) as TotalCash,
				 total + ImpoundCredit + UICredit + ISNULL(SuspenseCredit,0) - ToPrincipal as UPBPaid,
				 total + ImpoundCredit + UICredit + ISNULL(SuspenseCredit,0) - ToPrincipal - ISNULL(DUPBBal,0) as DUPBPaid,
				 total + ImpoundCredit + UICredit + ISNULL(SuspenseCredit,0) - ToPrincipal - ISNULL(DUPBBal,0) - ToInterest + ToUnpaidInterest as IntPaid,
				 total + ImpoundCredit + UICredit + ISNULL(SuspenseCredit,0) - ToPrincipal - ISNULL(DUPBBal,0) - ToInterest + ToUnpaidInterest - ToLateCharge as LatePaid,
				 total + ImpoundCredit + UICredit + ISNULL(SuspenseCredit,0) - ToPrincipal - ISNULL(DUPBBal,0) - ToInterest + ToUnpaidInterest - ToLateCharge - ToChargesPrin as ChargePrinPaid,
				 total + ImpoundCredit + UICredit + ISNULL(SuspenseCredit,0) - ToPrincipal - ISNULL(DUPBBal,0) - ToInterest + ToUnpaidInterest - ToLateCharge - ToChargesPrin - ToChargesInt as ChargeIntPaid,
				 --ImpoundCredit is removed from the following lines to avoid duplication.
				 total + UICredit + ISNULL(SuspenseCredit,0) - ToPrincipal - ISNULL(DUPBBal,0) - ToInterest + ToUnpaidInterest - ToLateCharge - ToChargesPrin - ToChargesInt - toimpound as ImpoundPaid,
				 total + UICredit + ISNULL(SuspenseCredit,0) - ToPrincipal - ISNULL(DUPBBal,0) - ToInterest + ToUnpaidInterest - ToLateCharge - ToChargesPrin - ToChargesInt - toimpound - ToLenderFee as LenderPaid,
				 case when LoanType = 3 or (AccrualMethod = 2 and Use365DailyRate = 1 and Use30DayMonths = 0 and AutoPayUnpaidInterest = 1)--This case statement is here because of differences in the way HELOCs and DSI handle interest
					  then total + UICredit + ISNULL(SuspenseCredit,0) - ToPrincipal - ISNULL(DUPBBal,0) - ToInterest - ToLateCharge - ToChargesPrin - ToChargesInt - toimpound - ToLenderFee + ISNULL(SuspenseDebit,0) - ISNULL(SUSLNCHGSPayoffDateActivity,0)
					  else total + UICredit + ISNULL(SuspenseCredit,0) - ToPrincipal - ISNULL(DUPBBal,0) - ToInterest + ToUnpaidInterest - ToLateCharge - ToChargesPrin - ToChargesInt - toimpound - ToLenderFee + ISNULL(SuspenseDebit,0) - ISNULL(SUSLNCHGSPayoffDateActivity,0)
				 end as SusPaid,
				 ISNULL(SuspenseDebit,0) as  SuspenseDebit, --I need this here so I can use it in the next level up. It's not part of the waterfall
                 ISNULL(SUSLNCHGSPayoffDateActivity,0) as SUSLNCHGSPayoffDateActivity --Same as above

		 from mars..vw_LoanHistory
	     inner join mars..vw_loans on vw_loans.RecID = vw_LoanHistory.LoanRecID
	     inner join (select vw_LoanHistory.RecID,
						    ToInterest
						    + ToPrincipal
						    +ToReserve
						    +ToImpound
						    +ToLateCharge
						    +ToChargesPrin
						    +ToChargesInt
						    +ToPrepay
						    +ToBrokerFee
						    +ToLenderFee
						    +ToOtherPayments
						    +ToOtherTaxable as Total,
						    case when ToImpound < 0 then ToImpound * -1 else 0 end as ImpoundCredit,
						    case when ToUnpaidInterest < 0 then ToUnpaidInterest * -1 else 0 end as UICredit
				     from MARS..vw_LoanHistory) as LHTotal on LHTotal.RecID = mars..vw_LoanHistory.RecID
	    left join (select vw_Reserve.Account, 
					      SUM(expr1) * -1 as DUPBBal
				   from MARS..vw_Reserve
				   inner join MARS..vw_Loans on vw_Reserve.Account = vw_Loans.Account
				   where Reference like '%DUPB%'
						 and vw_Reserve.DateDeposited < vw_Loans.PaidOffDate
				   group by vw_Reserve.Account) as DUPB on mars..vw_loans.Account = DUPB.Account
		left join (select vw_Reserve.Account, 
						  case when SUM(expr1) > 0 then SUM(expr1) else 0 end as SuspenseCredit,
						  case when SUM(expr1) < 0 then SUM(expr1) else 0 end as SuspenseDebit
				   from MARS..vw_Reserve
				   inner join MARS..vw_Loans on vw_Reserve.Account = vw_Loans.Account
				   where Reference like 'SUS%'
				   	     and Reference <> 'SUS LNCHGS'
						 and vw_Reserve.DateDeposited < vw_Loans.PaidOffDate--PaidOffDate has NULLs if the loan isn't paid off so this excludes non-payoff loans from this subquery.
				   group by vw_Reserve.Account
				   having SUM(expr1) <> 0) as SUSBal on mars..vw_loans.Account = SUSBal.Account
	    left join (select vw_Reserve.Account, --This is to handle moving funds to SUS LNCHGS during payoff
						  case when SUM(expr1) > 0 then SUM(expr1) else 0 end as SUSLNCHGSPayoffDateActivity
				   from MARS..vw_Reserve
				   inner join MARS..vw_Loans on vw_Reserve.Account = vw_Loans.Account
				   where Reference = 'SUS LNCHGS'
						 and vw_Reserve.DateDeposited = vw_Loans.PaidOffDate
				   group by vw_Reserve.Account
				   having SUM(expr1) <> 0) as SUSLNCHGSPayoffDateActivity on mars..vw_loans.Account = SUSLNCHGSPayoffDateActivity.Account
		) as LHWaterfall

	inner join (select RecID,
					   ToPrincipal,
				       ToInterest,
				       ToLateCharge,
				       ToChargesPrin,
				       ToChargesInt,
				       ToImpound,
				       ToLenderFee
			    from MARS..vw_LoanHistory) as LHActual 
		on LHwaterfall.RecID = LHActual.RecID
			
where SourceTyp = 'payoff'
	  and Reference = 'PAYOFF PIF')


--Start main query after CTE
select baselist.Account,

		--baselist.Reference,

	   case
			when baselist.Reference like 'MOD%' then 'MOD'
			when baselist.Reference like 'WAIVE%' then 'WAIVE'
			when baselist.Reference = 'REO' then 'REO'
			when baselist.Reference = 'PAYOFF PIF' then 'PIF'
			when baselist.SourceTyp = 'PAYOFF' and baselist.Reference <> 'PAYOFF PIF' then 'TE'
			else 'REG' 
	   end as 'TranType',

	   ISNULL(AC16000CASH,0)+ISNULL(AC16000NONCASH,0)+ISNULL(ctepayoffdata.UPBCashPortion,0)as AC16000, --UPB Both REG and MOD
	   ISNULL(AC16001CASH,0)+ISNULL(AC16001NONCASH,0)+ISNULL(ctepayoffdata.DUPBCashPortion,0)as AC16001, --DUPB
	   ISNULL(AC16050,0)as AC16050, --Discount
	   ISNULL(AC16225NONCASH,0)as AC16225, --Prior Servicer Loan Charges
	   ISNULL(AC16250NONCASH,0)+ISNULL(AC16250CASH,0)+ISNULL(ctepayoffdata.ChargePrinCashPortion,0) as AC16250, --GF Loan Charges.
	   ISNULL(AC16300NONCASH,0)as AC16300, --GF Escrow Advances#Commented out CASH segment
	   ISNULL(AC16375NONCASH,0)as AC16375, --Prior Servicer Escrow Advances#Commented out CASH segment
	   ISNULL(AC17300CASH,0) +ISNULL(ctepayoffdata.IntCashPortion,0) as AC17300, --Interest 
	   ISNULL(AC17605NONCASH,0)as AC17605, --Recoverable from Investor
	   ISNULL(AC23400CASH,0)+ISNULL(AC23400NONCASH,0)+ISNULL(ctepayoffdata.SusCashPortion,0) as AC23400, --Reserve non-DUPB
	   ISNULL(AC24180CASH,0) + ISNULL(WAIVEINSTLIMPOUNDAMT, 0)+ISNULL(ctepayoffdata.ImpoundCashPortion,0) as AC24180, --Impound 
	   0 as AC24250, --This creates the column for the client trust section so that we can union the trust ledger results
	   ISNULL(AC42020CASH,0)+ISNULL(AC42020NONCASH,0)+ISNULL(ctepayoffdata.LateCashPortion,0) as AC42020, --Late Charges 
	   ISNULL(AC42040CASH,0)+ISNULL(AC42040NONCASH,0)+ISNULL(ctepayoffdata.ChargeIntCashPortion,0) as AC42040, --Charges Interest
	   ISNULL(AC73330CASH,0)+ISNULL(AC73330NONCASH,0)+ISNULL(ctepayoffdata.LenderFeeCashPortion,0) as AC73330, --Lender Fees aka R&W
	   ISNULL(ACTE,0)as ACTE, --Terminating Events
   
	   CASE WHEN baselist.Reference = 'RCS' 
				THEN isnull(AC17300CASH,0)+isnull(AC16000CASH,0)+isnull(AC16001CASH,0) --P&I
			ELSE isnull(ACTOTAL,0) 
		END AS AC12500, --Total Received
	   --ISNULL(ACTOTAL,0)ACTOTAL, --Total Received
	   
	   ISNULL(AC17300CASH,0)
	   +ISNULL(AC16000CASH,0)
	   +ISNULL(AC16001CASH,0)
	   +ISNULL(ctepayoffdata.UPBCashPortion,0)
	   +ISNULL(ctepayoffdata.DUPBCashPortion,0)
	   +ISNULL(ctepayoffdata.IntCashPortion,0) as 'P&I', 
	   
		ISNULL(AC17300CASH,0) 
	   +ISNULL(AC16001CASH,0)
	   +ISNULL(AC16000CASH,0) 
	   +ISNULL(AC42020CASH,0)
	   +ISNULL(AC42040CASH,0) 
	   +ISNULL(ctepayoffdata.UPBCashPortion,0)
	   +ISNULL(ctepayoffdata.DUPBCashPortion,0)
	   +ISNULL(ctepayoffdata.IntCashPortion,0) 
	   +ISNULL(ctepayoffdata.LateCashPortion,0)
	   +ISNULL(ctepayoffdata.ChargeIntCashPortion,0)
	   +ISNULL(ACTE,0)as 'Due to Lender',
	   
	   ISNULL(ctepayoffdata.RefundPortion,0)as AC24225, --PIF Refunds
	   	   
	   ISNULL(AC16000CASH,0)+ISNULL(AC16000NONCASH,0)+ISNULL(ctepayoffdata.UPBCashPortion,0)
	   +ISNULL(AC16001CASH,0)+ISNULL(AC16001NONCASH,0)+ISNULL(ctepayoffdata.DUPBCashPortion,0)
	   +ISNULL(AC16050,0)
	   +ISNULL(AC16225NONCASH,0)
	   +ISNULL(AC16250NONCASH,0) +ISNULL(AC16250CASH,0)+ISNULL(ctepayoffdata.ChargePrinCashPortion,0)
	   +ISNULL(AC16300NONCASH,0)
	   +ISNULL(AC16375NONCASH,0)
	   +ISNULL(AC17300CASH,0)+ISNULL(ctepayoffdata.IntCashPortion,0)
	   +ISNULL(AC17605NONCASH,0)
	   +ISNULL(AC23400CASH,0)+ISNULL(AC23400NONCASH,0)+ISNULL(ctepayoffdata.SusCashPortion,0)
	   +ISNULL(AC24180CASH,0)+ISNULL(ctepayoffdata.ImpoundCashPortion,0)
	   +ISNULL(AC42020CASH,0)+ISNULL(AC42020NONCASH,0)+ISNULL(ctepayoffdata.LateCashPortion,0)
	   +ISNULL(AC42040CASH,0)+ISNULL(AC42040NONCASH,0)+ISNULL(ctepayoffdata.ChargeIntCashPortion,0)
	   +ISNULL(AC73330CASH,0)+ISNULL(AC73330NONCASH,0)+ISNULL(ctepayoffdata.LenderFeeCashPortion,0)
	   +ISNULL(ctepayoffdata.RefundPortion,0)
	   +ISNULL(ctepayoffdata.SUSLNCHGSPayoffDateActivity,0)
	   +ISNULL(ACTE,0)
	   -ISNULL(ACTOTAL,0) as 'Variance', --Check this
	   	   
	   SysRecCreated AS DateProc
into #temp
from --Creates the base list of loans with transactions in the time frame.
	(
		select account, 
			   MARS..vw_LoanHistory.RecID, 
			   MARS..vw_LoanHistory.SysRecCreated, 
			   SourceTyp, 
			   Reference,
			   case 
					when Reference like '%hamp%' 
					then ToInterest 	
						 +ToPrincipal	
						 +ToReserve
						 +ToImpound	
						 +ToLateCharge	
						 +ToChargesPrin	
						 +ToChargesInt	
						 +ToPrepay	
						 +ToBrokerFee	
						 +ToLenderFee	
						 +ToOtherTaxable	
						 +ToOtherTaxFree 
					else ToInterest 	
					     +ToPrincipal	
						 +ToReserve
						 +ToImpound	
						 +ToLateCharge	
						 +ToChargesPrin	
						 +ToChargesInt	
						 +ToPrepay	
						 +ToBrokerFee	
						 +ToLenderFee
						 +ToOtherPayments
						 +ToOtherTaxable	
						 +ToOtherTaxFree
			   end as ACTOTAL --I removed ToOTher from the total to address variances with HAMP

		from MARS..vw_LoanHistory
		inner join MARS..vw_loans on MARS..vw_loans.RecID = MARS..vw_LoanHistory.LoanRecID
		
		where MARS..vw_LoanHistory.SysRecCreated >= dateadd(day, @lookback, @today)
		and MARS..vw_LoanHistory.SysRecCreated < @today
		and SourceTyp <> 'Fundin'
		and Reference <> 'REO'
	) as baselist
	
left join ctepayoffdata on baselist.RecID = ctepayoffdata.RecID
	  
	
left join 

	(
		select RecID, ToReserve as AC23400CASH

		from MARS..vw_LoanHistory

		where MARS..vw_LoanHistory.SysRecCreated >= dateadd(day, @lookback, @today)
		and MARS..vw_LoanHistory.SysRecCreated < @today
		and right(Reference,4) <> 'DUPB'
		and Reference not like 'WAIVE%'
		and Reference not like 'MOD%'
		and SourceTyp <> 'Payoff'
	) as Q23400CASH on baselist.RecID = Q23400CASH.RecID
	
left join 

	(
		select RecID, ToReserve as AC23400NONCASH

		from MARS..vw_LoanHistory

		where MARS..vw_LoanHistory.SysRecCreated >= dateadd(day, @lookback, @today)
		and MARS..vw_LoanHistory.SysRecCreated < @today
		and right(Reference,4) <> 'DUPB'
		and (Reference like 'WAIVE%'
		or Reference like 'MOD%') 
	) as Q23400NONCASH on baselist.RecID = Q23400NONCASH.RecID
	  
left join 

	(
		
		select RecID, ToImpound as AC24180CASH

		from MARS..vw_LoanHistory

		where MARS..vw_LoanHistory.SysRecCreated >= dateadd(day, @lookback, @today)
		and MARS..vw_LoanHistory.SysRecCreated < @today
		and Reference not like 'WAIVE%'
		and Reference not like 'MOD%'
		and SourceTyp <> 'Payoff'
	) as Q24180CASH on baselist.RecID = Q24180CASH.RecID
	
left join 

	(
		
		select RecID, ToImpound as WAIVEINSTLIMPOUNDAMT

		from MARS..vw_LoanHistory

		where MARS..vw_LoanHistory.SysRecCreated >= dateadd(day, @lookback, @today)
		and MARS..vw_LoanHistory.SysRecCreated < @today
		and Reference = 'WAIVEINSTL'
		and SourceTyp <> 'Payoff'
	) as WAIVEINSTALLIMPOUND on baselist.RecID = WAIVEINSTALLIMPOUND.RecID
	
	
left join 

	(
		select RecID, ToInterest as AC17300CASH --Total to interest including UI

		from MARS..vw_LoanHistory

		where MARS..vw_LoanHistory.SysRecCreated >= dateadd(day, @lookback, @today)
		and MARS..vw_LoanHistory.SysRecCreated < @today
		and Reference not like 'WAIVE%'
		and Reference not like 'MOD%'
		and SourceTyp <> 'Payoff'
	) as Q17300CASH on baselist.RecID = Q17300CASH.RecID
	

left join 

	(
		select RecID, ToReserve as AC16001CASH --Split out MOD DUPB

		from MARS..vw_LoanHistory

		where MARS..vw_LoanHistory.SysRecCreated >= dateadd(day, @lookback, @today)
		and MARS..vw_LoanHistory.SysRecCreated < @today
		and right(Reference,4) = 'DUPB'
		and Reference not like 'WAIVE%'
		and Reference not like 'MOD%'
		and SourceTyp <> 'Payoff'
	) as Q16001CASH on baselist.RecID = Q16001CASH.RecID
	
left join 

	(
		select RecID, ToReserve as AC16001NONCASH --Split out MOD DUPB

		from MARS..vw_LoanHistory

		where MARS..vw_LoanHistory.SysRecCreated >= dateadd(day, @lookback, @today)
		and MARS..vw_LoanHistory.SysRecCreated < @today
		and right(Reference,4) = 'DUPB'
		and (Reference like 'WAIVE%'
		or Reference like 'MOD%') 
	) as Q16001NONCASH on baselist.RecID = Q16001NONCASH.RecID
	
left join 

	(
		select RecID, ToPrincipal as AC16000CASH

		from MARS..vw_LoanHistory

		where MARS..vw_LoanHistory.SysRecCreated >= dateadd(day, @lookback, @today)
		and MARS..vw_LoanHistory.SysRecCreated < @today
		and Reference not like 'WAIVE%'
		and Reference not like 'MOD%'
		and SourceTyp <> 'Payoff'
	) as Q16000CASH on baselist.RecID = Q16000CASH.RecID
	
left join 

	(
		select RecID, ToPrincipal as AC16000NONCASH --Review MOD Split 2017-04-03

		from MARS..vw_LoanHistory

		where MARS..vw_LoanHistory.SysRecCreated >= dateadd(day, @lookback, @today)
		and MARS..vw_LoanHistory.SysRecCreated < @today
		and (Reference like 'WAIVE%'
		or Reference like 'MOD%') 
	) as Q16000NONCASH on baselist.RecID = Q16000NONCASH.RecID
	
left join 

	(
		select RecID, ToLateCharge as AC42020CASH

		from MARS..vw_LoanHistory

		where MARS..vw_LoanHistory.SysRecCreated >= dateadd(day, @lookback, @today)
		and MARS..vw_LoanHistory.SysRecCreated < @today
		and Reference not like 'WAIVE%'
		and Reference not like 'MOD%'
		and SourceTyp <> 'Payoff'
	) as Q42020CASH on baselist.RecID = Q42020CASH.RecID

left join 

	(
		select RecID, 
		case
			when SourceTyp = 'Adj-LC' then LateCharge
			else ToLateCharge
		end as AC42020NONCASH
		from MARS..vw_LoanHistory

		where MARS..vw_LoanHistory.SysRecCreated >= dateadd(day, @lookback, @today)
		and MARS..vw_LoanHistory.SysRecCreated < @today
		and (Reference like 'WAIVE%'
		or Reference like 'MOD%')
		and Reference not like '%WAIVE LC%' 
		and Reference not like '%MOD 42020%'
	) as Q42020NONCASH on baselist.RecID = Q42020NONCASH.RecID

left join 

	(
		select RecID, ToChargesInt as AC42040CASH

		from MARS..vw_LoanHistory

		where MARS..vw_LoanHistory.SysRecCreated >= dateadd(day, @lookback, @today)
		and MARS..vw_LoanHistory.SysRecCreated < @today
		and Reference not like 'WAIVE%'
		and Reference not like 'MOD%'
		and SourceTyp <> 'Payoff'
	) as Q42040CASH on baselist.RecID = Q42040CASH.RecID
	
left join 

	(
		select RecID, ToChargesInt as AC42040NONCASH

		from MARS..vw_LoanHistory

		where MARS..vw_LoanHistory.SysRecCreated >= dateadd(day, @lookback, @today)
		and MARS..vw_LoanHistory.SysRecCreated < @today
		and (Reference like 'WAIVE%'
		or Reference like 'MOD%') 
	) as Q42040NONCASH on baselist.RecID = Q42040NONCASH.RecID
	
left join 

	(
		select MARS..vw_LoanHistory.RecID, ToInterest 	
										 +ToPrincipal	
										 +ToReserve
										 +ToImpound	
										 +ToLateCharge	
										 +ToChargesPrin	
										 +ToChargesInt	
										 +ToPrepay	
										 +ToBrokerFee	
										 +ToLenderFee	
										 +ToOtherPayments	
										 +ToOtherTaxable	
										 +ToOtherTaxFree as ACTE

		from MARS..vw_LoanHistory 
		inner join (select RecID
					from MARS..vw_LoanHistory
					where Reference in ('PAYOFF 3P', 'PAYOFF RED', 'PAYOFF SS', 'PAYOFF WO', 'SRVTRF')
					and Reference <> 'REO') PH2 on MARS..vw_LoanHistory.RecID = PH2.RecID

		where MARS..vw_LoanHistory.SysRecCreated >= dateadd(day, @lookback, @today)
		and MARS..vw_LoanHistory.SysRecCreated < @today
	) as QTE on baselist.RecID = QTE.RecID --Cash Only Create a seperate for REO
	

left join 

	(
		select RecID, ToOtherPayments as AC17605NONCASH

		from MARS..vw_LoanHistory

		where MARS..vw_LoanHistory.SysRecCreated >= dateadd(day, @lookback, @today)
		and MARS..vw_LoanHistory.SysRecCreated < @today
		and (Reference like 'MOD 16250%'
			 or Reference like 'MOD 16375%'
			 or Reference like 'WAIVE16375%'
			 or Reference like 'WAIVE16250%') 
	) as Q17605NONCASH on baselist.RecID = Q17605NONCASH.RecID
	

left join 

	(
		select RecID, ToChargesPrin as AC16225NONCASH

		from MARS..vw_LoanHistory

		where MARS..vw_LoanHistory.SysRecCreated >= dateadd(day, @lookback, @today)
		and MARS..vw_LoanHistory.SysRecCreated < @today
		and (Reference like 'WAIVE%'
		or Reference like 'MOD%') 
		and Reference not like '%16250%'
		and Reference not like '%NSF%'
	) as Q16225NONCASH on baselist.RecID = Q16225NONCASH.RecID
	

left join 

	(
		select RecID, ToChargesPrin as AC16250CASH

		from MARS..vw_LoanHistory

		where MARS..vw_LoanHistory.SysRecCreated >= dateadd(day, @lookback, @today)
		and MARS..vw_LoanHistory.SysRecCreated < @today
		and Reference not like 'WAIVE%'
		and Reference not like 'MOD%'
		and SourceTyp <> 'Payoff'
	) as Q16250CASH on baselist.RecID = Q16250CASH.RecID

	
left join 

	(
		select RecID, ToChargesPrin as AC16250NONCASH

		from MARS..vw_LoanHistory

		where MARS..vw_LoanHistory.SysRecCreated >= dateadd(day, @lookback, @today)
		and MARS..vw_LoanHistory.SysRecCreated < @today
		and (Reference like 'WAIVE%'
		or Reference like 'MOD%') 
		and Reference not like '%16225%'
		and Reference not like '%NSF%'
	) as Q16250NONCASH on baselist.RecID = Q16250NONCASH.RecID

left join 

	(
		select RecID, ToImpound as AC16300NONCASH

		from MARS..vw_LoanHistory

		where MARS..vw_LoanHistory.SysRecCreated >= dateadd(day, @lookback, @today)
		and MARS..vw_LoanHistory.SysRecCreated < @today
		and (Reference like 'WAIVE16300%'
		or Reference like 'MOD 16300%') 
	) as Q16300NONCASH on baselist.RecID = Q16300NONCASH.RecID

left join

	(
		select RecID, ToImpound as AC16375NONCASH

		from MARS..vw_LoanHistory

		where MARS..vw_LoanHistory.SysRecCreated >= dateadd(day, @lookback, @today)
		and MARS..vw_LoanHistory.SysRecCreated < @today
		and (Reference like 'WAIVE16375%'
		or Reference like 'MOD 16375%') 
	) as Q16375NONCASH on baselist.RecID = Q16375NONCASH.RecID

left join 

	(
		select RecID, 
			case
				when Reference like '%MOD 42020%' then ToLateCharge
                when Reference like '%WAIVE UPB%' then -1 * ToPrincipal
                when Reference like '%WAIVEINSTL%' then -1 * ToPrincipal
                when Reference like '%WAIVE16225%' then ToOtherPayments
                when Reference like '%WAIVE16300%' then ToOtherPayments
                when Reference like '%MOD 42040%' then ToChargesInt
                when Reference like '%WAIVE DUPB%' then -1 * ToReserve
                when Reference like '%MOD 17300%' then ToInterest
            end as AC16050
        from MARS..vw_LoanHistory

        where MARS..vw_LoanHistory.SysRecCreated >= dateadd(day, @lookback, @today)
              and MARS..vw_LoanHistory.SysRecCreated < @today
              and (Reference like '%MOD 17300%' 
                   or Reference like '%MOD 42020%' 
                   or Reference like '%WAIVE UPB%'
                   or Reference like '%WAIVEINSTL%'
                   or Reference like '%WAIVE16225%'
                   or Reference like '%WAIVE16300%'
                   or Reference like '%MOD 42040%'
                   or Reference like '%WAIVE DUPB%')
	) as Q16050 on baselist.RecID = Q16050.RecID
	
left join 

	(
		select RecID, ToLenderFee as AC73330CASH

		from MARS..vw_LoanHistory

		where MARS..vw_LoanHistory.SysRecCreated >= dateadd(day, @lookback, @today)
		and MARS..vw_LoanHistory.SysRecCreated < @today
		and Reference not like 'WAIVE%'
		and Reference not like 'MOD%'
		and SourceTyp <> 'Payoff'
	) as Q73330CASH on baselist.RecID = Q73330CASH.RecID

	
left join 

	(
		select RecID, ToLenderFee as AC73330NONCASH

		from MARS..vw_LoanHistory

		where MARS..vw_LoanHistory.SysRecCreated >= dateadd(day, @lookback, @today)
		and MARS..vw_LoanHistory.SysRecCreated < @today
		and (Reference like 'WAIVE%'
		or Reference like 'MOD%') 
		and Reference not like '%16225%'
		and Reference not like '%NSF%'
	) as Q73330NONCASH on baselist.RecID = Q73330NONCASH.RecID
	

where baselist.Reference not in ('BKWRITEOFF',  --This where clause excludes all of the reference codes that will be picked up by the Trust Ledger Subqueries.
								 'DUPB PRA',
								 'DUPB WO',
								 'HAZCLAIM',
								 'REFUND',
								 'WAIVE DUPB',
								 'DUPB 1099')
							
								 
---------------------------------REO SECTION-------------------------------------------------------

union all --This is to show REO Events but not totals

select Account, 
	   'REO',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,MARS..vw_LoanHistory.SysRecCreated

		from MARS..vw_LoanHistory 
		inner join (select RecID
					from MARS..vw_LoanHistory
					where SourceTyp = 'payoff'
					and Reference = 'REO') PH2 on MARS..vw_LoanHistory.RecID = PH2.RecID
		inner join mars..vw_loans on mars..vw_loans.RecID = MARS..vw_LoanHistory.LoanRecID
		
		where MARS..vw_LoanHistory.SysRecCreated >= dateadd(day, @lookback, @today)
		and MARS..vw_LoanHistory.SysRecCreated < @today
		
------------------------------------TRUST LEDGER POSTING--------------------------

union all

select Account,
	   'BK WRITE OFF',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,Amount,MARS..vw_TrustLedger.SysCreatedDate --force into variance so it shows on the exception report

from MARS..vw_TrustLedger

where MARS..vw_TrustLedger.SysCreatedDate >= dateadd(day, @lookback, @today)
	  and MARS..vw_TrustLedger.SysCreatedDate < @today
	  and Reference = 'BKWRITEOFF'
						
union all

--24180
select Account,
	   'TRUST',0,0,0,0,0,0,0,0,0,Amount * -1,Amount,0,0,0,0,0,0,0,0,0,0,MARS..vw_TrustLedger.SysCreatedDate

from MARS..vw_TrustLedger

where MARS..vw_TrustLedger.SysCreatedDate >= dateadd(day, @lookback, @today)
	  and MARS..vw_TrustLedger.SysCreatedDate < @today
	  and Reference = 'REFUND'
						
union all 

--16001
select Account,
	   'TRUST',0,Amount,Amount * -1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,MARS..vw_TrustLedger.SysCreatedDate

from MARS..vw_TrustLedger

where MARS..vw_TrustLedger.SysCreatedDate >= dateadd(day, @lookback, @today)
	  and MARS..vw_TrustLedger.SysCreatedDate < @today
	  and Reference in ('DUPB 1099', --Look into this code more
						'DUPB PRA',
						'DUPB WO',
						'WAIVE DUPB')
						
union all 

--24250
select Account,
	   'TRUST',0,0,0,0,0,0,0,0,0,Amount * -1,0,Amount,0,0,0,0,0,0,0,0,0,MARS..vw_TrustLedger.SysCreatedDate

from MARS..vw_TrustLedger

where MARS..vw_TrustLedger.SysCreatedDate >= dateadd(day, @lookback, @today)
	  and MARS..vw_TrustLedger.SysCreatedDate < @today
	  and Reference = 'HAZCLAIM'
						

order by 2, 1


------------------------------------------------------------------------------------------------------------------------
--Formatting Section
------------------------------------------------------------------------------------------------------------------------


select	t.Account
	,	t.[TranType]
	,	t.AC16000 AS [14110]
	,	t.AC16001 AS [14115]
	,	t.AC16050 AS [14131]
	,	t.AC16225 AS [18110]
	,	t.AC16250 AS [18120]
	,	t.AC16300 AS [18125]
	,	t.AC16375 AS [18130]
	,	t.AC17300 AS [40100]
	,	t.AC17605 AS [18145]
	,	t.AC23400 AS [23500]
	,	t.AC24180 AS [23505]
	,	t.AC24250 AS [23535]
	,	t.AC42020 AS [46100]
	,	t.AC42040 AS [40105]
	,	t.AC73330 AS [68130]
	,	t.AC12500 AS [15515]
	,	t.ACTE
	,	t.[P&I]
	,	t.[Due To Lender]
	,	t.AC24225 AS [23520]
	,	t.Variance
	,	pd.LastBusDay AS DateProc
INTO #DLTRaw
FROM #temp t
	JOIN #PrevDate pd 
		ON	pd.[Date] = CAST(t.DateProc AS DATE)
WHERE ISNULL(SUBSTRING(t.Account,7,1),'') <> 'B'


TRUNCATE TABLE Support.dbo.DLTExceptions

INSERT INTO Support.dbo.DLTExceptions
SELECT Account, TranType, [14110], [14115], [14131], [18110], [18120], [18125], [18130], [40100]
	,	[18145], [23500], [23505], [23535], [46100], [40105], [68130], [15515], [P&I], [Due to Lender], Variance, DateProc
	,	GETDATE() AS DateAdded
FROM #DLTRaw d
WHERE Variance <> 0
	
	

DELETE 
FROM #DLTRaw
WHERE Variance <> 0


SELECT	'DLTJ' AS JOURNAL
	,	up.DateProc AS PostingDate
	,	up.TranType
	,	up.Account
	,	up.GLNum
	,	CAST(CASE WHEN ((up.GLNum <> '15515' AND up.Amount > 0) OR (up.GLNum = '15515' AND up.Amount < 0)) THEN ABS(up.Amount) ELSE 0 END AS VARCHAR(15)) AS CREDIT
	,	CAST(CASE WHEN ((up.GLNum = '15515' AND up.Amount > 0) OR (up.GLNum <> '15515' AND up.Amount < 0)) THEN ABS(up.Amount) ELSE 0 END AS VARCHAR(15)) AS DEBIT
INTO #DLTListing
FROM
	(	SELECT d.Account, d.TranType , d.DateProc, d.[14110], d.[14115], d.[14131], d.[18110], d.[18120], d.[18125], d.[18130], d.[40100], d.[18145], d.[23500], d.[23505], d.[46100], d.[40105], d.[15515]
		FROM #DLTRaw d ) p
UNPIVOT
	(	Amount FOR GLNum IN
		([14110], [14115], [14131], [18110], [18120], [18125], [18130], [40100], [18145], [23500], [23505], [46100], [40105], [15515])
	) AS up
WHERE up.Amount <> 0
ORDER BY up.Account

SELECT DISTINCT 
		JOURNAL
	,	t.PostingDate
	,	t.TranType AS Descr
	,	t.Account
INTO #Header
FROM #DLTListing t;


SELECT	h.JOURNAL
	,	CONVERT(VARCHAR(10),h.PostingDate,101) AS [DATE]
	,	h.Descr AS [DESCRIPTION]
	,	h.Account AS REFERENCE_NO
	--,	ROW_NUMBER() OVER(PARTITION BY h.PostingDate, cnom.IntacctCompanyNum ORDER BY h.Account, d.GLNum) AS Line_No
	,	d.GLNum AS ACCT_NO
	,	CASE WHEN d.GLNum IN ('15515', '23500') THEN '150' 
			 WHEN (d.GLNum = '23505' AND cnom.IntacctCompanyNum <> '480') THEN '150'
			 ELSE cnom.IntacctCompanyNum END AS LOCATION_ID
	,	CONVERT(VARCHAR(10),h.PostingDate,101) + ' DLT' AS MEMO
	,	CASE WHEN d.Debit = '0.00' THEN '' ELSE d.Debit END AS DEBIT
	,	CASE WHEN d.Credit = '0.00' THEN '' ELSE d.Credit END AS CREDIT
	,	'11' AS GLDIMSTATUS
	,	CASE	WHEN h.Descr = 'REG' THEN 'PMTI'
				WHEN h.Descr = 'MOD' THEN 'MOD'
				WHEN h.Descr = 'WAIVE' THEN 'NCAJ'
				ELSE 'PMTI'
		END AS GLDIMACTIVITY
	,	CASE WHEN d.GLNum IN ('15515', '23500') THEN '150' 
			 WHEN (d.GLNum = '23505' AND cnom.IntacctCompanyNum <> '480') THEN '150'
			 ELSE cnom.IntacctCompanyNum END AS GLENTRY_CUSTOMERID
	,	h.Account AS GLENTRY_CLASSID
INTO #DLTJETemp
FROM	#Header h
	INNER JOIN #DLTListing d
		ON	d.Account = h.Account
		AND d.PostingDate = h.PostingDate
		AND d.TranType = h.Descr
	INNER JOIN MARS.dbo.vw_Loans l
		ON	l.Account = h.Account
	INNER JOIN MARS_DW.dbo.AcctConversionNoteOwnerMapping cnom
		ON	cnom.TMONoteOwner = l.NoteOwner;


--SELECT	t.LOCATION_ID
--	,	t.TotDebit - t.TotCredit
--	,	t.MaxLine + 1 AS LINE_NO

INSERT INTO #DLTJETemp
SELECT	t.JOURNAL
	,	t.[DATE]
	,	[DESCRIPTION]
	,	REFERENCE_NO 
	,	cnom.[ReceivableAcct] AS ACCT_NO
	,	t.LOCATION_ID
	,	MEMO
	,	CASE WHEN t.TotDebit - t.TotCredit < 0 THEN ABS(t.TotDebit - t.TotCredit) ELSE 0 END AS DEBIT
	,	CASE WHEN t.TotDebit - t.TotCredit < 0 THEN 0 ELSE t.TotDebit - t.TotCredit END AS CREDIT
	,	'11' AS GLDIMSTATUS
	,	CASE	WHEN t.[DESCRIPTION] = 'REG' THEN 'PMTI'
				WHEN t.[DESCRIPTION] = 'MOD' THEN 'MOD'
				WHEN t.[DESCRIPTION] = 'WAIVE' THEN 'NCAJ'
				ELSE 'PMTI'
		END AS GLDIMACTIVITY
	,	t.LOCATION_ID AS GLENTRY_CUSTOMERID
	,	REFERENCE_NO AS GLENTRY_CLASSID
FROM (	select	JOURNAL
			,	LOCATION_ID
			,	REFERENCE_NO
			,	[DATE]
			,	MEMO
			,	[DESCRIPTION]
			,	SUM(CAST(CASE WHEN CREDIT = '' THEN '0.00' ELSE CREDIT END AS DECIMAL(15,2))) AS TotCredit
			,	SUM(CAST(CASE WHEN DEBIT = '' THEN '0.00' ELSE DEBIT END AS DECIMAL(15,2))) AS TotDebit
		from #DLTJETemp
		group by JOURNAL, LOCATION_ID, REFERENCE_NO, [DATE], MEMO, [DESCRIPTION]
	) t
INNER JOIN (	SELECT DISTINCT c.IntacctCompanyNum, [ReceivableAcct]
				FROM MARS_DW.dbo.AcctConversionNoteOwnerMapping c
			) cnom
	ON	cnom.IntacctCompanyNum = t.LOCATION_ID
WHERE t.TotDebit - t.TotCredit <> 0


UPDATE #DLTJETemp 
SET		DEBIT = CASE WHEN DEBIT = '0.00' THEN '' ELSE DEBIT END
	,	CREDIT = CASE WHEN CREDIT = '0.00' THEN '' ELSE CREDIT END
	

TRUNCATE TABLE Support.dbo.IntacctJournalEntries;

INSERT INTO Support.dbo.IntacctJournalEntries
			(JOURNAL, [DATE], [DESCRIPTION], REFERENCE_NO, LINE_NO, ACCT_NO, LOCATION_ID, MEMO, DEBIT, CREDIT, GLDIMSTATUS, GLDIMACTIVITY, GLENTRY_CUSTOMERID, GLENTRY_CLASSID)
SELECT	d.JOURNAL
	,	d.[DATE]
	,	d.[DESCRIPTION]
	,	REFERENCE_NO
	,	ROW_NUMBER() OVER(PARTITION BY LOCATION_ID, d.[DESCRIPTION], d.[DATE] ORDER BY LOCATION_ID, d.[DESCRIPTION], d.[DATE], REFERENCE_NO, ACCT_NO ) AS LINE_NO
	,	d.ACCT_NO
	,	d.LOCATION_ID
	,	d.MEMO
	,	d.DEBIT
	,	d.CREDIT
	,	d.GLDIMSTATUS
	,	GLDIMACTIVITY
	,	GLENTRY_CUSTOMERID
	,	GLENTRY_CLASSID
FROM #DLTJETemp d
ORDER BY LOCATION_ID,d.[DESCRIPTION], d.[DATE], LINE_NO


END