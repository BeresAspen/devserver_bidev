USE MARS_DW;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.qy_DialerDashDistinctAccountContact') IS NOT NULL
	DROP PROCEDURE dbo.qy_DialerDashDistinctAccountContact;
GO

/* ********************************************************************************************************* *
   Procedure Name  :	qy_DialerDashDistinctAccountContact
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	02/07/2017

   *********************************************************************************************************
   **         Change History                                                                              **
   *********************************************************************************************************

   Date			Author				Description
   --------		-----------			------------
   02/14/2017	Benjacob Beres		Removed the InboundNotOnDialer grouping
   04/20/2017	Benjacob Beres		Added clause to not show today's data if it is available.

*  ********************************************************************************************************* */
CREATE PROCEDURE dbo.qy_DialerDashDistinctAccountContact 
	(
		 @DDDACProcessDays INT = NULL
	)

AS
BEGIN

	SET @DDDACProcessDays = ISNULL(@DDDACProcessDays,35)

	DECLARE @WorkingDate DATE		--This variable is placed in here due to an infinite loop the parameter creates in the vw_Loans_Archive call. 
	SET @WorkingDate = CAST(GETDATE() - @DDDACProcessDays AS DATE)

	/****************************************************************
	Create Ranking Table
	*****************************************************************/
	CREATE TABLE #Ranking
		(	CodeRank	INT
		,	CodeDescr	VARCHAR(50)
		)

	INSERT INTO #Ranking
	VALUES	(	1,'OutboundContact')
		,	(	2,'InboundContact')
		,	(	3,'OutboundContactNotOnDialer')
		,	(	4,'OutboundAttempt')
		,	(	5,'OutboundAttemptNotOnDialer' )
		,	(	6,'Untouched Loans' )

	/*****************************************************************
	Count of all possible calls per day since the dailerfile table creation

	*****************************************************************/

	SELECT DISTINCT
			d.Account
		,	CAST(d.RunDate AS DATE) AS RunDate
		,	CAST('Untouched Loans' AS VARCHAR(30)) AS CallType
	INTO	#FullList
	FROM	MARS.dbo.dialerfile d
	WHERE	CAST(d.RunDate AS DATE) >= @WorkingDate
	ORDER BY CAST(d.RunDate AS DATE) 

	/*****************************************************************
	Pull list of accounts and their Category for Manual Outbound and Inbound

	*****************************************************************/

	SELECT	l.Account
		,	CAST(lc.MyDateTime AS DATE) AS MyDateTime
		,	CASE WHEN lc.Category IN (	'AI - Attorney Contact on Inbound Call'
										,	'BI - Borrower Contact on Inbound Call'
										,	'TC - Authorized 3rd Party on Inbound'
										,	'NA - Non-Authorized 3rd Party on Inbound Call') 
						THEN 'InboundContact'
				 WHEN lc.Category IN (	'NO - Outbound Attempt/no Contact with Borrower'
										,	'TA - Outbound Attempt/no Contact with Authorized NO3'
										,	'AA - Outbound Attempt/no Contact with Attorney') 
						THEN 'OutboundAttemptNotOnDialer' 
				 WHEN lc.Category IN (	'TO - Authorized 3rd Party on Outbound Call'
										,	'AO - Attorney Contact on Outbound Call'
										,	'BO - Borrower Contact on Outbound Call') 
						THEN 'OutboundContactNotOnDialer'
			END AS CallType
	INTO	#Manual
	FROM	MARS.dbo.LoansContacts lc (NOLOCK)
	LEFT JOIN MARS.dbo.vw_Loans l
		ON	l.RecID = lc.LoanRecID
	WHERE lc.MyDateTime >= @WorkingDate
		AND lc.Category IN (	'NO - Outbound Attempt/no Contact with Borrower'
							,	'TA - Outbound Attempt/no Contact with Authorized NO3'
							,	'AA - Outbound Attempt/no Contact with Attorney'
							,	'TO - Authorized 3rd Party on Outbound Call'
							,	'AO - Attorney Contact on Outbound Call'
							,	'BO - Borrower Contact on Outbound Call'
							,	'AI - Attorney Contact on Inbound Call'
							,	'BI - Borrower Contact on Inbound Call'
							,	'TC - Authorized 3rd Party on Inbound'
							,	'NA - Non-Authorized 3rd Party on Inbound Call') 


	/*****************************************************************
	Gather all Accounts and their Phone Numbers per every archive date
		and associate those with the InContact table records to get account
	*****************************************************************/
	SELECT distinct cast(ArchiveDate as date) ArchiveDate 
		,	account
		,	mars.dbo.fn_RemoveNonNumericCharacters(phone) AS phone
	INTO #BorrowerPhoneNumbers
	FROM 
	   (SELECT account, l.ArchiveDate, l.Home, l.Work, l.Mobil, c.PhoneHome, c.PhoneWork, c.PhoneCell
		FROM mars_dw.dbo.vw_Loans_Archive l  
			LEFT JOIN dbo.vw_CoBorrowers_Archive c
				ON	c.[LoanRecID] = l.RecID
				AND c.ArchiveDate = l.ArchiveDate
		WHERE CAST(l.ArchiveDate AS DATE) >= @WorkingDate) p
	UNPIVOT
	   (phone for phones IN 
		  (home, work, mobil, PhoneHome, PhoneWork, PhoneCell)
	)AS unpvt
	where phone <> ''


	--------------------------------------------

	SELECT	b.account
		,	b.ArchiveDate
		,	CASE WHEN ISNULL(ddf.skill_name,'') IN ('IB Collections Spanish', 'IB Collections')
					THEN 'InboundContact'
				 WHEN ISNULL(ddf.skill_name,'') IN ('OB Manual', 'Outbound Lists 2')
							AND ddf.Total_Time <> 0
					THEN 'OutboundContact'
				 WHEN ISNULL(ddf.skill_name,'') IN ('OB Manual', 'Outbound Lists 2')
							AND ddf.Total_Time = 0
					THEN 'OutboundAttempt'
			END AS CallType
	INTO #ThroughDialer
	FROM	mars_dw.dbo.InContactDialerFile ddf
		LEFT JOIN	#BorrowerPhoneNumbers b
			ON	b.ArchiveDate = ddf.[start_date]
			AND b.phone = ddf.ani_dialnum
	WHERE	ISNULL(ddf.skill_name,'') IN ('IB Collections Spanish', 'IB Collections', 'OB Manual', 'Outbound Lists 2')
		AND	b.Account IS NOT NULL
		AND CAST(ddf.[start_date] AS DATE) >= @WorkingDate


	/*****************************************************************
	  Union the Manual and through dialer records together
	*****************************************************************/

	SELECT	d.Account
		,	d.ArchiveDate
		,	d.CallType
	INTO #Results
	FROM #ThroughDialer d

	UNION ALL

	SELECT	m.Account
		,	m.MyDateTime AS ArchiveDate
		,	m.CallType
	FROM #Manual m
	WHERE NOT EXISTS (	SELECT 'Here' 
						FROM	#ThroughDialer d
						WHERE	d.Account = m.Account
							AND	d.ArchiveDate = m.MyDateTime)


	/*****************************************************************
	Bring in the uncalled records from the #FullList from the dialerfile table
	*****************************************************************/
	INSERT INTO #Results
	SELECT	f.Account
		,	f.RunDate
		,	f.CallType
	FROM #FullList f
	WHERE NOT EXISTS (	SELECT	2
						FROM	#Results r
						WHERE	r.Account = f.Account
							AND	r.ArchiveDate = f.RunDate
					 )

					 
	/*****************************************************************
	 Create the Single record per account per day based on the ranking.
	*****************************************************************/

	SELECT	t.Account
		,	t.ArchiveDate
		,	t.CallType
	INTO #TopCallPerDay
	FROM (	
			SELECT	s.Account
				,	s.ArchiveDate
				,	s.CallType
				,	ROW_NUMBER() OVER (PARTITION BY s.Account, s.ArchiveDate ORDER BY r.CodeRank) AS RowNum
			FROM #Results s
				INNER JOIN #Ranking r
					ON	r.CodeDescr = s.CallType
		 ) t
	WHERE t.RowNum = 1
	ORDER BY ArchiveDate

	
	/*****************************************************************
	 Add the Carryover flag and return results
	*****************************************************************/

	SELECT	c.Account
		,	c.ArchiveDate
		,	c.CallType
		,	CASE WHEN c1.Account IS NULL 
					THEN 0
				 ELSE 1
			END AS Carryover
	--INTO #ResultswithCarryover
	FROM #TopCallPerDay c
		LEFT JOIN #TopCallPerDay c1
			ON	c1.Account = c.Account
			AND c1.ArchiveDate = DATEADD(DD,-1,c.ArchiveDate)
	WHERE	c.ArchiveDate < CAST(GETDATE() AS DATE)
	ORDER BY	c.ArchiveDate
			,	CASE WHEN c1.Account IS NULL 
						THEN 0
					 ELSE 1
				END
			,	c.Account			 



END
