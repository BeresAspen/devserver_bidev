USE [Support]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('di_IntacctTaxInvoiceDetailDownload') IS NOT NULL
	DROP PROC di_IntacctTaxInvoiceDetailDownload;
GO


CREATE PROCEDURE [dbo].[di_IntacctTaxInvoiceDetailDownload]
AS

/* ********************************************************************************************************* *
   Procedure Name  :	di_IntacctTaxInvoiceDetailDownload
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	
   
   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date			Author				Description
   --------		-----------			------------
	12/06/2017	Benjacob Beres		Modified the process to write the HistoryID back to the Queue table
	12/11/2017  Benjacob Beres		modified the code to account for duplicates and remove history from the Invoices table

*  ********************************************************************************************************* */

BEGIN 
SET NOCOUNT ON
DECLARE @Application varchar(128)='Intacct Invoice Sync'
DECLARE @Version     varchar(10) = ''

BEGIN TRY
	IF OBJECT_ID('tempdb..#IntacctPTData') IS NOT NULL
		DROP TABLE #IntacctPTData;
	IF OBJECT_ID('tempdb..#BillStaging') IS NOT NULL
		DROP TABLE #BillStaging;


	SELECT	ih.RECORDNO AS HeaderRecID
		,	id.RECORDNO AS DetailRecID
		,	ih.CUSTVENDID AS IntacctVendorID
		,	v.MARSVendorID
		,	id.CLASSID AS MARSAccount
		,	ih.DOCNO AS IntacctInvoiceNbr
		,	ih.INVOICE_DATE AS InvoiceDate
		,	CAST(NULL AS DATE) AS PaymentDate
		,	ih.TRX_TOTAL AS BillAmount
		,	id.TOTAL AS LineAmount
		,	CASE WHEN ih.InvoiceStatus = 'Closed' THEN 'Pending' ELSE ih.InvoiceStatus END AS InvoiceStatus
		,	CAST(CASE WHEN ISNUMERIC(id.TAX_CYCLE) = 1 THEN id.TAX_CYCLE ELSE NULL END AS INT) AS TaxCycle
		,	CAST(CASE WHEN ISNUMERIC(id.TAX_YEAR) = 1 THEN id.TAX_YEAR ELSE NULL END AS INT) AS TaxYear
		,	CAST(NULL AS INT) AS VendorPaymentsQueueID
		,	CAST(NULL AS DATE) AS EconomicLossDate
		,	CAST(NULL AS VARCHAR(30)) AS AccountNbr
	INTO #IntacctPTData
	--SELECT *
	FROM Support.dbo.StagingIntacctInvoicesHeader ih
	JOIN Support.dbo.StagingIntacctInvoicesDetail id 
		ON	id.DOCHDRNO = ih.RECORDNO
	JOIN IntacctDM.dbo.Vendors v
		ON	v.IntacctVendorID = ih.CUSTVENDID
	WHERE	id.DEPARTMENTNAME = 'Taxes'


	---------------------------------------------------------------------------------------
	--Overwrite PT Staging records with more current statuses from Bill recs
	---------------------------------------------------------------------------------------


	SELECT	ipt.HeaderRecID
		,	ipt.DetailRecID
		,	ipt.IntacctVendorID
		,	ipt.MARSVendorID
		,	ipt.MARSAccount
		,	ipt.IntacctInvoiceNbr
		,	ipt.InvoiceDate
		,	sb.WHENPAID AS PaymentDate
		,	0 AS TotalAmount
		,	ipt.LineAmount AS Amount
		,	CASE WHEN sb.InvoiceStatus = 'Paid' THEN 'Paid' ELSE 'Pending' END AS InvoiceStatus
		,	ipt.TaxCycle
		,	ipt.TaxYear
		,	CAST(NULL AS INT) AS VendorPaymentsQueueID
		,	CAST(NULL AS DATE) AS EconomicLossDate
		,	CAST(NULL AS VARCHAR(30)) AS AccountNbr
	INTO #BillStaging
	--select *
	FROM Support.dbo.StagingIntacctInvoicesBill sb	
	JOIN #IntacctPTData ipt
		ON	ipt.IntacctInvoiceNbr = SUBSTRING(sb.DESCRIPTION2, 24, 9)
	WHERE	LEFT(sb.DESCRIPTION2,23) = 'Purchasing Transaction-'
		AND sb.InvoiceStatus <> ipt.InvoiceStatus


	--Capture Bills that have a Purchase Tranasction from previous days.
	INSERT INTO #BillStaging
	SELECT  i.HeaderRecID
		,	i.DetailRecID
		,	i.IntacctVendorID
		,	v.MARSVendorID
		,	i.MARSAccount
		,	i.IntacctInvoiceNbr
		,	i.InvoiceDate
		,	sb.WHENPAID AS PaymentDate
		,	0 AS TotalAmount
		,	i.Amount AS Amount
		,	CASE WHEN sb.InvoiceStatus = 'Paid' THEN 'Paid' ELSE 'Pending' END AS InvoiceStatus
		,	i.TaxCycle
		,	i.TaxYear
		,	i.VendorPaymentsQueueID
		,	i.EconomicLossDate
		,	i.AccountNbr
	FROM Support.dbo.StagingIntacctInvoicesBill sb	
	JOIN IntacctDM.dbo.Invoices i
		ON	i.IntacctInvoiceNbr = SUBSTRING(sb.DESCRIPTION2, 24, 9)
	JOIN IntacctDM.dbo.Vendors v
		ON	v.IntacctVendorID = i.IntacctVendorID
	LEFT JOIN #BillStaging bs
		ON	bs.IntacctInvoiceNbr = SUBSTRING(sb.DESCRIPTION2, 24, 9)
	WHERE	LEFT(sb.DESCRIPTION2,23) = 'Purchasing Transaction-'
		AND i.InvoiceStatus <> CASE WHEN sb.InvoiceStatus = 'Paid' THEN 'Paid' ELSE 'Pending' END
		AND bs.IntacctInvoiceNbr IS NULL


	DELETE 
	FROM #IntacctPTData 
	WHERE IntacctInvoiceNbr IN (	SELECT bs.IntacctInvoiceNbr
										FROM #BillStaging bs
									)


	INSERT INTO #IntacctPTData
	SELECT *
	FROM #BillStaging


	--add queueid where needed
	UPDATE iptd
		SET VendorPaymentsQueueID = vpq.VendorPaymentsQueueId
		,	EconomicLossDate = vps.EconomicLossDate
		,	AccountNbr = LEFT(PC.AccountNumber, 30)
	--SELECT vpq.VendorPaymentsQueueId,vps.EconomicLossDate,PC.AccountNumber,vpq.DueAmount,iptd.LineAmount,iptd.MARSAccount,pc.REOPropertyID,vl.Account,*
	FROM #IntacctPTData iptd
	JOIN MARS.dbo.VendorsPaymentSchedule vps
		ON	vps.VendorId = iptd.MARSVendorID
		AND	vps.TaxYear = iptd.TaxYear
		AND vps.PaymentCycle = iptd.TaxCycle
		AND (VPS.Status = 'Active' or VPS.Status = 'In-Progress') 
	JOIN MARS.dbo.VendorPaymentsQueue vpq 
		ON	vpq.VendorPaymentScheduleId = vps.VendorPaymentScheduleId
	JOIN MARS.dbo.PropertyChildMaps PC
		ON	PC.PropertyChildMapId = VPQ.PropertyChildMapId
		AND PC.IsActive = 1
	LEFT JOIN MARS.dbo.vw_LoansProperties vlp
		ON	vlp.PropertyId = pc.PropertyId
	LEFT JOIN MARS.dbo.vw_Loans vl
		ON	vl.RecID = vlp.LoanRecID
	WHERE	iptd.MARSAccount =	CASE WHEN ISNUMERIC(LEFT(ISNULL(iptd.MARSAccount,'1'),1)) = 0
										THEN pc.REOPropertyID
									  ELSE ISNULL(vl.Account,'')
								END
		AND vpq.DueAmount = CAST(iptd.LineAmount AS DECIMAL(17,2))


	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------


	--Update VendorPaymentQueue if the status change is paid or voided. All other status keep CanProcess to P
	UPDATE VPQ
	SET		CanProcess = CASE WHEN iptd.InvoiceStatus LIKE '%Paid%' THEN 'H' ELSE 'N' END
		,	UpdatedBy = SYSTEM_USER               
		,	UpdatedDate = GETDATE() 
	--SELECT *
	FROM MARS.dbo.VendorPaymentsQueue vpq
	JOIN #IntacctPTData iptd
		ON	vpq.VendorPaymentsQueueId = iptd.VendorPaymentsQueueId
	WHERE	ISNULL(vpq.CanProcess, '') <> CASE WHEN iptd.InvoiceStatus LIKE '%Paid%' THEN 'H' ELSE 'N' END
		AND (	iptd.InvoiceStatus LIKE '%Paid%'
			 OR	iptd.InvoiceStatus LIKE '%Void%'
			)

		
	--Update VendorPaymentHistory if the status change is paid
	UPDATE vph
	SET		PaidDate = iptd.PaymentDate 
		,	PaymentStatus = 'Paid'
		,	UpdatedBy = SYSTEM_USER               
		,	UpdateDate = GETDATE() 
	--SELECT *
	FROM MARS.dbo.VendorPaymentsHistory vph
	JOIN #IntacctPTData iptd 
		ON	vph.VendorPaymentsQueueId = iptd.VendorPaymentsQueueId
	WHERE	iptd.InvoiceStatus LIKE '%Paid%'
		AND ISNULL(vph.PaidDate,'01/01/1905') <> iptd.PaymentDate 
	
	

	-------------------------------------------------------------------------------------------------
	--NEW INVOICE FROM INTACCT
	-------------------------------------------------------------------------------------------------

	--Insert Record into VendorPaymentHistory table 

	INSERT INTO MARS.dbo.VendorPaymentsHistory                                       
			(VendorPaymentsQueueId,AssetVendorMapId, PaymentStatus,PaymentRequestDate,PaidDate,PaidAmount, AFRRequestId,PaymentDueDate,DueAmount,InsertedBy,InsertedDate)                                          
	SELECT	VPQ.VendorPaymentsQueueId
		,	PCM.AssetVendorMapId
		,	CASE WHEN iptd.InvoiceStatus LIKE '%Paid%' 
					THEN 'Paid'                            
				 ELSE 'Pending'                            
			END AS PaymentStatus
		,	VPQ.DueDate as PaymentRequestDate
		,	iptd.PaymentDate as  PaidDate
		,	iptd.LineAmount  as PaidAmount
		,	iptd.IntacctInvoiceNbr AS AFRRequestId
		,	VPQ.DueDate AS PaymentDueDate
		,	VPQ.DueAmount
		,	SYSTEM_USER AS InsertedBy
		,	GetDate() AS InsertedDate   
		--select *                                           
	FROM #IntacctPTData iptd   
	JOIN MARS.dbo.VendorPaymentsQueue VPQ
		ON	VPQ.VendorPaymentsQueueId = iptd.VendorPaymentsQueueId
	INNER JOIN MARS.dbo.PropertyChildMaps PCM
		ON	VPQ.PropertyChildMapId = PCM.PropertyChildMapId
	LEFT JOIN MARS.dbo.VendorPaymentsHistory vph 
		ON	vph.VendorPaymentsQueueId = iptd.VendorPaymentsQueueId
	WHERE vph.VendorPaymentsQueueId IS NULL



	-------------------------------------------------------------------------------------------------
	--Update VendorQueue table with recent PaymentHitoryID for proper linking
	-------------------------------------------------------------------------------------------------

	UPDATE VPQ
	SET		VendorPaymentsHistoryId = vph.VendorPaymentsHistoryId
		,	UpdatedBy = SYSTEM_USER               
		,	UpdatedDate = GETDATE() 
	--SELECT *
	FROM MARS.dbo.VendorPaymentsQueue vpq
	JOIN (	SELECT	vph.VendorPaymentsQueueId
				,	MAX(vph.VendorPaymentsHistoryId) AS VendorPaymentsHistoryId
			FROM #IntacctPTData iptd
			JOIN MARS.dbo.VendorPaymentsHistory vph 
				ON	vph.VendorPaymentsQueueId = iptd.VendorPaymentsQueueId
			GROUP BY vph.VendorPaymentsQueueId
		 ) vph
		 ON	vph.VendorPaymentsQueueId = vpq.VendorPaymentsQueueId
	WHERE ISNULL(vpq.VendorPaymentsHistoryId,0) <> vph.VendorPaymentsHistoryId

	---------------------------------------------------------------------------------------
	---Update Invoices Table --------------------------------------------------------------
	---------------------------------------------------------------------------------------


	--Update the Invoice Record when there is a change to existing record
	UPDATE i
		SET	IntacctVendorID = iptd.IntacctVendorID
		,	MARSAccount = iptd.MARSAccount
		,	InvoiceDate = iptd.InvoiceDate
		,	PaymentDate = iptd.PaymentDate
		,	Amount = iptd.LineAmount
		,	InvoiceStatus = iptd.InvoiceStatus
		,	TaxCycle = iptd.TaxCycle
		,	TaxYear = iptd.TaxYear
		,	UpdatedBy = 'system' 
		,	UpdatedDate = GETDATE() 
	FROM IntacctDM.dbo.Invoices i
	JOIN #IntacctPTData iptd 
		ON	i.IntacctInvoiceNbr = iptd.IntacctInvoiceNbr
		AND i.VendorPaymentsQueueId = iptd.VendorPaymentsQueueId
	WHERE	iptd.IntacctVendorID <> i.IntacctVendorID
		OR  iptd.MARSAccount <> i.MARSAccount
		OR  iptd.InvoiceDate <> i.InvoiceDate
		OR  iptd.PaymentDate <> i.PaymentDate
		OR  iptd.LineAmount <> i.Amount
		OR  iptd.InvoiceStatus <> i.InvoiceStatus
		OR  iptd.TaxCycle <> i.TaxCycle



	--Insert new Invoice record when it didn't exist before.
	INSERT INTO IntacctDM.dbo.Invoices
					(HeaderRecID, DetailRecID, IntacctVendorID, MARSAccount, IntacctInvoiceNbr, InvoiceDate, PaymentDate, EconomicLossDate, AccountNbr, Amount, InvoiceStatus, TaxCycle, TaxYear, VendorPaymentsQueueID, InsertedBy, InsertedDate)
	SELECT	i.HeaderRecID
		,	i.DetailRecID
		,	i.IntacctVendorID
		,	i.MARSAccount
		,	i.IntacctInvoiceNbr
		,	i.InvoiceDate
		,	i.PaymentDate
		,	i.EconomicLossDate
		,	i.AccountNbr
		,	i.LineAmount
		,	i.InvoiceStatus
		,	i.TaxCycle
		,	i.TaxYear
		,	i.VendorPaymentsQueueID
		,	SYSTEM_USER               
		,	GETDATE() 
	FROM #IntacctPTData i
	LEFT JOIN IntacctDM.dbo.Invoices ii
		ON	ii.HeaderRecID = i.HeaderRecID
		AND	ii.DetailRecID = i.DetailRecID
	WHERE ii.HeaderRecID IS NULL

END TRY

BEGIN CATCH
	
	DECLARE @Err           INT
	DECLARE @ErrorMessage  Varchar(Max)
	DECLARE @ErrorLine     Varchar(128)
	DECLARE @Workstation   VarChar(128)
	DECLARE @Proc          VarChar(128)
	DECLARE @User		   VARCHAR(128)

	SET @User = SYSTEM_USER

	--IF @@TRANCOUNT > 0
	--	ROLLBACK TRANSACTION 
    

	IF Error_Number() IS NULL 
		SET @Err =0;
	Else
		SET @Err = Error_Number();


	SET @ErrorMessage = Error_Message()
	SET @ErrorLine    = 'SP Line Number: ' + Cast(Error_Line() as varchar(10))
	SET @Workstation  = HOST_NAME()
	SET @Proc         = OBJECT_NAME(@@ProcID)

	EXEC Applications.dbo.di_ErrorLog	@Application ,@Version ,@Err, @ErrorMessage, @Proc, @ErrorLine, @User , @Workstation;
END CATCH   


END


GO


