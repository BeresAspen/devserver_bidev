USE Support;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('di_IntacctCashProCheckFormatting') IS NOT NULL
	DROP PROCEDURE di_IntacctCashProCheckFormatting;
GO


CREATE PROCEDURE dbo.di_IntacctCashProCheckFormatting 
							(	@FileName VARCHAR(50)
							)
						--(	@ProcDate DATETIME OUTPUT
						--,	@CashProBatchID INT OUTPUT
						--)
AS
BEGIN

/********************************************************************************************************** *
   Procedure Name  :	di_IntacctCashProCheckFormatting
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	11/17/2017

   *********************************************************************************************************
   **         Change History                                                                              **
   *********************************************************************************************************

   Date			Author				Description
   --------		-----------			------------
   
************************************************************************************************************ */

DECLARE @CurrentDate Datetime
SET @CurrentDate = GETDATE()
SET @FileName = ISNULL(@FileName, CONVERT(VARCHAR(10), @CurrentDate, 112) + '_' + REPLACE(CONVERT(VARCHAR(10), @CurrentDate, 108),':',''))


IF OBJECT_ID('tempdb..#CashProImportWorking') IS NOT NULL
	DROP TABLE #CashProImportWorking;
IF OBJECT_ID('tempdb..#MaxCheckNo') IS NOT NULL
	DROP TABLE #MaxCheckNo;
IF OBJECT_ID('tempdb..#CheckNoStarting') IS NOT NULL
	DROP TABLE #CheckNoStarting;
IF OBJECT_ID('tempdb..#CashTRNRecs') IS NOT NULL
	DROP TABLE #CashTRNRecs;


TRUNCATE TABLE Support.dbo.IntacctToCashProExport;

SELECT	ap.RECORDNO AS TRNRecord
	,	apd.ENTRYKEY AS RDSRecord
	--,	ap.RECORDNO * 10000000 + apd.ENTRYKEY AS TransactionRef
	,	RIGHT('0000000' + ca.BANKACCOUNTNO,12) AS BANKACCOUNTNO
	,	ca.ROUTINGNO
	,	CA.BRANCHID
	,	ca.[CHECKINFOCONTACT.COMPANYNAME]
	,	ca.[CHECKINFOCONTACT.MAILADDRESS.ADDRESS1]
	,	ca.[CHECKINFOCONTACT.MAILADDRESS.ADDRESS2]
	,	ca.[CHECKINFOCONTACT.MAILADDRESS.CITY]
	,	ca.[CHECKINFOCONTACT.MAILADDRESS.STATE]
	,	ca.[CHECKINFOCONTACT.MAILADDRESS.ZIP]
	,	[DISPLAYCONTACT.PRINTAS] AS VENDORNAME
	,	ap.VENDORID
	,	[DISPLAYCONTACT.MAILADDRESS.ADDRESS1] AS VendorAddress1
	,	[DISPLAYCONTACT.MAILADDRESS.ADDRESS2] AS VendorAddress2
	,	[DISPLAYCONTACT.MAILADDRESS.CITY] AS VendorCity
	,	[DISPLAYCONTACT.MAILADDRESS.STATE] AS VendorState
	,	[DISPLAYCONTACT.MAILADDRESS.ZIP] AS VendorZip
	,	CONVERT(VARCHAR, @CurrentDate,112) AS CurrentDate
	,	CAST(CASE WHEN CAST(ap.TOTALDUE AS DECIMAL(13,2)) = 0 THEN ap.TOTALPAID ELSE ap.TOTALDUE END AS DECIMAL(17,2)) AS TOTALDUE
	,	CAST(apd.PAYMENTAMOUNT AS DECIMAL(15,2)) AS LINEAMOUNT
	,	ab.RECORDID --Bill Number
	,	abi.ENTRYDESCRIPTION AS ItemReference  --memo
INTO #CashProImportWorking
--select *
FROM IntacctDM.dbo.APPymt ap
JOIN IntacctDM.dbo.APPymtDetail apd
	ON	apd.PAYMENTKEY = ap.RECORDNO
JOIN IntacctDM.dbo.APBill ab
	ON	ab.RECORDNO = apd.RECORDKEY 
JOIN IntacctDM.dbo.CheckingAccount ca
	ON	ca.BANKACCOUNTID = ap.FINANCIALENTITY
JOIN IntacctDM.dbo.Vendor v 
	ON	ap.VendorID = v.VENDORID
JOIN IntacctDM.dbo.APBillItem abi
	ON	abi.RECORDNO = apd.ENTRYKEY
WHERE	CAST(DATEADD(HH,-8,CAST(ap.WHENMODIFIED AS DATETIME)) AS DATE) >=  CAST(DATEADD(DD,-21,GETDATE()) AS DATE)	--remove for prod	--if the record was created on the previous day
	AND ap.[STATE] = 'S'
	AND ap.PAYMENTMETHOD = 'Cash' 
	AND ab.PAYMENT_TYPE_HEADER = 'BoA Check'
	AND ap.DOCNUMBER IS NULL  --Add back in for prod



--Join the RECORDID into one for the memo field of the check.

SELECT distinct c2.TRNRecord
	, CAST(LEFT(stuff ( ( SELECT ', ' + c1.RECORDID 
				FROM #CashProImportWorking c1 
				WHERE c1.TRNRecord = c2.TRNRecord FOR XML PATH ( '' ) ) , 1 , 2 , '' ) 
				,140) AS VARCHAR(140)) AS Memo
INTO #TRNMemo
FROM #CashProImportWorking c2



--Find Max Check Number
SELECT	RIGHT('0000000' + ca.BANKACCOUNTNO,12) AS BANKACCOUNTNO
	,	MAX(ISNULL(ap.DOCNUMBER, 0)) AS MaxCheckNo
INTO #MaxCheckNo
FROM IntacctDM.dbo.CheckingAccount ca
LEFT JOIN IntacctDM.dbo.APPymt ap
	ON	ca.BANKACCOUNTID = ap.FINANCIALENTITY
	AND ISNUMERIC(ap.DOCNUMBER) = 1
WHERE ISNULL(ap.DOCNUMBER, 0) < CAST(200000 AS BIGINT)
GROUP BY RIGHT('0000000' + ca.BANKACCOUNTNO,12)


SELECT	mcn.BANKACCOUNTNO
	,	CASE WHEN mcn.MaxCheckNo < ibn.StartingCheckNo 
				THEN ibn.StartingCheckNo
			 ELSE mcn.MaxCheckNo
		END AS StartingCheckNo
INTO #CheckNoStarting
FROM #MaxCheckNo mcn
JOIN IntacctDM.dbo.IntacctBankCheckStartingNumber ibn
	ON	ibn.AccountNumber = mcn.BANKACCOUNTNO


--HEADER
INSERT INTO Support.dbo.IntacctToCashProExport (TransactionRef, RowType, RowData, CPFileName)
SELECT	0 AS TransactionRef
	,	1 AS RowType
	,	'BULKCSV,Gregory Funding LLC,aspencap,A613,'
		+ CONVERT(VARCHAR(10), @CurrentDate, 112) + ','
		+ @FileName
		+'.csv,TEST'  --+'.csv,PROD'
	,	@FileName + '.csv' AS CPFileName


--TRN Data
SELECT DISTINCT
		cp.TRNRecord
	,	cp.TOTALDUE AS RecAmount
	,	cns.StartingCheckNo 
	,	cp.BANKACCOUNTNO
	,	cp.ROUTINGNO
	,	CAST(cp.TRNRecord * CAST(RAND() * 100000000 AS INT) AS VARCHAR(16)) AS RandRecNum
	,	cp.[CHECKINFOCONTACT.COMPANYNAME]
	,	cp.[CHECKINFOCONTACT.MAILADDRESS.ADDRESS1]
	,	cp.[CHECKINFOCONTACT.MAILADDRESS.ADDRESS2]
	,	cp.[CHECKINFOCONTACT.MAILADDRESS.CITY]
	,	cp.[CHECKINFOCONTACT.MAILADDRESS.STATE]
	,	cp.[CHECKINFOCONTACT.MAILADDRESS.ZIP]
	,	cp.VENDORNAME
	,	cp.VENDORID
	,	cp.VendorAddress1
	,	cp.VendorAddress2
	,	cp.VendorCity
	,	cp.VendorState
	,	cp.VendorZip
	,	cp.CurrentDate
	,	cp.TOTALDUE
	,	tm.Memo
	--,	cp.RECORDID
INTO #CashTRNRecs
--select *
FROM	#CashProImportWorking cp
JOIN	#TRNMemo tm
	ON	tm.TRNRecord = cp.TRNRecord
JOIN	#CheckNoStarting cns
	ON	cns.BANKACCOUNTNO = cp.BANKACCOUNTNO


INSERT INTO Support.dbo.IntacctToCashProExport 
					(TransactionRef, RowType, RecAmount, CheckNo, RowData)
SELECT DISTINCT
			t.TRNRecord AS TransactionRef
		,	3 AS RowType
		,	t.TOTALDUE AS RecAmount
		,	t.CHECKNO
		,	'"TRN",'
		+	'"CHECK",'
		+	',,,'
		+	'"'  + ISNULL(t.BANKACCOUNTNO,'') + '",'
		+	'"'  + ISNULL(t.ROUTINGNO,'') + '",'
		+	'"USD","PAY","C",,,,'
		+	'"' + ISNULL(RandRecNum,'') + '",'  
		+	','
		+	'"'  + ISNULL(t.[CHECKINFOCONTACT.COMPANYNAME],'') + '",,'		--Ordering Party Name
		+	'"'  + ISNULL(t.[CHECKINFOCONTACT.MAILADDRESS.ADDRESS1],'') + '",'
		+	'"'  + ISNULL(t.[CHECKINFOCONTACT.MAILADDRESS.ADDRESS2],'') + '",'
		+	'"'  + ISNULL(t.[CHECKINFOCONTACT.MAILADDRESS.CITY],'') + '",'
		+	'"'  + ISNULL(t.[CHECKINFOCONTACT.MAILADDRESS.STATE],'') + '",'
		+	'"'  + ISNULL(CAST(t.[CHECKINFOCONTACT.MAILADDRESS.ZIP] AS VARCHAR(10)),'') + '","US",'
		+	'"'  + ISNULL(t.VENDORNAME,'') + '",'
		+	'"'  + ISNULL(CAST(t.VENDORID AS VARCHAR(15)),'') + '",'
		+	'"'  + ISNULL(t.VendorAddress1,'') + '",'
		+	'"'  + ISNULL(t.VendorAddress2,'') + '",'
		+	'"'  + ISNULL(t.VendorCity,'') + '",'
		+	'"'  + ISNULL(t.VendorState,'') + '",'
		+	'"'  + ISNULL(t.VendorZip,'') + '",'
		+	'"US",'
		+	'"'  + t.CurrentDate + '",,'
		+	'"USD",'
		+	'"'  + ISNULL(CAST(t.TOTALDUE AS VARCHAR(17)),'') + '",,'
		+	'"' + CAST(t.CHECKNO AS VARCHAR(10)) + '",'
		+	',,,,,,,,,,,,,,,,"PQ",'
		+	'"'  + ISNULL(CAST(t.Memo AS VARCHAR(140)),'') + '"'
		--+	',,,,,,,,,,,,,,,,,,,,'
FROM (	SELECT	*
			,	StartingCheckNo + ROW_NUMBER() OVER (PARTITION BY BANKACCOUNTNO ORDER BY TRNRecord) AS CHECKNO
		FROM #CashTRNRecs
		) t



--RDS Data
INSERT INTO Support.dbo.IntacctToCashProExport (TransactionRef, RowType, RecAmount, RowData)
SELECT		TRNRecord AS TransactionRef
		,	5 AS RowType
		,	cp.LINEAMOUNT * 2 AS RecAmount
		,	'"RDS",'
		+	'"INV",'
		+	'"'  + ISNULL(cp.RECORDID,'') + '",'
		+	'"'  + ISNULL(cp.CurrentDate,'') + '",'
		+	'"'  + ISNULL(CAST(cp.LINEAMOUNT AS VARCHAR(17)),'') + '",'
		+	'"'  + ISNULL(CAST(cp.LINEAMOUNT AS VARCHAR(17)),'') + '"'
		+	',"","","PO","-0.00",'
		+	'"'  + ISNULL(cp.ItemReference,'') + '",'
		+	'"",,,,,,,"",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,'
FROM	#CashProImportWorking cp

--Footer
INSERT INTO Support.dbo.IntacctToCashProExport (TransactionRef, RowType, RowData)
SELECT  999999999999
	,	9 AS RowType
	,	'BULKCSVTRAILER,'
		+ CAST(cp.TransRef AS VARCHAR(10)) + ','
		+ REPLACE(CAST(cp.SumTotal AS VARCHAR(20)),'.','')
FROM (
		SELECT COUNT(cp.RowType) AS TransRef
			,	SUM(cp.RecAmount) AS SumTotal
		FROM Support.dbo.IntacctToCashProExport cp
		WHERE RowType = 3
	 ) cp



/*
--Code to pull the extract for the csv file

SELECT RowData
FROM Support.dbo.IntacctToCashProExport
ORDER BY TransactionRef, RowType

*/

/*
--Code to pull parameters for the batch call in the SSIS package

SELECT DISTINCT
		CAST(TransactionRef AS VARCHAR(10)) AS APPayRECORDID
	,	CAST(CheckNo AS VARCHAR(10)) AS CheckNo
FROM Support.dbo.IntacctToCashProExport
WHERE RowType = 3
ORDER BY CAST(TransactionRef AS VARCHAR(10))


--code to count the number of checks
SELECT COUNT(CheckNo) AS CheckNoCount
FROM Support.dbo.IntacctToCashProExport
WHERE RowType = 3


*/

END