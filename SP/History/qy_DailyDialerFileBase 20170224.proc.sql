USE [MARS]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.GetDailyDialerFile') IS NOT NULL
	DROP PROCEDURE dbo.GetDailyDialerFile;
GO

CREATE PROCEDURE [dbo].[GetDailyDialerFile]
AS

/* ********************************************************************************************************* *
   Procedure Name  :	GetDailyDialerFile
   Business Analyis:	
   Project/Process :   
   Description     :	Collection Queue Strategy Project
   Author          :	Nathan Reed
   Create Date     :	

   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date			Author				Description
   --------		-----------			------------
   
*  ********************************************************************************************************* */


if object_id('tempdb..#ActiveDelq') is not null drop table #ActiveDelq
if object_id('tempdb..#NoContact') is not null drop table #NoContact
if object_id('tempdb..#lastContactDate') is not null drop table #lastContactDate
if object_id('tempdb..#noCon2') is not null drop table #noCon2
if object_id('tempdb..#lastcontactInfo') is not null drop table #lastcontactInfo
if object_id('tempdb..#LastContactAndAttemptDate') is not null drop table #LastContactAndAttemptDate
if object_id('tempdb..#report') is not null drop table #report
if object_id('tempdb..#dialerfile') is not null drop table #dialerfile
select
	--nextduedate, 
	recid,
	DATEDIFF(D,NextDueDate,GETDATE()) dayspastdue
into #ActiveDelq
from 
	mars.dbo.vw_loans with(nolock)
	LEFT JOIN
(SELECT account, FieldValue AS BKDischarge FROM dbo.vw_CustomFieldsForLoans WHERE fieldname ='BK - Debt Discharged') discharge
ON
	discharge.Account = vw_Loans.Account
where 
	LoanStatus not in (
						'BK11',
						'BK11,FC',
						'BK12',
						'BK13',
						'BK13, DL',
						'BK13,FC',
						'BK7',
						'BK7, DL',
						'BK7,FC',
						'FC,DIL',
						'Inactive - REO/FC',
						'PRELIM',
						'Service Xfer',
						'Trailing Claims',
						'FC',
						'Paid in Full',
						'Paidoff', 
						'DIL'
						)
and LoanStatus not like '%Inactive%'
and LoanStatus not like 'BK13%'
and DATEDIFF(D,NextDueDate,GETDATE()) > 15
AND discharge.BKDischarge IS null
and isActive = 1

--select * from #ActiveDelq

select 
	a.RecID,a.dayspastdue,
	isnull(MAX(MyDateTime),'01-01-1900')  LastContactDate
into 
	#LastContactDate
from 
	#ActiveDelq a
left join
(select * from mars.dbo.vw_LoansContacts  with(nolock) where Category in (
'AI - Attorney Contact on Inbound Call',
'BI - Borrower Contact on Inbound Call',
'BO - Borrower Contact on Outbound Call',
'TC - Authorized 3rd Party on Inbound',
'TO - Authorized 3rd Party on Outbound Call'

) )b
on 
	a.RecID = b.LoanRecID

and LEFT(Rep, 1) <> '*'
group by 
a.RecID,a.dayspastdue

select 
	a.RecID,a.dayspastdue,LastContactDate,
	isnull(MAX(MyDateTime),'01-01-1900') LastAttemptDate
into 
	#LastContactAndAttemptDate
from 
	#LastContactDate a
left join
(select * from mars.dbo.vw_LoansContacts  with(nolock) where Category in (
'AA - Outbound Attempt/no Contact with Attorney',
'NO - Outbound Attempt/no Contact with Borrower',
'TA - Outbound Attempt/no Contact with Authorized NO3',
'AO - Attorney Contact on Outbound Call'

) )b
on 
	a.RecID = b.LoanRecID

and LEFT(Rep, 1) <> '*'
group by 
	a.RecID,a.dayspastdue,LastContactDate


select 
	GETDATE() AS RunDate,
	ROW_NUMBER() OVER(ORDER BY 
		case 
			when lp.state = 'NY' then 0
			else 1000
		end,
		case
			when jud.judicial = 'M' then 0
			else 1000
		end, 
		case 
			WHEN valuation.newstatus = '24for24' then -1  
			when valuation.newstatus = '12for12' then 0  
			when valuation.newstatus = '7for7' then 1
			when valuation.newstatus = '4f4-6f6' then 2 
			when valuation.newstatus in('other > 600','other < 600') then 3
			when valuation.newstatus in('NPL') then 4  
		else 1000 end,  
			dayspastdue desc,
			c.userlogon, 
			lastcontactdate , 
			LastAttemptDate  ) 
	queuenumber,
	b.LoanStatus,
	lp.state,
	jud.judicial,
	valuation.NewStatus,
	b.Account,
	c.UserLogon as AssignedRep,
	0 AS TAD,
	ISNULL(escrow.EscrowBalance,0) CurrentTrustBalance,
	b.NextDueDate,
	b.PmtPI+b.PmtImpound MonthlyPayment,
	ISNULL(BKDIS.FieldValue,'') AS [BK - Debt Discharged],
	dnc.ReasonForRestrictCommunication AS DNCDetail,
	CASE WHEN b.HomeSelect = 1      THEN ISNULL(dbo.fn_RemoveNonNumericCharacters(b.home)        ,'') ELSE '' END AS DialerHome,
	CASE WHEN b.WorkSelect = 1      THEN ISNULL(dbo.fn_RemoveNonNumericCharacters(b.Work)	      ,'') ELSE '' END AS DialerWork,
	CASE WHEN b.MobileSelect = 1    THEN ISNULL(dbo.fn_RemoveNonNumericCharacters(b.Mobil)		  ,'') ELSE '' END AS DialerMobile,
	CASE WHEN b.PhoneHomeSelect = 1 THEN ISNULL(dbo.fn_RemoveNonNumericCharacters(cobo.PhoneHome),'') ELSE '' END AS DialerCoBorrowerHome,
	CASE WHEN b.PhoneWorkSelect = 1 THEN ISNULL(dbo.fn_RemoveNonNumericCharacters(cobo.PhoneWork),'') ELSE '' END AS DialerCoBorrowerWork,
	CASE WHEN b.PhoneCellSelect = 1 THEN ISNULL(dbo.fn_RemoveNonNumericCharacters(cobo.PhoneCell),'') ELSE '' END AS DialerCoBorrowerMobile,
	ISNULL(dbo.fn_RemoveNonNumericCharacters(b.home)        ,'') Home,
	ISNULL(dbo.fn_RemoveNonNumericCharacters(b.Work)	    ,'') Work,
	ISNULL(dbo.fn_RemoveNonNumericCharacters(b.Mobil)		,'') Mobile,
	ISNULL(dbo.fn_RemoveNonNumericCharacters(cobo.PhoneHome),'') CoBorrowerHome,
	ISNULL(dbo.fn_RemoveNonNumericCharacters(cobo.PhoneWork),'') CoBorrowerWork,
	ISNULL(dbo.fn_RemoveNonNumericCharacters(cobo.PhoneCell),'') CoBorrowerMobile,
	CASE 
		WHEN b.MobileSelect = 1    THEN cast(dbo.fn_RemoveNonNumericCharacters(b.Mobil)	    as bigint) 
		WHEN b.HomeSelect = 1      THEN cast(dbo.fn_RemoveNonNumericCharacters(b.home)         as bigint) 
		WHEN b.PhoneCellSelect = 1 THEN cast(dbo.fn_RemoveNonNumericCharacters(cobo.PhoneCell) as bigint) 
		WHEN b.PhoneHomeSelect = 1 THEN cast(dbo.fn_RemoveNonNumericCharacters(cobo.PhoneHome) as bigint) 
		WHEN b.WorkSelect = 1      THEN cast(dbo.fn_RemoveNonNumericCharacters(b.Work)	        as bigint) 
		WHEN b.PhoneWorkSelect = 1 THEN cast(dbo.fn_RemoveNonNumericCharacters(cobo.PhoneWork) as bigint) 
	End as PrimaryPhone,
	CASE 
		WHEN b.HomeSelect = 1     THEN 1
		WHEN b.WorkSelect = 1  THEN 1 
		WHEN b.MobileSelect = 1 THEN 1
		WHEN b.PhoneHomeSelect = 1 THEN 1
		WHEN b.PhoneWorkSelect = 1 THEN 1
		WHEN b.PhoneCellSelect = 1 THEN 1
		ELSE 0
	END [TCPADialable]
from 
	#LastContactAndAttemptDate a
left join
	mars.dbo.vw_loans b
on
	a.RecID = b.RecID
left JOIN
	TMO_AspenYo..[TDS CoBorrowers] cobo
ON
	A.RecID = cobo.LoanRecID
	
left join
	mars.dbo.vw_ActiveLoanRepAssignments c
on 
	b.Account = c.LoanNumber
left join
	(select loanrecid,  MAX(mydatetime) LastLetterDate from mars.dbo.vw_LoansContacts where left(Category,2) = 'LT' group by LoanRecID) Letter
on
	a.RecID = Letter.LoanRecID
LEFT join
	(SELECT * FROM mars..vw_CustomFieldsForLoans WHERE fieldname LIKE 'BK - Debt Discharged') BKDIS
ON
	b.Account = BKDIS.Account
left join
	(select loanrecid, category,MAX(mydatetime) LastContactD from mars.dbo.vw_LoansContacts where Category in('AI - Attorney Contact on Inbound Call',
	'BI - Borrower Contact on Inbound Call',
	'BO - Borrower Contact on Outbound Call',
	'TC - Authorized 3rd Party on Inbound',
	'TO - Authorized 3rd Party on Outbound Call') group by LoanRecID, Category )LastContact
on
	LastContact.LastContactD = a.LastContactDate and lastcontact.LoanRecID = a.RecID
left join
	(select loanrecid, category,MAX(mydatetime) LastAttemptD from mars.dbo.vw_LoansContacts where Category in('AA - Outbound Attempt/no Contact with Attorney',
'NO - Outbound Attempt/no Contact with Borrower',
'TA - Outbound Attempt/no Contact with Authorized NO3',
'AO - Attorney Contact on Outbound Call') group by LoanRecID, Category )LastAttempt
on
	LastAttempt.LastAttemptD = a.LastAttemptDate and LastAttempt.LoanRecID = a.RecID
left join
	( select loanrecid, COUNT(*) ContactLast30 from mars.dbo.vw_LoansContacts where Category in('AI - Attorney Contact on Inbound Call',
	'BI - Borrower Contact on Inbound Call',
	'BO - Borrower Contact on Outbound Call',
	'TC - Authorized 3rd Party on Inbound',
	'TO - Authorized 3rd Party on Outbound Call')
	and MyDateTime >= GETDATE()-30
group by LoanRecID) Last30Contact
on
	a.RecID = Last30Contact.LoanRecID
left join
	( select loanrecid, COUNT(*) ContactLast60 from mars.dbo.vw_LoansContacts where Category in('AI - Attorney Contact on Inbound Call',
	'BI - Borrower Contact on Inbound Call',
	'BO - Borrower Contact on Outbound Call',
	'TC - Authorized 3rd Party on Inbound',
	'TO - Authorized 3rd Party on Outbound Call')
	and MyDateTime >= GETDATE()-60
group by LoanRecID) Last60Contact
on
	a.RecID = Last60Contact.LoanRecID
left join
	( select loanrecid, COUNT(*) AttemptLast30 from mars.dbo.vw_LoansContacts where Category in('AA - Outbound Attempt/no Contact with Attorney',
'NO - Outbound Attempt/no Contact with Borrower',
'TA - Outbound Attempt/no Contact with Authorized NO3',
'AO - Attorney Contact on Outbound Call') 
	and MyDateTime >= GETDATE()-30
group by LoanRecID) Last30Attempt
on
	a.RecID = Last30Attempt.LoanRecID
left join
	( select loanrecid, COUNT(*) AttemptLast60 from mars.dbo.vw_LoansContacts where Category in('AA - Outbound Attempt/no Contact with Attorney',
'NO - Outbound Attempt/no Contact with Borrower',
'TA - Outbound Attempt/no Contact with Authorized NO3',
'AO - Attorney Contact on Outbound Call') 
	and MyDateTime >= GETDATE()-60
group by LoanRecID) Last60Attempt
on
	a.RecID = Last60Attempt.LoanRecID
left join
 mars.dbo.vw_GetLoans  DNC
on
	a.RecID = dnc.LoanRecID
left join
	(select distinct LoanRecID, P2PDate from   mars.dbo.vw_EIReport  with(nolock)) P2P
on
	a.RecID = P2P.LoanRecID
left join
	(select * from dbo.vw_ACHBillingsEnteredByLoanByRep with(nolock) where isactive = 1) ACHbyRep
on
b.Account = achbyrep.LoanNumber
left join
mars.[dbo].[ValuationPaymentHistory] valuation
on
b.account = valuation.LoanNumber
left join
	mars.dbo.vw_LoanLastPaidDate ldp
on
b.Account = ldp.LoanNumber
left join
	( select loanrecid, max(mydatetime) LastUnableToDialDate from mars.dbo.vw_LoansContacts
 where category = 'DN – Unable to Dial'
 group by LoanRecID) UATD
on a.RecID = uatd.LoanRecID
left join
	(select loanrecid, state from  MARS..vw_Properties where [primary] = 1) lp
on
	a.RecID = lp.LoanRecID
left join
	support.dbo.JudicialStates jud
on
	lp.State = jud.state
left join
	mars.dbo.vw_BPOCurrent BPO
on
	b.Account = BPO.LoanID
LEFT JOIN 
	mars.dbo.vw_EscrowBalanceByLoan escrow
ON
	b.account = escrow.Account
 where
DATEDIFF(d,lastcontactdate,getdate()) > 5 and
DATEDIFF(d,LastAttemptDate,getdate()) > 2 and
dnc.ReasonForRestrictCommunication not like '%REPRESENTED%' and
dnc.ReasonForRestrictCommunication  not like '%DO NOT CALL%' and
dnc.ReasonForRestrictCommunication  not like '%LAWSUIT%' and
dnc.ReasonForRestrictCommunication  not like '%COUNSEL%'  and 
dnc.ReasonForRestrictCommunication  not like '%CEASE AND DESIST%'  and 
cast(isnull(P2PDate,'01-01-1900') AS DATE) <= cast(getdate() as date) and
cast(getdate() as date)> cast(isnull(case when [Scheduled End Date] = '01-01-9999' then Null else [Scheduled End Date] end,'01-01-1900')   as date)and
case 
	when cast(ldp.LastPaidDate as date) >= cast(getdate() - 25 as date) then 1 
	else 0
end = 0 and
isnull(cast(LastUnableToDialDate as date),'01-01-2050') > cast(getdate()-7 as date)


