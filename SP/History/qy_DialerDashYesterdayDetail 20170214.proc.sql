USE MARS_DW;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.qy_DialerDashYesterdayDetail') IS NOT NULL
	DROP PROCEDURE dbo.qy_DialerDashYesterdayDetail;
GO

/* -----------------------------------------------------------------------------------------------------------
   Procedure Name  :	qy_DialerDashDistinctAccountContact
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	02/07/2017

   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date       Author        Description
   --------   -----------   ------------

*/ -----------------------------------------------------------------------------------------------------------
CREATE PROCEDURE dbo.qy_DialerDashYesterdayDetail 

AS
BEGIN

	/*****************************************************************
	Pull Contact Only Call information from the single source proc
	*****************************************************************/
	CREATE TABLE #ResultswithCarryover
		(	Account	VARCHAR(30)
		,	ArchiveDate DATE
		,	CallType VARCHAR(30)
		,	Carryover INT
		)

	INSERT INTO #ResultswithCarryover
	EXEC qy_DialerDashDistinctAccountContact 7
			 
	
	/*****************************************************************
	Aggregate the detail for the previous day only
	*****************************************************************/
	SELECT	r.ArchiveDate
		,	COUNT(r.Account) AS Total
		,	SUM(r.Carryover) AS CarryOverCnt
		,	SUM(CASE WHEN r.CallType = 'InboundContact' THEN 1 ELSE 0 END) AS InboundContact
		,	SUM(CASE WHEN r.CallType = 'OutboundContact' THEN 1 ELSE 0 END) AS OutboundContact
		,	SUM(CASE WHEN r.CallType = 'OutboundAttempt' THEN 1 ELSE 0 END) AS OutboundAttempt
		,	SUM(CASE WHEN r.CallType = 'InboundNotOnDialer' THEN 1 ELSE 0 END) AS InboundNotOnDialer
		,	SUM(CASE WHEN r.CallType = 'OutboundAttemptNotOnDialer' THEN 1 ELSE 0 END) AS OutboundAttemptNotOnDialer
		,	SUM(CASE WHEN r.CallType = 'OutboundContactNotOnDialer' THEN 1 ELSE 0 END) AS OutboundContactNotOnDialer
		,	SUM(CASE WHEN r.Carryover = 1 AND r.CallType IN ('Untouched Loans', 'OutboundAttempt', 'OutboundAttemptNotOnDialer') THEN 1 ELSE 0 END) AS UntouchedCarryoverLoans
		,	SUM(CASE WHEN r.CallType = 'Untouched Loans' THEN 1 ELSE 0 END) AS UnTouchedLoans
	INTO #TotalCounts
	FROM	#ResultswithCarryover r
	WHERE r.ArchiveDate = CAST(GETDATE()-1 AS DATE)
	GROUP BY r.ArchiveDate
	ORDER BY r.ArchiveDate

	
	SELECT	t.ArchiveDate AS CallDate
		,	t.Total
		,	t.CarryOverCnt AS Carryover
		,	t.InboundContact
		,	t.InboundNotOnDialer
		,	t.OutboundContact
		,	t.OutboundContactNotOnDialer
		,	t.Total - (t.InboundContact + t.OutboundContact + t.OutboundContactNotOnDialer + t.InboundNotOnDialer) AS TotalUntouchedLoans
		,	t.UntouchedCarryoverLoans
		,	t.Total - (t.InboundContact + t.OutboundContact + t.OutboundContactNotOnDialer + t.InboundNotOnDialer) - t.UntouchedCarryoverLoans AS UntouchedLoans
		,	CAST((CAST((t.InboundContact + t.OutboundContact) AS FLOAT)/CAST(t.Total AS FLOAT)) AS DECIMAL(3,2)) AS ContactPenetration
	FROM #TotalCounts t

END
