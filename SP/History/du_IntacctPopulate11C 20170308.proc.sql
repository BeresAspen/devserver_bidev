USE MARS_DW;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.du_IntacctPopulate11C') IS NOT NULL
	DROP PROCEDURE dbo.du_IntacctPopulate11C;

GO

/* ********************************************************************************************************* *
   Procedure Name  :	du_IntacctPopulate11C
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	02/17/2017

   *********************************************************************************************************
   **         Change History                                                                              **
   *********************************************************************************************************

   Date			Author				Description
   --------		-----------			------------
   
*  ********************************************************************************************************* */

CREATE PROCEDURE dbo.du_IntacctPopulate11C
AS
BEGIN

	DECLARE @BeginDate DATE
		,	@EndDate DATE
	SET @BeginDate = '01/01/2017'
	SET @EndDate = CAST(GETDATE()-1 AS DATE)


	SELECT account, '12500' AS GLAccount, CAST(SysTimeStamp AS DATE) AS PostingDate, SourceTyp AS Descr, SUM(Total+ToLenderFee) as Debit, 0 AS Credit
	INTO #Temp
	FROM MARS.dbo.vw_PaymentHistory
	WHERE CAST(SysTimeStamp AS DATE)  BETWEEN @BeginDate AND @EndDate
	GROUP BY account, CAST(SysTimeStamp AS DATE), SourceTyp

	UNION ALL

	SELECT account, '23400' AS GLAccount, CAST(SysTimeStamp AS DATE) AS PostingDate, SourceTyp AS Descr, 0 AS Debit, SUM(ToReserve) as Credit
	FROM MARS.dbo.vw_PaymentHistory
	WHERE CAST(SysTimeStamp AS DATE) BETWEEN @BeginDate AND @EndDate
	AND Reference in ('SUSPENSE','SUSPENSEDS','SUS-LNCHGS')
	AND SourceTyp <> 'Payoff'
	GROUP BY account, CAST(SysTimeStamp AS DATE), SourceTyp
		
	UNION ALL

	SELECT account, '24180' AS GLAccount, CAST(SysTimeStamp AS DATE) AS PostingDate, SourceTyp AS Descr, 0 AS Debit, SUM(ToImpound) as Credit
	FROM MARS.dbo.vw_PaymentHistory
	WHERE CAST(SysTimeStamp AS DATE) BETWEEN @BeginDate AND @EndDate
	AND SourceTyp <> 'Payoff'
	GROUP BY account, CAST(SysTimeStamp AS DATE), SourceTyp

	UNION ALL

	SELECT account, '17300' AS GLAccount, CAST(SysTimeStamp AS DATE) AS PostingDate, SourceTyp AS Descr, 0 AS Debit, SUM(ToInterest) as Credit --Total to interest including UI
	FROM MARS.dbo.vw_PaymentHistory
	WHERE CAST(SysTimeStamp AS DATE) BETWEEN @BeginDate AND @EndDate
	AND SourceTyp <> 'Payoff'
	GROUP BY account, CAST(SysTimeStamp AS DATE), SourceTyp

	UNION ALL

	SELECT account, '16001' AS GLAccount, CAST(SysTimeStamp AS DATE) AS PostingDate, SourceTyp AS Descr, 0 AS Debit, SUM(ToReserve) as Credit
	FROM MARS.dbo.vw_PaymentHistory
	WHERE CAST(SysTimeStamp AS DATE) BETWEEN @BeginDate AND @EndDate
	AND right(Reference,4) = 'DUPB'
	AND SourceTyp <> 'Payoff'
	GROUP BY account, CAST(SysTimeStamp AS DATE), SourceTyp

	UNION ALL

	SELECT account, '16000' AS GLAccount, CAST(SysTimeStamp AS DATE) AS PostingDate, SourceTyp AS Descr, 0 AS Debit, SUM(ToPrincipal) as Credit
	FROM MARS.dbo.vw_PaymentHistory
	WHERE CAST(SysTimeStamp AS DATE) BETWEEN @BeginDate AND @EndDate
	AND SourceTyp <> 'Payoff'
	GROUP BY account, CAST(SysTimeStamp AS DATE), SourceTyp

	UNION ALL

	SELECT account, '42020' AS GLAccount, CAST(SysTimeStamp AS DATE) AS PostingDate, SourceTyp AS Descr, 0 AS Debit, SUM(ToLateCharge) as Credit
	FROM MARS.dbo.vw_PaymentHistory
	WHERE CAST(SysTimeStamp AS DATE) BETWEEN @BeginDate AND @EndDate
	AND SourceTyp <> 'Payoff'
	GROUP BY account, CAST(SysTimeStamp AS DATE), SourceTyp

	UNION ALL

	SELECT account, '17606' AS GLAccount, CAST(SysTimeStamp AS DATE) AS PostingDate, SourceTyp AS Descr, 0 AS Debit, SUM(ToChargesPrin) as Credit
	FROM MARS.dbo.vw_PaymentHistory
	WHERE CAST(SysTimeStamp AS DATE) BETWEEN @BeginDate AND @EndDate
	AND SourceTyp <> 'Payoff'
	GROUP BY account, CAST(SysTimeStamp AS DATE), SourceTyp

	UNION ALL

	SELECT account, '42040' AS GLAccount, CAST(SysTimeStamp AS DATE) AS PostingDate, SourceTyp AS Descr, 0 AS Debit, SUM(ToChargesInt) as Credit
	FROM MARS.dbo.vw_PaymentHistory
	WHERE CAST(SysTimeStamp AS DATE) BETWEEN @BeginDate AND @EndDate
	AND SourceTyp <> 'Payoff'
	GROUP BY account, CAST(SysTimeStamp AS DATE), SourceTyp

	UNION ALL


	SELECT vw_PaymentHistory.account, '12300' AS GLAccount, CAST(SysTimeStamp AS DATE) AS PostingDate, SourceTyp AS Descr, 0 AS Debitr, SUM(Total) + SUM(ToLenderFee) as Credit
	FROM MARS.dbo.vw_PaymentHistory 
	WHERE CAST(SysTimeStamp AS DATE) BETWEEN @BeginDate AND @EndDate
	AND SourceTyp = 'payoff'
	GROUP BY account, CAST(SysTimeStamp AS DATE), SourceTyp
	ORDER BY Account;



	SELECT DISTINCT 
			'GJ' AS JOURNAL
		,	t.PostingDate
		,	t.Descr
		,	t.Debit
		,	t.Credit
	INTO #Header
	FROM #Temp t;

	TRUNCATE TABLE MARS_DW.dbo.Intacct11C;

	INSERT INTO MARS_DW.dbo.Intacct11C
				(JOURNAL, [DATE], [DESCRIPTION], REFERENCE_NO, LINE_NO, ACCT_NO, DEBIT, CREDIT, NOTEOWNER)
	SELECT	h.JOURNAL
		,	h.PostingDate
		,	h.Descr
		,	h.Account
		,	ROW_NUMBER() OVER(PARTITION BY h.PostingDate, h.Account, h.Descr ORDER BY t.GLAccount) AS Line_No
		,	t.GLAccount
		,	t.Debit
		,	t.Credit
		,	l.NoteOwner
	FROM	#Header h
		INNER JOIN #Temp t
			ON	t.Account = h.Account
			AND t.PostingDate = h.PostingDate
			AND t.Descr = h.Descr
		INNER JOIN MARS_DW.dbo.vw_Loans_Archive l
			ON	l.Account = h.Account
			AND l.ArchiveDate = h.PostingDate;

END

select *
from MARS_DW.dbo.Intacct11C