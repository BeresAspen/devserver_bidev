USE MARS_DW;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.du_IntacctPopulate1A') IS NOT NULL
	DROP PROCEDURE dbo.du_IntacctPopulate1A;

GO

/* ********************************************************************************************************* *
   Procedure Name  :	du_IntacctPopulate1A
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	02/15/2017

   *********************************************************************************************************
   **         Change History                                                                              **
   *********************************************************************************************************

   Date			Author				Description
   --------		-----------			------------
   
*  ********************************************************************************************************* */

CREATE PROCEDURE dbo.du_IntacctPopulate1A
AS
BEGIN
/*	QUESTIONS

- GL_ACCTNO
- FORM1099 fields
- Currency - USD?
- only active loans?

*/
	TRUNCATE TABLE dbo.Intacct1A


	INSERT INTO dbo.Intacct1A
			(MARS_ID, [NAME], TAX_ID, CURRENCY, CONTACT_NAME, FIRST_NAME, MI, LAST_NAME, PRINT_AS, PHONE1, PHONE2, CELLPHONE, EMAIL1, ADDRESS1, CITY, [STATE], ZIP)
	SELECT	l.Account AS MARS_ID
		,	l.FullName
		,	MARS.dbo.fn_RemoveNonNumericCharacters(l.TIN) AS TAX_ID
		,	'USD' AS CURRENCY
		,	l.[First Name] + ' ' + l.[Last Name] AS CONTACT_NAME
		,	l.[First Name]
		,	l.MI
		,	l.[Last Name]
		,	l.FullName AS PRINT_AS
		,	MARS.dbo.fn_RemoveNonNumericCharacters(l.Home) AS Phone1
		,	MARS.dbo.fn_RemoveNonNumericCharacters(l.Work) AS Phone2
		,	MARS.dbo.fn_RemoveNonNumericCharacters(l.Mobil) AS CellPhone
		,	CASE WHEN l.EmailAddress like '%@%#%'
					THEN	CASE WHEN l.EmailAddress like '%[[]%#%' THEN SUBSTRING(l.EmailAddress,CHARINDEX('[',l.EmailAddress)+1,CHARINDEX(']',l.EmailAddress)-1-CHARINDEX('[',l.EmailAddress))
								 WHEN l.EmailAddress like '%;%#%' THEN SUBSTRING(l.EmailAddress,1,CHARINDEX(';',l.EmailAddress)-1)
								 WHEN l.EmailAddress like '%#%' THEN SUBSTRING(l.EmailAddress,1,CHARINDEX('#',l.EmailAddress)-1)
							END
				 ELSE l.EmailAddress
			END AS Email1
		,	l.Street AS Address1
		,	l.City
		,	l.[State]
		,	l.[Zip Code] AS Zip
	FROM MARS.dbo.vw_Loans l
	WHERE	l.isActive = 1

END