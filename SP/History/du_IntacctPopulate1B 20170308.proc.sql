USE MARS_DW;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.du_IntacctPopulate1B') IS NOT NULL
	DROP PROCEDURE dbo.du_IntacctPopulate1B;

GO

/* ********************************************************************************************************* *
   Procedure Name  :	du_IntacctPopulate1B
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	02/15/2017

   *********************************************************************************************************
   **         Change History                                                                              **
   *********************************************************************************************************

   Date			Author				Description
   --------		-----------			------------
   
*  ********************************************************************************************************* */

CREATE PROCEDURE dbo.du_IntacctPopulate1B
AS
BEGIN

	TRUNCATE TABLE dbo.Intacct1B;

	INSERT INTO dbo.Intacct1B
	SELECT DISTINCT
			'New' AS NewOrUpdate
		,	ISNULL(r.FormerGFLoanNumber,aem.MARSID) AS ASSETID 
		,	aem.CompanyCorporateEntityID AS Entity
		,	LEFT(p.PoolName, 20) as POOLACQUIRED
		,	CASE WHEN aem.AssetEntityType = 'Loan' THEN lp.City + ', ' + lp.[State]
				 WHEN aem.AssetEntityType = 'REO' THEN SUBSTRING(r.City, 0, PATINDEX('%,%', r.City) + 4)
			END AS ASSET_STATE
		,	'' AS CustomerInvestorAffiliate
		,	aem.AssetEntityType AS ASSETSTATUS
	FROM MARS.dbo.vw_AssetEntityMapping aem
		LEFT JOIN MARS.dbo.REO r
			ON r.PropertyID = aem.MARSID
		LEFT JOIN MARS.dbo.vw_Loans l
			ON	l.Account = ISNULL(r.FormerGFLoanNumber,aem.MARSID)
		LEFT JOIN MARS.dbo.Pools p
			ON	p.PoolID = l.PoolID
		LEFT JOIN MARS.dbo.vw_LoansProperties lp
			ON lp.LoanRecID = l.RecID
	WHERE aem.AssetEntityType IN ('Loan', 'REO')


END
 