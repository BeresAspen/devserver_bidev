USE [Support]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('di_IntacctTaxInvoiceDetailDownload') IS NOT NULL
	DROP PROC di_IntacctTaxInvoiceDetailDownload;
GO


CREATE PROCEDURE [dbo].[di_IntacctTaxInvoiceDetailDownload]
AS

/* ********************************************************************************************************* *
   Procedure Name  :	di_IntacctTaxInvoiceDetailDownload
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	
   
   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date			Author				Description
   --------		-----------			------------
	12/06/2017	Benjacob Beres		Modified the process to write the HistoryID back to the Queue table

*  ********************************************************************************************************* */

BEGIN 
SET NOCOUNT ON
DECLARE @Application varchar(128)='Intacct Invoice Sync'
DECLARE @Version     varchar(10) = ''

BEGIN TRY
	SELECT	ih.CUSTVENDID AS IntacctVendorID
		,	id.CLASSID AS MARSAccount
		,	CAST(NULL AS VARCHAR(20)) AS CheckNbr
		,	ih.DOCNO AS IntacctInvoiceNbr
		,	ih.INVOICE_DATE AS InvoiceDate
		,	CAST(NULL AS DATE) AS PaymentDate
		,	CAST(VPS.EconomicLossDate AS DATE) AS EconomicLossDate
		,	CAST(PC.AccountNumber AS VARCHAR(30)) AS AccountNbr
		,	id.TOTAL AS Amount
		,	CASE WHEN ih.InvoiceStatus = 'Closed' THEN 'Pending' ELSE ih.InvoiceStatus END AS InvoiceStatus
		,	CAST(id.TAX_CYCLE AS INT) AS TaxCycle
		,	CAST(id.TAX_YEAR AS INT) AS TaxYear
		,	vpq.VendorPaymentsQueueId
	INTO #StagingTaxInvoicesLoans
	--SELECT *
	FROM Support.dbo.StagingIntacctInvoicesHeader ih
	JOIN Support.dbo.StagingIntacctInvoicesDetail id 
		ON	id.DOCHDRNO = ih.RECORDNO
	JOIN IntacctDM.dbo.Vendors v
		ON	v.IntacctVendorID = ih.CUSTVENDID
	JOIN MARS.dbo.VendorsPaymentSchedule vps
		ON	vps.VendorId = v.MARSVendorID
		AND	vps.TaxYear = id.TAX_YEAR
		AND vps.PaymentCycle = id.TAX_CYCLE
		AND (VPS.Status = 'Active' or VPS.Status = 'In-Progress') 
	JOIN MARS.dbo.VendorPaymentsQueue vpq 
		ON	vpq.VendorPaymentScheduleId = vps.VendorPaymentScheduleId
		AND vpq.DueAmount = CAST(id.TOTAL AS DECIMAL(17,2))
	LEFT JOIN MARS.dbo.PropertyChildMaps PC
		ON	PC.PropertyChildMapId = VPQ.PropertyChildMapId
		AND PC.IsActive = 1
	WHERE	id.DEPARTMENTNAME = 'Taxes'
		AND id.CLASSID = CASE WHEN ISNUMERIC(LEFT(ISNULL(id.CLASSID,'1'),1)) = 0
								THEN pc.REOPropertyID
							  ELSE ISNULL(pc.REOPropertyID, id.CLASSID)
						 END
		--AND v.MARSVendorID = 29287
	ORDER BY ih.RECORDNO, id.RECORDNO


	---------------------------------------------------------------------------------------
	--Overwrite PT Staging records with more current statuses from Bill recs
	---------------------------------------------------------------------------------------


	SELECT	sti.IntacctVendorID
		,	sti.MARSAccount
		,	sti.CheckNbr
		,	sti.IntacctInvoiceNbr
		,	sti.InvoiceDate
		,	sb.WHENPAID AS PaymentDate
		,	sti.EconomicLossDate
		,	sti.AccountNbr
		,	sti.Amount AS Amount
		,	CASE WHEN sb.InvoiceStatus = 'Paid' THEN 'Paid' ELSE 'Pending' END AS InvoiceStatus
		,	sti.TaxCycle
		,	sti.TaxYear
		,	sti.VendorPaymentsQueueID
	INTO #BillStaging
	FROM Support.dbo.StagingIntacctInvoicesBill sb	
	JOIN #StagingTaxInvoicesLoans sti
		ON	sti.IntacctInvoiceNbr = SUBSTRING(sb.DESCRIPTION2, 24, 9)
	WHERE	LEFT(sb.DESCRIPTION2,23) = 'Purchasing Transaction-'
		AND sb.InvoiceStatus <> sti.InvoiceStatus


	--Capture Bills that have Purchase Tranasction from previous days.
	INSERT INTO #BillStaging
	SELECT  i.IntacctVendorID
		,	i.MARSAccount
		,	i.CheckNbr
		,	i.IntacctInvoiceNbr
		,	i.InvoiceDate
		,	sb.WHENPAID AS PaymentDate
		,	i.EconomicLossDate
		,	i.AccountNbr
		,	i.Amount AS Amount
		,	CASE WHEN sb.InvoiceStatus = 'Paid' THEN 'Paid' ELSE 'Pending' END AS InvoiceStatus
		,	i.TaxCycle
		,	i.TaxYear
		,	i.VendorPaymentsQueueID
	FROM Support.dbo.StagingIntacctInvoicesBill sb	
	JOIN IntacctDM.dbo.Invoices i
		ON	i.IntacctInvoiceNbr = SUBSTRING(sb.DESCRIPTION2, 24, 9)
	LEFT JOIN #BillStaging bs
		ON	bs.IntacctInvoiceNbr = SUBSTRING(sb.DESCRIPTION2, 24, 9)
	WHERE	LEFT(sb.DESCRIPTION2,23) = 'Purchasing Transaction-'
		AND i.InvoiceStatus <> CASE WHEN sb.InvoiceStatus = 'Paid' THEN 'Paid' ELSE 'Pending' END
		AND bs.IntacctInvoiceNbr IS NULL


	DELETE 
	FROM #StagingTaxInvoicesLoans 
	WHERE IntacctInvoiceNbr IN (	SELECT bs.IntacctInvoiceNbr
										FROM #BillStaging bs
									)


	INSERT INTO #StagingTaxInvoicesLoans
	SELECT *
	FROM #BillStaging 


	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------

	--Update VendorPaymentQueue if the status change is paid or voided. All other status keep CanProcess to P
	UPDATE VPQ
	SET		CanProcess = CASE WHEN sti.InvoiceStatus LIKE '%Paid%' THEN 'H' ELSE 'N' END
		,	UpdatedBy = SYSTEM_USER               
		,	UpdatedDate = GETDATE() 
	--SELECT CanProcess, CASE WHEN sti.InvoiceStatus LIKE '%Paid%' THEN 'H' ELSE 'N' END, *
	FROM MARS.dbo.VendorPaymentsQueue VPQ
	JOIN #StagingTaxInvoicesLoans sti 
		ON	VPQ.VendorPaymentsQueueId = sti.VendorPaymentsQueueId
	JOIN (	SELECT	i.IntacctInvoiceNbr
				,	i.VendorPaymentsQueueId
				,	i.InvoiceStatus
				,	ROW_NUMBER() OVER(PARTITION BY i.VendorPaymentsQueueId, i.IntacctInvoiceNbr ORDER BY i.InsertedDate DESC) AS RowNum
			FROM	IntacctDM.dbo.Invoices i 
		 ) i 
		ON	i.IntacctInvoiceNbr = sti.IntacctInvoiceNbr
		AND i.VendorPaymentsQueueId = sti.VendorPaymentsQueueId
		AND i.RowNum = 1	
	WHERE	sti.InvoiceStatus <> i.InvoiceStatus
		AND (	sti.InvoiceStatus LIKE '%Paid%'
			 OR	sti.InvoiceStatus LIKE '%Void%'
			)
		AND CanProcess <> CASE WHEN sti.InvoiceStatus LIKE '%Paid%' THEN 'H' ELSE 'N' END

		
	--Update VendorPaymentHistory if the status change is paid
	UPDATE vph
	SET		PaidDate = PaymentDate 
		--,	PaymentStatus = 'Paid'
		,	UpdatedBy = SYSTEM_USER               
		,	UpdateDate = GETDATE() 
	--SELECT vph.PaidDate,sti.PaymentDate,*
	FROM MARS.dbo.VendorPaymentsHistory vph
	JOIN #StagingTaxInvoicesLoans sti 
		ON	vph.VendorPaymentsQueueId = sti.VendorPaymentsQueueId
	JOIN (	SELECT	i.IntacctInvoiceNbr
				,	i.VendorPaymentsQueueId
				,	i.InvoiceStatus
				,	ROW_NUMBER() OVER(PARTITION BY i.VendorPaymentsQueueId, i.IntacctInvoiceNbr ORDER BY i.InsertedDate DESC) AS RowNum
			FROM	IntacctDM.dbo.Invoices i 
		 ) i 
		ON	i.IntacctInvoiceNbr = sti.IntacctInvoiceNbr
		AND i.VendorPaymentsQueueId = sti.VendorPaymentsQueueId
		AND i.RowNum = 1	
	WHERE	sti.InvoiceStatus <> i.InvoiceStatus
		AND sti.InvoiceStatus LIKE '%Paid%'
		AND vph.PaymentStatus NOT LIKE '%Paid%'
		AND ISNULL(vph.PaidDate, '01/01/1950') <> sti.PaymentDate
	

	--Mark identical invoices as dupes
	SELECT DISTINCT sti.VendorPaymentsQueueID
	INTO #InvDupes
	FROM #StagingTaxInvoicesLoans sti 
	JOIN #StagingTaxInvoicesLoans st2
		ON	st2.IntacctVendorID = sti.IntacctVendorID
		AND st2.VendorPaymentsQueueID = sti.VendorPaymentsQueueID
		AND st2.Amount = sti.Amount
		AND st2.TaxCycle = sti.TaxCycle
		AND st2.TaxYear = sti.TaxYear
		AND st2.IntacctInvoiceNbr <> sti.IntacctInvoiceNbr



	--Insert New Record when there is a change to existing record
	INSERT INTO IntacctDM.dbo.Invoices
			(IntacctVendorID, MARSAccount, CheckNbr, IntacctInvoiceNbr, InvoiceDate, PaymentDate, EconomicLossDate, AccountNbr, Amount, InvoiceStatus, TaxCycle, TaxYear, VendorPaymentsQueueID, InsertedBy, InsertedDate)
	SELECT	sti.IntacctVendorID
		,	sti.MARSAccount
		,	sti.CheckNbr
		,	sti.IntacctInvoiceNbr
		,	sti.InvoiceDate
		,	sti.PaymentDate
		,	sti.EconomicLossDate
		,	sti.AccountNbr
		,	sti.Amount
		,	CASE WHEN id.VendorPaymentsQueueID IS NULL THEN sti.InvoiceStatus ELSE 'Duplicate' END AS InvoiceStatus
		,	sti.TaxCycle
		,	sti.TaxYear
		,	sti.VendorPaymentsQueueID
		,	'system' AS InsertedBy
		,	GETDATE() as InsertedDate
	FROM #StagingTaxInvoicesLoans sti 
	JOIN (	SELECT	i.IntacctVendorID
				,	i.IntacctInvoiceNbr
				,	i.MARSAccount
				,	i.CheckNbr
				,	i.InvoiceDate
				,	i.PaymentDate
				,	i.Amount
				,	i.InvoiceStatus
				,	i.TaxCycle
				,	i.TaxYear
				,	i.VendorPaymentsQueueId
				,	ROW_NUMBER() OVER(PARTITION BY i.VendorPaymentsQueueId, i.IntacctInvoiceNbr ORDER BY i.InsertedDate DESC) AS RowNum
			FROM	IntacctDM.dbo.Invoices i 
		 ) i 
		ON	i.IntacctInvoiceNbr = sti.IntacctInvoiceNbr
		AND i.VendorPaymentsQueueId = sti.VendorPaymentsQueueId
		AND i.RowNum = 1
	LEFT JOIN #InvDupes id 
		ON	id.VendorPaymentsQueueID = sti.VendorPaymentsQueueID
	WHERE	sti.IntacctVendorID <> i.IntacctVendorID
		OR  sti.MARSAccount <> i.MARSAccount
		OR  sti.CheckNbr <> i.CheckNbr
		OR  sti.InvoiceDate <> i.InvoiceDate
		OR  sti.PaymentDate <> i.PaymentDate
		OR  sti.Amount <> i.Amount
		OR  sti.InvoiceStatus <> i.InvoiceStatus
		OR  sti.TaxCycle <> i.TaxCycle
		OR  sti.TaxYear <> i.TaxYear


	
	-------------------------------------------------------------------------------------------------
	--NEW INVOICE FROM INTACCT
	-------------------------------------------------------------------------------------------------

	--Insert Record into VendorPaymentHistory table 

	INSERT INTO MARS.dbo.VendorPaymentsHistory                                       
			(VendorPaymentsQueueId,AssetVendorMapId, PaymentStatus,PaymentRequestDate,PaidDate,PaidAmount, AFRRequestId,PaymentDueDate,DueAmount,InsertedBy,InsertedDate)                                          
	SELECT	VPQ.VendorPaymentsQueueId
		,	PCM.AssetVendorMapId
		,	CASE WHEN sti.InvoiceStatus LIKE '%Paid%' 
					THEN 'Paid'                            
				 ELSE 'Pending'                            
			END AS PaymentStatus
		,	VPQ.DueDate as PaymentRequestDate
		,	sti.PaymentDate as  PaidDate
		,	sti.Amount  as PaidAmount
		,	sti.IntacctInvoiceNbr AS AFRRequestId
		,	VPQ.DueDate AS PaymentDueDate
		,	VPQ.DueAmount
		,	SYSTEM_USER AS InsertedBy
		,	GetDate() AS InsertedDate   
		--select *                                           
	FROM #StagingTaxInvoicesLoans sti   
	JOIN MARS.dbo.VendorPaymentsQueue VPQ
		ON	VPQ.VendorPaymentsQueueId = sti.VendorPaymentsQueueId
	INNER JOIN MARS.dbo.PropertyChildMaps PCM
		ON	VPQ.PropertyChildMapId = PCM.PropertyChildMapId
	LEFT JOIN IntacctDM.dbo.Invoices i 
		ON	i.VendorPaymentsQueueId = sti.VendorPaymentsQueueId
	WHERE i.VendorPaymentsQueueId IS NULL
             


	--Update VendorPaymentQueue table to show new status
	UPDATE VPQ
	SET		CanProcess = CASE WHEN sti.InvoiceStatus LIKE '%Paid%' THEN 'H' 
							  WHEN sti.InvoiceStatus LIKE '%Void%' THEN 'N'
							  ELSE 'P' 
						 END
		,	UpdatedBy = SYSTEM_USER               
		,	UpdatedDate = GETDATE() 
	--SELECT vph.*
	FROM MARS.dbo.VendorPaymentsQueue VPQ
	JOIN #StagingTaxInvoicesLoans sti 
		ON	VPQ.VendorPaymentsQueueId = sti.VendorPaymentsQueueId
	JOIN MARS.dbo.VendorPaymentsHistory vph
		ON VPQ.VendorPaymentsQueueId = vph.VendorPaymentsQueueId
	LEFT JOIN IntacctDM.dbo.Invoices i 
		ON	i.VendorPaymentsQueueId = sti.VendorPaymentsQueueId
	WHERE i.IntacctInvoiceNbr IS NULL



	/*
	-----------------------------------------------------------------------------------------------------------------------
	--Place the HistoryID into the Queue table so the record will appear in the center console on the Tax Payments screen
	-----------------------------------------------------------------------------------------------------------------------

	UPDATE  vpq
		SET VendorPaymentsHistoryId = ql.VendorPaymentsHistoryId
	--SELECT *
	FROM MARS.dbo.VendorPaymentsQueue vpq
	JOIN (	SELECT	vpq.VendorPaymentsQueueId
				,	MAX(vph.VendorPaymentsHistoryId) AS VendorPaymentsHistoryId
			FROM MARS.dbo.VendorPaymentsQueue VPQ
			JOIN MARS.dbo.VendorPaymentsHistory vph
				ON	vph.VendorPaymentsQueueId = vpq.VendorPaymentsQueueId
			JOIN #StagingTaxInvoicesLoans sti 
				ON	VPQ.VendorPaymentsQueueId = sti.VendorPaymentsQueueId
			GROUP BY vpq.VendorPaymentsQueueId
		) ql
		ON	ql.VendorPaymentsQueueId = vpq.VendorPaymentsQueueId
	WHERE vpq.VendorPaymentsHistoryId <> ql.VendorPaymentsHistoryId

	*/

	--Insert new Invoice record when Invoice hasn't existed before
	INSERT INTO IntacctDM.dbo.Invoices
			(IntacctVendorID, MARSAccount, CheckNbr, IntacctInvoiceNbr, InvoiceDate, PaymentDate, EconomicLossDate, AccountNbr, Amount, InvoiceStatus, TaxCycle, TaxYear, VendorPaymentsQueueID, InsertedBy, InsertedDate)
	SELECT	sti.IntacctVendorID
		,	sti.MARSAccount
		,	sti.CheckNbr
		,	sti.IntacctInvoiceNbr
		,	sti.InvoiceDate
		,	sti.PaymentDate
		,	sti.EconomicLossDate
		,	sti.AccountNbr
		,	sti.Amount
		,	CASE WHEN id.VendorPaymentsQueueID IS NULL THEN sti.InvoiceStatus ELSE 'Duplicate' END AS InvoiceStatus
		,	sti.TaxCycle
		,	sti.TaxYear
		,	sti.VendorPaymentsQueueID
		,	SYSTEM_USER  AS InsertedBy
		,	GETDATE() as InsertedDate
	FROM #StagingTaxInvoicesLoans sti 
	LEFT JOIN IntacctDM.dbo.Invoices i 
		ON	i.VendorPaymentsQueueID = sti.VendorPaymentsQueueID
	LEFT JOIN #InvDupes id 
		ON	id.VendorPaymentsQueueID = sti.VendorPaymentsQueueID
	WHERE i.IntacctInvoiceNbr IS NULL

END TRY

BEGIN CATCH
	
	DECLARE @Err           INT
	DECLARE @ErrorMessage  Varchar(Max)
	DECLARE @ErrorLine     Varchar(128)
	DECLARE @Workstation   VarChar(128)
	DECLARE @Proc          VarChar(128)
	DECLARE @User		   VARCHAR(128)

	SET @User = SYSTEM_USER

	--IF @@TRANCOUNT > 0
	--	ROLLBACK TRANSACTION 
    

	IF Error_Number() IS NULL 
		SET @Err =0;
	Else
		SET @Err = Error_Number();


	SET @ErrorMessage = Error_Message()
	SET @ErrorLine    = 'SP Line Number: ' + Cast(Error_Line() as varchar(10))
	SET @Workstation  = HOST_NAME()
	SET @Proc         = OBJECT_NAME(@@ProcID)

	EXEC Applications.dbo.di_ErrorLog	@Application ,@Version ,@Err, @ErrorMessage, @Proc, @ErrorLine, @User , @Workstation;
END CATCH   


END


GO


