USE IntacctDM;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('di_SyncDMTablesFromIntacct') IS NOT NULL
	DROP PROCEDURE di_SyncDMTablesFromIntacct;
GO


CREATE PROCEDURE dbo.di_SyncDMTablesFromIntacct
AS
BEGIN

/********************************************************************************************************** *
   Procedure Name  :	di_SyncDMTablesFromIntacct
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	11/17/2017

   *********************************************************************************************************
   **         Change History                                                                              **
   *********************************************************************************************************

   Date			Author				Description
   --------		-----------			------------
   
************************************************************************************************************ */


-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
--APBILL Process
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------


--When Records exists already, check if the modified date is different. If so, move Primary Record to History and insert Staging into Primary
IF OBJECT_ID('tempdb..#tempAPBill') IS NOT NULL
	DROP TABLE #tempAPBill;

--List of Existing Records
SELECT iab.*
INTO #tempAPBill
FROM Staging.dbo.IntacctStgAPBill sab
LEFT JOIN IntacctDM.dbo.APBill iab
	ON	iab.RECORDNO = sab.RECORDNO
	AND CAST(iab.WHENMODIFIED AS DATETIME) < CAST(sab.WHENMODIFIED AS DATETIME)
WHERE iab.RECORDNO IS NOT NULL


--Move existing records to History table
INSERT INTO IntacctDM.dbo.hstAPBill
SELECT *
FROM #tempAPBill

--Delete the records from the Primary table
DELETE 
--select *
FROM IntacctDM.dbo.APBill
WHERE EXISTS (	SELECT 1
				FROM #tempAPBill t
				WHERE t.RECORDNO = APBill.RECORDNO)

--Insert records that do not exist in Primary (this includes new records and the replacements for the records that were just deleted)
INSERT INTO IntacctDM.dbo.APBill
SELECT sab.*
FROM Staging.dbo.IntacctStgAPBill sab
LEFT JOIN IntacctDM.dbo.APBill iab
	ON	iab.RECORDNO = sab.RECORDNO
WHERE iab.RECORDNO IS NULL


-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
--APBILLITEM Process
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------


--When Records exists already, check if the modified date is different. If so, move Primary Record to History and insert Staging into Primary
IF OBJECT_ID('tempdb..#tempAPBillItem') IS NOT NULL
	DROP TABLE #tempAPBillItem;

--List of Existing Records
SELECT iab.*
INTO #tempAPBillItem
FROM Staging.dbo.IntacctStgAPBillItem sab
LEFT JOIN IntacctDM.dbo.APBillItem iab
	ON	iab.RECORDNO = sab.RECORDNO
	AND CAST(iab.WHENMODIFIED AS DATETIME) < CAST(sab.WHENMODIFIED AS DATETIME)
WHERE iab.RECORDNO IS NOT NULL


--Move existing records to History table
INSERT INTO IntacctDM.dbo.hstAPBillItem
SELECT *
FROM #tempAPBillItem

--Delete the records from the Primary table
DELETE 
FROM IntacctDM.dbo.APBillItem
WHERE EXISTS (	SELECT 1
				FROM #tempAPBillItem t
				WHERE t.RECORDNO = IntacctDM.dbo.APBillItem.RECORDNO)
				
--Insert records that do not exist in Primary (this includes new records and the replacements for the records that were just deleted)
INSERT INTO IntacctDM.dbo.APBillItem
SELECT sab.*
FROM Staging.dbo.IntacctStgAPBillItem sab
LEFT JOIN IntacctDM.dbo.APBillItem iab
	ON	iab.RECORDNO = sab.RECORDNO
WHERE iab.RECORDNO IS NULL


-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
--VENDOR Process
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------


--When Records exists already, check if the modified date is different. If so, move Primary Record to History and insert Staging into Primary
IF OBJECT_ID('tempdb..#tempVendor') IS NOT NULL
	DROP TABLE #tempVendor;

--List of Existing Records
SELECT iab.*
INTO #tempVendor
FROM Staging.dbo.IntacctStgVendor sab
LEFT JOIN IntacctDM.dbo.Vendor iab
	ON	iab.RECORDNO = sab.RECORDNO
	AND CAST(iab.WHENMODIFIED AS DATETIME) < CAST(sab.WHENMODIFIED AS DATETIME)
WHERE iab.RECORDNO IS NOT NULL


--Move existing records to History table
INSERT INTO IntacctDM.dbo.hstVendor
SELECT *
FROM #tempVendor

--Delete the records from the Primary table
DELETE 
--select *
FROM IntacctDM.dbo.Vendor
WHERE EXISTS (	SELECT 1
				FROM #tempVendor t
				WHERE t.RECORDNO = Vendor.RECORDNO)
				
--Insert records that do not exist in Primary (this includes new records and the replacements for the records that were just deleted)
INSERT INTO IntacctDM.dbo.Vendor
SELECT sab.*
FROM Staging.dbo.IntacctStgVendor sab
LEFT JOIN IntacctDM.dbo.Vendor iab
	ON	iab.RECORDNO = sab.RECORDNO
WHERE iab.RECORDNO IS NULL


END