USE MARS_DW;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.qy_DialerDashTCPAFlagActivations') IS NOT NULL
	DROP PROCEDURE dbo.qy_DialerDashTCPAFlagActivations;
GO

/* -----------------------------------------------------------------------------------------------------------
   Procedure Name  :	qy_DialerDashTCPAFlagActivations
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	02/07/2017

   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date			Author				Description
   --------		-----------			------------
   03/20/2017	Benjacob Beres		Added the % Activated amount and changed the date range to just the 
										previous 90 days.
*/ -----------------------------------------------------------------------------------------------------------

CREATE PROCEDURE dbo.qy_DialerDashTCPAFlagActivations
AS
BEGIN

	DECLARE @ProcessDate DATE
	SET	@ProcessDate = CAST(GETDATE()-90 AS DATE)



	/*****************************************************************
	Pull the list of all active Accounts and their current 
		TCPA Flag status
	*****************************************************************/	
	SELECT	T.Account
		,	MAX(T.TCPAMinFlagDate) AS TCPAFlagDate
		,	SUM(T.TCPAFlag) AS TCPAFlag
	INTO	#TCPAFlagData
	FROM (
			SELECT	vla.Account
				,	CASE WHEN CAST(ISNULL(vla.PhoneHomeSelect,0) AS SMALLINT) + CAST(ISNULL(vla.PhoneWorkSelect,0) AS SMALLINT) 
								+ CAST(ISNULL(vla.PhoneCellSelect,0) AS SMALLINT) + CAST(ISNULL(vla.HomeSelect,0) AS SMALLINT) 
								+ CAST(ISNULL(vla.WorkSelect,0) AS SMALLINT) + CAST(ISNULL(vla.MobileSelect,0) AS SMALLINT) > 0
							THEN 1
						 ELSE 0
					END AS TCPAFlag
				,	MIN(vla.ArchiveDate) AS TCPAMinFlagDate
			FROM	MARS_DW.dbo.vw_Loans_Archive vla
				INNER JOIN	MARS.dbo.vw_Loans mvl
					ON	mvl.Account = vla.Account
					AND mvl.isActive = 1
			GROUP BY vla.Account
				,	CASE WHEN CAST(ISNULL(vla.PhoneHomeSelect,0) AS SMALLINT) + CAST(ISNULL(vla.PhoneWorkSelect,0) AS SMALLINT) 
								+ CAST(ISNULL(vla.PhoneCellSelect,0) AS SMALLINT) + CAST(ISNULL(vla.HomeSelect,0) AS SMALLINT) 
								+ CAST(ISNULL(vla.WorkSelect,0) AS SMALLINT) + CAST(ISNULL(vla.MobileSelect,0) AS SMALLINT) > 0
							THEN 1
						 ELSE 0
					END
		 ) T
	GROUP BY T.Account


	/*****************************************************************
	Pull the list of all Accounts who were contacted.
	*****************************************************************/	
	SELECT DISTINCT 
			CAST(lc.MyDateTime AS DATE) AS MyDateTime
		,	l.Account
	INTO	#Contacts
	FROM	MARS.dbo.LoansContacts lc (NOLOCK)
	LEFT JOIN MARS.dbo.vw_Loans l
		ON	l.RecID = lc.LoanRecID
	WHERE lc.MyDateTime >= @ProcessDate
		AND lc.Category IN (	'TO - Authorized 3rd Party on Outbound Call'
							,	'AO - Attorney Contact on Outbound Call'
							,	'BO - Borrower Contact on Outbound Call'
							,	'AI - Attorney Contact on Inbound Call'
							,	'BI - Borrower Contact on Inbound Call'
							,	'TC - Authorized 3rd Party on Inbound'
							,	'NA - Non-Authorized 3rd Party on Inbound Call') 



	/*****************************************************************
	Calculate the Total Contacted per day minus the accounts who
		already were flagged as TCPA yes.
	*****************************************************************/	
	SELECT	c.MyDateTime AS RunDate
		,	c.Account
		,	SUM(1 - ISNULL(a.TCPAFlag,0)) AS PossibleActivations
	INTO	#FullActivations
	FROM	#Contacts c
		LEFT JOIN #TCPAFlagData a
			ON	a.Account = c.Account
			AND a.TCPAFlagDate < c.MyDateTime
			AND a.TCPAFlag = 1
	GROUP BY c.MyDateTime
		,	c.Account


	
	/*****************************************************************
	Pull the list of Contacted Accounts who declined TCPA
	*****************************************************************/	

	SELECT	l.Account
		,	CAST(lc.MyDateTime AS DATE) AS MyDateTime
		,	1 AS Declined
	INTO	#DeclinedTCPA
	FROM	MARS.dbo.vw_Loans l
		LEFT JOIN MARS.dbo.LoansContacts lc (NOLOCK)
			ON	l.RecID = lc.LoanRecID
	WHERE	lc.MyDateTime > '02/01/2017'		--These Categories only existed after this date
		AND	lc.Category = 'CD � Borr Declined TCPA'
	
	
	/*****************************************************************
	Calculate Possible Activations by subtracting the accounts who have 
		declined TCPA
	*****************************************************************/	
	SELECT	f.RunDate
		,	f.Account
		,	f.PossibleActivations - ISNULL(d.Declined,0) AS PossibleActivations
	INTO #PossActiveAccounts
	FROM #FullActivations f
		LEFT JOIN #DeclinedTCPA d
			ON	d.Account = f.Account
			AND d.MyDateTime > f.RunDate
	ORDER BY f.RunDate

	SELECT	p.RunDate
		,	SUM(p.PossibleActivations) AS PossibleActivations
	INTO #PossActive
	FROM #PossActiveAccounts p
	GROUP BY p.RunDate
	ORDER BY p.RunDate

	
	/*****************************************************************
	Count all the TCPA Flags that occurred on that day
	*****************************************************************/	
	SELECT	t.TCPAFlagDate
		,	COUNT(t.TCPAFlag) AS TCPACount
	INTO	#TCPAActive
	FROM	#TCPAFlagData t
	WHERE	t.TCPAFlag = 1
		AND t.TCPAFlagDate > @ProcessDate
	GROUP BY t.TCPAFlagDate
	ORDER BY t.TCPAFlagDate

	
	/*****************************************************************
	Return the results to the SSRS Report
	*****************************************************************/	
	SELECT	a.TCPAFlagDate
		,	a.TCPACount
		,	p.PossibleActivations
		,	CAST((CAST(a.TCPACount AS DECIMAL(12,3))/p.PossibleActivations)*100 AS DECIMAL(5,2)) AS TCPAPerc
	FROM #TCPAActive a
		INNER JOIN #PossActive p
			ON	p.RunDate = a.TCPAFlagDate
	WHERE	 a.TCPAFlagDate > getdate()-90
	ORDER BY a.TCPAFlagDate


END