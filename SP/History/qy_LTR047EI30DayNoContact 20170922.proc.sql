USE [MARS_DW]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.qy_LTR047EI30DayNoContact') IS NOT NULL
	DROP PROCEDURE dbo.qy_LTR047EI30DayNoContact;
GO

CREATE PROCEDURE dbo.qy_LTR047EI30DayNoContact
AS

/* ********************************************************************************************************* *
   Procedure Name  :	dbo.qy_LTR047EI30DayNoContact
   Business Analyis:	
   Project/Process :   
   Description     :	
   Issue ID			:	Jira - MARS-2082
   Author          :	Benjacob Beres
   Create Date     :	08/21/2017

   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date			Author				Description
   --------		-----------			------------
   
*  ********************************************************************************************************* */

BEGIN

IF OBJECT_ID('tempdb..#Temp') IS NOT NULL
	DROP TABLE #Temp;

SELECT	l.Account
	,	l.RecID
	,	ls.LoanID
	,	l.LoanStatus
	,	CASE WHEN l.LoanStatus IN (	'BK11','BK11,FC','BK12','BK13','BK13, DL','BK13,FC','BK7','BK7, DL','BK7,FC','FC,DIL','Inactive - REO/FC',
									'PRELIM','Service Xfer','Trailing Claims','FC','Paid in Full','Paidoff', 'DIL','FBA','PP') 
				THEN 'Y'
			 WHEN l.LoanStatus LIKE '%Inactive%'
				THEN 'Y'
			 WHEN l.LoanStatus LIKE 'BK13%'
				THEN 'Y'
			 ELSE 'N'
		END AS EliminatedByLoanStatus
	,	CASE WHEN BKDischarge IS NOT NULL 
				THEN 'Y'
			 ELSE 'N'
		END AS EliminatedByBKDischarge
	,	CASE WHEN RestrCorr IS NOT NULL 
				THEN 'Y'
			 ELSE 'N'
		END AS EliminatedByRestrictedCorr
	,	vgl.ReasonForRestrictCommunication
	,	CASE WHEN vgl.ReasonForRestrictCommunication LIKE '%Cease and Desist%'
				THEN 'Y'
			 ELSE 'N'
		END AS EliminatedByCandD
	,	DATEDIFF(D,l.NextDueDate,GETDATE()) AS dayspastdue
	,	CASE WHEN DATEDIFF(D,l.NextDueDate,GETDATE()) BETWEEN 31 AND 45
				THEN 'N'
			 ELSE 'Y'
		END AS EliminatedByDaysPastDue
INTO #Temp
FROM	mars.dbo.vw_loans l WITH(NOLOCK)
	JOIN	MARS.dbo.Loans ls
		ON	ls.LoanRecID = l.RecID
	LEFT JOIN MARS.dbo.vw_GetLoans vgl
		ON	vgl.LoanRecID = l.RecID
	LEFT JOIN
			(	SELECT	cf.Account
					,	cf.FieldValue AS BKDischarge 
				FROM	MARS.dbo.vw_CustomFieldsForLoans cf
				WHERE	cf.FieldName ='BK - Debt Discharged'
			) discharge
		ON	discharge.Account = l.Account
	LEFT JOIN
			(	SELECT	cf.Account
					,	cf.FieldValue AS RestrCorr 
				FROM	MARS.dbo.vw_CustomFieldsForLoans cf
				WHERE	cf.FieldName = 'RESTRICTED CORRESPONDENCE'
			) RC
		ON	RC.Account = l.Account
WHERE	l.isActive = 1
	
	
IF OBJECT_ID('tempdb..#Temp2') IS NOT NULL
	DROP TABLE #Temp2;


SELECT	t.*
	,	CASE WHEN b.LoanRecID IS NOT NULL 
				THEN 'Y' 
			 ELSE 'N'
		END AS EliminatedByCategoryInLast30Days
	,	CASE WHEN c.LoanRecID IS NOT NULL 
				THEN 'Y' 
			 ELSE 'N'
		END AS EliminatedByLTR047InLast30Days
	,	CASE WHEN cfpb.LoanID IS NOT NULL 
				THEN 'Y' 
			 ELSE 'N'
		END AS EliminatedByInLossMit
INTO	#Temp2
FROM	#Temp t
	LEFT JOIN
			(	SELECT	DISTINCT vlc.LoanRecID
				FROM	mars.dbo.vw_LoansContacts vlc WITH(NOLOCK) 
				WHERE	vlc.Category IN (	'AI - Attorney Contact on Inbound Call',
											'BI - Borrower Contact on Inbound Call',
											'BO - Borrower Contact on Outbound Call',
											'TC - Authorized 3rd Party on Inbound',
											'TO - Authorized 3rd Party on Outbound Call',
											'AO - Attorney Contact on Outbound Call',
											'L1 - Inbound Contact With Borrower',
											'L2 - Outbound Contact With Borrower',
											'L5 - Inbound Contact W/Authorized 3rd Party',
											'L6 - Outbound Contact W/Authorized 3rd Party',
											'L8 - Inbound Contact W/Attorney',
											'L9 - Outbound Contact W/Attorney'
										 ) 
					AND	DATEDIFF(D,vlc.MyDateTime,GETDATE()) <= 30
			) b
		ON	t.RecID = b.LoanRecID
	LEFT JOIN
			(	SELECT	DISTINCT vlc.LoanRecID
				FROM	mars.dbo.vw_LoansContacts vlc WITH(NOLOCK) 
				WHERE	vlc.Category IN ('LT - Letter Sent') 
					AND vlc.Contact = 'LTR047'
					AND	DATEDIFF(D,vlc.MyDateTime,GETDATE()) <= 30 
			) c
		ON	t.RecID = c.LoanRecID
	LEFT JOIN MARS.dbo.CFPBShortedUserEntry cfpb
		ON	cfpb.LoanId = t.LoanID
		AND cfpb.LMFilter = 'LM'
		AND cfpb.ProcessCompleted = 0


SELECT	t.Account
	,	t.dayspastdue AS DaysPastDue
	,	t.LoanStatus
FROM #Temp2 t
WHERE	t.EliminatedByLoanStatus = 'N'
	AND t.EliminatedByBKDischarge = 'N'
	AND t.EliminatedByRestrictedCorr = 'N'
	AND t.EliminatedByCandD = 'N'
	AND t.EliminatedByDaysPastDue = 'N'
	AND t.EliminatedByCategoryInLast30Days = 'N'
	AND t.EliminatedByLTR047InLast30Days = 'N'
	AND t.EliminatedByInLossMit = 'N'


END