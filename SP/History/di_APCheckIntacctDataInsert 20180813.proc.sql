USE MARS;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.di_APCheckIntacctDataInsert') IS NOT NULL
	DROP PROCEDURE dbo.di_APCheckIntacctDataInsert;
GO


CREATE PROCEDURE dbo.di_APCheckIntacctDataInsert
AS
BEGIN

/********************************************************************************************************** *
	Procedure Name		:	dbo.di_storedprocedurename
	Business Analyis	:	
	Project/Process		:   
	Creation Ticket		:	(JIRA) MARS-3199
	Author				:	Benjacob Beres
	Create Date			:	03/13/2018
	Description			:	Insert the new Intacct records into the REOAPCheck table so it can be displayed 
								in the MARS panel AP Checks.


	*********************************************************************************************************
	**         Change History                                                                              **
	*********************************************************************************************************
	Date		Author			Version			Ticket#(system)		Description of Change
	----------	-------------	-------------	-----------------	-----------------------
																
************************************************************************************************************ */

IF @User IS NULL 
  SET @User = SUSER_NAME ();


BEGIN TRY

	BEGIN TRAN

		INSERT INTO MARS.dbo.REOAPChecks (PropertyID, RefNbr, [Status], DocDate, CuryPmtAmt, CuryAdjdAmt, CuryTranAmt, [Name], ExtRefNbr, CheckSource)
		SELECT  CAST(vma.PropertyID AS VARCHAR(6))
			,	CAST(vma.RefNbr AS VARCHAR(12))
			,	CAST(vma.[Status] AS VARCHAR(1))
			,	CAST(vma.DocDate AS DATE)
			,	CAST(vma.CuryPmtAmt AS FLOAT)
			,	CAST(vma.CuryAdjdAmt AS FLOAT)
			,	CAST(vma.CuryTranAmt AS FLOAT)
			,	CAST(vma.[Name] AS VARCHAR(60))
			,	CAST(vma.CheckNotes  AS VARCHAR(30))
			,	CAST(vma.CheckSource AS VARCHAR(15))
		FROM IntacctDM.dbo.vw_MARSScreenAPCheck vma
		LEFT JOIN MARS.dbo.REOAPChecks mac
			ON	mac.PropertyID = vma.PropertyID
			AND	mac.DocDate = vma.DocDate
			AND mac.RefNbr = vma.RefNbr
		WHERE	mac.PropertyID IS NULL
			AND	vma.ChangeDate > GETDATE()-5


		
	COMMIT TRAN

END TRY

BEGIN CATCH

DECLARE @Err           INT
	,	@ErrorMessage  Varchar(Max)
	,	@ErrorLine     Varchar(128)
	,	@Workstation   VarChar(128)
	,	@Proc          VarChar(128)

	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION ;

	IF Error_Number() IS NULL 
		SET @Err =0;
	Else
		SET @Err = Error_Number();

	SET @ErrorMessage = Error_Message()
	SET @ErrorLine    = 'SP Line Number: ' + Cast(Error_Line() as varchar(10))
	SET @Workstation  = HOST_NAME()
	SET @Proc         = OBJECT_NAME(@@ProcID)

	EXEC Applications.dbo.di_ErrorLog	@Application ,@Version ,@Err, @ErrorMessage, @Proc, @ErrorLine, @User , @Workstation

END CATCH   

END