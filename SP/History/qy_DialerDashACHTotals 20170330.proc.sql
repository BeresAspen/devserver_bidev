USE MARS_DW;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.qy_DialerDashACHTotals') IS NOT NULL
	DROP PROCEDURE dbo.qy_DialerDashACHTotals;
GO

/* ********************************************************************************************************* *
   Procedure Name  :	qy_DialerDashACHTotals
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	02/27/2017

   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date			Author				Description
   --------		-----------			------------
   
*  ********************************************************************************************************* */
CREATE PROCEDURE dbo.qy_DialerDashACHTotals 

AS
BEGIN

	DECLARE @ProcessDate DATE
	SET @ProcessDate = CAST(GETDATE()-11 AS DATE)

	/*****************************************************************
	Pull Contact Only Call information from the single source proc
	*****************************************************************/
	CREATE TABLE #ResultswithCarryover
		(	Account	VARCHAR(30)
		,	ArchiveDate DATE
		,	CallType VARCHAR(30)
		,	Carryover INT
		)

	INSERT INTO #ResultswithCarryover
	EXEC qy_DialerDashDistinctAccountContact 11

	


	/*****************************************************************
	Pull the list of all Accounts who were contacted.
	*****************************************************************/	
	SELECT DISTINCT 
			CAST(lc.MyDateTime AS DATE) AS MyDateTime
		,	l.Account
		,	CASE WHEN lc.Category IN (	'AI - Attorney Contact on Inbound Call'
										,	'BI - Borrower Contact on Inbound Call'
										,	'TC - Authorized 3rd Party on Inbound'
										,	'NA - Non-Authorized 3rd Party on Inbound Call') 
						THEN 'InboundContact'
				 WHEN lc.Category IN (	'TO - Authorized 3rd Party on Outbound Call'
										,	'AO - Attorney Contact on Outbound Call'
										,	'BO - Borrower Contact on Outbound Call') 
						THEN 'OutboundContactNotOnDialer'
			END AS CallType
	INTO	#Contacts
	FROM	MARS.dbo.LoansContacts lc (NOLOCK)
	LEFT JOIN MARS.dbo.vw_Loans l
		ON	l.RecID = lc.LoanRecID
	WHERE lc.MyDateTime >= @ProcessDate
		AND lc.Category IN (	'TO - Authorized 3rd Party on Outbound Call'
							,	'AO - Attorney Contact on Outbound Call'
							,	'BO - Borrower Contact on Outbound Call'
							,	'AI - Attorney Contact on Inbound Call'
							,	'BI - Borrower Contact on Inbound Call'
							,	'TC - Authorized 3rd Party on Inbound'
							,	'NA - Non-Authorized 3rd Party on Inbound Call') 
	

	/*****************************************************************
	Retreive data from ACH to apply additional aggregates to the day
	*****************************************************************/
	SELECT	a.LoanNumber
		,	CAST(a.InsertedDate AS DATE) AS InsertedDate
		,	SUM(a.[Billing Amount]) AS TotalACH
	INTO #ACHData
	FROM MARS.[dbo].[vw_ACHBillingsEnteredByLoanByRep] a
	WHERE	CAST(a.InsertedDate AS DATE) >= GETDATE()-11
		AND	[Reoccuring Schedule/One Time] = 'One Time'
	GROUP BY a.LoanNumber
		,	CAST(a.InsertedDate AS DATE)
			 
	
	/*****************************************************************
	Aggregate the detail for the previous day only
	*****************************************************************/
	SELECT	r.ArchiveDate
		,	COUNT(r.Account) AS Total
		,	SUM(r.Carryover) AS CarryOverCnt
		,	SUM(CASE WHEN r.CallType = 'InboundContact' THEN 1 ELSE 0 END) AS InboundContact
		,	SUM(CASE WHEN r.CallType = 'OutboundContact' THEN 1 ELSE 0 END) AS OutboundContact
		,	SUM(CASE WHEN r.CallType = 'OutboundAttempt' THEN 1 ELSE 0 END) AS OutboundAttempt
		,	SUM(CASE WHEN r.CallType = 'OutboundAttemptNotOnDialer' THEN 1 ELSE 0 END) AS OutboundAttemptNotOnDialer
		,	SUM(CASE WHEN r.CallType = 'OutboundContactNotOnDialer' THEN 1 ELSE 0 END) AS OutboundContactNotOnDialer
		,	SUM(CASE WHEN r.Carryover = 1 AND r.CallType IN ('Untouched Loans', 'OutboundAttempt', 'OutboundAttemptNotOnDialer') THEN 1 ELSE 0 END) AS UntouchedCarryoverLoans
		,	SUM(CASE WHEN r.CallType = 'Untouched Loans' THEN 1 ELSE 0 END) AS UnTouchedLoans
		,	SUM(CASE WHEN r.CallType = 'InboundContact' AND a.LoanNumber IS NOT NULL THEN 1 ELSE 0 END) AS ICACHCount
		,	SUM(CASE WHEN r.CallType = 'InboundContact' AND a.LoanNumber IS NOT NULL THEN a.TotalACH ELSE 0 END) AS ICACHSum
		,	SUM(CASE WHEN r.CallType = 'OutboundContact' AND a.LoanNumber IS NOT NULL THEN 1 ELSE 0 END) AS OCACHCount
		,	SUM(CASE WHEN r.CallType = 'OutboundContact' AND a.LoanNumber IS NOT NULL THEN a.TotalACH ELSE 0 END) AS OCACHSum
		,	SUM(CASE WHEN r.CallType = 'OutboundAttemptNotOnDialer' AND a.LoanNumber IS NOT NULL THEN 1 ELSE 0 END) AS OMACHCount
		,	SUM(CASE WHEN r.CallType = 'OutboundAttemptNotOnDialer' AND a.LoanNumber IS NOT NULL THEN a.TotalACH ELSE 0 END) AS OMACHSum
		,	SUM(CASE WHEN a.LoanNumber IS NOT NULL THEN 1 ELSE 0 END) AS TotACHCount
		,	SUM(CASE WHEN a.LoanNumber IS NOT NULL THEN a.TotalACH ELSE 0 END) AS TotACHSum
	INTO #TotalCounts
	FROM	#ResultswithCarryover r
		LEFT JOIN #ACHData a
			ON	a.LoanNumber = r.Account
			AND a.InsertedDate = r.ArchiveDate
	GROUP BY r.ArchiveDate
	ORDER BY r.ArchiveDate


	SELECT *
	FROM #ResultswithCarryover


-------------------------------------------------------------------------------
	

	SELECT	t.ArchiveDate AS CallDate
		,	1 AS RowCnt
		,	CAST('' AS VARCHAR(3)) AS FieldGroup
		,	CAST('' AS VARCHAR(40)) AS Field
		,	CONVERT(VARCHAR(15),t.ArchiveDate,101) AS FldValue
	INTO #ToPivot
	FROM #TotalCounts t
	UNION ALL
	SELECT DISTINCT
			t.ArchiveDate AS CallDate
		,	2 AS RowCnt
		,	'IC' AS FieldGroup
		,	'Inbound Contact' AS Field
		,	CAST('' AS VARCHAR(15)) AS FldValue
	FROM #TotalCounts t
	UNION ALL
	SELECT	t.ArchiveDate AS CallDate
		,	3 AS RowCnt
		,	'IC' AS FieldGroup
		,	'Unique Borrowers where ACH collected' AS Field
		,	CAST(t.ICACHCount AS VARCHAR(15)) AS FldValue
	FROM #TotalCounts t
	UNION ALL
	SELECT	t.ArchiveDate AS CallDate
		,	4 AS RowCnt
		,	'IC' AS FieldGroup
		,	'Total Billing Amount Collected' AS Field
		,	CAST(t.ICACHSum AS VARCHAR(15)) AS FldValue
	FROM #TotalCounts t
	UNION ALL
	SELECT	t.ArchiveDate AS CallDate
		,	5 AS RowCnt
		,	'IC' AS FieldGroup
		,	'% of ACH collected vs All Contacts' AS Field
		,	CAST(CAST(CAST(CAST(t.ICACHCount AS DECIMAL(9,3))/t.Total AS DECIMAL(5,3)) AS VARCHAR(15)) AS VARCHAR(15)) AS FldValue
	FROM #TotalCounts t
	UNION ALL
	SELECT DISTINCT
			t.ArchiveDate AS CallDate
		,	6 AS RowCnt
		,	'OC' AS FieldGroup
		,	'Outbound Contact' AS Field
		,	CAST('' AS VARCHAR(15)) AS FldValue
	FROM #TotalCounts t
	UNION ALL
	SELECT	t.ArchiveDate AS CallDate
		,	7 AS RowCnt
		,	'OC' AS FieldGroup
		,	'Unique Borrowers where ACH collected' AS Field
		,	CAST(t.OCACHCount AS VARCHAR(15)) AS FldValue
	FROM #TotalCounts t
	UNION ALL
	SELECT	t.ArchiveDate AS CallDate
		,	8 AS RowCnt
		,	'OC' AS FieldGroup
		,	'Total Billing Amount Collected' AS Field
		,	CAST(t.OCACHSum AS VARCHAR(15)) AS FldValue
	FROM #TotalCounts t
	UNION ALL
	SELECT	t.ArchiveDate AS CallDate
		,	9 AS RowCnt
		,	'OC' AS FieldGroup
		,	'% of ACH collected vs All Contacts' AS Field
		,	CAST(CAST(CAST(t.OCACHCount AS DECIMAL(9,3))/t.Total AS DECIMAL(5,3)) AS VARCHAR(15)) AS FldValue
	FROM #TotalCounts t
	UNION ALL
	SELECT DISTINCT
			t.ArchiveDate AS CallDate
		,	10 AS RowCnt
		,	'OM' AS FieldGroup
		,	'Outbound Manual' AS Field
		,	CAST('' AS VARCHAR(15)) AS FldValue
	FROM #TotalCounts t
	UNION ALL
	SELECT	t.ArchiveDate AS CallDate
		,	11 AS RowCnt
		,	'OM' AS FieldGroup
		,	'Unique Borrowers where ACH collected' AS Field
		,	CAST(t.OMACHCount AS VARCHAR(15)) AS FldValue
	FROM #TotalCounts t
	UNION ALL
	SELECT	t.ArchiveDate AS CallDate
		,	12 AS RowCnt
		,	'OM' AS FieldGroup
		,	'Total Billing Amount Collected' AS Field
		,	CAST(t.OMACHSum AS VARCHAR(15)) AS FldValue
	FROM #TotalCounts t
	UNION ALL
	SELECT	t.ArchiveDate AS CallDate
		,	13 AS RowCnt
		,	'OM' AS FieldGroup
		,	'% of ACH collected vs All Contacts' AS Field
		,	CAST(CAST(CAST(t.OMACHCount AS DECIMAL(9,3))/t.Total AS DECIMAL(5,3)) AS VARCHAR(15)) AS FldValue
	FROM #TotalCounts t
	UNION ALL
	SELECT DISTINCT
			t.ArchiveDate AS CallDate
		,	14 AS RowCnt
		,	'Tot' AS FieldGroup
		,	'Total Contacts' AS Field
		,	CAST('' AS VARCHAR(15)) AS FldValue
	FROM #TotalCounts t
	UNION ALL
	SELECT	t.ArchiveDate AS CallDate
		,	15 AS RowCnt
		,	'Tot' AS FieldGroup
		,	'Unique Borrowers where ACH collected' AS Field
		,	CAST(t.TotACHCount AS VARCHAR(15)) AS FldValue
	FROM #TotalCounts t
	UNION ALL
	SELECT	t.ArchiveDate AS CallDate
		,	16 AS RowCnt
		,	'Tot' AS FieldGroup
		,	'Total Billing Amount Collected' AS Field
		,	CAST(t.TotACHSum AS VARCHAR(15)) AS FldValue
	FROM #TotalCounts t
	UNION ALL
	SELECT	t.ArchiveDate AS CallDate
		,	17 AS RowCnt
		,	'Tot' AS FieldGroup
		,	'% of ACH collected vs All Contacts' AS Field
		,	CAST(CAST(CAST(t.TotACHCount AS DECIMAL(9,3))/t.Total AS DECIMAL(5,3)) AS VARCHAR(15)) AS FldValue
	FROM #TotalCounts t

	
------------------------------------------------------------------
	SELECT	pt.Field
		,	pt.RowCnt
		,	pt.[1]
		,	pt.[2]
		,	pt.[3]
		,	pt.[4]
		,	pt.[5]
		,	pt.[6]
		,	pt.[7]
		,	pt.[8]
		,	pt.[9]
		,	pt.[10]
	INTO	#Temp
	FROM 
		(	SELECT	ROW_NUMBER() OVER(PARTITION BY tp.RowCnt ORDER BY tp.CallDate ASC) AS RowNum
				,	tp.RowCnt
				,	tp.Field
				,	tp.FldValue
			FROM #ToPivot tp
		) st

		PIVOT
		(
			MAX(FldValue)
			FOR RowNum IN ( [1], [2], [3], [4], [5], [6], [7], [8], [9], [10])
		) AS pt
	ORDER BY pt.RowCnt	


	UPDATE t
		SET		[1] = '$'+CONVERT(varchar, CAST(CAST(t.[1] AS decimal(15,2)) AS MONEY), 1)
			,	[2] = '$'+CONVERT(varchar, CAST(CAST(t.[2] AS decimal(15,2)) AS MONEY), 1)
			,	[3] = '$'+CONVERT(varchar, CAST(CAST(t.[3] AS decimal(15,2)) AS MONEY), 1)
			,	[4] = '$'+CONVERT(varchar, CAST(CAST(t.[4] AS decimal(15,2)) AS MONEY), 1)
			,	[5] = '$'+CONVERT(varchar, CAST(CAST(t.[5] AS decimal(15,2)) AS MONEY), 1)
			,	[6] = '$'+CONVERT(varchar, CAST(CAST(t.[6] AS decimal(15,2)) AS MONEY), 1)
			,	[7] = '$'+CONVERT(varchar, CAST(CAST(t.[7] AS decimal(15,2)) AS MONEY), 1)
			,	[8] = '$'+CONVERT(varchar, CAST(CAST(t.[8] AS decimal(15,2)) AS MONEY), 1)
			,	[9] = '$'+CONVERT(varchar, CAST(CAST(t.[9] AS decimal(15,2)) AS MONEY), 1)
			,	[10] = '$'+CONVERT(varchar, CAST(CAST(t.[10] AS decimal(15,2)) AS MONEY), 1)
		FROM	#Temp t
		WHERE	t.RowCnt IN (4,8,12,16)


	UPDATE t
		SET		[1] = CAST(CAST(CAST([1] AS DECIMAL(8,4))*100 AS DECIMAL(6,2)) AS VARCHAR(7))+'%'
			,	[2] = CAST(CAST(CAST([2] AS DECIMAL(8,4))*100 AS DECIMAL(6,2)) AS VARCHAR(7))+'%'
			,	[3] = CAST(CAST(CAST([3] AS DECIMAL(8,4))*100 AS DECIMAL(6,2)) AS VARCHAR(7))+'%'
			,	[4] = CAST(CAST(CAST([4] AS DECIMAL(8,4))*100 AS DECIMAL(6,2)) AS VARCHAR(7))+'%'
			,	[5] = CAST(CAST(CAST([5] AS DECIMAL(8,4))*100 AS DECIMAL(6,2)) AS VARCHAR(7))+'%'
			,	[6] = CAST(CAST(CAST([6] AS DECIMAL(8,4))*100 AS DECIMAL(6,2)) AS VARCHAR(7))+'%'
			,	[7] = CAST(CAST(CAST([7] AS DECIMAL(8,4))*100 AS DECIMAL(6,2)) AS VARCHAR(7))+'%'
			,	[8] = CAST(CAST(CAST([8] AS DECIMAL(8,4))*100 AS DECIMAL(6,2)) AS VARCHAR(7))+'%'
			,	[9] = CAST(CAST(CAST([9] AS DECIMAL(8,4))*100 AS DECIMAL(6,2)) AS VARCHAR(7))+'%'
			,	[10] = CAST(CAST(CAST([10] AS DECIMAL(8,4))*100 AS DECIMAL(6,2)) AS VARCHAR(7))+'%'
		FROM	#Temp t
		WHERE	t.RowCnt IN (5,9,13,17)


	SELECT	t.Field
		,	t.[1]
		,	t.[2]
		,	t.[3]
		,	t.[4]
		,	t.[5]
		,	t.[6]
		,	t.[7]
		,	t.[8]
		,	t.[9]
		,	t.[10]
	FROM	#Temp t
	ORDER BY t.RowCnt


END
