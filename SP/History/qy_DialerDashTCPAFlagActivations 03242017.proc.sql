USE [MARS_DW]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.qy_DialerDashTCPAFlagActivations') IS NOT NULL
	DROP PROCEDURE dbo.qy_DialerDashTCPAFlagActivations;
GO

/* -----------------------------------------------------------------------------------------------------------
   Procedure Name  :	qy_DialerDashTCPAFlagActivations
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	02/07/2017

   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date       Author        Description
   --------   -----------   ------------

*/ -----------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[qy_DialerDashTCPAFlagActivations]
AS
BEGIN

	SELECT	T.Account
		,	MAX(T.TCPAMinFlagDate) AS TCPAFlagDate
		,	SUM(T.TCPAFlag) AS TCPAFlag
	INTO	#TCPAFlagData
	FROM (
			SELECT	vla.Account
				,	CASE WHEN CAST(ISNULL(vla.PhoneHomeSelect,0) AS SMALLINT) + CAST(ISNULL(vla.PhoneWorkSelect,0) AS SMALLINT) 
								+ CAST(ISNULL(vla.PhoneCellSelect,0) AS SMALLINT) + CAST(ISNULL(vla.HomeSelect,0) AS SMALLINT) 
								+ CAST(ISNULL(vla.WorkSelect,0) AS SMALLINT) + CAST(ISNULL(vla.MobileSelect,0) AS SMALLINT) > 0
							THEN 1
						 ELSE 0
					END AS TCPAFlag
				,	MIN(vla.ArchiveDate) AS TCPAMinFlagDate
			FROM	dbo.vw_Loans_Archive vla
				INNER JOIN	MARS.dbo.vw_Loans mvl
					ON	mvl.Account = vla.Account
					AND mvl.isActive = 1
			GROUP BY vla.Account
				,	CASE WHEN CAST(ISNULL(vla.PhoneHomeSelect,0) AS SMALLINT) + CAST(ISNULL(vla.PhoneWorkSelect,0) AS SMALLINT) 
								+ CAST(ISNULL(vla.PhoneCellSelect,0) AS SMALLINT) + CAST(ISNULL(vla.HomeSelect,0) AS SMALLINT) 
								+ CAST(ISNULL(vla.WorkSelect,0) AS SMALLINT) + CAST(ISNULL(vla.MobileSelect,0) AS SMALLINT) > 0
							THEN 1
						 ELSE 0
					END
		 ) T
	GROUP BY T.Account


	SELECT	t.TCPAFlagDate
		,	COUNT(t.TCPAFlag) AS TCPACount
	FROM	#TCPAFlagData t
	WHERE	t.TCPAFlag = 1
		AND t.TCPAFlagDate > '11/08/2016'
	GROUP BY t.TCPAFlagDate
	ORDER BY t.TCPAFlagDate

END