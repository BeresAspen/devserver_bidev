USE IntacctDM;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('di_SyncDMTablesFromIntacct') IS NOT NULL
	DROP PROCEDURE di_SyncDMTablesFromIntacct;
GO


CREATE PROCEDURE dbo.di_SyncDMTablesFromIntacct (@IntacctObject VARCHAR(50))
AS
BEGIN

/********************************************************************************************************** *
   Procedure Name  :	di_SyncDMTablesFromIntacct
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	11/17/2017

   *********************************************************************************************************
   **         Change History                                                                              **
   *********************************************************************************************************

   Date			Author				Description
   --------		-----------			------------
   
************************************************************************************************************ */


-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
--APBILL Process
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
IF @IntacctObject = 'APBILL'
BEGIN

	--When Records exists already, check if the modified date is different. If so, move Primary Record to History and insert Staging into Primary
	IF OBJECT_ID('tempdb..#tempAPBill') IS NOT NULL
		DROP TABLE #tempAPBill;

	--List of Existing Records that have been modified
	SELECT iab.*
	INTO #tempAPBill
	FROM Staging.dbo.IntacctStgAPBill sab
	JOIN IntacctDM.dbo.APBill iab
		ON	iab.RECORDNO = sab.RECORDNO
	WHERE CAST(iab.WHENMODIFIED AS DATETIME) < CAST(sab.WHENMODIFIED AS DATETIME)


	--Move existing records to History table
	INSERT INTO IntacctDM.dbo.hstAPBill
	SELECT	*
		,	GETDATE() AS InsertedDate
		,	SYSTEM_USER AS InsertedBy 
	FROM #tempAPBill

	--Delete the records from the Primary table
	DELETE 
	--select *
	FROM IntacctDM.dbo.APBill
	WHERE EXISTS (	SELECT 1
					FROM #tempAPBill t
					WHERE t.RECORDNO = APBill.RECORDNO)

	--Insert records that do not exist in Primary (this includes new records and the replacements for the records that were just deleted)
	INSERT INTO IntacctDM.dbo.APBill
	SELECT sab.*
	FROM Staging.dbo.IntacctStgAPBill sab
	LEFT JOIN IntacctDM.dbo.APBill iab
		ON	iab.RECORDNO = sab.RECORDNO
	WHERE iab.RECORDNO IS NULL

END

-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
--APBILLITEM Process
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
IF @IntacctObject = 'APBILLITEM'
BEGIN

	--When Records exists already, check if the modified date is different. If so, move Primary Record to History and insert Staging into Primary
	IF OBJECT_ID('tempdb..#tempAPBillItem') IS NOT NULL
		DROP TABLE #tempAPBillItem;

	--List of Existing Records
	SELECT iab.*
	INTO #tempAPBillItem
	FROM Staging.dbo.IntacctStgAPBillItem sab
	JOIN IntacctDM.dbo.APBillItem iab
		ON	iab.RECORDNO = sab.RECORDNO
	WHERE CAST(iab.WHENMODIFIED AS DATETIME) < CAST(sab.WHENMODIFIED AS DATETIME)


	--Move existing records to History table
	INSERT INTO IntacctDM.dbo.hstAPBillItem
	SELECT  *
		,	GETDATE() AS InsertedDate
		,	SYSTEM_USER AS InsertedBy 
	FROM #tempAPBillItem

	--Delete the records from the Primary table
	DELETE 
	FROM IntacctDM.dbo.APBillItem
	WHERE EXISTS (	SELECT 1
					FROM #tempAPBillItem t
					WHERE t.RECORDNO = IntacctDM.dbo.APBillItem.RECORDNO)
				
	--Insert records that do not exist in Primary (this includes new records and the replacements for the records that were just deleted)
	INSERT INTO IntacctDM.dbo.APBillItem
	SELECT sab.*
	FROM Staging.dbo.IntacctStgAPBillItem sab
	LEFT JOIN IntacctDM.dbo.APBillItem iab
		ON	iab.RECORDNO = sab.RECORDNO
	WHERE iab.RECORDNO IS NULL

END

-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
--VENDOR Process
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
IF @IntacctObject = 'VENDOR'
BEGIN

	--When Records exists already, check if the modified date is different. If so, move Primary Record to History and insert Staging into Primary
	IF OBJECT_ID('tempdb..#tempVendor') IS NOT NULL
		DROP TABLE #tempVendor;

	--List of Existing Records
	SELECT iab.*
	INTO #tempVendor
	FROM Staging.dbo.IntacctStgVendor sab
	JOIN IntacctDM.dbo.Vendor iab
		ON	iab.RECORDNO = sab.RECORDNO
	WHERE CAST(iab.WHENMODIFIED AS DATETIME) < CAST(sab.WHENMODIFIED AS DATETIME)


	--Move existing records to History table
	INSERT INTO IntacctDM.dbo.hstVendor
	SELECT  *
		,	GETDATE() AS InsertedDate
		,	SYSTEM_USER AS InsertedBy 
	FROM #tempVendor

	--Delete the records from the Primary table
	DELETE 
	--select *
	FROM IntacctDM.dbo.Vendor
	WHERE EXISTS (	SELECT 1
					FROM #tempVendor t
					WHERE t.RECORDNO = Vendor.RECORDNO)
				
	--Insert records that do not exist in Primary (this includes new records and the replacements for the records that were just deleted)
	INSERT INTO IntacctDM.dbo.Vendor
	SELECT sab.*
	FROM Staging.dbo.IntacctStgVendor sab
	LEFT JOIN IntacctDM.dbo.Vendor iab
		ON	iab.RECORDNO = sab.RECORDNO
	WHERE iab.RECORDNO IS NULL

END

-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
--APPYMT Process
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
IF @IntacctObject = 'APPYMT'
BEGIN

	--When Records exists already, check if the modified date is different. If so, move Primary Record to History and insert Staging into Primary
	IF OBJECT_ID('tempdb..#tempAPPymt') IS NOT NULL
		DROP TABLE #tempAPPymt;

	--List of Existing Records
	SELECT iab.*
	INTO #tempAPPymt
	FROM Staging.dbo.IntacctStgAPPymt sab
	JOIN IntacctDM.dbo.APPymt iab
		ON	iab.RECORDNO = sab.RECORDNO
	WHERE CAST(iab.WHENMODIFIED AS DATETIME) < CAST(sab.WHENMODIFIED AS DATETIME)


	--Move existing records to History table
	INSERT INTO IntacctDM.dbo.hstAPPymt
	SELECT  *
		,	GETDATE() AS InsertedDate
		,	SYSTEM_USER AS InsertedBy 
	FROM #tempAPPymt

	--Delete the records from the Primary table
	DELETE 
	FROM IntacctDM.dbo.APPymt
	WHERE EXISTS (	SELECT 1
					FROM #tempAPPymt t
					WHERE t.RECORDNO = IntacctDM.dbo.APPymt.RECORDNO)
				
	--Insert records that do not exist in Primary (this includes new records and the replacements for the records that were just deleted)
	INSERT INTO IntacctDM.dbo.APPymt
	SELECT sab.*
	FROM Staging.dbo.IntacctStgAPPymt sab
	LEFT JOIN IntacctDM.dbo.APPymt iab
		ON	iab.RECORDNO = sab.RECORDNO
	WHERE iab.RECORDNO IS NULL

END

-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
--APPYMTDETAIL Process
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
IF @IntacctObject = 'APPYMTDETAIL'
BEGIN

	--When Records exists already, check if the modified date is different. If so, move Primary Record to History and insert Staging into Primary
	IF OBJECT_ID('tempdb..#tempAPPymtDetail') IS NOT NULL
		DROP TABLE #tempAPPymtDetail;

	--List of Existing Records
	SELECT iab.*
	INTO #tempAPPymtDetail
	FROM Staging.dbo.IntacctStgAPPymtDetail sab
	JOIN IntacctDM.dbo.APPymtDetail iab
		ON	iab.RECORDNO = sab.RECORDNO
	WHERE CAST(iab.WHENMODIFIED AS DATETIME) < CAST(sab.WHENMODIFIED AS DATETIME)


	--Move existing records to History table
	INSERT INTO IntacctDM.dbo.hstAPPymtDetail
	SELECT  *
		,	GETDATE() AS InsertedDate
		,	SYSTEM_USER AS InsertedBy 
	FROM #tempAPPymtDetail


	--Delete the records from the Primary table
	DELETE 
	FROM IntacctDM.dbo.APPymtDetail
	WHERE EXISTS (	SELECT 1
					FROM #tempAPPymtDetail t
					WHERE t.RECORDNO = IntacctDM.dbo.APPymtDetail.RECORDNO)
				
	--Insert records that do not exist in Primary (this includes new records and the replacements for the records that were just deleted)
	INSERT INTO IntacctDM.dbo.APPymtDetail
	SELECT sab.*
	FROM Staging.dbo.IntacctStgAPPymtDetail sab
	LEFT JOIN IntacctDM.dbo.APPymtDetail iab
		ON	iab.RECORDNO = sab.RECORDNO
	WHERE iab.RECORDNO IS NULL

END

-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
--CHECKINGACCOUNT Process
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
IF @IntacctObject = 'CHECKINGACCOUNT'
BEGIN

	--When Records exists already, check if the modified date is different. If so, move Primary Record to History and insert Staging into Primary
	IF OBJECT_ID('tempdb..#tempCheckingAccount') IS NOT NULL
		DROP TABLE #tempCheckingAccount;

	--List of Existing Records
	SELECT iab.*
	INTO #tempCheckingAccount
	FROM Staging.dbo.IntacctStgCheckingAccount sab
	JOIN IntacctDM.dbo.CheckingAccount iab
		ON	iab.RECORDNO = sab.RECORDNO
	WHERE CAST(iab.WHENMODIFIED AS DATETIME) < CAST(sab.WHENMODIFIED AS DATETIME)


	--Move existing records to History table
	INSERT INTO IntacctDM.dbo.hstCheckingAccount
	SELECT  *
		,	GETDATE() AS InsertedDate
		,	SYSTEM_USER AS InsertedBy 
	FROM #tempCheckingAccount

	--Delete the records from the Primary table
	DELETE 
	--select *
	FROM IntacctDM.dbo.CheckingAccount
	WHERE EXISTS (	SELECT 1
					FROM #tempCheckingAccount t
					WHERE t.RECORDNO = CheckingAccount.RECORDNO)
				
	--Insert records that do not exist in Primary (this includes new records and the replacements for the records that were just deleted)
	INSERT INTO IntacctDM.dbo.CheckingAccount
	SELECT sab.*
	FROM Staging.dbo.IntacctStgCheckingAccount sab
	LEFT JOIN IntacctDM.dbo.CheckingAccount iab
		ON	iab.RECORDNO = sab.RECORDNO
	WHERE iab.RECORDNO IS NULL

END

-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
--PODOCUMENT Process
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
IF @IntacctObject = 'PODOCUMENT'
BEGIN

	--When Records exists already, check if the modified date is different. If so, move Primary Record to History and insert Staging into Primary
	IF OBJECT_ID('tempdb..#tempPODocument') IS NOT NULL
		DROP TABLE #tempPODocument;

	--List of Existing Records
	SELECT iab.*
	INTO #tempPODocument
	FROM Staging.dbo.IntacctStgPODocument sab
	JOIN IntacctDM.dbo.PODocument iab
		ON	iab.RECORDNO = sab.RECORDNO
	WHERE CAST(iab.WHENMODIFIED AS DATETIME) < CAST(sab.WHENMODIFIED AS DATETIME)


	--Move existing records to History table
	INSERT INTO IntacctDM.dbo.hstPODocument
	SELECT  *
		,	GETDATE() AS InsertedDate
		,	SYSTEM_USER AS InsertedBy 
	FROM #tempPODocument

	--Delete the records from the Primary table
	DELETE 
	--select *
	FROM IntacctDM.dbo.PODocument
	WHERE EXISTS (	SELECT 1
					FROM #tempPODocument t
					WHERE t.RECORDNO = PODocument.RECORDNO)
				
	--Insert records that do not exist in Primary (this includes new records and the replacements for the records that were just deleted)
	INSERT INTO IntacctDM.dbo.PODocument
	SELECT sab.*
	FROM Staging.dbo.IntacctStgPODocument sab
	LEFT JOIN IntacctDM.dbo.PODocument iab
		ON	iab.RECORDNO = sab.RECORDNO
	WHERE iab.RECORDNO IS NULL

END

-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
--PODOCUMENTENTRY Process
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
IF @IntacctObject = 'PODOCUMENTENTRY'
BEGIN

	--When Records exists already, check if the modified date is different. If so, move Primary Record to History and insert Staging into Primary
	IF OBJECT_ID('tempdb..#tempPODocumentEntry') IS NOT NULL
		DROP TABLE #tempPODocumentEntry;

	--List of Existing Records
	SELECT iab.*
	INTO #tempPODocumentEntry
	FROM Staging.dbo.IntacctStgPODocumentEntry sab
	JOIN IntacctDM.dbo.PODocumentEntry iab
		ON	iab.RECORDNO = sab.RECORDNO
	WHERE CAST(iab.WHENMODIFIED AS DATETIME) < CAST(sab.WHENMODIFIED AS DATETIME)


	--Move existing records to History table
	INSERT INTO IntacctDM.dbo.hstPODocumentEntry
	SELECT  *
		,	GETDATE() AS InsertedDate
		,	SYSTEM_USER AS InsertedBy 
	FROM #tempPODocumentEntry

	--Delete the records from the Primary table
	DELETE 
	--select *
	FROM IntacctDM.dbo.PODocumentEntry
	WHERE EXISTS (	SELECT 1
					FROM #tempPODocumentEntry t
					WHERE t.RECORDNO = PODocumentEntry.RECORDNO)
				
	--Insert records that do not exist in Primary (this includes new records and the replacements for the records that were just deleted)
	INSERT INTO IntacctDM.dbo.PODocumentEntry
	SELECT sab.*
	FROM Staging.dbo.IntacctStgPODocumentEntry sab
	LEFT JOIN IntacctDM.dbo.PODocumentEntry iab
		ON	iab.RECORDNO = sab.RECORDNO
	WHERE iab.RECORDNO IS NULL

END

END