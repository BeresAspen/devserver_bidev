USE [MARS]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.di_DialerFileNightly') IS NOT NULL
	DROP PROCEDURE dbo.di_DialerFileNightly;
GO

CREATE PROCEDURE dbo.di_DialerFileNightly
AS

/* ********************************************************************************************************* *
   Procedure Name  :	di_DialerFileNightly
   Business Analyis:	
   Project/Process :   
   Description     :	Insert the DialerFile data every night
   Author          :	Benjacob Beres
   Create Date     :	

   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date			Author				Description
   --------		-----------			------------

*  ********************************************************************************************************* */


--create temp table to load stored proc results into
CREATE TABLE #Dialer
	(	[RunDate] [datetime] NOT NULL,
		[queuenumber] [int] NULL,
		cntr INT NULL,
		[Account] [nvarchar](10) NULL,
		LoanStatus varchar(100) NULL,
		[state] [nchar](3) NULL,
		[judicial] [nchar](3) NULL,
		[NewStatus] [nchar](25) NULL,
		[AssignedRep] [varchar](128) NULL,
		[TAD] [int] NOT NULL,
		[CurrentTrustBalance] [money] NOT NULL,
		[NextDueDate] [datetime] NULL,
		dayspastdue INT NULL,
		LastContactDate VARCHAR(10) NULL,
		LastContactType VARCHAR(128) NULL,
		[MonthlyPayment] [money] NULL,
		[BK - Debt Discharged] [varchar](max) NOT NULL,
		[DNCDetail] [varchar](max) NULL,
		[DialerHome] [varchar](1000) NOT NULL,
		[DialerWork] [varchar](1000) NOT NULL,
		[DialerMobile] [varchar](1000) NOT NULL,
		[DialerCoBorrowerHome] [varchar](1000) NOT NULL,
		[DialerCoBorrowerWork] [varchar](1000) NOT NULL,
		[DialerCoBorrowerMobile] [varchar](1000) NOT NULL,
		[Home] [varchar](1000) NOT NULL,
		[Work] [varchar](1000) NOT NULL,
		[Mobile] [varchar](1000) NOT NULL,
		[CoBorrowerHome] [varchar](1000) NOT NULL,
		[CoBorrowerWork] [varchar](1000) NOT NULL,
		[CoBorrowerMobile] [varchar](1000) NOT NULL,
		PrimaryPhone BIGINT,
		[TCPADialable] [int] NOT NULL
	)


-- run stored proc for dialy results
INSERT INTO #Dialer
EXEC MARS.dbo.qy_DailyDialerFileBase


--load new data into the dialerfile table
INSERT INTO MARS.dbo.dialerfile
SELECT RunDate, queuenumber, state, judicial, NewStatus, Account, AssignedRep, TAD, CurrentTrustBalance
	, NextDueDate, MonthlyPayment, [BK - Debt Discharged], DNCDetail, DialerHome, DialerWork, DialerMobile
	, DialerCoBorrowerHome, DialerCoBorrowerWork, DialerCoBorrowerMobile, Home, Work, Mobile, CoBorrowerHome
	, CoBorrowerWork, CoBorrowerMobile, TCPADialable, cntr, dayspastdue, LastContactDate, LastContactType, LoanStatus, PrimaryPhone
FROM #Dialer

