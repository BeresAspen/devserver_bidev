USE MARS_DW;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.qy_DialerDashTCPAAndActiveTotals') IS NOT NULL
	DROP PROCEDURE dbo.qy_DialerDashTCPAAndActiveTotals;
GO

/* -----------------------------------------------------------------------------------------------------------
   Procedure Name  :	qy_DialerDashTCPAAndActiveTotals
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	02/07/2017

   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date       Author        Description
   --------   -----------   ------------

*/ -----------------------------------------------------------------------------------------------------------

CREATE PROCEDURE dbo.qy_DialerDashTCPAAndActiveTotals
AS
BEGIN

SELECT	vla.Account
	,	CASE WHEN CAST(ISNULL(vla.PhoneHomeSelect,0) AS SMALLINT) + CAST(ISNULL(vla.PhoneWorkSelect,0) AS SMALLINT) 
					+ CAST(ISNULL(vla.PhoneCellSelect,0) AS SMALLINT) + CAST(ISNULL(vla.HomeSelect,0) AS SMALLINT) 
					+ CAST(ISNULL(vla.WorkSelect,0) AS SMALLINT) + CAST(ISNULL(vla.MobileSelect,0) AS SMALLINT) > 0
				THEN 1
				ELSE 0
		END AS TCPAFlag
	,	vla.ArchiveDate AS TCPAFlagDate
	,	CAST(mvl.isActive AS INT) AS Active
INTO	#TCPAFlags
FROM	dbo.vw_Loans_Archive vla
	INNER JOIN	MARS.dbo.vw_Loans mvl
		ON	mvl.Account = vla.Account
		AND mvl.isActive = 1



SELECT	t.TCPAFlagDate
	,	SUM(CASE WHEN t.TCPAFlag = 1 THEN 1 ELSE 0 END) AS TCPACount
	,	SUM(t.Active) AS Activecnt
FROM	#TCPAFlags t
WHERE t.TCPAFlagDate >= '11/08/2016'
GROUP BY t.TCPAFlagDate
HAVING SUM(CASE WHEN t.TCPAFlag = 1 THEN 1 ELSE 0 END) > 0


END