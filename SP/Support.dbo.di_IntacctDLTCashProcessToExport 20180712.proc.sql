USE [Support]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.di_IntacctDLTCashProcessToExport') IS NOT NULL
	DROP PROCEDURE dbo.di_IntacctDLTCashProcessToExport;
GO

CREATE PROCEDURE dbo.di_IntacctDLTCashProcessToExport
AS
BEGIN

/* ********************************************************************************************************* *
   Procedure Name  :	di_IntacctDLTCashProcessToExport
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	10/10/2017

   *********************************************************************************************************
   **         Change History                                                                              **
   *********************************************************************************************************

   Date			Author				Jira#			Description
   --------		-----------			----------		------------
   05/02/2018	Benjacob Beres		MARS-2980		Modified to add the grouping section at the end to 
														accomodate the request for the new import format
*  ********************************************************************************************************* */

	DECLARE @today DATE
	DECLARE @lookback INT

	SET @today = CAST(GETDATE() AS DATE)

	IF OBJECT_ID('tempdb..#DLTJETemp') IS NOT NULL 
		DROP TABLE #DLTJETemp;
	IF OBJECT_ID('tempdb..#CashMoves') IS NOT NULL 
		DROP TABLE #CashMoves;
	IF OBJECT_ID('tempdb..#CashMovesFinal') IS NOT NULL 
		DROP TABLE #CashMovesFinal;
	IF OBJECT_ID('tempdb..#PrevDate') IS NOT NULL 
		DROP TABLE #PrevDate;
	IF OBJECT_ID('tempdb..#ACHUsed') IS NOT NULL
		DROP TABLE #ACHUsed;

	-------------------------------------------------------------------------------------------
	;WITH cteDates
	AS
	(
		SELECT	dd.[Date]
			,	CAST(MAX(d1.[Date]) AS DATE) AS LastBusDay
			,	CAST(MIN(d2.[Date]) AS DATE) AS NextBusDay
		--INTO #PrevDate
		FROM	Support.dbo.DimDate dd
			JOIN Support.dbo.DimDate d1 
				ON	d1.DateKey < dd.DateKey
				AND d1.IsWeekday = 1 
				AND d1.IsHoliday = 0
				AND d1.[Date] BETWEEN DATEADD(DD,-90,@today) AND DATEADD(DD,90,@today)
			JOIN Support.dbo.DimDate d2
				ON	d2.DateKey > dd.DateKey
				AND d2.IsWeekday = 1 
				AND d2.IsHoliday = 0 
				AND d2.[Date] BETWEEN DATEADD(DD,-90,@today) AND DATEADD(DD,90,@today)
		WHERE	dd.[Date] BETWEEN DATEADD(DD,-45,@today) AND DATEADD(DD,45,@today)
			--AND dd.IsWeekday = 1 
			--AND dd.IsHoliday = 0
		GROUP BY dd.[Date]
	)
	, cteDates2
	AS
	(
		SELECT	c.[Date]
			,	c.LastBusDay
			,	c.NextBusDay
			,	c1.NextBusDay AS Next2ndBusDay
		FROM cteDates c
		JOIN cteDates c1
			ON	CAST(c1.[Date] AS DATE) = c.NextBusDay
	)
	, cteDates3
	AS
	(
		SELECT	c.[Date]
			,	c1.LastBusDay AS Last2ndBusDay
			,	c.LastBusDay
			,	c.NextBusDay
			,	c.Next2ndBusDay
		FROM cteDates2 c
		JOIN cteDates2 c1
			ON	CAST(c1.[Date] AS DATE) = c.LastBusDay
	)
		SELECT	c.[Date]
			,	c.Last2ndBusDay
			,	c.LastBusDay
			,	c.NextBusDay
			,	c.Next2ndBusDay
			,	c1.NextBusDay AS Next3rdBusDay
		INTO #PrevDate
		FROM cteDates3 c
		JOIN cteDates3 c1
			ON	CAST(c1.[Date] AS DATE) = c.Next2ndBusDay
		ORDER BY c.[Date];
	
	-------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------

	SELECT @lookback = DATEDIFF(DD,[Date],LastBusDay)
	FROM #PrevDate
	WHERE [Date] = @today

	--SELECT @lookback;

	--SET @lookback = -28;


	-------------------------------------------------------------------------------------------
	--Bring Data from the DLT Run
	-------------------------------------------------------------------------------------------

	SELECT ije.JOURNAL, 
			ije.[DATE], 
			ije.[DESCRIPTION], 
			ije.REFERENCE_NO, 
			ije.ACCT_NO, 
			CASE WHEN ije.LOCATION_ID = '150' and vl.NoteOwner = 'GFGP Mortgage Trust, a Delaware Trust, Wilmington Savings Fund Society, FSB, Trustee' 
						THEN '120' 
				 ELSE ije.LOCATION_ID END AS LOCATION_ID, 
			ije.DEBIT, 
			ije.CREDIT, 
			ije.GLDIMSTATUS, 
			ije.GLENTRY_CLASSID
	INTO #DLTJETemp
	FROM Support.dbo.IntacctJournalEntries ije
	LEFT JOIN MARS.dbo.vw_Loans vl
		ON	vl.Account = ije.GLENTRY_CLASSID


	-------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------


	SELECT	DISTINCT A.MARSID, CR.PaymentDate, 1 AS Dayadd
	INTO #ACHUsed
	FROM MARS.dbo.CashReceipt AS CR 
	JOIN MARS.dbo.AssetEntityMappings A 
		ON CR.AssetMappingID = A.AssetEntityMappingID
	WHERE	cr.PaymentDate >= DATEADD(DD,@lookback-7,@today)
		AND cr.PaymentDate < @today
		AND	cr.SourceTypeCD = 'ACH'

	---------------------------------------------------------------------------------------------------------

	--CASH Movements
	SELECT	JOURNAL
		,	[DATE]
		,	[DESCRIPTION]
		,	REFERENCE_NO
		,	ACCT_NO
		,	LOCATION_ID
		,	MEMO
		,	DEBIT
		,	CREDIT
	--	,	SUM(CAST(CASE WHEN RTRIM(LTRIM(DEBIT)) = '' THEN 0.00 ELSE RTRIM(LTRIM(DEBIT)) END AS DECIMAL(18,2))) AS DEBIT
	--	,	SUM(CAST(CASE WHEN RTRIM(LTRIM(CREDIT)) = '' THEN 0.00 ELSE RTRIM(LTRIM(CREDIT)) END AS DECIMAL(18,2))) AS CREDIT
		,	GLDIMSTATUS
		,	CASE WHEN (CAST(t.ACCT_NO AS INT) BETWEEN 10000 AND 13999) 
						AND CAST(CAST(CASE WHEN t.CREDIT = '' THEN '0.00' ELSE t.CREDIT END AS VARCHAR(17)) AS DECIMAL(17,2)) > 0 THEN 'PMTO'
					WHEN t.ACCT_NO = '15525' 
						AND CAST(CAST(CASE WHEN t.DEBIT = '' THEN '0.00' ELSE t.DEBIT END AS VARCHAR(17)) AS DECIMAL(17,2)) > 0 THEN 'PMTO'
					WHEN t.ACCT_NO = '23525' 
						AND CAST(CAST(CASE WHEN t.DEBIT = '' THEN '0.00' ELSE t.DEBIT END AS VARCHAR(17)) AS DECIMAL(17,2)) > 0 THEN 'PMTO'
					ELSE 'PMTI'
			END AS GLDIMACTIVITY
		,	GLENTRY_CUSTOMERID
		,	GLENTRY_CLASSID
	INTO #CashMoves
	FROM (	SELECT	'DLTTRANJ' AS JOURNAL
				,	CONVERT(VARCHAR(10), CASE WHEN ISNULL(ach.Dayadd,0) = 1 THEN pd.Next3rdBusDay ELSE pd.Next2ndBusDay END, 101) AS [DATE]
				,	d.[DESCRIPTION]
				,	d.[DESCRIPTION] + ' ' + CONVERT(VARCHAR(10), CASE WHEN ISNULL(ach.Dayadd,0) = 1 THEN pd.Next3rdBusDay ELSE pd.Next2ndBusDay END, 101) AS REFERENCE_NO
				,	ac.IncludeInCashMove
				,	CASE WHEN d.ACCT_NO IN ('14110', '14115', '14131', '18110', '18125', '40100', '46100', '40105', '15500', '18120', '18130') THEN ac.IntacctCollections
						-- WHEN d.ACCT_NO IN (, '18145') THEN '11000'  -- get back this escrow
							WHEN d.ACCT_NO = '23535' THEN '13602'
							WHEN d.ACCT_NO = '15515' THEN '13600'
							WHEN d.ACCT_NO = '23500' AND d.LOCATION_ID NOT IN ('480', '482') THEN '13600'
							WHEN d.ACCT_NO = '23500' AND d.LOCATION_ID IN ('480', '482') THEN ac.IntacctCollections
							WHEN d.ACCT_NO IN ('23505')  THEN ac2.IntacctEscrow			--associate escrow account with the companyid of the loan NOT 150
							WHEN d.ACCT_NO IN ('23525')  
												THEN	CASE WHEN ac2.IncludeInCashMove = 1
																THEN d.ACCT_NO
																ELSE ac2.IntacctCollections			--associate escrow account with the companyid of the loan NOT 150
														END
							WHEN d.ACCT_NO IN ('15505', '15510', '15525') THEN d.ACCT_NO --'23525', needs to be associated with the loan not 150
					END AS ACCT_NO
				,	d.LOCATION_ID
				,	CONVERT(VARCHAR(10), CASE WHEN ISNULL(ach.Dayadd,0) = 1 THEN pd.Next3rdBusDay ELSE pd.Next2ndBusDay END, 101) 
								+ ' DLTTRANJ' + CASE WHEN ISNULL(ach.Dayadd,0) = 1 THEN ' ACH' ELSE '' END AS MEMO
				,	d.CREDIT AS DEBIT
				,	d.DEBIT AS CREDIT
				,	d.GLDIMSTATUS
				,	'PMTI' AS GLDIMACTIVITY
				,	acom.IntacctCompanyNum AS GLENTRY_CUSTOMERID
				,	GLENTRY_CLASSID
			FROM #DLTJETemp d
			JOIN MARS.dbo.vw_Loans vl
				ON	vl.Account = d.REFERENCE_NO
			JOIN #PrevDate pd
				ON	pd.[Date] = d.[DATE]
			LEFT JOIN Support.dbo.AcctCompanyGLTranslations ac
				ON	ac.IntacctCo = d.LOCATION_ID
			LEFT JOIN MARS_DW.dbo.AcctConversionNoteOwnerMapping acom 
				ON	acom.TMONoteOwner = vl.NoteOwner
			LEFT JOIN Support.dbo.REPOLoanDetails rld
				ON	rld.Account = d.REFERENCE_NO
			LEFT JOIN #ACHUsed ach
				ON	ach.MARSID = d.REFERENCE_NO 
				AND ach.PaymentDate = pd.[DATE]
			LEFT JOIN Support.dbo.AcctCompanyGLTranslations ac2
				ON	ac2.IntacctCo = --acom.IntacctCompanyNum
				CASE WHEN acom.IntacctCompanyNum = '525' AND rld.Account IS NULL
											THEN '525A'
										 ELSE acom.IntacctCompanyNum
									END
			WHERE	d.[DESCRIPTION] IN ('REG', 'TRUST', 'PIF', 'TE')
			) t
	WHERE	T.ACCT_NO IS NOT NULL
		AND t.IncludeInCashMove = 1


	SELECT	JOURNAL
		,	[DATE]
		,	[DESCRIPTION]
		,	REFERENCE_NO
		,	LINE_NO
		,	ACCT_NO
		,	LOCATION_ID
		,	MEMO
		,	DEBIT
		,	CREDIT
		,	GLDIMSTATUS
		,	GLDIMACTIVITY
		,	GLENTRY_CUSTOMERID
		,	GLENTRY_CLASSID
	INTO #CashMovesFinal
	FROM (	SELECT	JOURNAL
				,	[DATE]
				,	[DESCRIPTION]
				,	REFERENCE_NO
				,	ROW_NUMBER() OVER(PARTITION BY LOCATION_ID, [DESCRIPTION], [DATE] ORDER BY REFERENCE_NO, ACCT_NO ) AS LINE_NO
				,	ACCT_NO
				,	LOCATION_ID
				,	MEMO
				,	DEBIT
				,	CREDIT
				,	GLDIMSTATUS
				,	GLDIMACTIVITY
				,	GLENTRY_CUSTOMERID
				,	GLENTRY_CLASSID
			FROM #CashMoves
		 ) c



	 
--------------------------------------------------------------------------------------
--Regular DLT
--------------------------------------------------------------------------------------
	TRUNCATE TABLE Support.dbo.IntacctJournalEntries;

	INSERT INTO Support.dbo.IntacctJournalEntries
				(JOURNAL, [DATE], [DESCRIPTION], REFERENCE_NO, LINE_NO, ACCT_NO, LOCATION_ID, MEMO, DEBIT, CREDIT, GLDIMSTATUS, GLDIMACTIVITY, GLENTRY_CUSTOMERID, GLENTRY_CLASSID)
	SELECT *
	FROM #CashMovesFinal
	WHERE MEMO NOT LIKE '%ACH%'


--------------------------------------------------------------------------------------
--ACH
--------------------------------------------------------------------------------------
	 
	TRUNCATE TABLE Support.dbo.IntacctJournalEntriesACH;

	INSERT INTO Support.dbo.IntacctJournalEntriesACH
				(JOURNAL, [DATE], [DESCRIPTION], REFERENCE_NO, LINE_NO, ACCT_NO, LOCATION_ID, MEMO, DEBIT, CREDIT, GLDIMSTATUS, GLDIMACTIVITY, GLENTRY_CUSTOMERID, GLENTRY_CLASSID)
	SELECT *
	FROM #CashMovesFinal
	WHERE MEMO LIKE '%ACH%'


---------------------------------------------------------------------------------------------------------------------------------------
--GROUPING SECTION
---------------------------------------------------------------------------------------------------------------------------------------
--Lockbox (non-ACH)
---------------------------------------------------------------------------------------------------------------------------------------

	IF OBJECT_ID('tempdb..#TempIJE') IS NOT NULL
		Drop Table #TempIJE;
	IF OBJECT_ID('tempdb..#TempIJE1') IS NOT NULL
		Drop Table #TempIJE1;
	IF OBJECT_ID('tempdb..#TempIJEBreakDown') IS NOT NULL
		Drop Table #TempIJEBreakDown;
	IF OBJECT_ID('tempdb..#TempIJECnt') IS NOT NULL
		Drop Table #TempIJECnt;

	Select	JOURNAL, 
			[DATE], 
			REVERSEDATE, 
			[DESCRIPTION], 
			REFERENCE_NO,
			ACCT_NO, 
			LOCATION_ID,
			MEMO,
			GLDIMSTATUS,
			GLENTRY_CUSTOMERID, 
			Sum(Convert(Numeric(16,2), Coalesce(Nullif(Debit,''), '0.0'))) as Debit,
			Sum(Convert(Numeric(16,2), Coalesce(Nullif(Credit,''), '0.0'))) as Credit
	Into #TempIJE
	From IntacctJournalEntries
	group by	JOURNAL, 
				[DATE], 
				REVERSEDATE, 
				[DESCRIPTION], 
				REFERENCE_NO,
				ACCT_NO, 
				LOCATION_ID,
				MEMO,
				GLDIMSTATUS,
				GLENTRY_CUSTOMERID


	select	JOURNAL, 
			[DATE], 
			REVERSEDATE, 
			[DESCRIPTION], 
			REFERENCE_NO,
			ACCT_NO, 
			LOCATION_ID,
			MEMO,
			GLDIMSTATUS,
			GLENTRY_CUSTOMERID,
			CASE WHEN Debit >= Credit THEN Debit - Credit ELSE 0.0 END AS Debit,
			CASE WHEN Credit >= Debit THEN Credit - Debit ELSE 0.0 END AS Credit
	into #TempIJE1
	from #TempIJE


	DELETE 
	FROM #TempIJE1 
	WHERE	Debit = 0 
		AND	Credit = 0

	
	select	DESCRIPTION,
			GLENTRY_CUSTOMERID, 
			LOCATION_ID,
			SUM(CASE WHEN Debit > 0 THEN 1 ELSE 0 END) AS DebitCnt,
			SUM(CASE WHEN Credit > 0 THEN 1 ELSE 0 END) AS CreditCnt
	into #TempIJECnt
	from #TempIJE1
	group by DESCRIPTION,GLENTRY_CUSTOMERID, LOCATION_ID


	select	t2.JOURNAL, 
			t2.[DATE], 
			t2.REVERSEDATE, 
			t2.[DESCRIPTION], 
			t2.REFERENCE_NO,
			t2.ACCT_NO, 
			t2.LOCATION_ID,
			t2.MEMO,
			t2.GLDIMSTATUS,
			t2.GLENTRY_CUSTOMERID,
			0.00 as Debit,
			t1.Debit AS Credit
	into #TempIJEBreakDown
	from #TempIJE1 t1
	join #TempIJE1 t2
		on	t1.GLENTRY_CUSTOMERID = t2.GLENTRY_CUSTOMERID
		and t1.LOCATION_ID = t2.LOCATION_ID
		and t1.DESCRIPTION = t2.DESCRIPTION
		and t2.Debit = 0
	join #TempIJECnt tc
		on	tc.GLENTRY_CUSTOMERID = t1.GLENTRY_CUSTOMERID
		and tc.LOCATION_ID = t1.LOCATION_ID
		and tc.DESCRIPTION = t1.DESCRIPTION
	where tc.DebitCnt > 1
		and tc.CreditCnt = 1
		and t1.Debit > 0

	UNION ALL
	
	select	t1.JOURNAL, 
			t1.[DATE], 
			t1.REVERSEDATE, 
			t1.[DESCRIPTION], 
			t1.REFERENCE_NO,
			t1.ACCT_NO, 
			t1.LOCATION_ID,
			t1.MEMO,
			t1.GLDIMSTATUS,
			t1.GLENTRY_CUSTOMERID,
			t1.Debit AS Debit,
			0.00 as Credit
	from #TempIJE1 t1
	join #TempIJECnt tc
		on	tc.GLENTRY_CUSTOMERID = t1.GLENTRY_CUSTOMERID
		and tc.LOCATION_ID = t1.LOCATION_ID
		and tc.DESCRIPTION = t1.DESCRIPTION
	where tc.DebitCnt > 1
		and tc.CreditCnt = 1
		and t1.Debit > 0

	UNION ALL

	select	t2.JOURNAL, 
			t2.[DATE], 
			t2.REVERSEDATE, 
			t2.[DESCRIPTION], 
			t2.REFERENCE_NO,
			t2.ACCT_NO, 
			t2.LOCATION_ID,
			t2.MEMO,
			t2.GLDIMSTATUS,
			t2.GLENTRY_CUSTOMERID,
			t1.Credit as Debit,
			0.0 AS Credit
	from #TempIJE1 t1
	join #TempIJE1 t2
		on	t1.GLENTRY_CUSTOMERID = t2.GLENTRY_CUSTOMERID
		and t1.LOCATION_ID = t2.LOCATION_ID
		and t1.DESCRIPTION = t2.DESCRIPTION
		and t2.Credit = 0
	join #TempIJECnt tc
		on	tc.GLENTRY_CUSTOMERID = t1.GLENTRY_CUSTOMERID
		and tc.LOCATION_ID = t1.LOCATION_ID
		and tc.DESCRIPTION = t1.DESCRIPTION
	where tc.DebitCnt = 1
		and tc.CreditCnt > 1
		and t1.Credit > 0

	UNION ALL
	
	select	t1.JOURNAL, 
			t1.[DATE], 
			t1.REVERSEDATE, 
			t1.[DESCRIPTION], 
			t1.REFERENCE_NO,
			t1.ACCT_NO, 
			t1.LOCATION_ID,
			t1.MEMO,
			t1.GLDIMSTATUS,
			t1.GLENTRY_CUSTOMERID,
			0.00 as Debit,
			t1.Credit AS Credit
	from #TempIJE1 t1
	join #TempIJECnt tc
		on	tc.GLENTRY_CUSTOMERID = t1.GLENTRY_CUSTOMERID
		and tc.LOCATION_ID = t1.LOCATION_ID
		and tc.DESCRIPTION = t1.DESCRIPTION
	where tc.DebitCnt = 1
		and tc.CreditCnt > 1
		and t1.Credit > 0

	UNION ALL
	
	select	t1.JOURNAL, 
			t1.[DATE], 
			t1.REVERSEDATE, 
			t1.[DESCRIPTION], 
			t1.REFERENCE_NO,
			t1.ACCT_NO, 
			t1.LOCATION_ID,
			t1.MEMO,
			t1.GLDIMSTATUS,
			t1.GLENTRY_CUSTOMERID,
			t1.Debit,
			t1.Credit
	from #TempIJE1 t1
	join #TempIJECnt tc
		on	tc.GLENTRY_CUSTOMERID = t1.GLENTRY_CUSTOMERID
		and tc.LOCATION_ID = t1.LOCATION_ID
		and tc.DESCRIPTION = t1.DESCRIPTION
	where tc.DebitCnt = 1
		and tc.CreditCnt = 1

	TRUNCATE TABLE IntacctJournalEntries;

	INSERT INTO IntacctJournalEntries (JOURNAL, [DATE], REVERSEDATE, [DESCRIPTION], REFERENCE_NO,LINE_NO
										,	ACCT_NO, LOCATION_ID , MEMO, DEBIT, CREDIT, GLDIMSTATUS, GLENTRY_CUSTOMERID)
	SELECT	JOURNAL, [DATE], REVERSEDATE, [DESCRIPTION], REFERENCE_NO
		,	ROW_NUMBER() OVER (PARTITION BY LOCATION_ID, [DESCRIPTION] ORDER BY GLENTRY_CUSTOMERID, DESCRIPTION, Debit+Credit, Debit)
		,	ACCT_NO, LOCATION_ID
		,	MEMO
		,	ISNULL(CAST(CASE WHEN DEBIT = 0 THEN NULL ELSE DEBIT END AS VARCHAR(19)),'') AS DEBIT
		,	ISNULL(CAST(CASE WHEN CREDIT = 0 THEN NULL ELSE CREDIT END AS VARCHAR(19)),'') AS CREDIT
		, GLDIMSTATUS
		, GLENTRY_CUSTOMERID
	FROM #TempIJEBreakDown
	ORDER BY DESCRIPTION, LOCATION_ID, GLENTRY_CUSTOMERID, Debit+Credit, Debit

	
---------------------------------------------------------------------------------------------------------------------------------------
--ACH
---------------------------------------------------------------------------------------------------------------------------------------

	IF OBJECT_ID('tempdb..#TempAJE') IS NOT NULL
		Drop Table #TempAJE;
	IF OBJECT_ID('tempdb..#TempAJE1') IS NOT NULL
		Drop Table #TempAJE1;
	IF OBJECT_ID('tempdb..#TempAJEBreakDown') IS NOT NULL
		Drop Table #TempAJEBreakDown;
	IF OBJECT_ID('tempdb..#TempAJECnt') IS NOT NULL
		Drop Table #TempAJECnt;


	Select	JOURNAL, 
			[DATE], 
			REVERSEDATE, 
			[DESCRIPTION], 
			REFERENCE_NO,
			ACCT_NO, 
			LOCATION_ID,
			MEMO,
			GLDIMSTATUS,
			GLENTRY_CUSTOMERID, 
			Sum(Convert(Numeric(16,2), Coalesce(Nullif(Debit,''), '0.0'))) as Debit,
			Sum(Convert(Numeric(16,2), Coalesce(Nullif(Credit,''), '0.0'))) as Credit
	Into #TempAJE
	From IntacctJournalEntriesACH
	group by	JOURNAL, 
				[DATE], 
				REVERSEDATE, 
				[DESCRIPTION], 
				REFERENCE_NO,
				ACCT_NO, 
				LOCATION_ID,
				MEMO,
				GLDIMSTATUS,
				GLENTRY_CUSTOMERID


	select	JOURNAL, 
			[DATE], 
			REVERSEDATE, 
			[DESCRIPTION], 
			REFERENCE_NO,
			ACCT_NO, 
			LOCATION_ID,
			MEMO,
			GLDIMSTATUS,
			GLENTRY_CUSTOMERID,
			CASE WHEN Debit >= Credit THEN Debit - Credit ELSE 0.0 END AS Debit,
			CASE WHEN Credit >= Debit THEN Credit - Debit ELSE 0.0 END AS Credit
	into #TempAJE1
	from #TempAJE

	DELETE 
	FROM #TempAJE1 
	WHERE	Debit = 0 
		AND	Credit = 0

	
	select	DESCRIPTION,
			GLENTRY_CUSTOMERID,
			LOCATION_ID,
			SUM(CASE WHEN Debit > 0 THEN 1 ELSE 0 END) AS DebitCnt,
			SUM(CASE WHEN Credit > 0 THEN 1 ELSE 0 END) AS CreditCnt
	into #TempAJECnt
	from #TempAJE1
	group by DESCRIPTION, GLENTRY_CUSTOMERID, LOCATION_ID


	select	t2.JOURNAL, 
			t2.[DATE], 
			t2.REVERSEDATE, 
			t2.[DESCRIPTION], 
			t2.REFERENCE_NO,
			t2.ACCT_NO, 
			t2.LOCATION_ID,
			t2.MEMO,
			t2.GLDIMSTATUS,
			t2.GLENTRY_CUSTOMERID,
			0.00 as Debit,
			t1.Debit AS Credit
	into #TempAJEBreakDown
	from #TempAJE1 t1
	join #TempAJE1 t2
		on	t1.GLENTRY_CUSTOMERID = t2.GLENTRY_CUSTOMERID
		and t1.LOCATION_ID = t2.LOCATION_ID
		and t1.DESCRIPTION = t2.DESCRIPTION
		and t2.Debit = 0
	join #TempAJECnt tc
		on	tc.GLENTRY_CUSTOMERID = t1.GLENTRY_CUSTOMERID
		and tc.LOCATION_ID = t1.LOCATION_ID
		and tc.DESCRIPTION = t1.DESCRIPTION
	where tc.DebitCnt > 1
		and tc.CreditCnt = 1
		and t1.Debit > 0

	UNION ALL
	
	select	t1.JOURNAL, 
			t1.[DATE], 
			t1.REVERSEDATE, 
			t1.[DESCRIPTION], 
			t1.REFERENCE_NO,
			t1.ACCT_NO, 
			t1.LOCATION_ID,
			t1.MEMO,
			t1.GLDIMSTATUS,
			t1.GLENTRY_CUSTOMERID,
			t1.Debit AS Debit,
			0.00 as Credit
	from #TempAJE1 t1
	join #TempAJECnt tc
		on	tc.GLENTRY_CUSTOMERID = t1.GLENTRY_CUSTOMERID
		and tc.LOCATION_ID = t1.LOCATION_ID
		and tc.DESCRIPTION = t1.DESCRIPTION
	where tc.DebitCnt > 1
		and tc.CreditCnt = 1
		and t1.Debit > 0

	UNION ALL

	select	t2.JOURNAL, 
			t2.[DATE], 
			t2.REVERSEDATE, 
			t2.[DESCRIPTION], 
			t2.REFERENCE_NO,
			t2.ACCT_NO, 
			t2.LOCATION_ID,
			t2.MEMO,
			t2.GLDIMSTATUS,
			t2.GLENTRY_CUSTOMERID,
			t1.Credit as Debit,
			0.0 AS Credit
	from #TempAJE1 t1
	join #TempAJE1 t2
		on	t1.GLENTRY_CUSTOMERID = t2.GLENTRY_CUSTOMERID
		and t1.LOCATION_ID = t2.LOCATION_ID
		and t1.DESCRIPTION = t2.DESCRIPTION
		and t2.Credit = 0
	join #TempAJECnt tc
		on	tc.GLENTRY_CUSTOMERID = t1.GLENTRY_CUSTOMERID
		and tc.LOCATION_ID = t1.LOCATION_ID
		and tc.DESCRIPTION = t1.DESCRIPTION
	where tc.DebitCnt = 1
		and tc.CreditCnt > 1
		and t1.Credit > 0

	UNION ALL
	
	select	t1.JOURNAL, 
			t1.[DATE], 
			t1.REVERSEDATE, 
			t1.[DESCRIPTION], 
			t1.REFERENCE_NO,
			t1.ACCT_NO, 
			t1.LOCATION_ID,
			t1.MEMO,
			t1.GLDIMSTATUS,
			t1.GLENTRY_CUSTOMERID,
			0.00 as Debit,
			t1.Credit AS Credit
	from #TempAJE1 t1
	join #TempAJECnt tc
		on	tc.GLENTRY_CUSTOMERID = t1.GLENTRY_CUSTOMERID
		and tc.LOCATION_ID = t1.LOCATION_ID
		and tc.DESCRIPTION = t1.DESCRIPTION
	where tc.DebitCnt = 1
		and tc.CreditCnt > 1
		and t1.Credit > 0

	UNION ALL
	
	select	t1.JOURNAL, 
			t1.[DATE], 
			t1.REVERSEDATE, 
			t1.[DESCRIPTION], 
			t1.REFERENCE_NO,
			t1.ACCT_NO, 
			t1.LOCATION_ID,
			t1.MEMO,
			t1.GLDIMSTATUS,
			t1.GLENTRY_CUSTOMERID,
			t1.Debit,
			t1.Credit
	from #TempAJE1 t1
	join #TempAJECnt tc
		on	tc.GLENTRY_CUSTOMERID = t1.GLENTRY_CUSTOMERID
		and tc.LOCATION_ID = t1.LOCATION_ID
		and tc.DESCRIPTION = t1.DESCRIPTION
	where tc.DebitCnt = 1
		and tc.CreditCnt = 1

	TRUNCATE TABLE IntacctJournalEntriesACH;

	INSERT INTO IntacctJournalEntriesACH (JOURNAL, [DATE], REVERSEDATE, [DESCRIPTION], REFERENCE_NO,LINE_NO
										,	ACCT_NO, LOCATION_ID , MEMO, DEBIT, CREDIT, GLDIMSTATUS, GLENTRY_CUSTOMERID)
	SELECT	JOURNAL, [DATE], REVERSEDATE, [DESCRIPTION], REFERENCE_NO
		,	ROW_NUMBER() OVER (PARTITION BY LOCATION_ID, [DESCRIPTION] ORDER BY DESCRIPTION, LOCATION_ID, GLENTRY_CUSTOMERID, Debit+Credit, Debit)
		,	ACCT_NO, LOCATION_ID
		,	MEMO
		,	ISNULL(CAST(CASE WHEN DEBIT = 0 THEN NULL ELSE DEBIT END AS VARCHAR(19)),'') AS DEBIT
		,	ISNULL(CAST(CASE WHEN CREDIT = 0 THEN NULL ELSE CREDIT END AS VARCHAR(19)),'') AS CREDIT
		, GLDIMSTATUS
		, GLENTRY_CUSTOMERID
	FROM #TempAJEBreakDown
	ORDER BY DESCRIPTION, LOCATION_ID, GLENTRY_CUSTOMERID, Debit+Credit, Debit

END