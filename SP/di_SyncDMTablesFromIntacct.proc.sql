USE IntacctDM;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('di_SyncDMTablesFromIntacct') IS NOT NULL
	DROP PROCEDURE di_SyncDMTablesFromIntacct;
GO


CREATE PROCEDURE dbo.di_SyncDMTablesFromIntacct (@IntacctObject VARCHAR(50))
AS
BEGIN

/********************************************************************************************************** *
   Procedure Name  :	di_SyncDMTablesFromIntacct
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	11/17/2017

   *********************************************************************************************************
   **         Change History                                                                              **
   *********************************************************************************************************

   Date			Author				Description
   --------		-----------			------------
   
************************************************************************************************************ */

	--When Records exists already, check if the modified date is different. If so, move Primary Record to History and insert Staging into Primary
	--Archive records that exist in DM but not in Intacct
	--List of Existing Records that have been modified
	--Move existing records to History table
	--Delete the records from the Primary table
	--Insert records that do not exist in Primary (this includes new records and the replacements for the records that were just deleted)

DECLARE	@SQL VARCHAR(MAX)
	,	@User     varchar(128) = NULL
	,	@Application varchar(128) = 'di_SyncDMTablesFromIntacct'
	,	@Version     varchar(10) = '1.00.00'
	
IF @User IS NULL 
  SET @User = SUSER_NAME ();

BEGIN TRY
	BEGIN TRANSACTION

		SET @SQL = 'DECLARE @MinDate DATETIME
						,	@MaxDate DATETIME

			IF OBJECT_ID(''tempdb..#temp' + @IntacctObject + ''') IS NOT NULL
				DROP TABLE #temp' + @IntacctObject + ';
			IF OBJECT_ID(''tempdb..#DeletedInIntacct' + @IntacctObject + ''') IS NOT NULL
				DROP TABLE #DeletedInIntacct' + @IntacctObject + ';

			SELECT	@MinDate = MIN(CAST(sab.WHENMODIFIED AS DATETIME))
				,	@MaxDate = MAX(CAST(sab.WHENMODIFIED AS DATETIME))
			FROM Staging.dbo.IntacctStg' + @IntacctObject + ' sab
	
			SELECT iab.RECORDNO
			INTO #DeletedInIntacct' + @IntacctObject + '
			FROM IntacctDM.dbo.' + @IntacctObject + ' iab 
			LEFT JOIN Staging.dbo.IntacctStg' + @IntacctObject + ' sab
				ON	iab.RECORDNO = sab.RECORDNO
			WHERE	sab.RECORDNO IS NULL
				AND CAST(iab.WHENMODIFIED AS DATETIME) BETWEEN @MinDate AND @MaxDate


			INSERT INTO IntacctDM.dbo.Obsolete' + @IntacctObject + '	
			SELECT	iab.*
				,	GETDATE() AS DMObsoletedDate
			FROM IntacctDM.dbo.' + @IntacctObject + ' iab 
			JOIN #DeletedInIntacct' + @IntacctObject + ' DII
				ON	dii.RECORDNO = iab.RECORDNO

			DELETE 
			FROM IntacctDM.dbo.' + @IntacctObject + ' 
			WHERE RECORDNO IN (	SELECT dii.RECORDNO 
								FROM #DeletedInIntacct' + @IntacctObject + ' dii
								)

			SELECT iab.*
			INTO #temp' + @IntacctObject + '
			FROM Staging.dbo.IntacctStg' + @IntacctObject + ' sab
			JOIN IntacctDM.dbo.' + @IntacctObject + ' iab
				ON	iab.RECORDNO = sab.RECORDNO
			WHERE CAST(iab.WHENMODIFIED AS DATETIME) < CAST(sab.WHENMODIFIED AS DATETIME)


			INSERT INTO IntacctDM.dbo.hst' + @IntacctObject + '
			SELECT	*
				,	GETDATE() AS InsertedDate
				,	SYSTEM_USER AS InsertedBy 
			FROM #temp' + @IntacctObject + '

			DELETE 
			FROM IntacctDM.dbo.' + @IntacctObject + '
			WHERE EXISTS (	SELECT 1
							FROM #temp' + @IntacctObject + ' t
							WHERE t.RECORDNO = ' + @IntacctObject + '.RECORDNO)


			DELETE 
			FROM Staging.dbo.IntacctStg' + @IntacctObject + '
			WHERE EXISTS (	SELECT 2
							FROM IntacctDM.dbo.' + @IntacctObject + ' iab
							WHERE iab.RECORDNO = IntacctStg' + @IntacctObject + '.RECORDNO
						 )
			
			
			INSERT INTO IntacctDM.dbo.' + @IntacctObject + '
			SELECT sab.*
			FROM Staging.dbo.IntacctStg' + @IntacctObject + ' sab
			'

	EXEC(@SQL)

	COMMIT

END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK
		
      DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
      SELECT @ErrMsg = ERROR_MESSAGE(),
             @ErrSeverity = ERROR_SEVERITY()

      RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH


END