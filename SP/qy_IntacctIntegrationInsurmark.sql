USE [Support]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('qy_IntacctIntegrationInsurmark') IS NOT NULL
	DROP PROC qy_IntacctIntegrationInsurmark;
GO

CREATE PROCEDURE dbo.qy_IntacctIntegrationInsurmark
AS


/* ********************************************************************************************************* *
   Procedure Name  :	qy_IntacctIntegrationInsurmark
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Nancy Tanaka
   Create Date     :	05/30/2017

   *********************************************************************************************************
   **         Change History                                                                              **
   *********************************************************************************************************

   Date			Author				Description
   --------		-----------			------------
   08/15/2017	Benjacob Beres		Updated query per Roy's specs
   01/02/2017	Benjacob Beres		More modification per Roy's request
*  ********************************************************************************************************* */

BEGIN

SELECT 
		'' AS [DONOTIMPORT]
	,	'Purchasing Transaction'  AS [TRANSACTIONTYPE]  --Carol: 1 Purchasing Transactio per Vendor 7/27/2017; testing purpose, remove all except for the first line--
	,	 convert(varchar(10),GETDATE(),101) AS [DATE]  
	,	'10001443' AS [VENDOR_ID] 
	,	convert(varchar(10),DATEADD(D,10,GETDATE()),101) AS [DATEDUE]
	,	ISM.[Lender Id] AS [REFERENCENO]
	,	'USD' AS [CURRENCY]
	,	'USD' AS [BASECURR]
	,	'Draft' AS [STATE] 
	,	ROW_NUMBER() OVER (PARTITION BY ISM.InvoiceNumber ORDER BY ISM.InvoiceNumber) AS [LINE] --reset back to 1 whenever there is a new Insurmark Invoice Number--
	,	CASE 
			/*1. where asset is a loan and it is active then code to item 532004*/
			WHEN LOA.Account IS NOT NULL AND LOA.isActive = 1 THEN CAST('532004' AS VARCHAR(20))

			/*2. where asset is an REO and it is active
                a. check to see if REO note owner contains US Bank Trust then code to 532005
                b. check to see if REO note owner does not contain US Bank Trust then code to 532006*/
			WHEN REO.PropertyID IS NOT NULL AND REO.Active = 1 THEN CASE 
																		WHEN REO.OWNER LIKE  '%U.S. Bank%Series%' THEN CAST('532005' AS VARCHAR(20)) 
																		WHEN REO.OWNER NOT LIKE  '%U.S. Bank%Series%' THEN CAST('532006' AS VARCHAR(20))
																END 
																
			/*3. where asset is a loan and
							a. it is not active 
							b. it did not convert to an REO then code to 532010 (just added a few minutes ago)*/							
			WHEN LOA.Account IS NOT NULL AND LOA.isActive = 0 AND 															--Inactive loan; Carol/Roy will follow up; new category--
				 REO.PropertyID IS NULL   THEN CAST('532010' AS VARCHAR(20))

			/*4. where asset is a loan and
							a. it is not active
							b. it did convert to an REO then
							c. check to see if REO note owner contains US Bank Trust then code to 532005
							d. check to see if REO note owner does not contain US Bank Trust then code to 532006*/			
			WHEN LOA.Account IS NOT NULL AND LOA.isActive = 0 AND 
				 REO.PropertyID IS NOT NULL THEN CASE	WHEN REO.OWNER LIKE  '%U.S. Bank%Series%' THEN CAST('532005' AS VARCHAR(20))			--Active REIT REO--
														WHEN REO.OWNER NOT LIKE  '%U.S. Bank%Series%' THEN CAST('532006' AS VARCHAR(20))			--Active REIT REO--
												 END
			/*5. where asset is an REO and 
							a. it is not active
							b. check to see if REO note owner contains US Bank Trust then code to 532005
							c. check to see if REO note owner does not contain US Bank Trust then code to 532006*/				 
			WHEN REO.PropertyID IS NOT NULL AND REO.Active = 0 THEN CASE 
																			WHEN REO.OWNER LIKE  '%U.S. Bank%Series%' THEN CAST('532005' AS VARCHAR(20))			--Active NON-REIT REO--
																			WHEN REO.OWNER NOT LIKE  '%U.S. Bank%Series%' THEN CAST('532006' AS VARCHAR(20))		--Active NON-REIT REO--
																	END
			/*6. where asset is an REO and 
							a. it is not active
							b. and it has an active loan then
							c. where asset is a loan and it is active then code to item 532004*/				 
			WHEN REO.PropertyID IS NOT NULL AND REO.Active = 0 AND 
				 LOA.Account IS NOT NULL AND LOA.isActive = 1	THEN  CAST('532004' AS VARCHAR(20))
			ELSE 'Further Review' END AS [ITEMID]  --confirmationed & done from Roy--
	,	CASE 
			/*1. where asset is a loan and it is active then code to item 532004*/
			WHEN LOA.Account IS NOT NULL AND LOA.isActive = 1 THEN CAST('Insurance - Loan' AS VARCHAR(100))

			/*2. where asset is an REO and it is active
                a. check to see if REO note owner contains US Bank Trust then code to 532005
                b. check to see if REO note owner does not contain US Bank Trust then code to 532006*/
			WHEN REO.PropertyID IS NOT NULL AND REO.Active = 1 THEN CASE 
																		WHEN REO.OWNER LIKE  '%U.S. Bank%Series%' THEN CAST('Insurance - REO - REIT' AS VARCHAR(100)) 
																		WHEN REO.OWNER NOT LIKE  '%U.S. Bank%Series%' THEN CAST('Insurance - REO - Non-REIT' AS VARCHAR(100))
																END 
																
			/*3. where asset is a loan and
							a. it is not active 
							b. it did not convert to an REO then code to 532010 (just added a few minutes ago)*/							
			WHEN LOA.Account IS NOT NULL AND LOA.isActive = 0 AND 															--Inactive loan; Carol/Roy will follow up; new category--
				 REO.PropertyID IS NULL   THEN CAST('Insurance - Inactive Loan No Active REO' AS VARCHAR(100))

			/*4. where asset is a loan and
							a. it is not active
							b. it did convert to an REO then
							c. check to see if REO note owner contains US Bank Trust then code to 532005
							d. check to see if REO note owner does not contain US Bank Trust then code to 532006*/			
			WHEN LOA.Account IS NOT NULL AND LOA.isActive = 0 AND 
				 REO.PropertyID IS NOT NULL THEN CASE	WHEN REO.OWNER LIKE  '%U.S. Bank%Series%' THEN CAST('Insurance - REO - REIT' AS VARCHAR(100))			--Active REIT REO--
														WHEN REO.OWNER NOT LIKE  '%U.S. Bank%Series%' THEN CAST('Insurance - REO - Non-REIT' AS VARCHAR(100))			--Active REIT REO--
												 END
			/*5. where asset is an REO and 
							a. it is not active
							b. check to see if REO note owner contains US Bank Trust then code to 532005
							c. check to see if REO note owner does not contain US Bank Trust then code to 532006*/				 
			WHEN REO.PropertyID IS NOT NULL AND REO.Active = 0 THEN CASE 
																			WHEN REO.OWNER LIKE  '%U.S. Bank%Series%' THEN CAST('Insurance - REO - REIT' AS VARCHAR(100))			--Active NON-REIT REO--
																			WHEN REO.OWNER NOT LIKE  '%U.S. Bank%Series%' THEN CAST('Insurance - REO - Non-REIT' AS VARCHAR(100))		--Active NON-REIT REO--
																	END
			/*6. where asset is an REO and 
							a. it is not active
							b. and it has an active loan then
							c. where asset is a loan and it is active then code to item 532004*/				 
			WHEN REO.PropertyID IS NOT NULL AND REO.Active = 0 AND 
				 LOA.Account IS NOT NULL AND LOA.isActive = 1	THEN  CAST('Insurance - Loan' AS VARCHAR(100))
			ELSE 'Further Review' END AS [ITEMDESC]  --confirmationed & done from Roy--
	,	1 AS [QUANTITY]
	,	'Each' AS [UNIT]
	,	ISM.[Net Due] AS [PRICE]
			--	,'5310' AS [DEPARTMENTID]
	,	CAST(''  AS VARCHAR(40)) AS [DEPARTMENTID]   --Carol 7/28/2017; the first 4 characters of ITEMID--
	,	'150' AS [LOCATIONID]  
	,	CASE WHEN CAST(LOA.Account AS VARCHAR(20)) IS NULL THEN CAST( ISNULL(REO.PropertyID,'') AS VARCHAR(20)) ELSE CAST(LOA.Account AS VARCHAR(20)) END AS  [PODOCUMENTENTRY_CLASSID]	
	,	CASE WHEN LOA.Account IS NOT NULL THEN CAST( LoanNoteOwners.IntacctCo AS VARCHAR(5))			
			WHEN REO.PropertyID IS NOT NULL THEN CAST(REONoteOwners.IntacctCo AS VARCHAR(5)) 		
			ELSE '' END AS [PODOCUMENTENTRY_CUSTOMERID]      --to be completed after mars note owner is fixed; AJXM Trust 1--
	,	ISM.[Lender Id] AS [VENDORDOCNO]
	,	CAST(ISM.[Property Number] AS VARCHAR(35)) AS [MEMO] 

	,	CASE 
			WHEN LOA.Account IS NOT NULL THEN CAST('11' AS VARCHAR(2))			--Loan--
			WHEN REO.PropertyID IS NOT NULL AND REO.status NOT LIKE '%rent%' THEN CAST('21' AS VARCHAR(2))  --REO - HFS --
			WHEN REO.PropertyID IS NOT NULL AND REO.status LIKE '%rent%' THEN CAST('22' AS VARCHAR(2))  --REO - LT(Long Term) Rental--
			ELSE '' END AS [PODOCUMENTENTRY_GLDIMSTATUS]   --NON rental-- 
	,	'PMTO'AS [PODOCUMENTENTRY_GLDIMACTIVITY]	 
	,	'BoA Check' AS [PAYMENT_TYPE_HEADER]
	,	CONVERT(VARCHAR(10),ISM.[Month End Date],101) AS [INVOICE_DATE]
	,	'High' AS [INVOICE_PRIORITY]
	--,'9525 BA 13705' AS [BANK_ACCOUNT]
	,	CASE WHEN REO.PropertyID IS NULL
				THEN '9525 BA 13705'
			 ELSE 
				'6678 BA 11000'
		END AS BANK_ACCOUNT_ENTRY_LEVEL

INTO #Insurmark
FROM support.dbo.insurmark AS ISM 
LEFT JOIN mars.dbo.vw_loans AS LOA ON ISM.[Loan No] = LOA.Account
LEFT JOIN MARS.dbo.vw_REO as REO ON REO.PropertyID = ISM.[Loan No]
LEFT JOIN Support.dbo.AcctCompanyGLTranslations as LoanNoteOwners on  LOA.NoteOwner = LoanNoteOwners.MARSNoteOwner
LEFT JOIN Support.dbo.AcctCompanyGLTranslations as REONoteOwners on  CASE WHEN REO.[Owner] = 'Aspen U LLC' THEN 'AU LLC' ELSE REO.[Owner] END = REONoteOwners.MARSNoteOwner
LEFT JOIN Support.dbo.AcctCompanyGLTranslations acgt 
	ON	acgt.MARSNoteOwner = CASE WHEN REO.PropertyID IS NULL THEN LOA.NoteOwner ELSE CASE WHEN REO.[Owner] = 'Aspen U LLC' THEN 'AU LLC' ELSE REO.[Owner] END END
WHERE ISM.[Net Due] > 0.0


UPDATE #Insurmark
SET [TRANSACTIONTYPE] = ''
WHERE LINE > 1

UPDATE #Insurmark
SET BANK_ACCOUNT_ENTRY_LEVEL = '6678 BA 11000'
WHERE ITEMID = '532010'


UPDATE #Insurmark
SET [DEPARTMENTID]  = LEFT(ITEMID,4)


TRUNCATE TABLE Support.dbo.IntacctPurchasingTransactions


INSERT INTO Support.dbo.IntacctPurchasingTransactions
		(	TRANSACTIONTYPE, [DATE], VENDOR_ID, DATEDUE, REFERENCENO, CURRENCY, BASECURR, [STATE], LINE, ITEMID, ITEMDESC, QUANTITY, UNIT, PRICE, DEPARTMENTID
		,	LOCATIONID, PODOCUMENTENTRY_CLASSID, PODOCUMENTENTRY_CUSTOMERID, VENDORDOCNO, MEMO, PODOCUMENTENTRY_GLDIMSTATUS, PODOCUMENTENTRY_GLDIMACTIVITY
		,	PAYMENT_TYPE_HEADER, INVOICE_DATE, INVOICE_PRIORITY, BANK_ACCOUNT_ENTRY_LEVEL)
SELECT	[TRANSACTIONTYPE] 
	,	[DATE]
	,	[VENDOR_ID]
	,	[DATEDUE]
	,	[REFERENCENO]
	,	[CURRENCY]
	,	[BASECURR]
	,	[STATE] 
	,	[LINE] 
	,	[ITEMID]
	,	[ITEMDESC] 
	,	[QUANTITY]
	,	[UNIT]
	,	[PRICE]
	,	[DEPARTMENTID]
	,	[LOCATIONID]  
	,	[PODOCUMENTENTRY_CLASSID]	
	,	[PODOCUMENTENTRY_CUSTOMERID]
	,	[VENDORDOCNO]
	,	[MEMO] 
	,	[PODOCUMENTENTRY_GLDIMSTATUS]
	,	[PODOCUMENTENTRY_GLDIMACTIVITY]	  
	,	[PAYMENT_TYPE_HEADER]
	,	[INVOICE_DATE]
	,	[INVOICE_PRIORITY]
	,	BANK_ACCOUNT_ENTRY_LEVEL
from #Insurmark

END
