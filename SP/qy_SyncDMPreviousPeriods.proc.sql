USE IntacctDM;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('qy_SyncDMPreviousPeriods') IS NOT NULL
	DROP PROCEDURE qy_SyncDMPreviousPeriods;
GO


CREATE PROCEDURE dbo.qy_SyncDMPreviousPeriods
AS
BEGIN

	DECLARE @EndDate DATE

	SELECT @EndDate = CAST(rp.END_DATE AS DATE)
	FROM IntacctDM.dbo.SyncParameters sp
	JOIN IntacctDM.dbo.ReportingPeriod rp
		ON	rp.NAME = sp.EarliestPeriod


	SELECT	NAME
		,	START_DATE
		,	END_DATE
	FROM	IntacctDM.dbo.ReportingPeriod
	WHERE	CAST(END_DATE AS DATE) < GETDATE()
		AND CAST(END_DATE AS DATE) >= @EndDate
		AND NAME LIKE '%Month%'
		AND NAME NOT IN ('Month Ended July 2018')
	ORDER BY CAST(END_DATE AS DATE)

END