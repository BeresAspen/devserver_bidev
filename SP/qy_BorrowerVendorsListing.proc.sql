USE Support
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.qy_IntacctBorrowerVendorsListing') IS NOT NULL
	DROP PROCEDURE qy_IntacctBorrowerVendorsListing;
GO

CREATE PROCEDURE [dbo].[qy_IntacctBorrowerVendorsListing]
AS
BEGIN

/* ********************************************************************************************************* *
   Procedure Name  :	qy_IntacctBorrowerVendorsListing
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	08/15/2017

   *********************************************************************************************************
   **         Change History                                                                              **
   *********************************************************************************************************

   Date			Author				Description
   --------		-----------			------------
   
*  ********************************************************************************************************* */

CREATE TABLE #IntacctVendor
	(	VENDOR_ID VARCHAR(20)
	,	[NAME] VARCHAR(100)
	,	PARENT_ID VARCHAR(20)
	,	TERM_NAME VARCHAR(40)
	,	VENDTYPE_NAME VARCHAR(80)
	,	TAX_ID VARCHAR(20)
	,	CREDIT_LIMIT VARCHAR(40)
	,	VENDOR_ACCTNO VARCHAR(30)
	,	ACCT_LABEL VARCHAR(80)
	,	GL_ACCTNO VARCHAR(24)
	,	BILLING_TYPE VARCHAR(1)
	,	DISC_ON_CKSTUB VARCHAR(5)
	,	ONHOLD VARCHAR(5)
	,	COMMENTS VARCHAR(200)
	,	FORM1099TYPE VARCHAR(10)
	,	FORM1099BOX VARCHAR(1)
	,	NAME1099 VARCHAR(100)
	,	CURRENCY VARCHAR(3)
	,	PAYMENTPRIORITY VARCHAR(10)
	,	HIDEDISPLAYCONTACT VARCHAR(1)
	,	ACTIVE VARCHAR(1)
	,	CONTACT_NAME VARCHAR(200)
	,	COMPANY_NAME VARCHAR(100)
	,	MRMS VARCHAR(15)
	,	FIRST_NAME VARCHAR(40)
	,	MI VARCHAR(40)
	,	LAST_NAME VARCHAR(40)
	,	PRINT_AS VARCHAR(200)
	,	TAXABLE VARCHAR(1)
	,	PHONE1 VARCHAR(30)
	,	PHONE2 VARCHAR(30)
	,	CELLPHONE VARCHAR(30)
	,	PAGER VARCHAR(30)
	,	FAX VARCHAR(30)
	,	EMAIL1 VARCHAR(255)
	,	EMAIL2 VARCHAR(255)
	,	URL1 VARCHAR(255)
	,	URL2 VARCHAR(255)
	,	ADDRESS1 VARCHAR(80)
	,	ADDRESS2 VARCHAR(80)
	,	CITY VARCHAR(80)
	,	[STATE] VARCHAR(40)
	,	ZIP VARCHAR(30)
	,	COUNTRY VARCHAR(60)
	,	LATITUDE VARCHAR(12)
	,	LONGITUDE VARCHAR(13)
	,	R_CONTACT_NAME VARCHAR(64)
	,	R_COMPANY_NAME VARCHAR(64)
	,	R_MRMS VARCHAR(10)
	,	R_FIRST_NAME VARCHAR(30)
	,	R_MI VARCHAR(1)
	,	R_LAST_NAME VARCHAR(30)
	,	R_PRINT_AS VARCHAR(64)
	,	R_TAXABLE VARCHAR(1)
	,	R_PHONE1 VARCHAR(20)
	,	R_PHONE2 VARCHAR(20)
	,	R_CELLPHONE VARCHAR(20)
	,	R_PAGER VARCHAR(20)
	,	R_FAX VARCHAR(20)
	,	R_EMAIL1 VARCHAR(255)
	,	R_EMAIL2 VARCHAR(255)
	,	R_URL1 VARCHAR(255)
	,	R_URL2 VARCHAR(255)
	,	R_ADDRESS1 VARCHAR(64)
	,	R_ADDRESS2 VARCHAR(64)
	,	R_CITY VARCHAR(50)
	,	R_STATE VARCHAR(3)
	,	R_ZIP VARCHAR(10)
	,	R_COUNTRY VARCHAR(30)
	,	P_CONTACT_NAME VARCHAR(64)
	,	P_COMPANY_NAME VARCHAR(64)
	,	P_MRMS VARCHAR(10)
	,	P_FIRST_NAME VARCHAR(30)
	,	P_MI VARCHAR(1)
	,	P_LAST_NAME VARCHAR(30)
	,	P_PRINT_AS VARCHAR(64)
	,	P_TAXABLE VARCHAR(1)
	,	P_PHONE1 VARCHAR(20)
	,	P_PHONE2 VARCHAR(20)
	,	P_CELLPHONE VARCHAR(20)
	,	P_PAGER VARCHAR(20)
	,	P_FAX VARCHAR(20)
	,	P_EMAIL1 VARCHAR(255)
	,	P_EMAIL2 VARCHAR(255)
	,	P_URL1 VARCHAR(255)
	,	P_URL2 VARCHAR(255)
	,	P_ADDRESS1 VARCHAR(64)
	,	P_ADDRESS2 VARCHAR(64)
	,	P_CITY VARCHAR(50)
	,	P_STATE VARCHAR(3)
	,	P_ZIP VARCHAR(10)
	,	P_COUNTRY VARCHAR(30)
	,	APACCOUNT VARCHAR(24)
	,	MERGE_PAYMENT_REQUESTS VARCHAR(10)
	,	DYNAMIC_VENDOR_ID VARCHAR(20)
	,	VENDOR_PAYMENT_FOR_CHECKLIST VARCHAR(30)
	,	VENDOR_TYPE VARCHAR(10)
	,	DESTINATION_COUNTRY	VARCHAR(5)
	,	WIRE_PAYMENT_TYPE	VARCHAR(20)
	,	INTL_BENEFICIARY_ADDRESS	VARCHAR(100)
	,	BENEFICIARY_NAME	VARCHAR(100)
	,	BENEFICIARY_ADDRESS	VARCHAR(100)
	,	BENEFICIARY_CITY	VARCHAR(35)
	,	INTL_DESTINATION_COUNTRY	VARCHAR(5)
	,	BENEFICIARY_ACCOUNT	VARCHAR(34)
	,	BENEFICIARY_BANK_ID	VARCHAR(9)
	,	INTL_PAYMENT_TYPE	VARCHAR(30)
	,	INTL_BENEFICIARY_NAME	VARCHAR(100)
	,	BENEFICIARY_POSTAL_CODE	VARCHAR(10)
	,	INTL_CREDIT_CURRENCY	VARCHAR(50)
	,	INTL_BENEFICIARY_CITY	VARCHAR(100)
	,	INTL_BENEFICIARY_ACCOUNT	VARCHAR(100)
	,	INTL_BENEFICIARY_BANK_ID	VARCHAR(100)
	,	INTL_PURPOSE_OF_PAYMENT	VARCHAR(100)
	,	CHARGES	VARCHAR(20)
	,	INTL_INTERMEDIARY_BANK_ID	VARCHAR(100)
	,	INTL_RECEIVER_INFORMATION	VARCHAR(100)
	,	RITEM	VARCHAR(100)
	)


	INSERT INTO #IntacctVendor
			([NAME], TAX_ID, CURRENCY, CONTACT_NAME, FIRST_NAME, MI, LAST_NAME, PRINT_AS, PHONE1, PHONE2, CELLPHONE, EMAIL1, ADDRESS1, CITY, [STATE], ZIP, MERGE_PAYMENT_REQUESTS ,DYNAMIC_VENDOR_ID, VENDOR_PAYMENT_FOR_CHECKLIST, VENDOR_TYPE)
	SELECT	vl.VendorName
		,	MARS.dbo.fn_RemoveNonNumericCharacters(vl.TIN) AS TAX_ID
		,	'USD' AS CURRENCY
		,	vl.VendorName AS CONTACT_NAME
		,	l.[First Name]
		,	l.MI
		,	l.[Last Name]
		,	l.FullName AS PRINT_AS
		,	MARS.dbo.fn_RemoveNonNumericCharacters(l.Home) AS Phone1
		,	MARS.dbo.fn_RemoveNonNumericCharacters(l.Work) AS Phone2
		,	MARS.dbo.fn_RemoveNonNumericCharacters(l.Mobil) AS CellPhone
		,	CASE WHEN l.EmailAddress like '%@%#%'
					THEN	CASE WHEN l.EmailAddress like '%[[]%#%' THEN SUBSTRING(l.EmailAddress,CHARINDEX('[',l.EmailAddress)+1,CHARINDEX(']',l.EmailAddress)-1-CHARINDEX('[',l.EmailAddress))
								 WHEN l.EmailAddress like '%;%#%' THEN SUBSTRING(l.EmailAddress,1,CHARINDEX(';',l.EmailAddress)-1)
								 WHEN l.EmailAddress like '%#%' THEN SUBSTRING(l.EmailAddress,1,CHARINDEX('#',l.EmailAddress)-1)
							END
				 ELSE l.EmailAddress
			END AS Email1
		,	bai.Street AS Address1
		,	bai.City
		,	bai.[State]
		,	bai.[ZipCode] AS Zip
		,	CASE WHEN vl.MultiCheck = 1 THEN 'NO' ELSE 'YES' END AS MERGE_PAYMENT_REQUESTS
		,	vl.VendorId AS DYNAMIC_VENDOR_ID
		,	pt.PaymentType as VENDOR_PAYMENT_FOR_CHECKLIST
		,	'Borrower' AS VENDOR_TYPE

	FROM  MARS.dbo.vw_Loans l
	INNER JOIN MARS.dbo.VendorsList AS vl  
		ON	vl.TIN = l.TIN
	JOIN	MARS.dbo.BORROWERVENDOR bv
		ON	bv.VENDORID = vl.VendorID
	JOIN	MARS.dbo.BorrowerAddressInfo bai
		ON	bai.BorrowerID = bv.BORROWERID
		AND bai.IsActive = 1
	INNER JOIN MARS.dbo.VendorPaymentsInfo AS vpi
		ON	vpi.VendorId = vl.VendorId
	INNER JOIN MARS.dbo.PaymentTypeDetail AS ptd 
		ON vpi.VendorPaymentInfoId = ptd.PaymentTypeDetailID
	INNER JOIN MARS.dbo.PaymentTypes AS pt 
		ON ptd.PaymentTypeID = pt.PaymentTypeId
	LEFT JOIN IntacctDM.dbo.Vendors iv
		ON	iv.MARSVendorID = vl.VendorId
	WHERE	l.isActive = 1
		AND	vl.InsertedDate > '07/01/2017'
		AND iv.IntacctVendorID IS NULL


	SELECT *
	FROM #IntacctVendor


END
