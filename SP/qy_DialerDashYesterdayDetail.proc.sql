USE MARS_DW;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.qy_DialerDashYesterdayDetail') IS NOT NULL
	DROP PROCEDURE dbo.qy_DialerDashYesterdayDetail;
GO

/* ********************************************************************************************************* *
   Procedure Name  :	qy_DialerDashDistinctAccountContact
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	02/07/2017

   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date			Author				Description
   --------		-----------			------------
   02/14/2017	Benjacob Beres		Removed the InboundNotOnDialer grouping
   05/04/2017	Benjacob Beres		Add @AsOfDate Code to pull previous working date.
   
*  ********************************************************************************************************* */
CREATE PROCEDURE dbo.qy_DialerDashYesterdayDetail 

AS
BEGIN

	DECLARE @AsOfDate DATE
	SET @AsOfDate =	(	SELECT MAX(dd.[Date])
						FROM  Support.[dbo].[DimDate] dd
						WHERE	dd.[Date] < CAST(GETDATE() AS DATE)
							AND dd.IsWeekday = 1 
							AND dd.IsHoliday = 0
					)

	/*****************************************************************
	Pull Contact Only Call information from the single source proc
	*****************************************************************/
	CREATE TABLE #ResultswithCarryover
		(	Account	VARCHAR(30)
		,	ArchiveDate DATE
		,	CallType VARCHAR(30)
		,	Carryover INT
		)

	INSERT INTO #ResultswithCarryover
	EXEC qy_DialerDashDistinctAccountContact 7


	/*****************************************************************
	Retreive data from ACH to apply additional aggregates to the day
	*****************************************************************/
	SELECT	a.LoanNumber
		,	CAST(a.InsertedDate AS DATE) AS InsertedDate
		,	SUM(a.[Billing Amount]) AS TotalACH
	INTO #ACHData
	FROM MARS.[dbo].[vw_ACHBillingsEnteredByLoanByRep] a
	WHERE	CAST(a.InsertedDate AS DATE) >= GETDATE()-8
		AND	[Reoccuring Schedule/One Time] = 'One Time'
	GROUP BY a.LoanNumber
		,	CAST(a.InsertedDate AS DATE)
			 
	
	/*****************************************************************
	Aggregate the detail for the previous day only
	*****************************************************************/
	SELECT	r.ArchiveDate
		,	COUNT(r.Account) AS Total
		,	SUM(r.Carryover) AS CarryOverCnt
		,	SUM(CASE WHEN r.CallType = 'InboundContact' THEN 1 ELSE 0 END) AS InboundContact
		,	SUM(CASE WHEN r.CallType = 'OutboundContact' THEN 1 ELSE 0 END) AS OutboundContact
		,	SUM(CASE WHEN r.CallType = 'OutboundAttempt' THEN 1 ELSE 0 END) AS OutboundAttempt
		,	SUM(CASE WHEN r.CallType = 'OutboundAttemptNotOnDialer' THEN 1 ELSE 0 END) AS OutboundAttemptNotOnDialer
		,	SUM(CASE WHEN r.CallType = 'OutboundContactNotOnDialer' THEN 1 ELSE 0 END) AS OutboundContactNotOnDialer
		,	SUM(CASE WHEN r.Carryover = 1 AND r.CallType IN ('Untouched Loans', 'OutboundAttempt', 'OutboundAttemptNotOnDialer') THEN 1 ELSE 0 END) AS UntouchedCarryoverLoans
		,	SUM(CASE WHEN r.CallType = 'Untouched Loans' THEN 1 ELSE 0 END) AS UnTouchedLoans
		,	SUM(CASE WHEN r.CallType = 'InboundContact' AND a.LoanNumber IS NOT NULL THEN 1 ELSE 0 END) AS ICACHCount
		,	SUM(CASE WHEN r.CallType = 'InboundContact' AND a.LoanNumber IS NOT NULL THEN a.TotalACH ELSE 0 END) AS ICACHSum
		,	SUM(CASE WHEN r.CallType = 'OutboundContact' AND a.LoanNumber IS NOT NULL THEN 1 ELSE 0 END) AS OCACHCount
		,	SUM(CASE WHEN r.CallType = 'OutboundContact' AND a.LoanNumber IS NOT NULL THEN a.TotalACH ELSE 0 END) AS OCACHSum
		,	SUM(CASE WHEN r.CallType = 'OutboundAttemptNotOnDialer' AND a.LoanNumber IS NOT NULL THEN 1 ELSE 0 END) AS OMACHCount
		,	SUM(CASE WHEN r.CallType = 'OutboundAttemptNotOnDialer' AND a.LoanNumber IS NOT NULL THEN a.TotalACH ELSE 0 END) AS OMACHSum
	INTO #TotalCounts
	FROM	#ResultswithCarryover r
		LEFT JOIN #ACHData a
			ON	a.LoanNumber = r.Account
			AND a.InsertedDate = r.ArchiveDate
	WHERE r.ArchiveDate = @AsOfDate
	GROUP BY r.ArchiveDate
	ORDER BY r.ArchiveDate


	SELECT	t.ArchiveDate AS CallDate
		,	t.Total
		,	t.CarryOverCnt AS Carryover
		,	t.InboundContact
		,	t.OutboundContact
		,	t.OutboundContactNotOnDialer
		,	t.Total - (t.InboundContact + t.OutboundContact + t.OutboundContactNotOnDialer) AS TotalUntouchedLoans
		,	t.UntouchedCarryoverLoans
		,	t.Total - (t.InboundContact + t.OutboundContact + t.OutboundContactNotOnDialer) - t.UntouchedCarryoverLoans AS UntouchedLoans
		,	CAST((CAST((t.InboundContact + t.OutboundContact) AS FLOAT)/CAST(t.Total AS FLOAT)) AS DECIMAL(3,2)) AS ContactPenetration
		,	t.ICACHCount
		,	t.ICACHSum
		,	t.OCACHCount
		,	t.OCACHSum
		,	t.OMACHCount
		,	t.OMACHSum
	FROM #TotalCounts t

END
