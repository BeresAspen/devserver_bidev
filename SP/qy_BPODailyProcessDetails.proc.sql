USE [PI]
GO
/****** Object:  StoredProcedure [dbo].[qy_BPODailyProcessDetails]    Script Date: 2/27/2018 3:00:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[qy_BPODailyProcessDetails] (@BPOProvider VARCHAR(100), @ProcDate DATE)
AS
BEGIN

/********************************************************************************************************** *
   Procedure Name  :	qy_BPODailyProcessDetails
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	02/05/2018

   *********************************************************************************************************
   **         Change History                                                                              **
   *********************************************************************************************************

   Date			Author				Description
   --------		-----------			------------
   
************************************************************************************************************ */
DECLARE @LoadBatchID INT

SET @LoadBatchID = (
					SELECT LoadBatchID
					FROM dbo.LoadBatch lb
					JOIN dbo.BPOProviders bp
						ON	bp.BPOProviderID = lb.BPOProviderID
					WHERE	bp.BPOProviderName = @BPOProvider
						AND	CAST(lb.BatchDate AS DATE) = @ProcDate
					)


SELECT bp.BPOProviderName
	,	COUNT(htlh.LoadBatchID) AS RowCnt
	,	CASE WHEN Error = 'none'
				THEN 'Orders Loaded to Aspen' 
			ELSE 'Orders Contained Data Validation Errors. Records sent back to Provider.' 
		END AS ErrorFlag
INTO #DataGroup
FROM dbo.HomeTrustLoadHistory htlh
JOIN dbo.LoadBatch lb
	ON	lb.LoadBatchID = htlh.LoadBatchID
JOIN dbo.BPOProviders bp
	ON	bp.BPOProviderID = lb.BPOProviderID
WHERE htlh.LoadBatchID = @LoadBatchID
GROUP BY bp.BPOProviderName
	,	CASE WHEN Error = 'none'
				THEN 'Orders Loaded to Aspen' 
			ELSE 'Orders Contained Data Validation Errors. Records sent back to Provider.' 
		END


SELECT	2 AS RowOrder
	,	BPOProviderName
	,	RowCnt
	,	ErrorFlag
INTO #Display
FROM #DataGroup
WHERE ErrorFlag <> 'Orders Loaded to Aspen'

UNION ALL

SELECT	1
	,	BPOProviderName
	,	SUM(RowCnt) AS RowCnt
	,	'Orders Loaded to Aspen tables'
FROM #DataGroup
GROUP BY BPOProviderName



SELECT BPOProviderName
	,	RowCnt
	,	ErrorFlag
FROM #Display
ORDER BY RowOrder

END