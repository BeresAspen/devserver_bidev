USE [Applications]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.qy_ExecuteSQLAgentJob') IS NOT NULL
	DROP PROCEDURE dbo.qy_ExecuteSQLAgentJob;

GO

CREATE PROCEDURE [dbo].[qy_ExecuteSQLAgentJob]
					(	@SQLAgentJob VarChar(255)
					,	@User		 VarChar(128) = NULL
					)

AS
BEGIN


/********************************************************************************************************** *
	Procedure Name		:	dbo.qy_ExecuteSQLAgentJob 'UAT Intacct CashPro Check File Creation'
	Business Analyis	:	
	Project/Process		:   
	Creation Ticket		:	(JIRA) MARS-3126
	Author				:	Bob Bowers
	Create Date			:	02/06/2018
	Description			:	


	*********************************************************************************************************
	**         Change History                                                                              **
	*********************************************************************************************************
	Date		Author			Version			Ticket#(system)		Description of Change
	----------	-------------	-------------	-----------------	-----------------------
	02/21/2018	Benjacob Beres					(JIRA) MARS-3126	Added reference to Intacct Job

************************************************************************************************************ */

DECLARE	@Application	varchar(128) = 'Intacct CashPro Integration'
	,	@Version		varchar(10)  = '1.00.000'
	,	@JobRunning		bit = 0
	,	@JobRunningMsg	varchar(100)
	,	@Intacct		bit = 0
	,	@BatchIDField	INT
	,	@ProcDate		DATETIME = GETDATE()
	,	@RunningFlag	BIT = 0

SET @JobRunningMsg = 'The Job '+@SQLAgentJob+' is already running'

IF @User IS NULL 
  SET @User = SUSER_NAME ();

IF @SQLAgentJob IN ('UAT Intacct Sync Cloud to DM', 'UAT Intacct CashPro Check File Creation', 'Intacct CashPro Check File Creation')
BEGIN
	SET @Intacct = 1

	INSERT INTO Applications.dbo.[Batches] ([Description], [Start], UserID, ProcessID)
	SELECT	@SQLAgentJob AS [Description]
		,	@ProcDate AS [Start]
		,	18 AS UserID
		,	13 AS ProcessID

	SELECT @BatchIDField = SCOPE_IDENTITY()

	INSERT INTO Applications.dbo.BatchLogs (BatchID, [Description], StartDate, Step)
	SELECT	@BatchIDField AS BatchID
		,	@User AS [Description]
		,	@ProcDate AS StartDate
		,	'Run By' AS Step
	

END;
------------------------------------------------------------------------------------------------------
-- This logs the number of times 
------------------------------------------------------------------------------------------------------


BEGIN TRY
	
	IF @Intacct = 1
	BEGIN

		DELETE 
		FROM Utilities.dbo.IntacctCashProStatus

	END

	IF NOT EXISTS ( SELECT 1 
					FROM msdb.dbo.sysjobs_view job  WITH(NOLOCK)
					INNER JOIN msdb.dbo.sysjobactivity activity WITH(NOLOCK)
						ON job.job_id = activity.job_id 
					WHERE	activity.run_Requested_date IS NOT NULL  
						AND activity.stop_execution_date IS NULL  
						AND job.name = @SQLAgentJob 
				  ) 
	BEGIN
		SET @RunningFlag = 1

		EXEC msdb.dbo.sp_start_job @SQLAgentJob; 

		WHILE @RunningFlag = 1
		BEGIN
			
			WAITFOR DELAY '00:00:03';

			IF NOT EXISTS ( SELECT 1 
							FROM msdb.dbo.sysjobs_view job WITH(NOLOCK)
							INNER JOIN msdb.dbo.sysjobactivity activity WITH(NOLOCK)
								ON job.job_id = activity.job_id 
							WHERE	activity.run_Requested_date IS NOT NULL  
								AND activity.stop_execution_date IS NULL  
								AND job.name = @SQLAgentJob 
						  ) 
			BEGIN
				SET @RunningFlag = 0
			END
		END --WHILE

		UPDATE b
			SET [End] = GETDATE()
			,	[Message] = 'Success'
			FROM Applications.dbo.[Batches] b
			WHERE b.BatchID = @BatchIDField

	END 
	ELSE 
	BEGIN 
		SET @JobRunning = 1
		RAISERROR(@JobRunningMsg,16,1) 
	END 
		

END TRY

BEGIN CATCH

DECLARE @Err           INT
	,	@ErrorMessage  Varchar(Max)
	,	@ErrorLine     Varchar(128)
	,	@Workstation   VarChar(128)
	,	@Proc          VarChar(128)

	

	IF Error_Number() IS NULL 
		SET @Err =0;
	ELSE
		SET @Err = Error_Number();
		
	SET @ErrorMessage = Error_Message()
	SET @ErrorLine    = 'SP Line Number: ' + Cast(Error_Line() as varchar(10))
	SET @Workstation  = HOST_NAME()
	SET @Proc         = OBJECT_NAME(@@ProcID)

	IF @Intacct = 1
	BEGIN

		IF @JobRunning = 1
		BEGIN 

			UPDATE b
				SET [End] = GETDATE()
				,	[Message] = 'Process already running.'
				FROM Applications.dbo.[Batches] b
				WHERE b.BatchID = @BatchIDField

			INSERT INTO Utilities.dbo.IntacctCashProStatus
				VALUES (GETDATE(), 'The Process is already running')
		END
		ELSE
		BEGIN

			UPDATE b
				SET [End] = GETDATE()
				,	[Message] = 'Failure. SQL Error Code ' + CAST(@Err AS VARCHAR(10)) + '. "' + @ErrorMessage + '"'
				FROM Applications.dbo.[Batches] b
				WHERE b.BatchID = @BatchIDField

			INSERT INTO Utilities.dbo.IntacctCashProStatus
				VALUES (GETDATE(), 'Process Failed')
		END

	END

	EXEC Applications.dbo.di_ErrorLog	@Application ,@Version ,@Err, @ErrorMessage, @Proc, @ErrorLine, @User , @Workstation

END CATCH   

END