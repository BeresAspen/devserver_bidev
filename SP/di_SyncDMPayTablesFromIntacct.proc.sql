USE IntacctDM;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('di_SyncDMPayTablesFromIntacct') IS NOT NULL
	DROP PROCEDURE di_SyncDMPayTablesFromIntacct;
GO


CREATE PROCEDURE dbo.di_SyncDMPayTablesFromIntacct
AS
BEGIN

/********************************************************************************************************** *
   Procedure Name  :	di_SyncDMPayTablesFromIntacct
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	11/17/2017

   *********************************************************************************************************
   **         Change History                                                                              **
   *********************************************************************************************************

   Date			Author				Description
   --------		-----------			------------
   
************************************************************************************************************ */


-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
--APPYMT Process
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------


--When Records exists already, check if the modified date is different. If so, move Primary Record to History and insert Staging into Primary
IF OBJECT_ID('tempdb..#tempAPPymt') IS NOT NULL
	DROP TABLE #tempAPPymt;

--List of Existing Records
SELECT iab.*
INTO #tempAPPymt
FROM Staging.dbo.IntacctStgAPPymt sab
LEFT JOIN IntacctDM.dbo.APPymt iab
	ON	iab.RECORDNO = sab.RECORDNO
	AND CAST(iab.WHENMODIFIED AS DATETIME) < CAST(sab.WHENMODIFIED AS DATETIME)
WHERE iab.RECORDNO IS NOT NULL


--Move existing records to History table
INSERT INTO IntacctDM.dbo.hstAPPymt
SELECT *
FROM #tempAPPymt

--Delete the records from the Primary table
DELETE 
FROM IntacctDM.dbo.APPymt
WHERE EXISTS (	SELECT 1
				FROM #tempAPPymt t
				WHERE t.RECORDNO = IntacctDM.dbo.APPymt.RECORDNO)
				
--Insert records that do not exist in Primary (this includes new records and the replacements for the records that were just deleted)
INSERT INTO IntacctDM.dbo.APPymt
SELECT sab.*
FROM Staging.dbo.IntacctStgAPPymt sab
LEFT JOIN IntacctDM.dbo.APPymt iab
	ON	iab.RECORDNO = sab.RECORDNO
WHERE iab.RECORDNO IS NULL


-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
--APPYMTDETAIL Process
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------


--When Records exists already, check if the modified date is different. If so, move Primary Record to History and insert Staging into Primary
IF OBJECT_ID('tempdb..#tempAPPymtDetail') IS NOT NULL
	DROP TABLE #tempAPPymtDetail;

--List of Existing Records
SELECT iab.*
INTO #tempAPPymtDetail
FROM Staging.dbo.IntacctStgAPPymtDetail sab
LEFT JOIN IntacctDM.dbo.APPymtDetail iab
	ON	iab.RECORDNO = sab.RECORDNO
	AND CAST(iab.WHENMODIFIED AS DATETIME) < CAST(sab.WHENMODIFIED AS DATETIME)
WHERE iab.RECORDNO IS NOT NULL


--Move existing records to History table
INSERT INTO IntacctDM.dbo.hstAPPymtDetail
SELECT *
FROM #tempAPPymtDetail


--Delete the records from the Primary table
DELETE 
FROM IntacctDM.dbo.APPymtDetail
WHERE EXISTS (	SELECT 1
				FROM #tempAPPymtDetail t
				WHERE t.RECORDNO = IntacctDM.dbo.APPymtDetail.RECORDNO)
				
--Insert records that do not exist in Primary (this includes new records and the replacements for the records that were just deleted)
INSERT INTO IntacctDM.dbo.APPymtDetail
SELECT sab.*
FROM Staging.dbo.IntacctStgAPPymtDetail sab
LEFT JOIN IntacctDM.dbo.APPymtDetail iab
	ON	iab.RECORDNO = sab.RECORDNO
WHERE iab.RECORDNO IS NULL


-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
--CHECKINGACCOUNT Process
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------

--When Records exists already, check if the modified date is different. If so, move Primary Record to History and insert Staging into Primary
IF OBJECT_ID('tempdb..#tempCheckingAccount') IS NOT NULL
	DROP TABLE #tempCheckingAccount;

--List of Existing Records
SELECT iab.*
INTO #tempCheckingAccount
FROM Staging.dbo.IntacctStgCheckingAccount sab
LEFT JOIN IntacctDM.dbo.CheckingAccount iab
	ON	iab.RECORDNO = sab.RECORDNO
	AND CAST(iab.WHENMODIFIED AS DATETIME) < CAST(sab.WHENMODIFIED AS DATETIME)
WHERE iab.RECORDNO IS NOT NULL


--Move existing records to History table
INSERT INTO IntacctDM.dbo.hstCheckingAccount
SELECT *
FROM #tempCheckingAccount

--Delete the records from the Primary table
DELETE 
--select *
FROM IntacctDM.dbo.CheckingAccount
WHERE EXISTS (	SELECT 1
				FROM #tempCheckingAccount t
				WHERE t.RECORDNO = CheckingAccount.RECORDNO)
				
--Insert records that do not exist in Primary (this includes new records and the replacements for the records that were just deleted)
INSERT INTO IntacctDM.dbo.CheckingAccount
SELECT sab.*
FROM Staging.dbo.IntacctStgCheckingAccount sab
LEFT JOIN IntacctDM.dbo.CheckingAccount iab
	ON	iab.RECORDNO = sab.RECORDNO
WHERE iab.RECORDNO IS NULL


END