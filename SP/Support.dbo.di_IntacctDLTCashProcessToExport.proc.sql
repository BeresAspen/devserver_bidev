USE [Support]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.di_IntacctDLTCashProcessToExport') IS NOT NULL
	DROP PROCEDURE dbo.di_IntacctDLTCashProcessToExport;
GO

CREATE PROCEDURE dbo.di_IntacctDLTCashProcessToExport
AS
BEGIN

/* ********************************************************************************************************* *
   Procedure Name  :	di_IntacctDLTCashProcessToExport
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	10/10/2017

   *********************************************************************************************************
   **         Change History                                                                              **
   *********************************************************************************************************

   Date			Author				Jira#			Description
   --------		-----------			----------		------------
	05/02/2018	Benjacob Beres		MARS-2980		Modified to add the grouping section at the end to 
														accomodate the request for the new import format
	07/12/2018	Benjacob Beres		MARS-3865		Added the update to the #DLTJETemp table so the locationID is updated from 120 to 150
*  ********************************************************************************************************* */

	DECLARE @today DATE
	DECLARE @lookback INT

	SET @today = CAST(GETDATE() AS DATE)

	IF OBJECT_ID('tempdb..#DLTJETemp') IS NOT NULL 
		DROP TABLE #DLTJETemp;
	IF OBJECT_ID('tempdb..#CashMoves') IS NOT NULL 
		DROP TABLE #CashMoves;
	IF OBJECT_ID('tempdb..#CashMovesFinal') IS NOT NULL 
		DROP TABLE #CashMovesFinal;
	IF OBJECT_ID('tempdb..#PrevDate') IS NOT NULL 
		DROP TABLE #PrevDate;
	IF OBJECT_ID('tempdb..#ACHUsed') IS NOT NULL
		DROP TABLE #ACHUsed;

	-------------------------------------------------------------------------------------------
	;WITH cteDates
	AS
	(
		SELECT	dd.[Date]
			,	CAST(MAX(d1.[Date]) AS DATE) AS LastBusDay
			,	CAST(MIN(d2.[Date]) AS DATE) AS NextBusDay
		--INTO #PrevDate
		FROM	Support.dbo.DimDate dd
			JOIN Support.dbo.DimDate d1 
				ON	d1.DateKey < dd.DateKey
				AND d1.IsWeekday = 1 
				AND d1.IsHoliday = 0
				AND d1.[Date] BETWEEN DATEADD(DD,-90,@today) AND DATEADD(DD,90,@today)
			JOIN Support.dbo.DimDate d2
				ON	d2.DateKey > dd.DateKey
				AND d2.IsWeekday = 1 
				AND d2.IsHoliday = 0 
				AND d2.[Date] BETWEEN DATEADD(DD,-90,@today) AND DATEADD(DD,90,@today)
		WHERE	dd.[Date] BETWEEN DATEADD(DD,-45,@today) AND DATEADD(DD,45,@today)
			--AND dd.IsWeekday = 1 
			--AND dd.IsHoliday = 0
		GROUP BY dd.[Date]
	)
	, cteDates2
	AS
	(
		SELECT	c.[Date]
			,	c.LastBusDay
			,	c.NextBusDay
			,	c1.NextBusDay AS Next2ndBusDay
		FROM cteDates c
		JOIN cteDates c1
			ON	CAST(c1.[Date] AS DATE) = c.NextBusDay
	)
	, cteDates3
	AS
	(
		SELECT	c.[Date]
			,	c1.LastBusDay AS Last2ndBusDay
			,	c.LastBusDay
			,	c.NextBusDay
			,	c.Next2ndBusDay
		FROM cteDates2 c
		JOIN cteDates2 c1
			ON	CAST(c1.[Date] AS DATE) = c.LastBusDay
	)
		SELECT	c.[Date]
			,	c.Last2ndBusDay
			,	c.LastBusDay
			,	c.NextBusDay
			,	c.Next2ndBusDay
			,	c1.NextBusDay AS Next3rdBusDay
		INTO #PrevDate
		FROM cteDates3 c
		JOIN cteDates3 c1
			ON	CAST(c1.[Date] AS DATE) = c.Next2ndBusDay
		ORDER BY c.[Date];
	
	-------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------

	SELECT @lookback = DATEDIFF(DD,[Date],LastBusDay)
	FROM #PrevDate
	WHERE [Date] = @today

	--SELECT @lookback;

	--SET @lookback = -28;


	-------------------------------------------------------------------------------------------
	--Bring Data from the DLT Run
	-------------------------------------------------------------------------------------------

	SELECT ije.JOURNAL, 
			ije.[DATE], 
			ije.[DESCRIPTION], 
			ije.REFERENCE_NO, 
			ije.ACCT_NO, 
			CASE WHEN ije.LOCATION_ID = '150' and vl.NoteOwner = 'GFGP Mortgage Trust, a Delaware Trust, Wilmington Savings Fund Society, FSB, Trustee' 
						THEN '120' 
				 ELSE ije.LOCATION_ID END AS LOCATION_ID, 
			ije.DEBIT, 
			ije.CREDIT, 
			ije.GLDIMSTATUS, 
			ije.GLENTRY_CLASSID
	INTO #DLTJETemp
	FROM Support.dbo.IntacctJournalEntries ije
	LEFT JOIN MARS.dbo.vw_Loans vl
		ON	vl.Account = ije.GLENTRY_CLASSID


	-------------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------------


	SELECT	DISTINCT A.MARSID, CR.PaymentDate, 1 AS Dayadd
	INTO #ACHUsed
	FROM MARS.dbo.CashReceipt AS CR 
	JOIN MARS.dbo.AssetEntityMappings A 
		ON CR.AssetMappingID = A.AssetEntityMappingID
	WHERE	cr.PaymentDate >= DATEADD(DD,@lookback-7,@today)
		AND cr.PaymentDate < @today
		AND	cr.SourceTypeCD = 'ACH'

	---------------------------------------------------------------------------------------------------------

	--CASH Movements
	SELECT	JOURNAL
		,	[DATE]
		,	[DESCRIPTION]
		,	REFERENCE_NO
		,	ACCT_NO
		,	LOCATION_ID
		,	MEMO
		,	DEBIT
		,	CREDIT
	--	,	SUM(CAST(CASE WHEN RTRIM(LTRIM(DEBIT)) = '' THEN 0.00 ELSE RTRIM(LTRIM(DEBIT)) END AS DECIMAL(18,2))) AS DEBIT
	--	,	SUM(CAST(CASE WHEN RTRIM(LTRIM(CREDIT)) = '' THEN 0.00 ELSE RTRIM(LTRIM(CREDIT)) END AS DECIMAL(18,2))) AS CREDIT
		,	GLDIMSTATUS
		,	CASE WHEN (CAST(t.ACCT_NO AS INT) BETWEEN 10000 AND 13999) 
						AND CAST(CAST(CASE WHEN t.CREDIT = '' THEN '0.00' ELSE t.CREDIT END AS VARCHAR(17)) AS DECIMAL(17,2)) > 0 THEN 'PMTO'
					WHEN t.ACCT_NO = '15525' 
						AND CAST(CAST(CASE WHEN t.DEBIT = '' THEN '0.00' ELSE t.DEBIT END AS VARCHAR(17)) AS DECIMAL(17,2)) > 0 THEN 'PMTO'
					WHEN t.ACCT_NO = '23525' 
						AND CAST(CAST(CASE WHEN t.DEBIT = '' THEN '0.00' ELSE t.DEBIT END AS VARCHAR(17)) AS DECIMAL(17,2)) > 0 THEN 'PMTO'
					ELSE 'PMTI'
			END AS GLDIMACTIVITY
		,	GLENTRY_CUSTOMERID
		,	GLENTRY_CLASSID
	INTO #CashMoves
	FROM (	SELECT	'DLTTRANJ' AS JOURNAL
				,	CONVERT(VARCHAR(10), CASE WHEN ISNULL(ach.Dayadd,0) = 1 THEN pd.Next3rdBusDay ELSE pd.Next2ndBusDay END, 101) AS [DATE]
				,	d.[DESCRIPTION]
				,	d.[DESCRIPTION] + ' ' + CONVERT(VARCHAR(10), CASE WHEN ISNULL(ach.Dayadd,0) = 1 THEN pd.Next3rdBusDay ELSE pd.Next2ndBusDay END, 101) AS REFERENCE_NO
				,	ac.IncludeInCashMove
				,	CASE WHEN d.ACCT_NO IN ('14110', '14115', '14131', '18110', '18125', '40100', '46100', '40105', '15500', '18120', '18130') THEN ac.IntacctCollections
						-- WHEN d.ACCT_NO IN (, '18145') THEN '11000'  -- get back this escrow
							WHEN d.ACCT_NO = '23535' THEN '13602'
							WHEN d.ACCT_NO = '15515' THEN '13600'
							WHEN d.ACCT_NO = '23500' AND d.LOCATION_ID NOT IN ('480', '482') THEN '13600'
							WHEN d.ACCT_NO = '23500' AND d.LOCATION_ID IN ('480', '482') THEN ac.IntacctCollections
							WHEN d.ACCT_NO IN ('23505')  THEN ac2.IntacctEscrow			--associate escrow account with the companyid of the loan NOT 150
							WHEN d.ACCT_NO IN ('23525')  
												THEN	CASE WHEN ac2.IncludeInCashMove = 1
																THEN d.ACCT_NO
																ELSE ac2.IntacctCollections			--associate escrow account with the companyid of the loan NOT 150
														END
							WHEN d.ACCT_NO IN ('15505', '15510', '15525') THEN d.ACCT_NO --'23525', needs to be associated with the loan not 150
					END AS ACCT_NO
				,	d.LOCATION_ID
				,	CONVERT(VARCHAR(10), CASE WHEN ISNULL(ach.Dayadd,0) = 1 THEN pd.Next3rdBusDay ELSE pd.Next2ndBusDay END, 101) 
								+ ' DLTTRANJ' + CASE WHEN ISNULL(ach.Dayadd,0) = 1 THEN ' ACH' ELSE '' END AS MEMO
				,	d.CREDIT AS DEBIT
				,	d.DEBIT AS CREDIT
				,	d.GLDIMSTATUS
				,	'PMTI' AS GLDIMACTIVITY
				,	acom.IntacctCompanyNum AS GLENTRY_CUSTOMERID
				,	GLENTRY_CLASSID
			FROM #DLTJETemp d
			JOIN MARS.dbo.vw_Loans vl
				ON	vl.Account = d.REFERENCE_NO
			JOIN #PrevDate pd
				ON	pd.[Date] = d.[DATE]
			LEFT JOIN Support.dbo.AcctCompanyGLTranslations ac
				ON	ac.IntacctCo = d.LOCATION_ID
			LEFT JOIN MARS_DW.dbo.AcctConversionNoteOwnerMapping acom 
				ON	acom.TMONoteOwner = vl.NoteOwner
			LEFT JOIN Support.dbo.REPOLoanDetails rld
				ON	rld.Account = d.REFERENCE_NO
			LEFT JOIN #ACHUsed ach
				ON	ach.MARSID = d.REFERENCE_NO 
				AND ach.PaymentDate = pd.[DATE]
			LEFT JOIN Support.dbo.AcctCompanyGLTranslations ac2
				ON	ac2.IntacctCo = --acom.IntacctCompanyNum
				CASE WHEN acom.IntacctCompanyNum = '525' AND rld.Account IS NULL
											THEN '525A'
										 ELSE acom.IntacctCompanyNum
									END
			WHERE	d.[DESCRIPTION] IN ('REG', 'TRUST', 'PIF', 'TE')
			) t
	WHERE	T.ACCT_NO IS NOT NULL
		AND t.IncludeInCashMove = 1

	
	UPDATE #CashMoves 
	SET		LOCATION_ID = '150'
		,	GLENTRY_CUSTOMERID = '150'
	WHERE	LOCATION_ID = '120'


	SELECT	JOURNAL
		,	[DATE]
		,	[DESCRIPTION]
		,	REFERENCE_NO
		,	LINE_NO
		,	ACCT_NO
		,	LOCATION_ID
		,	MEMO
		,	DEBIT
		,	CREDIT
		,	GLDIMSTATUS
		,	GLDIMACTIVITY
		,	GLENTRY_CUSTOMERID
		,	GLENTRY_CLASSID
	INTO #CashMovesFinal
	FROM (	SELECT	JOURNAL
				,	[DATE]
				,	[DESCRIPTION]
				,	REFERENCE_NO
				,	ROW_NUMBER() OVER(PARTITION BY LOCATION_ID, [DESCRIPTION], [DATE] ORDER BY REFERENCE_NO, ACCT_NO ) AS LINE_NO
				,	ACCT_NO
				,	LOCATION_ID
				,	MEMO
				,	DEBIT
				,	CREDIT
				,	GLDIMSTATUS
				,	GLDIMACTIVITY
				,	GLENTRY_CUSTOMERID
				,	GLENTRY_CLASSID
			FROM #CashMoves
		 ) c



	 
--------------------------------------------------------------------------------------
--Regular DLT
--------------------------------------------------------------------------------------
	TRUNCATE TABLE Support.dbo.IntacctJournalEntries;

	INSERT INTO Support.dbo.IntacctJournalEntries
				(JOURNAL, [DATE], [DESCRIPTION], REFERENCE_NO, LINE_NO, ACCT_NO, LOCATION_ID, MEMO, DEBIT, CREDIT, GLDIMSTATUS, GLDIMACTIVITY, GLENTRY_CUSTOMERID, GLENTRY_CLASSID)
	SELECT *
	FROM #CashMovesFinal
	WHERE MEMO NOT LIKE '%ACH%'


--------------------------------------------------------------------------------------
--ACH
--------------------------------------------------------------------------------------
	 
	TRUNCATE TABLE Support.dbo.IntacctJournalEntriesACH;

	INSERT INTO Support.dbo.IntacctJournalEntriesACH
				(JOURNAL, [DATE], [DESCRIPTION], REFERENCE_NO, LINE_NO, ACCT_NO, LOCATION_ID, MEMO, DEBIT, CREDIT, GLDIMSTATUS, GLDIMACTIVITY, GLENTRY_CUSTOMERID, GLENTRY_CLASSID)
	SELECT *
	FROM #CashMovesFinal
	WHERE MEMO LIKE '%ACH%'


END