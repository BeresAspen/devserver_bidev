USE [MARS_DW]
GO

/****** Object:  StoredProcedure [dbo].[rpt_preDialerReport]    Script Date: 4/6/2017 7:59:08 AM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


/************************************************************************

	Object : preDialerReport.proc.sql
	Author: Benjacob Beres
	Create date: 01/10/2017
	Description: SSRS report for the PreDialer

*************************************************************************/

CREATE PROCEDURE [dbo].[rpt_preDialerReport]
AS
BEGIN

;with cteCategoryWorking
AS
(
	SELECT	t.Account
		,	t.MyDateTime
		,	t.Category
	FROM (
			select	l.Account
				,	CAST(lc.MyDateTime AS DATE) AS MyDateTime
				,	lc.Category
				,	ROW_NUMBER() OVER( PARTITION BY l.Account, CAST(lc.MyDateTime AS DATE) ORDER BY lc.MyDateTime DESC) AS RowNum
			from	MARS.dbo.LoansContacts lc (NOLOCK)
			left join MARS.dbo.vw_Loans l
				ON	l.RecID = lc.LoanRecID
			where lc.MyDateTime BETWEEN '07/01/2016' AND '10/01/2016'
				and lc.Category IN (	'NO - Outbound Attempt/no Contact with Borrower'
									,	'TA - Outbound Attempt/no Contact with Authorized NO3'
									,	'AA - Outbound Attempt/no Contact with Attorney'
									,	'TO - Authorized 3rd Party on Outbound Call'
									,	'AO - Attorney Contact on Outbound Call'
									,	'BO - Borrower Contact on Outbound Call'
									,	'AI - Attorney Contact on Inbound Call'
									,	'BI - Borrower Contact on Inbound Call'
									,	'TC - Authorized 3rd Party on Inbound'
									,	'NA - Non-Authorized 3rd Party on Inbound Call') 
		) t
	WHERE t.RowNum = 1
)
select	c.Account
	,	c.MyDateTime
	,	CASE WHEN c.Category IN (	'AI - Attorney Contact on Inbound Call'
								,	'BI - Borrower Contact on Inbound Call'
								,	'TC - Authorized 3rd Party on Inbound'
								,	'NA - Non-Authorized 3rd Party on Inbound Call') 
				THEN 1 
			 ELSE 0
		END AS InboundContacts
	,	CASE WHEN c.Category IN (	'NO - Outbound Attempt/no Contact with Borrower'
								,	'TA - Outbound Attempt/no Contact with Authorized NO3'
								,	'AA - Outbound Attempt/no Contact with Attorney') 
				THEN 1 
			 ELSE 0
		END AS OutboundAttempt
	,	CASE WHEN c.Category IN (	'TO - Authorized 3rd Party on Outbound Call'
								,	'AO - Attorney Contact on Outbound Call'
								,	'BO - Borrower Contact on Outbound Call') 
				THEN 1 
			 ELSE 0
		END AS OutboundContact
INTO	#ManualOutbound
FROM	cteCategoryWorking c


SELECT	m.MyDateTime
	,	SUM(m.InboundContacts) AS InboundContact
	,	SUM(OutboundAttempt) AS OutboundAttempt
	,	SUM(OutboundContact) AS OutboundContact
FROM	#ManualOutbound m
GROUP BY m.MyDateTime
ORDER BY m.MyDateTime

END
GO


