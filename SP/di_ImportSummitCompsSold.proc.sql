USE [BPO]
GO
/****** Object:  StoredProcedure [dbo].[di_ImportSummitCompsSold]    Script Date: 3/22/2018 1:50:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:		Phil Finkle
-- Create date: 09/21/2016
-- Description:	Inserts into the dbo.SummitComps
--	table fro the staging table.
-- =======================================================================
ALTER PROCEDURE [dbo].[di_ImportSummitCompsSold]


AS
BEGIN

	SET NOCOUNT ON;

	;WITH CTE ([LoanNumber],[InspectionDate],[LSType],[LSNumber],[Address],[City],[State]
						,[Zip]	,[County],[DistanceToSubject],[LastListPrice],[Dom],[NumOfUnits]
						,[PropertyType],[PropertyStyle],[Condition],[YearBuilt],[LotSize],[SqFtAboveGrade]
						,[PricePerSqFt],[RoomsAboveGrade],[Beds],[BathsFull]
						,[BasementFinishType],[BasementFinish],[AttachedCars]
						,[CarStorageAttached],[StorageDetached],[DetachedCars],[PoolSpa],[Fireplace]
						,[Comments],[SalePrice],[SaleDate],[SrcFile])
	AS
		(
		SELECT [Loan]
					   ,[InspectionDate]
					   ,'S'
					   ,1
					   ,[Sale1Address]
					   ,[dbo].[fn_SplitAddress]([Sale1CityStateZip],'A')
					   ,[dbo].[fn_SplitAddress]([Sale1CityStateZip],'S')
					   ,[dbo].[fn_SplitAddress]([Sale1CityStateZip],'Z')
					   ,NULL as 'Sale1County'
					   ,[Sale1DistanceToSubject]
					   ,[Sale1LastListPrice]
					   ,[Sale1Dom]
					   ,[Sale1NumOfUnits]
					   ,[Sale1PropertyType]
					   ,[Sale1PropertyStyle]
					   ,[Sale1Condition]
					   ,[Sale1YearBuilt]
					   ,[Sale1LotSize]
					   ,[Sale1SqFtAboveGrade]
					   ,[Sale1PricePerSqFt]
					   ,[Sale1RoomsAboveGrade]
					   ,[Sale1Beds]
					   ,[Sale1BathsFull]
					   ,[Sale1BasementFinishType]
					   ,[Sale1BasementFinish]
					   ,[Sale1AttachedCars]
					   ,[Sale1CarStorageAttached]
					   ,[Sale1StorageDetached]
					   ,[Sale1DetachedCars]
					   ,[Sale1PoolSpa]
					   ,[Sale1Fireplace]
					   ,[Sale1Comments]
					   ,[Sale1SalePrice]
					   ,[Sale1SaleDate]
					   ,[SrcFile]
			FROM [Staging].[Summit]
			
			UNION

			SELECT [Loan]
					   ,[InspectionDate]
					   ,'S'
					   ,2
					   ,[Sale2Address]
					   ,[dbo].[fn_SplitAddress]([Sale2CityStateZip],'A')
					   ,[dbo].[fn_SplitAddress]([Sale2CityStateZip],'S')
					   ,[dbo].[fn_SplitAddress]([Sale2CityStateZip],'Z')
					   ,NULL as 'Sale2County'
					   ,[Sale2DistanceToSubject]
					   ,[Sale2LastListPrice]
					   ,[Sale2Dom]
					   ,[Sale2NumOfUnits]
					   ,[Sale2PropertyType]
					   ,[Sale2PropertyStyle]
					   ,[Sale2Condition]
					   ,[Sale2YearBuilt]
					   ,[Sale2LotSize]
					   ,[Sale2SqFtAboveGrade]
					   ,[Sale2PricePerSqFt]
					   ,[Sale2RoomsAboveGrade]
					   ,[Sale2Beds]
					   ,[Sale2BathsFull]
					   ,[Sale2BasementFinishType]
					   ,[Sale2BasementFinish]
					   ,[Sale2AttachedCars]
					   ,[Sale2CarStorageAttached]
					   ,[Sale2StorageDetached]
					   ,[Sale2DetachedCars]
					   ,[Sale2PoolSpa]
					   ,[Sale2Fireplace]
					   ,[Sale2Comments]
					   ,[Sale2SalePrice]
					   ,[Sale2SaleDate]
					   ,[SrcFile]
			FROM [Staging].[Summit]

			UNION

			SELECT [Loan]
					   ,[InspectionDate]
					   ,'S'
					   ,3
					   ,[Sale3Address]
					   ,[dbo].[fn_SplitAddress]([Sale3CityStateZip],'A')
					   ,[dbo].[fn_SplitAddress]([Sale3CityStateZip],'S')
					   ,[dbo].[fn_SplitAddress]([Sale3CityStateZip],'Z')
					   ,[Sale3County]
					   ,[Sale3DistanceToSubject]
					   ,[Sale3LastListPrice]
					   ,[Sale3Dom]
					   ,[Sale3NumOfUnits]
					   ,[Sale3PropertyType]
					   ,[Sale3PropertyStyle]
					   ,[Sale3Condition]
					   ,[Sale3YearBuilt]
					   ,[Sale3LotSize]
					   ,[Sale3SqFtAboveGrade]
					   ,[Sale3PricePerSqFt]
					   ,[Sale3RoomsAboveGrade]
					   ,[Sale3Beds]
					   ,[Sale3BathsFull]
					   ,[Sale3BasementFinishType]
					   ,[Sale3BasementFinish]
					   ,[Sale3AttachedCars]
					   ,[Sale3CarStorageAttached]
					   ,[Sale3StorageDetached]
					   ,[Sale3DetachedCars]
					   ,[Sale3PoolSpa]
					   ,[Sale3Fireplace]
					   ,[Sale3Comments]
					   ,[Sale3SalePrice]
					   ,[Sale3SaleDate]
					   ,[SrcFile]
			FROM [Staging].[Summit]
		)

INSERT INTO [dbo].[SummitComps]
					   ([LoanNumber]
					   ,[InspectionDate]
					   ,[LSType]
					   ,[LSNumber]
					   ,[Address]
					   ,[City]
					   ,[State]
					   ,[Zip]
					   ,[County]
					   ,[DistanceToSubject]
					   ,[LastListPrice]
					   ,[Dom]
					   ,[NumOfUnits]
					   ,[PropertyType]
					   ,[PropertyStyle]
					   ,[Condition]
					   ,[YearBuilt]
					   ,[LotSize]
					   ,[SqFtAboveGrade]
					   ,[PricePerSqFt]
					   ,[RoomsAboveGrade]
					   ,[Beds]
					   ,[BathsFull]
					   ,[BasementFinishType]
					   ,[BasementFinish]
					   ,[AttachedCars]
					   ,[CarStorageAttached]
					   ,[StorageDetached]
					   ,[DetachedCars]
					   ,[PoolSpa]
					   ,[Fireplace]
					   ,[Comments]
					   ,[SalePrice]
					   ,[SaleDate]
					   ,[SaleDateComments]
					   ,[SrcFile]
					   )

				SELECT DISTINCT
				 CAST(S.[LoanNumber] as varchar(10))
				 ,CASE WHEN ISDATE(S.[InspectionDate]) = 1 THEN CAST(S.[InspectionDate] as date) ELSE NULL END as [InspectionDate]
				,[LSType]
				,[LSNumber]
				,CAST(S.[Address] as varchar(100))
				,CAST(S.[City] as varchar(50))
				,CAST(S.[State] as char(2))
				,CAST(S.[Zip] as varchar(10))
				,CAST(S.[County] as varchar(100))
				,CAST(S.[DistanceToSubject] as varchar(2000)) as [DistanceToSubject]
				,ISNULL(CAST([dbo].[fn_RemoveNonNumericCharacters](S.[LastListPrice],'') as money),0) as [LastListPrice]
				,ISNULL(CAST([dbo].[fn_RemoveNonNumericCharacters](S.[Dom],'') as smallint),0) as [Dom]
				,ISNULL(CAST([dbo].[fn_RemoveNonNumericCharacters](S.[NumOfUnits],'') as smallint),0) as [NumOfUnits]
				,CAST(S.[PropertyType] as varchar(20))
				,CAST(S.[PropertyStyle] as varchar(40))
				,CAST(S.[Condition] as varchar(15))
				,ISNULL(CAST([dbo].[fn_RemoveNonNumericCharacters](S.[YearBuilt],'') as smallint),0) as [YearBuilt]
				,ISNULL(CAST([dbo].[fn_CleanAcre](S.[LotSize]) as decimal(18,2)),0)
				,ISNULL(CAST([dbo].[fn_RemoveNonNumericCharacters](S.[SqFtAboveGrade],'') as int),0) as [SqFtAboveGrade]
				,ISNULL(CAST([dbo].[fn_RemoveNonNumericCharacters](S.[PricePerSqFt],'') as money),0)
				,ISNULL(CAST([dbo].[fn_RemoveNonNumericCharacters](S.[RoomsAboveGrade],'') as smallint),0) as [RoomsAboveGrade]
				,ISNULL(CAST([dbo].[fn_RemoveNonNumericCharacters](S.[Beds],'') as smallint),0) as [Bedrooms]
				,ISNULL(CAST([dbo].[fn_RemoveNonNumericCharacters](S.[BathsFull],'') as smallint),0) as [FullBaths]
				,CAST(CAST(REPLACE(ISNULL([dbo].[fn_RemoveNonNumericCharacters](S.[BasementFinishType],''),0),'.','0')  as decimal(18,0)) as int) as [BasementFinishType]
				,CAST(CAST(REPLACE(ISNULL([dbo].[fn_RemoveNonNumericCharacters](S.[BasementFinish],''),0),'.','0')  as decimal(18,0)) as int) as [BasementFinish]
				,ISNULL(CAST([dbo].[fn_RemoveNonNumericCharacters](S.[AttachedCars],'') as smallint),0) as [AttachedCars]
				,ISNULL(CAST([dbo].[fn_RemoveNonNumericCharacters](S.[CarStorageAttached],'') as bit),0)
				,ISNULL(CAST([dbo].[fn_RemoveNonNumericCharacters](S.[StorageDetached],'') as smallint),0) as [StorageDetached]
				,ISNULL(CAST([dbo].[fn_RemoveNonNumericCharacters](S.[DetachedCars],'') as smallint),0) as [StorageDetached]
				,ISNULL(CAST([dbo].[fn_RemoveNonNumericCharacters](S.[PoolSpa],'') as smallint),0) as [PoolSpa]
				,ISNULL(CAST([dbo].[fn_RemoveNonNumericCharacters](S.[Fireplace],'') as smallint),0) as [Fireplace]
				,CAST(S.[Comments] as varchar(2000))
				,ISNULL(CAST([dbo].[fn_RemoveNonNumericCharacters](S.[SalePrice],'') as money),0)
				,CASE 
						WHEN ISDATE(S.[SaleDate])=1 THEN
							CAST(S.[SaleDate] as date)
						ELSE
							CASE 
								WHEN LEN(S.[SaleDate])>10 AND ISDATE(LEFT(S.[SaleDate],LEN(LEFT(S.[SaleDate],CHARINDEX(' ', [SaleDate],1))))) = 1 THEN
									CAST(LEFT(S.[SaleDate],LEN(LEFT(S.[SaleDate],CHARINDEX(' ', [SaleDate],1)))) as date)
								END
						END
				,CASE 
						WHEN LEN(S.[SaleDate])>10  AND ISDATE(LEFT(S.[SaleDate],LEN(LEFT(S.[SaleDate],CHARINDEX(' ', [SaleDate],1))))) = 1 THEN
							RIGHT(S.[SaleDate], LEN(S.[SaleDate]) -CHARINDEX(' ', [SaleDate],1))
						END
				,[SrcFile]
	FROM CTE S
		WHERE NOT ISNULL(S.[LoanNumber],'')='' AND ISDATE(S.InspectionDate) =1
		  AND NOT EXISTS(SELECT * FROM [dbo].[SummitComps] M
										WHERE M.[LoanNumber] = CAST(S.[LoanNumber] as varchar(10))
										   AND M.[InspectionDate] = CAST(S.[InspectionDate] as date)
										               AND M.[LSType] = 'S'
										          AND M.[LSNumber] = S.[LSNumber])


END



