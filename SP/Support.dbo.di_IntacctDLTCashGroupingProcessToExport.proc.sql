USE [Support]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[di_IntacctDLTCashGroupingProcessToExport]
AS
BEGIN

/* ********************************************************************************************************* *
   Procedure Name  :	di_IntacctDLTCashGroupingProcessToExport
   Business Analyis:	
   Project/Process :   
   JIRA            :	MARS-2980
   Description     :	Run from DLT SSIS Daily Process to group the captured data into the proper import format. 
   Author          :	Benjacob Beres
   Create Date     :	05/02/2017

   *********************************************************************************************************
   **         Change History                                                                              **
   *********************************************************************************************************

   Date			Author				Jira#			Description
   --------		-----------			----------		------------

*  ********************************************************************************************************* */


---------------------------------------------------------------------------------------------------------------------------------------
--GROUPING SECTION
---------------------------------------------------------------------------------------------------------------------------------------
--Lockbox (non-ACH)
---------------------------------------------------------------------------------------------------------------------------------------

	IF OBJECT_ID('tempdb..#TempIJE') IS NOT NULL
		Drop Table #TempIJE;
	IF OBJECT_ID('tempdb..#TempIJE1') IS NOT NULL
		Drop Table #TempIJE1;
	IF OBJECT_ID('tempdb..#TempIJEBreakDown') IS NOT NULL
		Drop Table #TempIJEBreakDown;
	IF OBJECT_ID('tempdb..#TempIJECnt') IS NOT NULL
		Drop Table #TempIJECnt;

	Select	JOURNAL, 
			[DATE], 
			REVERSEDATE, 
			[DESCRIPTION], 
			REFERENCE_NO,
			ACCT_NO, 
			LOCATION_ID,
			MEMO,
			GLDIMSTATUS,
			GLENTRY_CUSTOMERID, 
			Sum(Convert(Numeric(16,2), Coalesce(Nullif(Debit,''), '0.0'))) as Debit,
			Sum(Convert(Numeric(16,2), Coalesce(Nullif(Credit,''), '0.0'))) as Credit
	Into #TempIJE
	From IntacctJournalEntries
	group by	JOURNAL, 
				[DATE], 
				REVERSEDATE, 
				[DESCRIPTION], 
				REFERENCE_NO,
				ACCT_NO, 
				LOCATION_ID,
				MEMO,
				GLDIMSTATUS,
				GLENTRY_CUSTOMERID


	select	JOURNAL, 
			[DATE], 
			REVERSEDATE, 
			[DESCRIPTION], 
			REFERENCE_NO,
			ACCT_NO, 
			LOCATION_ID,
			MEMO,
			GLDIMSTATUS,
			GLENTRY_CUSTOMERID,
			CASE WHEN Debit >= Credit THEN Debit - Credit ELSE 0.0 END AS Debit,
			CASE WHEN Credit >= Debit THEN Credit - Debit ELSE 0.0 END AS Credit
	into #TempIJE1
	from #TempIJE


	DELETE 
	FROM #TempIJE1 
	WHERE	Debit = 0 
		AND	Credit = 0

	
	select	DESCRIPTION,
			GLENTRY_CUSTOMERID, 
			LOCATION_ID,
			SUM(CASE WHEN Debit > 0 THEN 1 ELSE 0 END) AS DebitCnt,
			SUM(CASE WHEN Credit > 0 THEN 1 ELSE 0 END) AS CreditCnt
	into #TempIJECnt
	from #TempIJE1
	group by DESCRIPTION,GLENTRY_CUSTOMERID, LOCATION_ID


	select	t2.JOURNAL, 
			t2.[DATE], 
			t2.REVERSEDATE, 
			t2.[DESCRIPTION], 
			t2.REFERENCE_NO,
			t2.ACCT_NO, 
			t2.LOCATION_ID,
			t2.MEMO,
			t2.GLDIMSTATUS,
			t2.GLENTRY_CUSTOMERID,
			0.00 as Debit,
			t1.Debit AS Credit
	into #TempIJEBreakDown
	from #TempIJE1 t1
	join #TempIJE1 t2
		on	t1.GLENTRY_CUSTOMERID = t2.GLENTRY_CUSTOMERID
		and t1.LOCATION_ID = t2.LOCATION_ID
		and t1.DESCRIPTION = t2.DESCRIPTION
		and t2.Debit = 0
	join #TempIJECnt tc
		on	tc.GLENTRY_CUSTOMERID = t1.GLENTRY_CUSTOMERID
		and tc.LOCATION_ID = t1.LOCATION_ID
		and tc.DESCRIPTION = t1.DESCRIPTION
	where tc.DebitCnt > 1
		and tc.CreditCnt = 1
		and t1.Debit > 0

	UNION ALL
	
	select	t1.JOURNAL, 
			t1.[DATE], 
			t1.REVERSEDATE, 
			t1.[DESCRIPTION], 
			t1.REFERENCE_NO,
			t1.ACCT_NO, 
			t1.LOCATION_ID,
			t1.MEMO,
			t1.GLDIMSTATUS,
			t1.GLENTRY_CUSTOMERID,
			t1.Debit AS Debit,
			0.00 as Credit
	from #TempIJE1 t1
	join #TempIJECnt tc
		on	tc.GLENTRY_CUSTOMERID = t1.GLENTRY_CUSTOMERID
		and tc.LOCATION_ID = t1.LOCATION_ID
		and tc.DESCRIPTION = t1.DESCRIPTION
	where tc.DebitCnt > 1
		and tc.CreditCnt = 1
		and t1.Debit > 0

	UNION ALL

	select	t2.JOURNAL, 
			t2.[DATE], 
			t2.REVERSEDATE, 
			t2.[DESCRIPTION], 
			t2.REFERENCE_NO,
			t2.ACCT_NO, 
			t2.LOCATION_ID,
			t2.MEMO,
			t2.GLDIMSTATUS,
			t2.GLENTRY_CUSTOMERID,
			t1.Credit as Debit,
			0.0 AS Credit
	from #TempIJE1 t1
	join #TempIJE1 t2
		on	t1.GLENTRY_CUSTOMERID = t2.GLENTRY_CUSTOMERID
		and t1.LOCATION_ID = t2.LOCATION_ID
		and t1.DESCRIPTION = t2.DESCRIPTION
		and t2.Credit = 0
	join #TempIJECnt tc
		on	tc.GLENTRY_CUSTOMERID = t1.GLENTRY_CUSTOMERID
		and tc.LOCATION_ID = t1.LOCATION_ID
		and tc.DESCRIPTION = t1.DESCRIPTION
	where tc.DebitCnt = 1
		and tc.CreditCnt > 1
		and t1.Credit > 0

	UNION ALL
	
	select	t1.JOURNAL, 
			t1.[DATE], 
			t1.REVERSEDATE, 
			t1.[DESCRIPTION], 
			t1.REFERENCE_NO,
			t1.ACCT_NO, 
			t1.LOCATION_ID,
			t1.MEMO,
			t1.GLDIMSTATUS,
			t1.GLENTRY_CUSTOMERID,
			0.00 as Debit,
			t1.Credit AS Credit
	from #TempIJE1 t1
	join #TempIJECnt tc
		on	tc.GLENTRY_CUSTOMERID = t1.GLENTRY_CUSTOMERID
		and tc.LOCATION_ID = t1.LOCATION_ID
		and tc.DESCRIPTION = t1.DESCRIPTION
	where tc.DebitCnt = 1
		and tc.CreditCnt > 1
		and t1.Credit > 0

	UNION ALL
	
	select	t1.JOURNAL, 
			t1.[DATE], 
			t1.REVERSEDATE, 
			t1.[DESCRIPTION], 
			t1.REFERENCE_NO,
			t1.ACCT_NO, 
			t1.LOCATION_ID,
			t1.MEMO,
			t1.GLDIMSTATUS,
			t1.GLENTRY_CUSTOMERID,
			t1.Debit,
			t1.Credit
	from #TempIJE1 t1
	join #TempIJECnt tc
		on	tc.GLENTRY_CUSTOMERID = t1.GLENTRY_CUSTOMERID
		and tc.LOCATION_ID = t1.LOCATION_ID
		and tc.DESCRIPTION = t1.DESCRIPTION
	where tc.DebitCnt = 1
		and tc.CreditCnt = 1

	TRUNCATE TABLE IntacctJournalEntries;

	INSERT INTO IntacctJournalEntries (JOURNAL, [DATE], REVERSEDATE, [DESCRIPTION], REFERENCE_NO,LINE_NO
										,	ACCT_NO, LOCATION_ID , MEMO, DEBIT, CREDIT, GLDIMSTATUS, GLENTRY_CUSTOMERID)
	SELECT	JOURNAL, [DATE], REVERSEDATE, [DESCRIPTION], REFERENCE_NO
		,	ROW_NUMBER() OVER (PARTITION BY LOCATION_ID, [DESCRIPTION] ORDER BY GLENTRY_CUSTOMERID, DESCRIPTION, Debit+Credit, Debit)
		,	ACCT_NO, LOCATION_ID
		,	MEMO
		,	ISNULL(CAST(CASE WHEN DEBIT = 0 THEN NULL ELSE DEBIT END AS VARCHAR(19)),'') AS DEBIT
		,	ISNULL(CAST(CASE WHEN CREDIT = 0 THEN NULL ELSE CREDIT END AS VARCHAR(19)),'') AS CREDIT
		, GLDIMSTATUS
		, GLENTRY_CUSTOMERID
	FROM #TempIJEBreakDown
	ORDER BY DESCRIPTION, LOCATION_ID, GLENTRY_CUSTOMERID, Debit+Credit, Debit

	
---------------------------------------------------------------------------------------------------------------------------------------
--ACH
---------------------------------------------------------------------------------------------------------------------------------------

	IF OBJECT_ID('tempdb..#TempAJE') IS NOT NULL
		Drop Table #TempAJE;
	IF OBJECT_ID('tempdb..#TempAJE1') IS NOT NULL
		Drop Table #TempAJE1;
	IF OBJECT_ID('tempdb..#TempAJEBreakDown') IS NOT NULL
		Drop Table #TempAJEBreakDown;
	IF OBJECT_ID('tempdb..#TempAJECnt') IS NOT NULL
		Drop Table #TempAJECnt;


	Select	JOURNAL, 
			[DATE], 
			REVERSEDATE, 
			[DESCRIPTION], 
			REFERENCE_NO,
			ACCT_NO, 
			LOCATION_ID,
			MEMO,
			GLDIMSTATUS,
			GLENTRY_CUSTOMERID, 
			Sum(Convert(Numeric(16,2), Coalesce(Nullif(Debit,''), '0.0'))) as Debit,
			Sum(Convert(Numeric(16,2), Coalesce(Nullif(Credit,''), '0.0'))) as Credit
	Into #TempAJE
	From IntacctJournalEntriesACH
	group by	JOURNAL, 
				[DATE], 
				REVERSEDATE, 
				[DESCRIPTION], 
				REFERENCE_NO,
				ACCT_NO, 
				LOCATION_ID,
				MEMO,
				GLDIMSTATUS,
				GLENTRY_CUSTOMERID


	select	JOURNAL, 
			[DATE], 
			REVERSEDATE, 
			[DESCRIPTION], 
			REFERENCE_NO,
			ACCT_NO, 
			LOCATION_ID,
			MEMO,
			GLDIMSTATUS,
			GLENTRY_CUSTOMERID,
			CASE WHEN Debit >= Credit THEN Debit - Credit ELSE 0.0 END AS Debit,
			CASE WHEN Credit >= Debit THEN Credit - Debit ELSE 0.0 END AS Credit
	into #TempAJE1
	from #TempAJE

	DELETE 
	FROM #TempAJE1 
	WHERE	Debit = 0 
		AND	Credit = 0

	
	select	DESCRIPTION,
			GLENTRY_CUSTOMERID,
			LOCATION_ID,
			SUM(CASE WHEN Debit > 0 THEN 1 ELSE 0 END) AS DebitCnt,
			SUM(CASE WHEN Credit > 0 THEN 1 ELSE 0 END) AS CreditCnt
	into #TempAJECnt
	from #TempAJE1
	group by DESCRIPTION, GLENTRY_CUSTOMERID, LOCATION_ID


	select	t2.JOURNAL, 
			t2.[DATE], 
			t2.REVERSEDATE, 
			t2.[DESCRIPTION], 
			t2.REFERENCE_NO,
			t2.ACCT_NO, 
			t2.LOCATION_ID,
			t2.MEMO,
			t2.GLDIMSTATUS,
			t2.GLENTRY_CUSTOMERID,
			0.00 as Debit,
			t1.Debit AS Credit
	into #TempAJEBreakDown
	from #TempAJE1 t1
	join #TempAJE1 t2
		on	t1.GLENTRY_CUSTOMERID = t2.GLENTRY_CUSTOMERID
		and t1.LOCATION_ID = t2.LOCATION_ID
		and t1.DESCRIPTION = t2.DESCRIPTION
		and t2.Debit = 0
	join #TempAJECnt tc
		on	tc.GLENTRY_CUSTOMERID = t1.GLENTRY_CUSTOMERID
		and tc.LOCATION_ID = t1.LOCATION_ID
		and tc.DESCRIPTION = t1.DESCRIPTION
	where tc.DebitCnt > 1
		and tc.CreditCnt = 1
		and t1.Debit > 0

	UNION ALL
	
	select	t1.JOURNAL, 
			t1.[DATE], 
			t1.REVERSEDATE, 
			t1.[DESCRIPTION], 
			t1.REFERENCE_NO,
			t1.ACCT_NO, 
			t1.LOCATION_ID,
			t1.MEMO,
			t1.GLDIMSTATUS,
			t1.GLENTRY_CUSTOMERID,
			t1.Debit AS Debit,
			0.00 as Credit
	from #TempAJE1 t1
	join #TempAJECnt tc
		on	tc.GLENTRY_CUSTOMERID = t1.GLENTRY_CUSTOMERID
		and tc.LOCATION_ID = t1.LOCATION_ID
		and tc.DESCRIPTION = t1.DESCRIPTION
	where tc.DebitCnt > 1
		and tc.CreditCnt = 1
		and t1.Debit > 0

	UNION ALL

	select	t2.JOURNAL, 
			t2.[DATE], 
			t2.REVERSEDATE, 
			t2.[DESCRIPTION], 
			t2.REFERENCE_NO,
			t2.ACCT_NO, 
			t2.LOCATION_ID,
			t2.MEMO,
			t2.GLDIMSTATUS,
			t2.GLENTRY_CUSTOMERID,
			t1.Credit as Debit,
			0.0 AS Credit
	from #TempAJE1 t1
	join #TempAJE1 t2
		on	t1.GLENTRY_CUSTOMERID = t2.GLENTRY_CUSTOMERID
		and t1.LOCATION_ID = t2.LOCATION_ID
		and t1.DESCRIPTION = t2.DESCRIPTION
		and t2.Credit = 0
	join #TempAJECnt tc
		on	tc.GLENTRY_CUSTOMERID = t1.GLENTRY_CUSTOMERID
		and tc.LOCATION_ID = t1.LOCATION_ID
		and tc.DESCRIPTION = t1.DESCRIPTION
	where tc.DebitCnt = 1
		and tc.CreditCnt > 1
		and t1.Credit > 0

	UNION ALL
	
	select	t1.JOURNAL, 
			t1.[DATE], 
			t1.REVERSEDATE, 
			t1.[DESCRIPTION], 
			t1.REFERENCE_NO,
			t1.ACCT_NO, 
			t1.LOCATION_ID,
			t1.MEMO,
			t1.GLDIMSTATUS,
			t1.GLENTRY_CUSTOMERID,
			0.00 as Debit,
			t1.Credit AS Credit
	from #TempAJE1 t1
	join #TempAJECnt tc
		on	tc.GLENTRY_CUSTOMERID = t1.GLENTRY_CUSTOMERID
		and tc.LOCATION_ID = t1.LOCATION_ID
		and tc.DESCRIPTION = t1.DESCRIPTION
	where tc.DebitCnt = 1
		and tc.CreditCnt > 1
		and t1.Credit > 0

	UNION ALL
	
	select	t1.JOURNAL, 
			t1.[DATE], 
			t1.REVERSEDATE, 
			t1.[DESCRIPTION], 
			t1.REFERENCE_NO,
			t1.ACCT_NO, 
			t1.LOCATION_ID,
			t1.MEMO,
			t1.GLDIMSTATUS,
			t1.GLENTRY_CUSTOMERID,
			t1.Debit,
			t1.Credit
	from #TempAJE1 t1
	join #TempAJECnt tc
		on	tc.GLENTRY_CUSTOMERID = t1.GLENTRY_CUSTOMERID
		and tc.LOCATION_ID = t1.LOCATION_ID
		and tc.DESCRIPTION = t1.DESCRIPTION
	where tc.DebitCnt = 1
		and tc.CreditCnt = 1

	TRUNCATE TABLE IntacctJournalEntriesACH;

	INSERT INTO IntacctJournalEntriesACH (JOURNAL, [DATE], REVERSEDATE, [DESCRIPTION], REFERENCE_NO,LINE_NO
										,	ACCT_NO, LOCATION_ID , MEMO, DEBIT, CREDIT, GLDIMSTATUS, GLENTRY_CUSTOMERID)
	SELECT	JOURNAL, [DATE], REVERSEDATE, [DESCRIPTION], REFERENCE_NO
		,	ROW_NUMBER() OVER (PARTITION BY LOCATION_ID, [DESCRIPTION] ORDER BY DESCRIPTION, LOCATION_ID, GLENTRY_CUSTOMERID, Debit+Credit, Debit)
		,	ACCT_NO, LOCATION_ID
		,	MEMO
		,	ISNULL(CAST(CASE WHEN DEBIT = 0 THEN NULL ELSE DEBIT END AS VARCHAR(19)),'') AS DEBIT
		,	ISNULL(CAST(CASE WHEN CREDIT = 0 THEN NULL ELSE CREDIT END AS VARCHAR(19)),'') AS CREDIT
		, GLDIMSTATUS
		, GLENTRY_CUSTOMERID
	FROM #TempAJEBreakDown
	ORDER BY DESCRIPTION, LOCATION_ID, GLENTRY_CUSTOMERID, Debit+Credit, Debit



END

GO


