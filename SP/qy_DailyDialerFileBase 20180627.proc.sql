USE [MARS]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('dbo.qy_DailyDialerFileBase') IS NOT NULL
	DROP PROCEDURE dbo.qy_DailyDialerFileBase;
GO


CREATE PROCEDURE [dbo].[qy_DailyDialerFileBase] 
			(	@AssignedAgent varchar(128) = NULL
			)
AS
BEGIN

/* ********************************************************************************************************* *
   Procedure Name  :	qy_DailyDialerFileBase
   Business Analyis:	
   Project/Process :   
   Description     :	Collection Queue Strategy Project
   Author          :	Nathan Reed
   Create Date     :	

   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date			Author			Version    	Description
   --------		-----------		---------	------------
   02/27/2017	Benjacob Beres		        Reformatted the existing code FROM Nathan
   03/01/2017	Benjacob Beres		        Removed unnecesary joins and renamed sp from GetDailyDialerFile to fit
										      Aspen sp standards
   04/06/2017	Nancy Tanaka		        Exclude loans assigned to the following agents as these are in Loss Mitigation 
										    and should not be called by Collections:
										    � Angela Shephard
										    � Giovanni Lopez
										    � Jeff Pugh
										    � Michael Hager
										    � Brandy Casey
										    � Kendra Roberts		
	01/04/2018	Tarun Tuteja   1.00.002    Removed Loan Status FBA from Logic against JIRA(MARS-3004)
	04/04/2018	Tarun Tuteja   1.00.003    Dialer Report need to be ignore the TCPA Flag(MARS-3380). Changes are mainly done to PrimaryPhone fields on how information needs to be displayed.
								           Logic for joining with CoBorrowers has been moved to temp table.
*  ********************************************************************************************************* */

	DECLARE   @Application	Varchar(128) = 'Reporting'
			, @Version		Varchar(10) = '1.00.003'
			, @User			Varchar(128) = NULL
						
	If @User IS NULL 
	Set @User = SUSER_NAME ();

	Select @Application = ApplicationName from Applications..Applications Where ApplicationName = @Application
	
	BEGIN TRY

		IF OBJECT_ID('tempdb..#ActiveDelq') IS NOT NULL DROP TABLE #ActiveDelq
		IF OBJECT_ID('tempdb..#NoContact') IS NOT NULL DROP TABLE #NoContact
		IF OBJECT_ID('tempdb..#lastContactDate') IS NOT NULL DROP TABLE #lastContactDate
		IF OBJECT_ID('tempdb..#noCon2') IS NOT NULL DROP TABLE #noCon2
		IF OBJECT_ID('tempdb..#lastcontactInfo') IS NOT NULL DROP TABLE #lastcontactInfo
		IF OBJECT_ID('tempdb..#LastContactAndAttemptDate') IS NOT NULL DROP TABLE #LastContactAndAttemptDate
		IF OBJECT_ID('tempdb..#report') IS NOT NULL DROP TABLE #report
		IF OBJECT_ID('tempdb..#dialerfile') IS NOT NULL DROP TABLE #dialerfile
		IF OBJECT_ID('tempdb..#CoBorrowers') IS NOT NULL DROP TABLE #CoBorrowers

		--------------------------------------------------------------------------------------
		Select * into #CoBorrowers
			from 
			(Select *, ROW_NUMBER() Over ( Partition by LoanRecID Order by SortName) as Seq
			from TMO_AspenYO..[TDS CoBorrowers]
			) T
			Where T.seq = 1
		
		SELECT	l.RecID
			,	DATEDIFF(D,l.NextDueDate,GETDATE()) AS dayspastdue
		INTO	#ActiveDelq 
		FROM	mars.dbo.vw_loans l WITH(NOLOCK)
			LEFT JOIN
					(	SELECT	cf.Account
							,	cf.FieldValue AS BKDischarge 
						FROM	dbo.vw_CustomFieldsForLoans cf
						WHERE	cf.FieldName ='BK - Debt Discharged'
					) discharge
				ON	discharge.Account = l.Account
		WHERE	l.LoanStatus NOT IN (
									'BK11',
									'BK11,FC',
									'BK12',
									'BK13',
									'BK13, DL',
									'BK13,FC',
									'BK7',
									'BK7, DL',
									'BK7,FC',
									'FC,DIL',
									'Inactive - REO/FC',
									'PRELIM',
									'Service Xfer',
									'Trailing Claims',
									'FC',
									'Paid in Full',
									'Paidoff', 
									'DIL'
									--,'FBA'
									)
			AND	l.LoanStatus NOT LIKE '%Inactive%'
			AND l.LoanStatus NOT LIKE 'BK13%'
			AND DATEDIFF(D,l.NextDueDate,GETDATE()) > 15
			AND discharge.BKDischarge IS NULL
			AND l.isActive = 1


		--------------------------------------------------------------------------------------
		SELECT	a.RecID
			,	a.dayspastdue
			,	isnull(MAX(MyDateTime),'01-01-1900') AS LastContactDate
		INTO	#LastContactDate
		FROM	#ActiveDelq a
			LEFT JOIN
					(	SELECT	vlc.MyDateTime
							,	vlc.LoanRecID
							,	vlc.Rep
						FROM	mars.dbo.vw_LoansContacts vlc WITH(NOLOCK) 
						WHERE	vlc.Category IN (	'AI - Attorney Contact on Inbound Call',
													'BI - Borrower Contact on Inbound Call',
													'BO - Borrower Contact on Outbound Call',
													'TC - Authorized 3rd Party on Inbound',
													'TO - Authorized 3rd Party on Outbound Call'
												) 
					) b
				ON	a.RecID = b.LoanRecID
				AND LEFT(b.Rep, 1) <> '*'
		GROUP BY	a.RecID
				,	a.dayspastdue



		--------------------------------------------------------------------------------------
		SELECT	lcd.RecID
			,	lcd.dayspastdue
			,	lcd.LastContactDate
			,	isnull(MAX(lc.MyDateTime),'01-01-1900') LastAttemptDate
		INTO	#LastContactAndAttemptDate
		FROM	#LastContactDate lcd
			LEFT JOIN	(	SELECT	vlc.MyDateTime
								,	vlc.LoanRecID
								,	vlc.Rep
							FROM mars.dbo.vw_LoansContacts vlc WITH(NOLOCK) 
							WHERE vlc.Category in ( 'AA - Outbound Attempt/no Contact with Attorney',
													'NO - Outbound Attempt/no Contact with Borrower',
													'TA - Outbound Attempt/no Contact with Authorized NO3',
													'AO - Attorney Contact on Outbound Call'
												  ) 
						) lc
				ON	lcd.RecID = lc.LoanRecID
				AND LEFT(lc.Rep, 1) <> '*'
		GROUP BY	lcd.RecID
				,	lcd.dayspastdue
				,	lcd.LastContactDate


		--------------------------------------------------------------------------------------
		SELECT	GETDATE() AS RunDate,
				ROW_NUMBER() OVER(ORDER BY 
										CASE 
											WHEN lp.[state] = 'NY' THEN 0
											ELSE 1000
										END,
										CASE
											WHEN jud.judicial = 'M' THEN 0
											ELSE 1000
										END, 
										CASE 
											WHEN valuation.newstatus = '24for24' THEN -1  
											WHEN valuation.newstatus = '12for12' THEN 0  
											WHEN valuation.newstatus = '7for7' THEN 1
											WHEN valuation.newstatus = '4f4-6f6' THEN 2 
											WHEN valuation.newstatus in('other > 600','other < 600') THEN 3
											WHEN valuation.newstatus in('NPL') THEN 4  
										ELSE 1000 END,  
											dayspastdue DESC,
											c.userlogon, 
											lastcontactdate , 
											LastAttemptDate  
								  ) queuenumber,
				1 as cntr,
				b.Account,
				b.LoanStatus,
				lp.[state],
				jud.judicial,
				valuation.NewStatus,
				c.UserLogon AS AssignedAgent,
				0 AS TAD,
				ISNULL(escrow.EscrowBalance,0) CurrentTrustBalance,
				CASE WHEN b.nextduedate = '01-01-1900' THEN '' ELSE CONVERT(VARCHAR(10),b.NextDueDate,101) END AS NextDueDate,
				a.dayspastdue,
				CASE WHEN a.LastContactDate = '01-01-1900' THEN '' ELSE CONVERT(VARCHAR(10),a.LastContactDate,101) END AS LastContactDate,
				ISNULL(lastcontact.Category,'') AS LastContactType,
				b.PmtPI+b.PmtImpound MonthlyPayment,
				ISNULL(BKDIS.FieldValue,'') AS [BK - Debt Discharged],
				dnc.ReasonForRestrictCommunication AS DNCDetail,
				CASE WHEN b.HomeSELECT = 1      THEN ISNULL(dbo.fn_RemoveNonNumericCharacters(b.home)        ,'') ELSE '' END AS DialerHome,
				CASE WHEN b.WorkSELECT = 1      THEN ISNULL(dbo.fn_RemoveNonNumericCharacters(b.Work)	      ,'') ELSE '' END AS DialerWork,
				CASE WHEN b.MobileSELECT = 1    THEN ISNULL(dbo.fn_RemoveNonNumericCharacters(b.Mobil)		  ,'') ELSE '' END AS DialerMobile,
				CASE WHEN b.PhoneHomeSELECT = 1 THEN ISNULL(dbo.fn_RemoveNonNumericCharacters(cobo.PhoneHome),'') ELSE '' END AS DialerCoBorrowerHome,
				CASE WHEN b.PhoneWorkSELECT = 1 THEN ISNULL(dbo.fn_RemoveNonNumericCharacters(cobo.PhoneWork),'') ELSE '' END AS DialerCoBorrowerWork,
				CASE WHEN b.PhoneCellSELECT = 1 THEN ISNULL(dbo.fn_RemoveNonNumericCharacters(cobo.PhoneCell),'') ELSE '' END AS DialerCoBorrowerMobile,
				ISNULL(dbo.fn_RemoveNonNumericCharacters(b.home)        ,'') Home,
				ISNULL(dbo.fn_RemoveNonNumericCharacters(b.Work)	    ,'') Work,
				ISNULL(dbo.fn_RemoveNonNumericCharacters(b.Mobil)		,'') Mobile,
				ISNULL(dbo.fn_RemoveNonNumericCharacters(cobo.PhoneHome),'') CoBorrowerHome,
				ISNULL(dbo.fn_RemoveNonNumericCharacters(cobo.PhoneWork),'') CoBorrowerWork,
				ISNULL(dbo.fn_RemoveNonNumericCharacters(cobo.PhoneCell),'') CoBorrowerMobile,
				COALESCE(NULLIF(CAST(dbo.fn_RemoveNonNumericCharacters(b.Mobil) AS BIGINT) , ''),
						 NULLIF(CAST(dbo.fn_RemoveNonNumericCharacters(b.home) AS BIGINT) , ''), 
						 NULLIF(CAST(dbo.fn_RemoveNonNumericCharacters(cobo.PhoneCell) AS BIGINT) , ''), 
						 NULLIF(CAST(dbo.fn_RemoveNonNumericCharacters(cobo.PhoneHome) AS BIGINT) ,' '), 
						 NULLIF(CAST(dbo.fn_RemoveNonNumericCharacters(b.Work) AS BIGINT), ''), 
						 NULLIF(CAST(dbo.fn_RemoveNonNumericCharacters(cobo.PhoneWork) AS BIGINT) ,'')) AS PrimaryPhone,
				CASE 
					WHEN b.HomeSELECT = 1     THEN 1
					WHEN b.WorkSELECT = 1  THEN 1 
					WHEN b.MobileSELECT = 1 THEN 1
					WHEN b.PhoneHomeSELECT = 1 THEN 1
					WHEN b.PhoneWorkSELECT = 1 THEN 1
					WHEN b.PhoneCellSELECT = 1 THEN 1
					ELSE 0
				END [TCPADialable]
		FROM	#LastContactAndAttemptDate a
			LEFT JOIN	mars.dbo.vw_loans b WITH(NOLOCK)
				ON	a.RecID = b.RecID
			LEFT JOIN #CoBorrowers cobo WITH(NOLOCK)
				ON	A.RecID = cobo.LoanRecID
			LEFT JOIN	mars.dbo.vw_ActiveLoanRepAssignments  c  WITH(NOLOCK)
				ON	b.Account = c.LoanNumber
			LEFT JOIN
					(	SELECT	vcf.Account
							,	vcf.FieldValue 
						FROM	MARS.dbo.vw_CustomFieldsForLoans  vcf WITH(NOLOCK)
						WHERE	vcf.FieldName LIKE 'BK - Debt Discharged'
					) BKDIS
				ON	b.Account = BKDIS.Account
			LEFT JOIN
					(	SELECT	loanrecid
							,	category
							,	MAX(mydatetime) LastContactD 
						FROM	MARS.dbo.vw_LoansContacts  WITH(NOLOCK)
						WHERE	Category IN	(	'AI - Attorney Contact on Inbound Call',
												'BI - Borrower Contact on Inbound Call',
												'BO - Borrower Contact on Outbound Call',
												'TC - Authorized 3rd Party on Inbound',
												'TO - Authorized 3rd Party on Outbound Call'
											) 
						GROUP BY LoanRecID, Category 
					) LastContact
				ON	LastContact.LastContactD = a.LastContactDate AND lastcontact.LoanRecID = a.RecID
			LEFT JOIN	mars.dbo.vw_GetLoans  DNC WITH(NOLOCK)
				ON	a.RecID = DNC.LoanRecID
			LEFT JOIN
					(	SELECT DISTINCT 
								LoanRecID
							,	P2PDate 
						FROM   mars.dbo.vw_EIReport  WITH(NOLOCK) Where P2PDate is not null
					) P2P
				ON	a.RecID = P2P.LoanRecID
			LEFT JOIN
					(	SELECT	* 
						FROM	dbo.vw_ACHBillingsEnteredByLoanByRep  achb WITH(NOLOCK) 
						WHERE	achb.isActive = 1
					) ACHbyRep
				ON	b.Account = ACHbyRep.LoanNumber
			LEFT JOIN	MARS.[dbo].[ValuationPaymentHistory] valuation WITH(NOLOCK)
				ON	b.account = valuation.LoanNumber
			LEFT JOIN	MARS.dbo.vw_LoanLastPaidDate ldp WITH(NOLOCK)
				ON	b.Account = ldp.LoanNumber
			LEFT JOIN
					(	SELECT	loanrecid
							,	MAX(mydatetime) LastUnableToDialDate 
						FROM	mars.dbo.vw_LoansContacts WITH(NOLOCK)
						WHERE	category = 'DN � Unable to Dial'
						GROUP BY LoanRecID
					) UATD
				ON	a.RecID = uatd.LoanRecID
			LEFT JOIN
					(	SELECT	vp.LoanRecID
							,	vp.[state] 
						FROM  MARS.dbo.vw_Properties vp WITH(NOLOCK)
						WHERE vp.[Primary] = 1
					) lp
				ON	a.RecID = lp.LoanRecID
			LEFT JOIN	support.dbo.JudicialStates jud WITH(NOLOCK)
				ON	lp.[State] = jud.[state]
			LEFT JOIN	mars.dbo.vw_EscrowBalanceByLoan escrow WITH(NOLOCK)
				ON	b.account = escrow.Account
		WHERE	DATEDIFF(d,a.LastContactDate,GETDATE()) > 5
			AND	DATEDIFF(d,a.LastAttemptDate,GETDATE()) > 2 
			AND	dnc.ReasonForRestrictCommunication not like '%REPRESENTED%' 
			AND	dnc.ReasonForRestrictCommunication  not like '%DO NOT CALL%' 
			AND	dnc.ReasonForRestrictCommunication  not like '%LAWSUIT%' 
			AND	dnc.ReasonForRestrictCommunication  not like '%COUNSEL%'   
			AND	dnc.ReasonForRestrictCommunication  not like '%CEASE AND DESIST%'   
			AND	CAST(ISNULL(P2P.P2PDate,'01-01-1900') AS DATE) <= CAST(GETDATE() AS DATE) 
			AND	CAST(GETDATE() AS DATE)> CAST(isnull(CASE WHEN ACHbyRep.[Scheduled End Date] = '01-01-9999' THEN NULL ELSE ACHbyRep.[Scheduled End Date] END,'01-01-1900') AS DATE)
			AND	CASE 
					WHEN CAST(ldp.LastPaidDate AS DATE) >= CAST(GETDATE() - 25 AS DATE) THEN 1 
					ELSE 0
				END = 0 
			AND	ISNULL(CAST(UATD.LastUnableToDialDate AS DATE),'01-01-2050') > CAST(GETDATE()-7 AS DATE)
			AND	c.userlogon NOT IN ('Angela.Shephard','Giovanni.Lopez','Jeff.Pugh','Michael.Hager','Brandy.Casey','Kendra.Roberts')
			AND c.userlogon = ISNULL(@AssignedAgent, c.userlogon)
	
	END TRY

BEGIN CATCH

DECLARE @Err           INT
	,	@ErrorMessage      Varchar(Max)
	,	@ErrorLine         Varchar(128)
	,	@Workstation       VarChar(128)
	,	@Proc              VarChar(128)

	--Uncomment the below if there are any BEGIN TRANS in the above stored proc
	-- IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION ;

	IF Error_Number() IS NULL 
		SET @Err =0;
	Else
		SET @Err = Error_Number();

	SET @ErrorMessage = Error_Message()
	SET @ErrorLine    = 'SP Line Number: ' + Cast(Error_Line() as varchar(10))
	SET @Workstation  = HOST_NAME()
	SET @Proc         = OBJECT_NAME(@@ProcID)

	EXEC Applications.dbo.di_ErrorLog	@Application ,@Version ,@Err, @ErrorMessage, @Proc, @ErrorLine, @User , @Workstation

END CATCH   

END
	





GO


