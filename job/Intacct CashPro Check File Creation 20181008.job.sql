USE [msdb]
GO

/****** Object:  Job [Intacct CashPro Check File Creation]    Script Date: 10/8/2018 9:52:31 AM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 10/8/2018 9:52:31 AM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Intacct CashPro Check File Creation', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'FLANDERSCAPITAL\saSSIS', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Send Email Notifying User of Start]    Script Date: 10/8/2018 9:52:32 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Send Email Notifying User of Start', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @Recipients VARCHAR(2000)
	,	@Subject VARCHAR(200)
	,	@Message VARCHAR(MAX)
	,	@CheckCount VARCHAR(10)


SET @Recipients = ''Shannon.Engels@gregoryfunding.com''
SET @Subject = ''Begining Process for Intacct Check Creation for BoA - '' + CONVERT(VARCHAR(10), GETDATE(), 102)

SET @Message = ''The process has started to create the file for BoA upload and write check numbers to Intacct.''



EXEC Boarding.msdb.dbo.sp_send_dbmail                               
                    @profile_name = ''BI''
                    ,@Recipients   = @Recipients
	    ,@copy_recipients = ''benjacob.beres@aspencapital.com''
                    ,@subject      = @Subject 
                    ,@body         = @Message   
	   ,@from_address = ''BIReporting@aspencapital.com''
	   ,@reply_to     = ''benjacob.beres@aspencapital.com''', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [SSIS - Sync APBILL]    Script Date: 10/8/2018 9:52:32 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'SSIS - Sync APBILL', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=13, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\"S:\SSISPackages\ESP\Intacct Sync BoA APBILL.dtsx\"" /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@flags=0, 
		@proxy_name=N'saSSIS'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [SSIS - Sync APBILLITEM]    Script Date: 10/8/2018 9:52:32 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'SSIS - Sync APBILLITEM', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=13, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\"S:\SSISPackages\ESP\Intacct Sync BoA APBILLITEM.dtsx\"" /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@flags=0, 
		@proxy_name=N'saSSIS'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [SSIS - Sync APPYMT]    Script Date: 10/8/2018 9:52:32 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'SSIS - Sync APPYMT', 
		@step_id=4, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=13, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\"S:\SSISPackages\ESP\Intacct Sync BoA APPYMT.dtsx\"" /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@flags=0, 
		@proxy_name=N'saSSIS'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [SSIS - Sync APPYMTDETAIL]    Script Date: 10/8/2018 9:52:32 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'SSIS - Sync APPYMTDETAIL', 
		@step_id=5, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=13, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\"S:\SSISPackages\ESP\Intacct Sync BoA APPYMTDETAIL.dtsx\"" /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@flags=0, 
		@proxy_name=N'saSSIS'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [SSIS - Validate If Checks Exist to Process]    Script Date: 10/8/2018 9:52:32 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'SSIS - Validate If Checks Exist to Process', 
		@step_id=6, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=14, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\"S:\SSISPackages\ESP\Intacct Validate Existing Checks.dtsx\"" /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [SSIS - Intacct Export CashPro Upload File]    Script Date: 10/8/2018 9:52:32 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'SSIS - Intacct Export CashPro Upload File', 
		@step_id=7, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=13, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\"S:\SSISPackages\ESP\Intacct Create CashPro File.dtsx\"" /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [SSIS - Intacct Update Checks]    Script Date: 10/8/2018 9:52:32 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'SSIS - Intacct Update Checks', 
		@step_id=8, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=13, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\"S:\SSISPackages\ESP\Intacct Update Checks.dtsx\"" /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@flags=0, 
		@proxy_name=N'saSSIS'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [SSIS - ReSync APPYMT]    Script Date: 10/8/2018 9:52:32 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'SSIS - ReSync APPYMT', 
		@step_id=9, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=13, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\"S:\SSISPackages\ESP\Intacct Sync BoA APPYMT.dtsx\"" /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@flags=0, 
		@proxy_name=N'saSSIS'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [SSIS - ReSync APPYMTDETAIL]    Script Date: 10/8/2018 9:52:32 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'SSIS - ReSync APPYMTDETAIL', 
		@step_id=10, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=13, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\"S:\SSISPackages\ESP\Intacct Sync BoA APPYMTDETAIL.dtsx\"" /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@flags=0, 
		@proxy_name=N'saSSIS'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [SSIS - Run Final Process Checks]    Script Date: 10/8/2018 9:52:32 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'SSIS - Run Final Process Checks', 
		@step_id=11, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=13, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\"S:\SSISPackages\ESP\Intacct CashPro End Validations.dtsx\"" /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Success Write to Status Table]    Script Date: 10/8/2018 9:52:32 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Success Write to Status Table', 
		@step_id=12, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'TRUNCATE TABLE Utilities.dbo.IntacctCashProStatus

INSERT INTO Utilities.dbo.IntacctCashProStatus
VALUES (GETDATE(), ''Success. The DB was synced.'')

DECLARE @Recipients VARCHAR(2000)
	,	@Subject VARCHAR(200)
	,	@Message VARCHAR(MAX)
	,	@CheckCount VARCHAR(10)


SET @Recipients = ''Shannon.Engels@gregoryfunding.com''
SET @Subject = ''Success Intacct Check Creation for BoA - '' + CONVERT(VARCHAR(10), GETDATE(), 102)

SET @Message = ''The checks have been issued check numbers in Intacct.

The file to upload into CashPro has been created and is located here:
\\flanderscapital\processes\Production\BofA\TPN\Intacct
''



EXEC Boarding.msdb.dbo.sp_send_dbmail                               
                    @profile_name = ''BI''
                    ,@Recipients   = @Recipients
	    ,@copy_recipients = ''benjacob.beres@aspencapital.com''
                    ,@subject      = @Subject 
                    ,@body         = @Message   
	   ,@from_address = ''BIReporting@aspencapital.com''
	   ,@reply_to     = ''benjacob.beres@aspencapital.com''
', 
		@database_name=N'Utilities', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Failure - Write to Status tables]    Script Date: 10/8/2018 9:52:32 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Failure - Write to Status tables', 
		@step_id=13, 
		@cmdexec_success_code=0, 
		@on_success_action=2, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'TRUNCATE TABLE Utilities.dbo.IntacctCashProStatus

INSERT INTO Utilities.dbo.IntacctCashProStatus
VALUES (GETDATE(), ''Failure. Contact IT'')

GO

DECLARE @Recipients VARCHAR(2000)
	,	@Subject VARCHAR(200)
	,	@Message VARCHAR(MAX)
	,	@CheckCount VARCHAR(10)


SET @Recipients = ''Shannon.Engels@gregoryfunding.com''
SET @Subject = ''Failure - Intacct Check Creation for BoA - '' + CONVERT(VARCHAR(10), GETDATE(), 102)

SET @Message = ''There was an issue in the CashPro File Upload Creation Process

IT has been notified but please follow up with them.

''



EXEC Boarding.msdb.dbo.sp_send_dbmail                               
                    @profile_name = ''BI''
                    ,@Recipients   = @Recipients
					,@copy_recipients = ''benjacob.beres@aspencapital.com''
                    ,@subject      = @Subject 
                    ,@body         = @Message   
					,@from_address = ''BIReporting@aspencapital.com''
					,@reply_to     = ''benjacob.beres@aspencapital.com''
', 
		@database_name=N'Utilities', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Completion With No Checks - Write to Status Table]    Script Date: 10/8/2018 9:52:32 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Completion With No Checks - Write to Status Table', 
		@step_id=14, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'
TRUNCATE TABLE Utilities.dbo.IntacctCashProStatus

INSERT INTO Utilities.dbo.IntacctCashProStatus
VALUES (GETDATE(), ''No checks to create.'')

DECLARE @Recipients VARCHAR(2000)
	,	@Subject VARCHAR(200)
	,	@Message VARCHAR(MAX)
	,	@CheckCount VARCHAR(10)

	
SET @Recipients = ''Shannon.Engels@gregoryfunding.com''
SET @Subject = ''Nothing to Process - Intacct Check Creation for BoA - '' + CONVERT(VARCHAR(10), GETDATE(), 102)

SET @Message = ''There are no checks to issue numbers to in Intacct.

If this is incorrect, please contact IT.
''



EXEC Boarding.msdb.dbo.sp_send_dbmail                               
					@profile_name = ''BI''
					,@Recipients   = @Recipients
		,@copy_recipients = ''benjacob.beres@aspencapital.com''
					,@subject      = @Subject 
					,@body         = @Message   
		,@from_address = ''BIReporting@aspencapital.com''
		,@reply_to     = ''benjacob.beres@aspencapital.com''
', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO


