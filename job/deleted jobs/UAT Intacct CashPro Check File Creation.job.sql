USE [msdb]
GO

/****** Object:  Job [UAT Intacct CashPro Check File Creation]    Script Date: 6/28/2018 1:17:08 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 6/28/2018 1:17:08 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'UAT Intacct CashPro Check File Creation', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'FLANDERSCAPITAL\saSSIS', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Write Dummy File]    Script Date: 6/28/2018 1:17:08 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Write Dummy File', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\"S:\SSISPackages\ESP\Intacct Write Dummy File.dtsx\"" /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [SSIS - Sync APBILL]    Script Date: 6/28/2018 1:17:08 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'SSIS - Sync APBILL', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=14, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\"S:\SSISPackages\ESP\UAT\Intacct Sync SAND APBILL.dtsx\"" /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@flags=0, 
		@proxy_name=N'saSSIS'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [SSIS - Sync APBILLITEM]    Script Date: 6/28/2018 1:17:08 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'SSIS - Sync APBILLITEM', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=14, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\"S:\SSISPackages\ESP\UAT\Intacct Sync SAND APBILLITEM.dtsx\"" /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@flags=0, 
		@proxy_name=N'saSSIS'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [SSIS - Sync APPYMT]    Script Date: 6/28/2018 1:17:08 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'SSIS - Sync APPYMT', 
		@step_id=4, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=14, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\"S:\SSISPackages\ESP\UAT\Intacct Sync SAND APPYMT.dtsx\"" /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@flags=0, 
		@proxy_name=N'saSSIS'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [SSIS - Sync APPYMTDETAIL]    Script Date: 6/28/2018 1:17:08 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'SSIS - Sync APPYMTDETAIL', 
		@step_id=5, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\"S:\SSISPackages\ESP\UAT\Intacct Sync SAND APPYMTDETAIL.dtsx\"" /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@flags=0, 
		@proxy_name=N'saSSIS'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [SSIS - Sync CHECKINGACCOUNT]    Script Date: 6/28/2018 1:17:08 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'SSIS - Sync CHECKINGACCOUNT', 
		@step_id=6, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=14, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\"S:\SSISPackages\ESP\UAT\Intacct Sync SAND CHECKINGACCOUNT.dtsx\"" /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@flags=0, 
		@proxy_name=N'saSSIS'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [SSIS - Sync VENDOR]    Script Date: 6/28/2018 1:17:08 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'SSIS - Sync VENDOR', 
		@step_id=7, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=14, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\"S:\SSISPackages\ESP\UAT\Intacct Sync SAND VENDOR.dtsx\"" /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@flags=0, 
		@proxy_name=N'saSSIS'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Check for Bills to Pay]    Script Date: 6/28/2018 1:17:08 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Check for Bills to Pay', 
		@step_id=8, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=1, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @RecCount INT
SET @RecCount = 1  --remove the 1 and uncomment below for prod
--(	SELECT  COUNT(*)
--					FROM IntacctDM.dbo.APPymt ap
--					JOIN IntacctDM.dbo.APPymtDetail apd
--						ON	apd.PAYMENTKEY = ap.RECORDNO
--					JOIN IntacctDM.dbo.APBill ab
--						ON	ab.RECORDNO = apd.RECORDKEY 
--					WHERE	CAST(DATEADD(HH,-8,CAST(ap.WHENMODIFIED AS DATETIME)) AS DATE) >=  CAST(DATEADD(DD,-21,GETDATE()) AS DATE)	--remove for prod	--if the record was created on the previous day
--						AND ap.[STATE] = ''S''
--						AND ap.PAYMENTMETHOD = ''Cash'' 
--						AND ab.PAYMENT_TYPE_HEADER = ''BoA Check''
--						AND ap.DOCNUMBER IS NULL  --Add back in for prod
--				 )

IF @RecCount = 0   --There are no checks to process
BEGIN

		TRUNCATE TABLE Utilities.dbo.IntacctCashProStatus

		INSERT INTO Utilities.dbo.IntacctCashProStatus
		VALUES (GETDATE(), ''No checks to create.'')

		DECLARE @Recipients VARCHAR(2000)
			,	@Subject VARCHAR(200)
			,	@Message VARCHAR(MAX)
			,	@CheckCount VARCHAR(10)


		SET @Recipients = ''Caroline.Ngere@gregoryfunding.com''
		SET @Subject = ''Nothing to Process - Intacct Check Creation for BoA - '' + CONVERT(VARCHAR(10), GETDATE(), 102)

		SET @Message = ''There are no checks to issue numbers to in Intacct.

If this is incorrect, please contact IT.
''



		EXEC Boarding.msdb.dbo.sp_send_dbmail                               
							@profile_name = ''BI''
							,@Recipients   = @Recipients
				,@copy_recipients = ''benjacob.beres@aspencapital.com''
							,@subject      = @Subject 
							,@body         = @Message   
			   ,@from_address = ''BIReporting@aspencapital.com''
			   ,@reply_to     = ''benjacob.beres@aspencapital.com''

		RAISERROR(''No Checks to Process'', 16, 1)
END', 
		@database_name=N'Utilities', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [SSIS - Intacct Export CashPro Upload File]    Script Date: 6/28/2018 1:17:08 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'SSIS - Intacct Export CashPro Upload File', 
		@step_id=9, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=14, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\"S:\SSISPackages\ESP\UAT\Intacct Export CashPro Upload File.dtsx\"" /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@flags=0, 
		@proxy_name=N'saSSIS'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [SSIS - Intacct Update Checks]    Script Date: 6/28/2018 1:17:08 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'SSIS - Intacct Update Checks', 
		@step_id=10, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=14, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\"S:\SSISPackages\ESP\UAT\Intacct Update Checks.dtsx\"" /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@flags=0, 
		@proxy_name=N'saSSIS'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [SSIS - ReSync APPYMT]    Script Date: 6/28/2018 1:17:08 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'SSIS - ReSync APPYMT', 
		@step_id=11, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=14, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\"S:\SSISPackages\ESP\UAT\Intacct Sync SAND APPYMT.dtsx\"" /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@flags=0, 
		@proxy_name=N'saSSIS'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [SSIS - ReSync APPYMTDETAIL]    Script Date: 6/28/2018 1:17:08 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'SSIS - ReSync APPYMTDETAIL', 
		@step_id=12, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=14, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\"S:\SSISPackages\ESP\UAT\Intacct Sync SAND APPYMTDETAIL.dtsx\"" /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@flags=0, 
		@proxy_name=N'saSSIS'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Success Write to Status Table]    Script Date: 6/28/2018 1:17:08 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Success Write to Status Table', 
		@step_id=13, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'TRUNCATE TABLE Utilities.dbo.IntacctCashProStatus

INSERT INTO Utilities.dbo.IntacctCashProStatus
VALUES (GETDATE(), ''Success. The DB was synced.'')

DECLARE @Recipients VARCHAR(2000)
	,	@Subject VARCHAR(200)
	,	@Message VARCHAR(MAX)
	,	@CheckCount VARCHAR(10)


--SET @Recipients = ''Caroline.Ngere@gregoryfunding.com''
SET @Recipients = ''benjacob.beres@aspencapital.com''
SET @Subject = ''SANDBOX Success Intacct Check Creation for BoA - '' + CONVERT(VARCHAR(10), GETDATE(), 102)

SET @Message = ''The checks have been issued check numbers in Intacct.

The file to upload into CashPro has been created and is located here:
\\cottonwood\users\Accounting\Project-New GL\1F-Integrations\IT\5. Intacct to BOA
''



EXEC Boarding.msdb.dbo.sp_send_dbmail                               
                    @profile_name = ''BI''
                    ,@Recipients   = @Recipients
	    ,@copy_recipients = ''benjacob.beres@aspencapital.com''
                    ,@subject      = @Subject 
                    ,@body         = @Message   
	   ,@from_address = ''BIReporting@aspencapital.com''
	   ,@reply_to     = ''benjacob.beres@aspencapital.com''
', 
		@database_name=N'Utilities', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Failure - Write to Status tables]    Script Date: 6/28/2018 1:17:08 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Failure - Write to Status tables', 
		@step_id=14, 
		@cmdexec_success_code=0, 
		@on_success_action=2, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'TRUNCATE TABLE Utilities.dbo.IntacctCashProStatus

INSERT INTO Utilities.dbo.IntacctCashProStatus
VALUES (GETDATE(), ''Failure. Contact IT'')

GO

DECLARE @Recipients VARCHAR(2000)
	,	@Subject VARCHAR(200)
	,	@Message VARCHAR(MAX)
	,	@CheckCount VARCHAR(10)


--SET @Recipients = ''Caroline.Ngere@gregoryfunding.com''
SET @Recipients = ''benjacob.beres@aspencapital.com''
SET @Subject = ''SANDBOX Failure - Intacct Check Creation for BoA - '' + CONVERT(VARCHAR(10), GETDATE(), 102)

SET @Message = ''There was an issue in the CashPro File Upload Creation Process

IT has been notified but please follow up with them.

''



EXEC Boarding.msdb.dbo.sp_send_dbmail                               
                    @profile_name = ''BI''
                    ,@Recipients   = @Recipients
					,@copy_recipients = ''benjacob.beres@aspencapital.com''
                    ,@subject      = @Subject 
                    ,@body         = @Message   
					,@from_address = ''BIReporting@aspencapital.com''
					,@reply_to     = ''benjacob.beres@aspencapital.com''
', 
		@database_name=N'Utilities', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO


