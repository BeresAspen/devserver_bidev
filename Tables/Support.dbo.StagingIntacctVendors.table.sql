USE [Support]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[StagingIntacctVendors](
	[IntacctVendorID] [int] NULL,
	[VendorName] [varchar](100) NULL,
	[VendorAddress1] [varchar](80) NULL,
	[VendorAddress2] [varchar](80) NULL,
	[VendorCity] [varchar](40) NULL,
	[VendorState] [varchar](10) NULL,
	[VendorZip] [varchar](11) NULL,
	[VendorCountry] [varchar](50) NULL,
	[VendorStatus] [varchar](20) NULL,
	[TIN] [varchar](25) NULL,
	[VendorType] [varchar](30) NULL,
	[DynamicsVendorID] [varchar](25) NULL,
	[DefaultPaymentType] [varchar](20) NULL,
	[InsertedDate] [datetime] NULL
) ON [PRIMARY]

GO


