USE Staging;
GO


CREATE TABLE dbo.IntacctStgClass (
		[RECORDNO] bigint,
		[CLASSID] varchar(255),
		[NAME] varchar(255),
		[DESCRIPTION] varchar(255),
		[STATUS] varchar(255),
		[PARENTKEY] varchar(255),
		[PARENTID] varchar(255),
		[PARENTNAME] varchar(255),
		[WHENCREATED] varchar(255),
		[WHENMODIFIED] varchar(255),
		[CREATEDBY] bigint,
		[MODIFIEDBY] bigint,
		[MEGAENTITYKEY] varchar(255),
		[MEGAENTITYID] varchar(255),
		[MEGAENTITYNAME] varchar(255),
		[ACTIVATED_DATE] varchar(255),
		[INACTIVE_EFFECTIVE_DATE] varchar(255),
		[US_STATE] varchar(255),
		[POOL_NUMBER] varchar(255),
		[ASSET_TYPE] varchar(255),
		DMImportDate datetime
	)
