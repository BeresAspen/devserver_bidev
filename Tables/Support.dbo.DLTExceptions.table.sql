USE Support;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* -----------------------------------------------------------------------------------------------------------
   Table Name	   :	dbo.DLTExceptions
   Business Analyis:	
   Project/Process :   
   Description     :	DLT Exceptions for reporting
   Author          :	Benjacob Beres
   Create Date     :	05/24/2017

   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date       Author        Description
   --------   -----------   ------------

*/ -----------------------------------------------------------------------------------------------------------

CREATE TABLE dbo.DLTExceptions(
		[Account] [nvarchar](10) NULL,
		[TranType] [varchar](20) NOT NULL,
		[14110] [money] NULL,
		[14115] [money] NULL,
		[14131] [money] NULL,
		[18110] [money] NULL,
		[18120] [money] NULL,
		[18125] [money] NULL,
		[18130] [money] NULL,
		[17000] [money] NULL,
		[18145] [money] NULL,
		[23500] [money] NULL,
		[23505] [money] NULL,
		[23535] [money] NULL,
		[46100] [money] NULL,
		[40105] [money] NULL,
		[68130] [money] NULL,
		[15515] [money] NULL,
		[P&I] [money] NULL,
		[Due to Lender] [money] NULL,
		[Variance] [money] NULL,
		[SysRecCreated] [datetime] NULL,
		[DateAdded] [datetime] NULL
) ON [PRIMARY]

GO
