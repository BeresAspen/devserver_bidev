USE Utilities
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Package_Log](
	[RowID] [int] IDENTITY(1,1) NOT NULL,
	[PackageName] [varchar](400) NULL,
	[PackageStartTime] [datetime] NULL,
	[PackageEndTime] [datetime] NULL,
	[RowCount] [int] NULL,
	[DataSource] [varchar](max) NULL,
	[Status] VARCHAR(20) NULL,
	[InsertedBy] [varchar](400) NULL,
	[InsertDate] [datetime] NULL,
	[ModifiedBy] [varchar](400) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


