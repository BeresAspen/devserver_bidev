USE Support;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* -----------------------------------------------------------------------------------------------------------
   Table Name	   :	dbo.IntacctVendorXReference
   Business Analyis:	
   Project/Process :   
   Description     :	Create the table to cross reference the Intacct Vendor ID to the MARS/Lereta data
   Author          :	Benjacob Beres
   Create Date     :	07/06/2017

   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date       Author        Description
   --------   -----------   ------------

*/ -----------------------------------------------------------------------------------------------------------



CREATE TABLE dbo.IntacctVendorXReference
	(	IntacctVendorID		INT
	,	VendorName			VARCHAR(100)
	,	VendorType			VARCHAR(30)
	,	VendorAddress1		VARCHAR(80)
	,	VendorAddress2		VARCHAR(80)
	,	VendorCity			VARCHAR(40)
	,	VendorState			VARCHAR(10)
	,	AFRVendorName		VARCHAR(100)
	,	TIN					VARCHAR(25)
	,	LeretaID			VARCHAR(30)
	,	DynamicsVendorID	VARCHAR(25)
	,	DefaultPaymentType	VARCHAR(20)
	,	VendorStatus		VARCHAR(20)
	,	UpdatedBy			VARCHAR(50)
	,	UpdatedDate			DATETIME
	)