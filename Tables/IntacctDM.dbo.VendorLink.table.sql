USE IntacctDM;
GO

CREATE TABLE dbo.VendorLink
	(	VendorLinkID INT IDENTITY
	,	IntacctVendorID INT
	,	MARSVendorID INT
	)