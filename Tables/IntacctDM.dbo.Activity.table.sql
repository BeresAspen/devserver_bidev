USE IntacctDM;
GO


CREATE TABLE dbo.Activity (
	[IntacctTblActivityID] [int] IDENTITY(1,1) NOT NULL,
    [name] varchar(255),
    [comment] varchar(255),
    [createdBy] varchar(255),
    [createdAt] datetime,
    [updatedBy] varchar(255),
    [updatedAt] datetime,
    [id] bigint,
    [activity_dimension_name] varchar(255),
    [DMImportDate] datetime
)