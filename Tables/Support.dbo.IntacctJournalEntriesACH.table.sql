USE Support;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* -----------------------------------------------------------------------------------------------------------
   Table Name	   :	dbo.IntacctJournalEntriesACH
   Business Analyis:	
   Project/Process :   
   Description     :	Staging table for the Intacct Integration
   Author          :	Benjacob Beres
   Create Date     :	02/15/2017

   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date       Author        Description
   --------   -----------   ------------

*/ -----------------------------------------------------------------------------------------------------------

CREATE TABLE dbo.IntacctJournalEntriesACH
	(		JOURNAL VARCHAR(10)
		,	[DATE] VARCHAR(24)
		,	REVERSEDATE VARCHAR(24)
		,	[DESCRIPTION] VARCHAR(80)
		,	REFERENCE_NO VARCHAR(20)
		,	LINE_NO VARCHAR(20)
		,	ACCT_NO VARCHAR(24)
		,	LOCATION_ID VARCHAR(20)
		,	DEPT_ID VARCHAR(20)
		,	DOCUMENT VARCHAR(30)
		,	MEMO VARCHAR(1000)
		,	DEBIT VARCHAR(41)
		,	CREDIT VARCHAR(41)
		,	SOURCEENTITY VARCHAR(20)
		,	CURRENCY VARCHAR(3)
		,	EXCH_RATE_DATE VARCHAR(10)
		,	EXCH_RATE_TYPE_ID VARCHAR(40)
		,	EXCHANGE_RATE VARCHAR(18)
		,	[STATE] VARCHAR(6)
		,	ALLOCATION_ID VARCHAR(50)
		,	ATTACHMENT_URL VARCHAR(120)
		,	GLDIMPROPERTY_ID VARCHAR(30)
		,	GLDIMSTATUS VARCHAR(20)
		,	GLDIMPOOL_ID VARCHAR(30)
		,	GLDIMSTATE VARCHAR(20)
		,	GLDIMTRANSACTION_TYPE VARCHAR(50)
		,	GLDIMACTIVITY VARCHAR(30)
		,	GLENTRY_CUSTOMERID VARCHAR(20)
		,	GLENTRY_VENDORID VARCHAR(20)
		,	GLENTRY_CLASSID VARCHAR(20)

	)
	ON [PRIMARY]