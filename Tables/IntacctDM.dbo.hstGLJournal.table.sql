USE IntacctDM;
GO


CREATE TABLE hstGLJournal (
	IntacctTblGLJournalID INT,
    [RECORDNO] bigint,
    [SYMBOL] varchar(255),
    [STATUS] varchar(255),
    [TITLE] varchar(255),
    [START_DATE] varchar(255),
    [LAST_DATE] varchar(255),
    [ADJ] bit,
    [BOOKID] varchar(255),
    [WHENCREATED] varchar(255),
    [WHENMODIFIED] varchar(255),
    [CREATEDBY] int,
    [MODIFIEDBY] int,
    DMImportDate datetime,
	InsertedDate datetime,
	InsertedBy varchar(100)
)