USE Support
GO

/****** Object:  Table [dbo].[StagingInvoices]    Script Date: 9/29/2017 3:07:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[StagingIntacctInvoices](
	[PaymentType] [varchar](30) NULL,
	[BankAccount] [varchar](30) NULL,
	[LocationID] [varchar](5) NULL,
	[Amount] [varchar](18) NULL,
	[InvoicePriority] [varchar](10) NULL,
	[IntacctVendorID] [int] NULL,
	[InvoiceDate] [date] NULL,
	[PostingDate] [date] NULL,
	[DueDate] [date] NULL,
	[PaymentDate] [date] NULL,
	[Account] [varchar](30) NULL,
	[Memo] [varchar](64) NULL,
	[AssetID] [varchar](20) NULL,
	[InvoiceStatus] [varchar](40) NULL,
	[TaxCycle] [int] NULL,
	[TaxYear] [int] NULL,
	[InvoiceNbr] [varchar](30) NULL,
	[CheckNbr] [int] NULL,
	[InsertedDate] [datetime] NULL
) ON [PRIMARY]

GO


