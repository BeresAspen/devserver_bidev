USE IntacctDM;
GO


CREATE TABLE dbo.Department (
	IntacctTblDepartmentID int IDENTITY,
    [DEPARTMENTID] bigint,
    [RECORDNO] bigint,
    [TITLE] varchar(255),
    [PARENTKEY] varchar(255),
    [PARENTID] varchar(255),
    [SUPERVISORKEY] bigint,
    [SUPERVISORID] bigint,
    [WHENCREATED] varchar(255),
    [WHENMODIFIED] varchar(255),
    [SUPERVISORNAME] varchar(255),
    [STATUS] varchar(255),
    [CUSTTITLE] varchar(255),
    [CREATEDBY] int,
    [MODIFIEDBY] int,
    DMImportDate datetime
)