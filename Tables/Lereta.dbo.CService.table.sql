USE Lereta;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* -----------------------------------------------------------------------------------------------------------
   Table Name	   :	dbo.CService
   Business Analyis:	
   Project/Process :   
   Description     :	Table for the Lereta CService File Uploads
   Author          :	Benjacob Beres
   Create Date     :	03/22/2017

   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date       Author        Description
   --------   -----------   ------------

*/ -----------------------------------------------------------------------------------------------------------


	CREATE TABLE dbo.CService
		(	AgencyID	VARCHAR(9)
		,	AgencyType	VARCHAR(2)
		,	CustomerNumber	VARCHAR(7)
		,	LoanID	VARCHAR(21)
		,	ContractNumber	VARCHAR(8)
		,	ContractSuffix	VARCHAR(4)
		,	HitCode		VARCHAR(1)
		,	HandleManualCode	VARCHAR(1)
		,	StateCountry	VARCHAR(5)
		,	LienCount	VARCHAR(4)
		,	FirstInstallPaymentStatus	VARCHAR(1)
		,	FirstInstallTaxAmount	VARCHAR(9)
		,	SecondInstallPaymentStatus	VARCHAR(1)
		,	SecondInstallTaxAmount	VARCHAR(9)
		,	ThirdInstallPaymentStatus	VARCHAR(1)
		,	ThirdInstallTaxAmount	VARCHAR(9)
		,	FourthInstallPaymentStatus	VARCHAR(1)
		,	FourthInstallTaxAmount	VARCHAR(9)
		,	TotalTaxAmount	VARCHAR(10)
		,	DiscountTaxAmount	VARCHAR(10)
		,	TaxID	VARCHAR(50)
		,	ReferenceNumber	VARCHAR(9)
		,	PriorYearInd	VARCHAR(1)
		,	CustSRVBurNo	VARCHAR(5)
		,	NumberOfInstallments	VARCHAR(1)
		,	CurrentInstallment	VARCHAR(1)
		,	PropertyCode VARCHAR(2)
		,	ImportFile VARCHAR(100)
		,	ImportDate DATETIME
		);