USE [Support]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--DROP TABLE dbo.StagingIntacctInvoicesDetail


CREATE TABLE [dbo].[StagingIntacctInvoicesDetail](
	[RECORDNO] [int] NULL,
	[DOCHDRNO] [int] NULL,
	[DOCHDRID] [varchar](50) NULL,
	[LINE_NO] [int] NULL,
	[ITEMID] [int] NULL,
	[ITEMNAME] [varchar](50) NULL,
	[MEMO] [varchar](100) NULL,
	[TOTAL] [varchar](18) NULL,
	[WHENCREATED] [varchar](20) NULL,
	[WHENMODIFIEDGMT] [varchar](20) NULL,
	[WHENCREATEDGMT] [varchar](20) NULL,
	[CREATEDBY] [varchar](10) NULL,
	[MODIFIEDBY] [varchar](10) NULL,
	[DEPARTMENTID] [varchar](10) NULL,
	[DEPARTMENTNAME] [varchar](50) NULL,
	[TAX_CYCLE] [varchar](4) NULL,
	[BANK_ACCOUNT_ENTRY_LEVEL] [varchar](20) NULL,
	[TAX_YEAR] [varchar](4) NULL,
	[CUSTOMERID] [varchar](20) NULL,
	[CUSTOMERNAME] [varchar](100) NULL,
	[VENDORID] [int] NULL,
	[VENDORNAME] [varchar](100) NULL,
	[CLASSID] [varchar](20) NULL,
	[InsertedBy] [varchar](20) NULL,
	[InsertedRecordDate] [datetime] NULL
) ON [PRIMARY]

GO


