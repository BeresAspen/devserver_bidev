USE Staging;
GO

IF OBJECT_ID('dbo.IntacctStgGLAcctGrpHierarchy') IS NOT NULL
	DROP TABLE dbo.IntacctStgGLAcctGrpHierarchy;

GO

CREATE TABLE dbo.IntacctStgGLAcctGrpHierarchy (
    [RECORDNO] varchar(255),
    [GLACCTGRPKEY] varchar(255),
    [GLACCTGRPNAME] varchar(255),
    [GLACCTGRPTITLE] varchar(255),
    [GLACCTGRPNORMALBALANCE] varchar(255),
    [GLACCTGRPMEMBERTYPE] varchar(255),
    [GLACCTGRPHOWCREATED] varchar(255),
    [GLACCTGRPLOCATIONKEY] varchar(255),
    [ACCOUNTKEY] varchar(255),
    [ACCOUNTNO] varchar(255),
    [ACCOUNTTITLE] varchar(255),
    [ACCOUNTNORMALBALANCE] varchar(255),
    [ACCOUNTTYPE] varchar(255),
    [ACCOUNTLOCATIONKEY] varchar(255),
    [DMImportDate] datetime
)