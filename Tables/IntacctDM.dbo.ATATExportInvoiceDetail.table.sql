USE IntacctDM;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.ATATExportInvoiceDetail') IS NOT NULL
	DROP TABLE dbo.ATATExportInvoiceDetail;

/* -----------------------------------------------------------------------------------------------------------
   Table Name	   :	dbo.ATATExportInvoiceDetail
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	09/07/2017

   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date       Author        Description
   --------   -----------   ------------

*/ -----------------------------------------------------------------------------------------------------------



CREATE TABLE dbo.ATATExportInvoiceDetail
	(	Account VARCHAR(20)
	,	LoanStatus VARCHAR(40)
	,	TaxBill VARCHAR(12)
	,	EscrowBalance VARCHAR(12)
	,	TaxDueDate DATE
	,	TaxSaleDate DATE
	,	EscrowBalanceAfterPmt VARCHAR(12)
	,	AmountToEscrow VARCHAR(12)
	,	CurrentTI VARCHAR(12)
	,	[State] VARCHAR(2)
	,	TaxSaleMonths VARCHAR(4)
	,	AspenValue VARCHAR(20)
	,	SaleDate DATE
	,	LastPaidDate DATE
	,	TaxEscrowed VARCHAR(1)
	,	OneYearTaxDue VARCHAR(20)
	,	NextTaxDueDate DATE
	,	LastEscrowDate DATE
	,	PayStatus VARCHAR(15)
	,	NoteOwner VARCHAR(300)
	,	SECRorREIT VARCHAR(1)
	,	TransferredEscrowBalance VARCHAR(12)
	,	PayFlag VARCHAR(1)
	,	ShortComment VARCHAR(100)
	,	SentToMgmtFlag VARCHAR(1)
	,	EAFlag VARCHAR(1)
	,	ShortEAComment VARCHAR(100)
	,	HotStateFlag VARCHAR(1)
	,	BK_RFSDate DATE
	,	BK_DismissalDate DATE
	,	LoanType VARCHAR(50)
	,	BK_MFRDate DATE
	,	ImportFileName VARCHAR(100)
	,	ImportDate DATETIME
	)