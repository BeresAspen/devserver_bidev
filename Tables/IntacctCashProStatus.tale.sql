USE Utilities;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.IntacctCashProStatus') IS NOT NULL
	DROP TABLE dbo.IntacctCashProStatus;

	

/********************************************************************************************************** *
	Table Name			:	dbo.IntacctCashProStatus
	Business Analyis	:	
	Project/Process		:   
	Creation Ticket		:	(JIRA) MARS-3126
	Author				:	Benjacob Beres
	Creation Date		:	02/20/2018
	Description			:	


	*********************************************************************************************************
	**         Change History                                                                              **
	*********************************************************************************************************
	Date		Author			Version			Ticket#(system)		Description of Change
	----------	-------------	-------------	-----------------	-----------------------

************************************************************************************************************ */



CREATE TABLE dbo.IntacctCashProStatus
		(	ProcessDate DATETIME
		,	[Status] VARCHAR(100)
		)

