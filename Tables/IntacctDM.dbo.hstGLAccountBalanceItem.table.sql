USE IntacctDM;
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[hstGLAccountBalanceItem](
		IntacctTblGLAccountBalanceID BIGINT,
		[BOOKID] varchar(255),
		[CURRENCY] varchar(255),
		[PERIOD] varchar(255),
		[OPENBAL] decimal(28,10),
		[TOTDEBIT] decimal(28,10),
		[TOTCREDIT] decimal(28,10),
		[TOTADJDEBIT] decimal(28,10),
		[TOTADJCREDIT] decimal(28,10),
		[FORBAL] decimal(28,10),
		[ENDBAL] decimal(28,10),
		[ACCOUNTREC] int,
		[ACCOUNTNO] varchar(255),
		[ACCOUNTTITLE] varchar(255),
		[DEPARTMENTDIMKEY] int,
		[DEPARTMENTID] varchar(255),
		[DEPARTMENTTITLE] varchar(255),
		[LOCATIONDIMKEY] bigint,
		[LOCATIONID] varchar(255),
		[LOCATIONNAME] varchar(255),
		[WHENCREATED] varchar(255),
		[WHENMODIFIED] varchar(255),
		[CREATEDBY] int,
		[MODIFIEDBY] int,
		[LOCATIONNO] int,
		[CUSTOMERDIMKEY] int,
		[CUSTOMERID] varchar(255),
		[CUSTOMERNAME] varchar(255),
		[VENDORDIMKEY] int,
		[VENDORID] int,
		[VENDORNAME] varchar(255),
		[CLASSDIMKEY] int,
		[CLASSID] varchar(255),
		[CLASSNAME] varchar(255),
		[ITEMDIMKEY] int,
		[ITEMID] varchar(255),
		[ITEMNAME] varchar(255),
		[GLDIMACTIVITY] varchar(255),
		[GLDIMSTATUS] varchar(255),
		[DMImportDate] [datetime],
		InsertedDate datetime,
		InsertedBy varchar(100)
) ON [PRIMARY]

GO


