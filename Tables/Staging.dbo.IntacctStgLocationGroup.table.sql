USE Staging;
GO

IF OBJECT_ID('dbo.IntacctStgLocationGroup') IS NOT NULL
	DROP TABLE dbo.IntacctStgLocationGroup;
GO

CREATE TABLE dbo.IntacctStgLocationGroup (
    [RECORDNO] int,
    [ID] varchar(255),
    [NAME] varchar(255),
    [DESCRIPTION] varchar(255),
    [GROUPTYPE] varchar(255),
    [CONTACTINFO.CONTACTNAME] varchar(255),
    [CONTACTINFO.PRINTAS] varchar(255),
    [CONTACTINFO.PHONE1] varchar(255),
    [CONTACTINFO.PHONE2] varchar(255),
    [CONTACTINFO.MAILADDRESS.ADDRESS1] varchar(255),
    [CONTACTINFO.MAILADDRESS.ADDRESS2] varchar(255),
    [CONTACTINFO.MAILADDRESS.CITY] varchar(255),
    [CONTACTINFO.MAILADDRESS.STATE] varchar(255),
    [CONTACTINFO.MAILADDRESS.ZIP] varchar(255),
    [CONTACTINFO.MAILADDRESS.COUNTRY] varchar(255),
    [MEMBERFILTERS] nvarchar(MAX),
    [WHENCREATED] varchar(255),
    [WHENMODIFIED] varchar(255),
    [CREATEDBY] int,
    [MODIFIEDBY] int,
    [DIMGRPCOMP] varchar(255),
    [MEGAENTITYKEY] varchar(255),
    [MEGAENTITYID] varchar(255),
    [MEGAENTITYNAME] varchar(255),
    DMImportDate datetime
)
