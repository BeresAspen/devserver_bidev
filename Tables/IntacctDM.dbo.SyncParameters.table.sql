USE [IntacctDM]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE dbo.SyncParameters (
		EarliestPeriod varchar(50) NOT NULL
	,	LastRunDate datetime NOT NULL
	,	ValidationPass bit NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[SyncParameters] ADD  CONSTRAINT [DF_SyncParameters_LastRunDate]  DEFAULT (getdate()) FOR [LastRunDate]
GO


