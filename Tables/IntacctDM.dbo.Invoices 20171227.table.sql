USE IntacctDM;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.Invoices') IS NOT NULL
	DROP TABLE dbo.Invoices;

/* -----------------------------------------------------------------------------------------------------------
   Table Name	   :	dbo.Invoices
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	09/18/2017

   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date       Author        Description
   --------   -----------   ------------

*/ -----------------------------------------------------------------------------------------------------------



CREATE TABLE dbo.Invoices
	(	IntacctVendorID		INT
	,	MARSAccount			VARCHAR(20)
	,	CheckNbr			INT
	,	IntacctInvoiceNbr	VARCHAR(30)
	,	InvoiceDate			DATE
	,	PaymentDate			DATE
	,	EconomicLossDate	DATE
	,	AccountNbr			VARCHAR(30)				--??????
	,	Amount				VARCHAR(18)
	,	InvoiceStatus		VARCHAR(40)
	,	TaxCycle			INT
	,	TaxYear				INT
	,	VendorPaymentsQueueID	INT
	,	UpdatedBy			VARCHAR(100)
	,	UpdatedDate			DATETIME
	,	InsertedBy			VARCHAR(100)
	,	InsertedDate		DATETIME
	)