USE [PI];


IF OBJECT_ID('dbo.HomeTrustSubjectOrders') IS NOT NULL
	DROP TABLE dbo.HomeTrustSubjectOrders;



CREATE TABLE dbo.HomeTrustSubjectOrders
	(	SubjectOrderID	INT IDENTITY
	,	OrderID	INT
	,	LoanNo	VARCHAR(20)
	,	OrderReferenceNo	VARCHAR(30)
	,	OrderDateChanged	DATE
	,	SubjectAddress	VARCHAR(100)
	,	SubjectCity	VARCHAR(50)
	,	SubjectState	VARCHAR(2)
	,	SubjectZip	VARCHAR(5)
	,	SubjectSqFt	INT
	,	SubjectBath	DECIMAL(4,1)
	,	SubjectBedroom	INT
	,	SubjectGarage	VARCHAR(20)
	,	SubjectLotSize	VARCHAR(12)
	,	SubjectValueAsIs	INT
	,	SubjectValueRepair	INT
	,	SubjectYearBuilt	INT
	,	InsertedLoadBatchID INT
	,	UpdatedLoadBatchID INT
	,	DataIssue	BIT
	,	DataIssueColumns VARCHAR(1000)
	)

