USE [MARS]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/* ********************************************************************************************************* *
   Object Name		:	Dialerfile.table.sql
   Description		:	
   Author			:	Nathan Reed
   Create Date		:	

   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date			Author				Issue #			Description
   --------		-----------			---------		------------
   03/02/2017	Benjacob Beres		MARS-1377		Added 6 new columns to the dialerfile
*  ********************************************************************************************************* */

IF OBJECT_ID('dbo.dialerfile') IS NULL
	BEGIN

		CREATE TABLE [dbo].[dialerfile](
			[RunDate] [datetime] NOT NULL,
			[queuenumber] [int] NULL,
			[state] [nchar](3) NULL,
			[judicial] [nchar](3) NULL,
			[NewStatus] [nchar](25) NULL,
			[Account] [nvarchar](10) NULL,
			[AssignedRep] [varchar](128) NULL,
			[TAD] [int] NOT NULL,
			[CurrentTrustBalance] [money] NOT NULL,
			[NextDueDate] [datetime] NULL,
			[MonthlyPayment] [money] NULL,
			[BK - Debt Discharged] [varchar](max) NOT NULL,
			[DNCDetail] [varchar](max) NULL,
			[DialerHome] [varchar](1000) NOT NULL,
			[DialerWork] [varchar](1000) NOT NULL,
			[DialerMobile] [varchar](1000) NOT NULL,
			[DialerCoBorrowerHome] [varchar](1000) NOT NULL,
			[DialerCoBorrowerWork] [varchar](1000) NOT NULL,
			[DialerCoBorrowerMobile] [varchar](1000) NOT NULL,
			[Home] [varchar](1000) NOT NULL,
			[Work] [varchar](1000) NOT NULL,
			[Mobile] [varchar](1000) NOT NULL,
			[CoBorrowerHome] [varchar](1000) NOT NULL,
			[CoBorrowerWork] [varchar](1000) NOT NULL,
			[CoBorrowerMobile] [varchar](1000) NOT NULL,
			[TCPADialable] [int] NOT NULL
		) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

	END;


IF NOT EXISTS(	SELECT	column_id
				FROM	sys.columns 
				WHERE	Name = N'cntr'
					AND Object_ID = Object_ID('dbo.dialerfile'))
	BEGIN
		
		ALTER TABLE dialerfile
			ADD 	cntr INT NULL
				,	dayspastdue INT NULL
				,	LastContactDate VARCHAR(10) NULL
				,	LastContactType VARCHAR(128) NULL
				,	LoanStatus varchar(100) NULL
				,	PrimaryPhone BIGINT
		
	END;

