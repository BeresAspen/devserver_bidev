USE Staging;
GO


CREATE TABLE dbo.IntacctStgGLAcctGrpMember (
    [RECORDNO] bigint,
    [PARENTKEY] bigint,
    [CHILDKEY] bigint,
    [CHILDNAME] varchar(255),
    [SORTORD] bigint,
    [WHENCREATED] varchar(255),
    [WHENMODIFIED] varchar(255),
    [CREATEDBY] varchar(255),
    [MODIFIEDBY] varchar(255),
    DMImportDate datetime
)