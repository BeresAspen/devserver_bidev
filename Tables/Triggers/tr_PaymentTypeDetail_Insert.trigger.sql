USE [MARS]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






/*  
 Modification History:  
  -- Muthu, On 4th-Nov-2014, Added with (NoLock) statement, whereever locked resournce used in this trigger (3 + 4 places).  
  -- Muthu, On 10th-Nov-2014, Added Begin tran with Retry sub-routine. and Retry count for 3 times if Dead lock scenario occurs, Ticket-2225.     
  -- Muthu, On 20th-Nov-2014, Retry counter changed and @@TRANCOUNT handled with statement with (nolock) , Ticket-2225.
  -- Amit Jain on 17 Aug 2015 for adding cashier check if check payment type  is selected for taxes vendor
*/  
ALTER TRIGGER [dbo].[tr_PaymentTypeDetail_Insert] ON [MARS].[dbo].[PaymentTypeDetail]  
FOR INSERT, Update  
AS  
BEGIN  
   SET NOCOUNT ON  
  
DECLARE @RetryCounter INT  
SET @RetryCounter = 0  
RETRY: -- SubRoutine RETRY  
BEGIN TRANSACTION  
BEGIN TRY  
   Declare @JULIAN VARCHAR(5)  
   Declare @TIMER VARCHAR(5)  
   Declare @VENDOR_ID VARCHAR(15)  
   DECLARE @Reference Varchar(100)  
   Declare @VendorId int        
                          
              
            Select @VendorId = VendorId from VendorPaymentsInfo VPI with (nolock) INNER JOIN inserted PTD  
   ON VPI.VendorPaymentInfoId = PTD.VendorPaymentInfoId  
   WHERE PTD.PaymentTypeID in (Select PaymentTypeID from PaymentTypes  with (nolock) where PaymentType = 'Check')  
    and VPI.IsPaymentActive = 1 and PTD.IsActive = 1  
  
  
  -- Added by Amit Jain on 17 Aug 2015 for adding cashier check if check payment type  is selected for taxes vendor, Starts
  if exists (select * from inserted ptd --order by BeneficiaryID
			inner join PaymentTypes pt on ptd.PaymentTypeID = pt.PaymentTypeId
			inner join VendorPaymentsInfo vpi on ptd.VendorPaymentInfoId = vpi.VendorPaymentInfoId
			inner join VendorsList vl on vpi.VendorId=vl.VendorId
			inner join VendorTypes vt on vl.VendorTypeId =vt.VendorTypeId
			where pt.PaymentType='Check' and vt.VendorType='Taxes' )
  begin
  
  declare @PTID int
  select @PTID=paymenttypeid from PaymentTypes where PaymentType='Cashiers Check'
  
  
  if not exists( select * from PaymentTypeDetail  where VendorPaymentInfoId=(select distinct ptd.VendorPaymentInfoId from inserted ptd ) and PaymentTypeID=@PTID)
  begin
  
  insert into PaymentTypeDetail(VendorPaymentInfoId,PaymentTypeID,PaymentAppliedLookUpID,BeneficiaryID,BeneficiaryName,VendorRemittanceAddressesID,IsPreferred,IsActive,InsertedBy,InsertedDate)
  select ptd.VendorPaymentInfoId, @PTID, ptd.PaymentAppliedLookUpID, ptd.BeneficiaryID, ptd.BeneficiaryName, ptd.VendorRemittanceAddressesID,0,1, ptd.InsertedBy, ptd.InsertedDate from inserted ptd 
		inner join PaymentTypes pt on ptd.PaymentTypeID = pt.PaymentTypeId
		where pt.PaymentType='Check'
  end
  
  end
  
  
IF @@TRANCOUNT > 0 COMMIT TRANSACTION;  
  
END TRY  
BEGIN CATCH  
 -- PRINT 'Rollback Transaction'  
 IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION  
 DECLARE @DoRetry bit; -- Whether to Retry transaction or not  
 DECLARE @ErrorMessage varchar(500)  
 DECLARE @Err INT  
 DECLARE @ErrorLine INT  
 --  
 SET @DoRetry = 0;  
 SET @Err=Error_Number();  
 SET @ErrorLine = Error_Line()  
 SET @ErrorMessage = ERROR_MESSAGE()  
 --  
 IF ERROR_NUMBER() = 1205 -- Deadlock Error Number  
 BEGIN  
  SET @DoRetry = 1; -- Set @doRetry to 1 only for Deadlock  
 END  
 --  
 IF @DoRetry = 1  
 BEGIN  
  SET @RetryCounter = @RetryCounter + 1 -- Increment Retry Counter By one  
  IF (@RetryCounter > 3) -- Check whether Retry Counter reached to 3  
  BEGIN  
   EXEC Applications.dbo.di_ErrorLog 'MARSII','''', @Err, @ErrorMessage,'MARS.tr_PaymentTypeDetail_Insert', @ErrorLine,0,0;  
   --  
   RAISERROR(@ErrorMessage, 18, 1) -- Raise Error Message, if still deadlock occurred after three retries.  
   --  
  END  
  ELSE  
  BEGIN  
   WAITFOR DELAY '00:00:00.05' -- Wait for 5 ms  
   Declare  @LocatedAt as varchar(200);  
   Set @LocatedAt = 'MARS.tr_PaymentTypeDetail_Insert - DEAD LOCK - WARNING Attempt (' + Convert(varchar,@RetryCounter) + ')';  
   EXEC Applications.dbo.di_ErrorLog 'MARSII','''', @Err, @ErrorMessage, @LocatedAt, @ErrorLine,0,0;  
   GOTO RETRY     -- Go to Label RETRY  
  END  
 END  
 ELSE  
 BEGIN  
   EXEC Applications.dbo.di_ErrorLog 'MARSII','''', @Err, @ErrorMessage,'MARS.tr_PaymentTypeDetail_Insert', @ErrorLine,0,0;  
   --  
   RAISERROR(@ErrorMessage, 18, 1)  
   --  
 END  
END CATCH  
  
END   





