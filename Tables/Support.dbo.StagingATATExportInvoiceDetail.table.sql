USE Support;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.StagingATATExportInvoiceDetail') IS NOT NULL
	DROP TABLE dbo.StagingATATExportInvoiceDetail;

/* -----------------------------------------------------------------------------------------------------------
   Table Name	   :	dbo.StagingATATExportInvoiceDetail
   Business Analyis:	
   Project/Process :   
   Description     :	
   Author          :	Benjacob Beres
   Create Date     :	09/07/2017

   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date       Author        Description
   --------   -----------   ------------

*/ -----------------------------------------------------------------------------------------------------------



CREATE TABLE dbo.StagingATATExportInvoiceDetail
	(	StagingATATExportInvoiceDetailID INT IDENTITY NOT NULL
	,	Account VARCHAR(100)
	,	LoanStatus VARCHAR(100)
	,	TaxBill VARCHAR(100)
	,	EscrowBalance VARCHAR(100)
	,	TaxDueDate VARCHAR(100)
	,	TaxSaleDate VARCHAR(100)
	,	EscrowBalanceAfterPmt VARCHAR(100)
	,	AmountToEscrow VARCHAR(100)
	,	CurrentTI VARCHAR(100)
	,	[State] VARCHAR(100)
	,	TaxSaleMonths VARCHAR(100)
	,	AspenValue VARCHAR(260)
	,	SaleDate VARCHAR(100)
	,	LastPaidDate VARCHAR(100)
	,	TaxEscrowed VARCHAR(150)
	,	OneYearTaxDue VARCHAR(100)
	,	NextTaxDueDate VARCHAR(100)
	,	LastEscrowDate VARCHAR(10)
	,	PayStatus VARCHAR(15)
	,	NoteOwner VARCHAR(300)
	,	SECRorREIT VARCHAR(1)
	,	TransferredEscrowBalance VARCHAR(18)
	,	PayFlag VARCHAR(1)
	,	ShortComment VARCHAR(100)
	,	SentToMgmtFlag VARCHAR(20)
	,	EAFlag VARCHAR(1)
	,	ShortEAComment VARCHAR(100)
	,	HotStateFlag VARCHAR(1)
	,	BK_RFSDate VARCHAR(10)
	,	BK_DismissalDate VARCHAR(10)
	,	LoanType VARCHAR(50)
	,	BK_MFRDate VARCHAR(10)
	,	ImportFileName VARCHAR(100)
	,	ImportDate DATETIME
	)