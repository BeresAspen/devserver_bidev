USE [Support]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE dbo.REPOLoanDetails (
			Account varchar(10) NULL
		,	PropertyID varchar(10) NULL
		,	NoteOwner [varchar](255) NULL
		,	REPOLineStartDate DATE NULL
		,	REPOLineEndDate DATE NULL
) ON [PRIMARY]

GO


