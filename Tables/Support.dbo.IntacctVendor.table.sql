USE Support;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* -----------------------------------------------------------------------------------------------------------
   Table Name	   :	dbo.IntacctVendor
   Business Analyis:	
   Project/Process :   
   Description     :	Staging table for the Intacct Integration
   Author          :	Benjacob Beres
   Create Date     :	02/13/2017

   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date       Author        Description
   --------   -----------   ------------

*/ -----------------------------------------------------------------------------------------------------------
	

CREATE TABLE dbo.IntacctVendor
	(	VENDOR_ID VARCHAR(20)
	,	[NAME] VARCHAR(100)
	,	PARENT_ID VARCHAR(20)
	,	TERM_NAME VARCHAR(40)
	,	VENDTYPE_NAME VARCHAR(80)
	,	TAX_ID VARCHAR(20)
	,	CREDIT_LIMIT VARCHAR(40)
	,	VENDOR_ACCTNO VARCHAR(30)
	,	ACCT_LABEL VARCHAR(80)
	,	GL_ACCTNO VARCHAR(24)
	,	BILLING_TYPE VARCHAR(1)
	,	DISC_ON_CKSTUB VARCHAR(5)
	,	ONHOLD VARCHAR(5)
	,	COMMENTS VARCHAR(200)
	,	FORM1099TYPE VARCHAR(10)
	,	FORM1099BOX VARCHAR(1)
	,	NAME1099 VARCHAR(100)
	,	CURRENCY VARCHAR(3)
	,	PAYMENTPRIORITY VARCHAR(10)
	,	HIDEDISPLAYCONTACT VARCHAR(1)
	,	ACTIVE VARCHAR(1)
	,	CONTACT_NAME VARCHAR(200)
	,	COMPANY_NAME VARCHAR(100)
	,	MRMS VARCHAR(15)
	,	FIRST_NAME VARCHAR(40)
	,	MI VARCHAR(40)
	,	LAST_NAME VARCHAR(40)
	,	PRINT_AS VARCHAR(200)
	,	TAXABLE VARCHAR(1)
	,	PHONE1 VARCHAR(30)
	,	PHONE2 VARCHAR(30)
	,	CELLPHONE VARCHAR(30)
	,	PAGER VARCHAR(30)
	,	FAX VARCHAR(30)
	,	EMAIL1 VARCHAR(255)
	,	EMAIL2 VARCHAR(255)
	,	URL1 VARCHAR(255)
	,	URL2 VARCHAR(255)
	,	ADDRESS1 VARCHAR(80)
	,	ADDRESS2 VARCHAR(80)
	,	CITY VARCHAR(80)
	,	[STATE] VARCHAR(40)
	,	ZIP VARCHAR(30)
	,	COUNTRY VARCHAR(60)
	,	LATITUDE VARCHAR(12)
	,	LONGITUDE VARCHAR(13)
	,	R_CONTACT_NAME VARCHAR(64)
	,	R_COMPANY_NAME VARCHAR(64)
	,	R_MRMS VARCHAR(10)
	,	R_FIRST_NAME VARCHAR(30)
	,	R_MI VARCHAR(1)
	,	R_LAST_NAME VARCHAR(30)
	,	R_PRINT_AS VARCHAR(64)
	,	R_TAXABLE VARCHAR(1)
	,	R_PHONE1 VARCHAR(20)
	,	R_PHONE2 VARCHAR(20)
	,	R_CELLPHONE VARCHAR(20)
	,	R_PAGER VARCHAR(20)
	,	R_FAX VARCHAR(20)
	,	R_EMAIL1 VARCHAR(255)
	,	R_EMAIL2 VARCHAR(255)
	,	R_URL1 VARCHAR(255)
	,	R_URL2 VARCHAR(255)
	,	R_ADDRESS1 VARCHAR(64)
	,	R_ADDRESS2 VARCHAR(64)
	,	R_CITY VARCHAR(50)
	,	R_STATE VARCHAR(3)
	,	R_ZIP VARCHAR(10)
	,	R_COUNTRY VARCHAR(30)
	,	P_CONTACT_NAME VARCHAR(64)
	,	P_COMPANY_NAME VARCHAR(64)
	,	P_MRMS VARCHAR(10)
	,	P_FIRST_NAME VARCHAR(30)
	,	P_MI VARCHAR(1)
	,	P_LAST_NAME VARCHAR(30)
	,	P_PRINT_AS VARCHAR(64)
	,	P_TAXABLE VARCHAR(1)
	,	P_PHONE1 VARCHAR(20)
	,	P_PHONE2 VARCHAR(20)
	,	P_CELLPHONE VARCHAR(20)
	,	P_PAGER VARCHAR(20)
	,	P_FAX VARCHAR(20)
	,	P_EMAIL1 VARCHAR(255)
	,	P_EMAIL2 VARCHAR(255)
	,	P_URL1 VARCHAR(255)
	,	P_URL2 VARCHAR(255)
	,	P_ADDRESS1 VARCHAR(64)
	,	P_ADDRESS2 VARCHAR(64)
	,	P_CITY VARCHAR(50)
	,	P_STATE VARCHAR(3)
	,	P_ZIP VARCHAR(10)
	,	P_COUNTRY VARCHAR(30)
	,	APACCOUNT VARCHAR(24)
	,	MERGE_PAYMENT_REQUESTS VARCHAR(10)
	,	DYNAMIC_VENDOR_ID VARCHAR(20)
	,	VENDOR_PAYMENT_FOR_CHECKLIST VARCHAR(30)
	,	VENDOR_TYPE VARCHAR(10)
	) ON [PRIMARY]