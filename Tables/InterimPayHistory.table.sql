USE InterimPayments;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* -----------------------------------------------------------------------------------------------------------
   Table Name	   :	dbo.InterimPayHistory
   Business Analyis:	
   Project/Process :   
   Description     :	Table for the InterimPayHistory File Uploads
   Author          :	Benjacob Beres
   Create Date     :	03/8/2017

   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date       Author        Description
   --------   -----------   ------------

*/ -----------------------------------------------------------------------------------------------------------

CREATE TABLE dbo.InterimPayHistory
	(	RecID INT
	,	PoolID VARCHAR(30) NULL
	,	GFAccount VARCHAR(20) NULL
	,	PLAccount VARCHAR(20) NULL
	,	PSAccount VARCHAR(20) NULL
	,	TransferDate DATE NULL
	,	TransactionDate DATE NULL
	,	DueDate DATE NULL
	,	Principal MONEY NULL
	,	Interest MONEY NULL
	,	Escrow MONEY NULL
	,	EscrowAdv MONEY NULL
	,	TotalEscrow MONEY NULL
	,	ImportFile VARCHAR(100) NULL
	,	ImportDate DATETIME NULL
	) ON [PRIMARY]