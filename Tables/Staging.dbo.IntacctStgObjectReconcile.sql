USE [Staging]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IntacctStgObjectReconcile](
		[RECORDNO] [bigint] NULL
	,	[EarliestDate] [datetime] NULL
) ON [PRIMARY]

GO


