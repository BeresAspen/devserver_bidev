USE [Staging]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.IntacctStgCheckingAccountRestrictedLocations') IS NOT NULL
DROP TABLE dbo.IntacctStgCheckingAccountRestrictedLocations;
GO

CREATE TABLE dbo.IntacctStgCheckingAccountRestrictedLocations  (
		[checkingaccountID] numeric(20,0),
		[LOCATIONID] varchar(255),
		[DMImportDate] datetime
)