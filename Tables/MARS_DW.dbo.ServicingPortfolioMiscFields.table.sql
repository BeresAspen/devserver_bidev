USE MARS_DW;
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* -----------------------------------------------------------------------------------------------------------
   Table Name	   :	dbo.ServicingPortfolioMiscFields
   Business Analyis:	
   Project/Process :   
   Description     :	User entered values needed for spreadsheet
   Author          :	Benjacob Beres
   Create Date     :	07/10/2017

   ***********************************************************************************************************
   **         Change History                                                                                **
   ***********************************************************************************************************

   Date       Author        Description
   --------   -----------   ------------

*/ -----------------------------------------------------------------------------------------------------------


CREATE TABLE dbo.ServicingPortfolioMiscFields
	(	GFLoanID		VARCHAR(20)
	,	GFPropertyID	VARCHAR(20)
	,	PurchasedUPB	DECIMAL(15,2) NULL
	,	PurchasePrice	DECIMAL(15,2)
	,	AspenValue		INT
	,	Comment			VARCHAR(1000)
	)