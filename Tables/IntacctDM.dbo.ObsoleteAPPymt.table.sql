USE IntacctDM;

--DROP TABLE dbo.ObsoleteAPPymt

CREATE TABLE dbo.ObsoleteAPPymt (
	IntacctTblAPPymtID int,
    [RECORDNO] bigint,
    [PRBATCHKEY] int,
    [RECORDTYPE] varchar(10),
    [RECORDID] varchar(255),
    [FINANCIALENTITY] varchar(255),
    [FINANCIALENTITYTYPE] varchar(255),
    [FINANCIALACCOUNTNAME] varchar(255),
    [FINANCIALACCOUNTCURRENCY] varchar(255),
    [STATE] varchar(5),
    [RAWSTATE] varchar(255),
    [PAYMENTMETHOD] varchar(255),
    [PAYMENTMETHODKEY] tinyint,
    [ENTITY] varchar(50),
    [VENDORID] bigint,
    [VENDORNAME] varchar(255),
    [DOCNUMBER] varchar(255),
    [DESCRIPTION] varchar(255),
    [DESCRIPTION2] varchar(255),
    [WHENCREATED] varchar(15),
    [WHENPAID] varchar(255),
    [BASECURR] varchar(255),
    [CURRENCY] varchar(255),
    [EXCH_RATE_DATE] varchar(255),
    [EXCH_RATE_TYPE_ID] varchar(255),
    [EXCHANGE_RATE] varchar(255),
    [TOTALENTERED] decimal(28,10),
    [TOTALSELECTED] decimal(28,10),
    [TOTALPAID] decimal(28,10),
    [TOTALDUE] decimal(28,10),
    [TRX_TOTALENTERED] decimal(28,10),
    [TRX_TOTALSELECTED] decimal(28,10),
    [TRX_TOTALPAID] decimal(28,10),
    [TRX_TOTALDUE] decimal(28,10),
    [BILLTOPAYTOCONTACTNAME] varchar(255),
    [PRBATCH] varchar(255),
    [AUWHENCREATED] varchar(50),
    [WHENMODIFIED] varchar(50),
    [CREATEDBY] int,
    [MODIFIEDBY] int,
    [USERKEY] int,
    [CLEARED] varchar(255),
    [CLRDATE] varchar(255),
    [STATUS] varchar(255),
    [SYSTEMGENERATED] varchar(255),
    [PAYMENTPRIORITY] varchar(255),
    [BILLTOPAYTOKEY] varchar(255),
    [ONHOLD] varchar(255),
    [PARENTPAYMENTKEY] varchar(255),
    [MEGAENTITYKEY] varchar(255),
    [MEGAENTITYID] varchar(255),
    [MEGAENTITYNAME] varchar(255),
	DMImportDate DATETIME,
	DMObsoletedDate DATETIME
)