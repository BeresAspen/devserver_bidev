USE IntacctDM;
GO


CREATE TABLE ReportingPeriod (
		IntacctTblReportingPeriod INT IDENTITY,
		[RECORD] bigint,
		[RECORDNO] bigint,
		[NAME] varchar(255),
		[HEADER1] varchar(255),
		[HEADER2] varchar(255),
		[START_DATE] varchar(255),
		[END_DATE] varchar(255),
		[BUDGETING] bit,
		[DATETYPE] varchar(255),
		[STATUS] varchar(255),
		[WHENCREATED] varchar(255),
		[WHENMODIFIED] varchar(255),
		[CREATEDBY] varchar(255),
		[MODIFIEDBY] varchar(255),
		[MEGAENTITYKEY] varchar(255),
		[MEGAENTITYID] varchar(255),
		[MEGAENTITYNAME] varchar(255),
		DMImportDate datetime
)