USE [PI];


IF OBJECT_ID('dbo.HomeTrustSubjectComparibles') IS NOT NULL
	DROP TABLE dbo.HomeTrustSubjectComparibles;



CREATE TABLE dbo.HomeTrustSubjectComparibles
	(	SubjectOrderID	INT
	,	ComparibleType	VARCHAR(25)
	,	ComparibleIndex INT
	,	Address	VARCHAR(100)
	,	City	VARCHAR(50)
	,	State	VARCHAR(2)
	,	SqFt	INT
	,	Bath	DECIMAL(4,1)
	,	Bedroom	INT
	,	Basement	BIT
	,	BasementType	VARCHAR(20)
	,	Garage	BIT
	,	GarageAttached	INT DEFAULT 0
	,	GarageDetached INT DEFAULT 0
	,	Carport INT DEFAULT 0
	,	LotSize	VARCHAR(12)
	,	Units	INT
	,	Condition	VARCHAR(20)
	,	Dom	INT
	,	ListDate	DATE
	,	PriceList	INT
	,	PriceListOrig	INT
	,	SaleDate	DATE
	,	PriceSale	INT
	,	Proximity	DECIMAL(5,2)
	,	YearBuilt	INT
	,	InsertedLoadBatchID INT
	,	UpdatedLoadBatchID INT
	,	DataIssue	BIT
	,	DataIssueColumns VARCHAR(1000)
	)
